#include <Application/Application.hpp>
#include <Utilities/Log.hpp>

#include <stdexcept>

LOGCAT("Main");

void main() {
	Log::logToFile();
	try {
		Application app;
		app.run();
	} catch (std::exception& e) {
		LOGE("Uncaught exception: " << e.what());
	}
}
