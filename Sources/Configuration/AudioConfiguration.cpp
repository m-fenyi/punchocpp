#include "AudioConfiguration.hpp"

int AudioConfiguration::getMasterVolume() const { return config.masterVolume; }
int AudioConfiguration::getMusicVolume() const { return config.musicVolume; }
int AudioConfiguration::getSoundVolume() const { return config.soundVolume; }

void AudioConfiguration::setMasterVolume(int volume) { config.masterVolume = volume; }
void AudioConfiguration::setMusicVolume(int volume) { config.musicVolume = volume; }
void AudioConfiguration::setSoundVolume(int volume) { config.soundVolume = volume; }
