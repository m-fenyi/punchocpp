#include "SystemConfiguration.hpp"

void SystemConfiguration::postLoad() {
	UIInputConfiguration.load(config.uiInput);
}

void SystemConfiguration::preSave(SystemConfiguration_t& systemConf) const {
	systemConf.uiInput = UIInputConfiguration.save();
}

const std::string & SystemConfiguration::getVersion() const { return config.version; }
const InputConfiguration & SystemConfiguration::getUIInputConfiguration() const { return UIInputConfiguration; }
const std::vector<Resolution>& SystemConfiguration::getResolutions() const { return config.resolutions; }

InputHandler<UIActionID> SystemConfiguration::getInputHandler() const {
	return UIInputConfiguration.createHandler<UIActionID>();
}
