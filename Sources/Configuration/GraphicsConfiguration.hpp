#ifndef GRAPHICSCONFIGURATION_HPP
#define GRAPHICSCONFIGURATION_HPP


#include "Configuration.hpp"
#include <Window/Resolution.hpp>
#include <Window/WindowMode.hpp>



struct GraphicsConfiguration_t {
	bool vsync;
	int antiAliasingLevel;
	Resolution resolution;
	WindowMode windowMode;
};

class GraphicsConfiguration : public SimpleConfiguration<GraphicsConfiguration_t> {
public:
	bool getVSync() const;
	int getAALevel() const;
	Resolution getResolution() const;
	WindowMode getWindowMode() const;


	void setVSync(bool VSync);
	void setAALevel(int AALevel);
	void setResolution(Resolution resolution);
	void setWindowMode(WindowMode windowMode);
};

#endif // !GRAPHICSCONFIGURATION_HPP

