#include "GraphicsConfiguration.hpp"

bool GraphicsConfiguration::getVSync() const { return config.vsync; }
int GraphicsConfiguration::getAALevel() const { return config.antiAliasingLevel; }
Resolution GraphicsConfiguration::getResolution() const { return config.resolution; }
WindowMode GraphicsConfiguration::getWindowMode() const { return config.windowMode; }

void GraphicsConfiguration::setVSync(bool VSync) { config.vsync = VSync; }
void GraphicsConfiguration::setAALevel(int AALevel) { config.antiAliasingLevel = AALevel; }
void GraphicsConfiguration::setResolution(Resolution resolution) { config.resolution = resolution; }
void GraphicsConfiguration::setWindowMode(WindowMode windowMode) { config.windowMode = windowMode; }
