#include "GeneralConfiguration.hpp"

const std::string & GeneralConfiguration::getLocale() const { return config.locale; }
void GeneralConfiguration::setLocale(const std::string & locale) { config.locale = locale; }
