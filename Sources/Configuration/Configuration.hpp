#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP


template<class ConfigValue>
class Configuration {
public:
	virtual void load(ConfigValue& config) = 0;
	virtual ConfigValue save() const = 0;
};


template<class ConfigValue>
class SimpleConfiguration : public Configuration<ConfigValue> {
public:
	virtual void load(ConfigValue& configuration) override { 
		config = configuration; 
		postLoad();
	}
	virtual ConfigValue save() const override { 
		ConfigValue simpleConfig = config;
		preSave(simpleConfig);
		return simpleConfig;
	}

	virtual void postLoad() {}
	virtual void preSave(ConfigValue& simpleConfig) const {}
	
protected:
	ConfigValue config;
};

#endif // CONFIGURATION_HPP
