#ifndef INPUTHANDLERCONFIGURATION_HPP
#define INPUTHANDLERCONFIGURATION_HPP

#include <Input/Input.hpp>
#include <Input/InputHandler.hpp>
#include <Input/InputMatcherBuilder.hpp>

#include "Configuration.hpp"

#include <vector>

struct ActionProperty {
	Math::Range<float> strength;
	Math::Range<sf::Time> duration;
	bool requirePressed;
};

struct BindingConfiguration {
	unsigned actionId;
	std::vector<Input::ID> inputs;
	ActionProperty property;
};

using InputConfiguration_t = std::vector<BindingConfiguration>;

class InputConfiguration : public SimpleConfiguration<InputConfiguration_t> {
public:

	template<class ActionID>
	InputHandler<ActionID> createHandler() const {
		InputHandler<ActionID> handler;
		createHandler(handler);
		return handler;
	}

	template<class ActionID>
	void createHandler(InputHandler<ActionID>& handler) const {
		InputMatcherBuilder builder;
		for (auto bindings : config) {
			for (auto input : bindings.inputs) {
				builder.clean();
				builder.setInputID(input)
					.setRequirePressed(bindings.property.requirePressed)
					.setDuration(bindings.property.duration)
					.setStrength(bindings.property.strength);

				handler.addMatcher(
					builder.build(),
					(ActionID)bindings.actionId);
			}
		}
	}
};


#endif // INPUTHANDLERCONFIGURATION_HPP
