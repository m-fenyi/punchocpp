#ifndef GAMECONFIGURATION_HPP
#define GAMECONFIGURATION_HPP


#include "Configuration.hpp"
#include "AudioConfiguration.hpp"
#include "ControlsConfiguration.hpp"
#include "GeneralConfiguration.hpp"
#include "GraphicsConfiguration.hpp"

struct GameConfiguration_t {
	AudioConfiguration_t audioConf;
	ControlsConfiguration_t controlsConf;
	GeneralConfiguration_t generalConf;
	GraphicsConfiguration_t graphicsConf;
};

class GameConfiguration : public Configuration<GameConfiguration_t> {
public:
	void load(GameConfiguration_t& config) override;
	GameConfiguration_t save() const override;

	AudioConfiguration&    getAudioConfiguration();
	ControlsConfiguration& getControlsConfiguration();
	GeneralConfiguration&  getGeneralConfiguration();
	GraphicsConfiguration& getGraphicsConfiguration();

private:
	AudioConfiguration audioConf;
	ControlsConfiguration controlsConf;
	GeneralConfiguration generalConf;
	GraphicsConfiguration graphicsConf;
};

#endif // GAMECONFIGURATION_HPP

