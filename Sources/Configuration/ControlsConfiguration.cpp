#include "ControlsConfiguration.hpp"


void ControlsConfiguration::postLoad() {
	inputConfig.load(config.inputConf);
}

void ControlsConfiguration::preSave(ControlsConfiguration_t & simpleConfig) const {
	simpleConfig.inputConf = inputConfig.save();
}


bool ControlsConfiguration::getXboxController() const { return config.xboxController; }
Input::PreferedSource ControlsConfiguration::getPreferedInput() const { return config.preferedInput; }
Joystick::Stick ControlsConfiguration::getJoystickSetup() const { return config.joystickSetup; }
Input::KeyboardSetup ControlsConfiguration::getKeyboardSetup() const { return config.keyboardSetup; }
InputConfiguration& ControlsConfiguration::getInputConfiguration() { return inputConfig; }

PlayerController ControlsConfiguration::getPlayerController() {
	PlayerController controller;
	inputConfig.createHandler(controller);
	return controller;
}
