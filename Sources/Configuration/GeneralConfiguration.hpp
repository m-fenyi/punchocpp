#ifndef GENERALCONFIGURATION_HPP
#define GENERALCONFIGURATION_HPP


#include "Configuration.hpp"
#include <string>

struct GeneralConfiguration_t {
	std::string locale;
};

class GeneralConfiguration : public SimpleConfiguration<GeneralConfiguration_t> {
public:
	const std::string& getLocale() const;
	void setLocale(const std::string& locale);
};

#endif // !GENERALCONFIGURATION_HPP

