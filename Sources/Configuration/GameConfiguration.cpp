#include "GameConfiguration.hpp"



void GameConfiguration::load(GameConfiguration_t & config) {
	audioConf.load(config.audioConf);
	controlsConf.load(config.controlsConf);
	generalConf.load(config.generalConf);
	graphicsConf.load(config.graphicsConf);
}

GameConfiguration_t GameConfiguration::save() const {
	GameConfiguration_t gameConf;
	gameConf.audioConf    = audioConf.save();
	gameConf.controlsConf = controlsConf.save();
	gameConf.generalConf  = generalConf.save();
	gameConf.graphicsConf = graphicsConf.save();
	return gameConf;
}

AudioConfiguration& GameConfiguration::getAudioConfiguration() { return audioConf; }
ControlsConfiguration& GameConfiguration::getControlsConfiguration() { return controlsConf; }
GeneralConfiguration& GameConfiguration::getGeneralConfiguration() { return generalConf; }
GraphicsConfiguration& GameConfiguration::getGraphicsConfiguration() { return graphicsConf; }
