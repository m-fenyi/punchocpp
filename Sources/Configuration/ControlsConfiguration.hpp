#ifndef CONTROLSCONFIGURATION_HPP
#define CONTROLSCONFIGURATION_HPP


#include "Configuration.hpp"
#include "InputConfiguration.hpp"

#include <Action/Controller.hpp>

#include <Action/GameAction.hpp>
#include <Input/JoystickInput.hpp>
#include <Input/Input.hpp>


struct ControlsConfiguration_t {
	InputConfiguration_t inputConf;
	Input::PreferedSource preferedInput;
	Joystick::Stick joystickSetup;
	Input::KeyboardSetup keyboardSetup;
	bool xboxController;
};

class ControlsConfiguration : public SimpleConfiguration<ControlsConfiguration_t> {
public:
	void postLoad() override;
	void preSave(ControlsConfiguration_t& simpleConfig) const override;

	bool getXboxController() const;
	Input::PreferedSource getPreferedInput() const;
	Joystick::Stick getJoystickSetup() const;
	Input::KeyboardSetup getKeyboardSetup() const;

	InputConfiguration& getInputConfiguration();

	PlayerController getPlayerController();

private:
	InputConfiguration inputConfig;
};

#endif // !CONTROLSCONFIGURATION_HPP

