#ifndef SYSTEMCONFIGURATION_HPP
#define SYSTEMCONFIGURATION_HPP

#include "Configuration.hpp"
#include "InputConfiguration.hpp"

#include <GUI/UIAction.hpp>
#include <Window/WindowWrapper.hpp>

#include <string>

struct SystemConfiguration_t {
	std::string version;
	InputConfiguration_t uiInput;
	std::vector<Resolution> resolutions;
};


class SystemConfiguration : public SimpleConfiguration<SystemConfiguration_t> {
public:
	void postLoad() override;
	void preSave(SystemConfiguration_t& systemConf) const override;

	const std::string& getVersion() const;
	const InputConfiguration& getUIInputConfiguration() const;
	const std::vector<Resolution>& getResolutions() const;

	InputHandler<UIActionID> getInputHandler() const;

private:
	InputConfiguration UIInputConfiguration;
};

#endif // SYSTEMCONFIGURATION_HPP