#ifndef AUDIOCONFIGURATION_HPP
#define AUDIOCONFIGURATION_HPP


#include "Configuration.hpp"

struct AudioConfiguration_t {
	int masterVolume;
	int musicVolume;
	int soundVolume;
};

class AudioConfiguration : public SimpleConfiguration<AudioConfiguration_t> {
public:
	int getMasterVolume() const;
	int getMusicVolume() const;
	int getSoundVolume() const;


	void setMasterVolume(int volume);
	void setMusicVolume (int volume);
	void setSoundVolume (int volume);
};

#endif // !AUDIOCONFIGURATION_HPP

