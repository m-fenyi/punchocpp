#ifndef WEAPON_HPP
#define WEAPON_HPP

#include "ProjectileNode.hpp"

#include <DAL/DataStruct.hpp>

#include <SFML\System\Time.hpp>

class WeaponNode;

class Weapon {
public:

	Weapon(WeaponNode& owner, WeaponData& data, ProjectileData& projectile);
	Weapon(WeaponNode& owner, WeaponData& data);
	Weapon(WeaponNode& owner);

	void setData(const WeaponData& data, const ProjectileData& projectileData);
	void setWeaponData(const WeaponData& weaponData);
	void setProjectileData(const ProjectileData& data);

	const WeaponData& getWeaponData() const;
	const ProjectileData& getProjectileData() const;

	int getAmmo();

	void addAmmo(unsigned ammo);
	void setAmmo(unsigned ammo);
	void update(sf::Time dt);

	bool canFire();

	void fire();

private:
	void shoot();
	void kickback();

protected:
	static SceneNode* layer;

private:
	WeaponNode& owner;
	WeaponData data;
	ProjectileData projectile;

	sf::Time cooldown;

	int ammo;
};

#endif // WEAPON_HPP