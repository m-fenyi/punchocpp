#include "WeaponNode.hpp"

#include "CharacterEntity.hpp"

#include <Audio/AudioPlayer.hpp>
#include <Interaction/Properties.hpp>

#include <Utilities/Utilities.hpp>

LOGCAT("WeaponNode");

WeaponNode::WeaponNode(): weapon(*this) {}

WeaponNode::WeaponNode(WeaponData& data): weapon(*this, data), sprite(Context::get().textures.get(data.spriteID)) {}

WeaponNode::WeaponNode(WeaponData & data, ProjectileData & projectile) : weapon(*this, data, projectile), sprite(Context::get().textures.get(data.spriteID)) {}

sf::Vector2f WeaponNode::getProjectileSpawn() {
	sf::Vector2f offset = Math::toVector(getRotation());
	offset *= sprite.getGlobalBounds().width/2;
	return getWorldPosition() + offset;
}

//TODO
EntityNode* WeaponNode::getShooter() { return static_cast<EntityNode*>(getParent()); }
Weapon& WeaponNode::getWeapon() { return weapon; }

void WeaponNode::setWeapon(const WeaponData& data) {
	this->type = type;
	weapon.setWeaponData(data);
	sprite = Context::get().textures.get(data.spriteID);
	Node::centerOrigin(sprite);
}

void WeaponNode::kickback(sf::Vector2f strength) {
	if (getShooter()) getShooter()->accelerate(strength/1.5f);
	accelerate(strength);
}

void WeaponNode::pickup(CharacterEntity& player) const {
	player.getWeapon().setWeapon(weapon.getWeaponData());
	Context::get().audio.play(SoundEffect::CollectPickup, getWorldPosition());
}

WorldPartition::PartitionLayer WeaponNode::getPartitionLayer() { return WorldPartition::LowPartition; }

Hitbox & WeaponNode::getHitbox() {
	hitbox.setTransform(this);
	hitbox.setPosition(getWorldPosition());
	sf::Vector2f scale = sprite.getScale();
	scale.x *= getScale().x;
	scale.y *= getScale().y;
	hitbox.setScale(scale);
	hitbox.setSize(sprite.getLocalBounds());
	hitbox.setOrigin(sprite.getOrigin());
	return hitbox;
}

void WeaponNode::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const {
	target.draw(sprite, states);
	target.draw(hitbox, states);
}

void WeaponNode::updateCurrent(sf::Time dt, CommandQueue & commands) {
	EntityNode::updateCurrent(dt, commands);
	if (getShooter()) {
		accelerate(Node::lerpMove(getWorldPosition(), getShooter()->getWorldPosition(), 160, dt));
	}
	setVelocity(getVelocity() / 1.2f);
	weapon.update(dt);
}
