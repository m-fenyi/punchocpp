#ifndef PICKUPNODE_HPP
#define PICKUPNODE_HPP


#include "EntityNode.hpp"
#include <Interaction/Hitbox.hpp>
#include <Action/Command.hpp>

#include <DAL/DataType.hpp>
#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/TextureWrapper.hpp>

#include <SFML/Graphics/Sprite.hpp>

class CharacterEntity;

class PickupNode: public EntityNode {
public:
	PickupNode(PickupData& data);
	
	void pickup(CharacterEntity& player) const;

	WorldPartition::PartitionLayer getPartitionLayer() override;

	Hitbox& getHitbox() override;

protected:
	void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;


private:
	RectangleHitbox hitbox;
	PickupData data;
	sf::Sprite sprite;
};

#endif // PICKUPNODE_HPP