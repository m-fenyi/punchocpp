#ifndef SPRITENODE_HPP
#define SPRITENODE_HPP

#include "SceneNode.hpp"
#include <DAL/ResourceIdentifiers.hpp>

#include <SFML/Graphics/Sprite.hpp>


class SpriteNode: public SceneNode {
public:
	explicit SpriteNode(const sf::Sprite& sprite);
	SpriteNode(Textures::Sprites::ID type);

private:
	void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;


private:
	sf::Sprite sprite;
};

#endif // SPRITENODE_HPP
