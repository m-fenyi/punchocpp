#include "AnimationNode.hpp"
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RenderTarget.hpp>

AnimationNode::AnimationNode(Animation& animation): animation(animation) {}

AnimationNode::AnimationNode(Textures::Animations::ID type): animation(Context::get().textures.get(type)) {}

void AnimationNode::center() { Node::centerOrigin(animation); }

bool AnimationNode::isMarkedForRemoval() {
	return animation.isFinished() && !animation.isRepeating();
}

void AnimationNode::updateCurrent(sf::Time dt, CommandQueue & commands) { animation.update(dt); }

void AnimationNode::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const { target.draw(animation, states);}
