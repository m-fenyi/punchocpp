#ifndef PROJECTILENODE_HPP
#define PROJECTILENODE_HPP

#include "EntityNode.hpp"

#include <Interaction/Hitbox.hpp>

#include <DAL/DataType.hpp>
#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/TextureWrapper.hpp>

#include <SFML/Graphics/Sprite.hpp>


class ProjectileNode: public EntityNode {
public:

	ProjectileNode(ProjectileData& data);

	void					guideTowards(sf::Vector2f position);
	bool					isGuided() const;

	float					getMaxSpeed() const;
	int						getDamage() const;

	void                    setShooter(EntityNode* shooter);
	EntityNode*             getShooter();

	WorldPartition::PartitionLayer getPartitionLayer() override;
	
	Hitbox& getHitbox() override;

private:
	void updateCurrent(sf::Time dt, CommandQueue& commands) override;
	void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;


private:
	ProjectileData   data;
	RectangleHitbox  hitbox;
	sf::Sprite	     sprite;
	sf::Time         life;
	sf::Vector2f     targetDirection;
};

#endif // PROJECTILENODE_HPP