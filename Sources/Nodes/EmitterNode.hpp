#ifndef EMITTERNODE_HPP
#define EMITTERNODE_HPP

#include "SceneNode.hpp"
#include <Display/Particle.hpp>
#include <DAL/DataStruct.hpp>


class ParticleNode;

class EmitterNode: public SceneNode {
public:
	explicit	 EmitterNode(ParticleData& data);
				 
				 
private:		 
	void         updateCurrent(sf::Time dt, CommandQueue& commands) override;
				 
	void		 emitParticles(sf::Time dt);


private:
	sf::Time	  accumulatedTime;
	ParticleData& data;
	ParticleNode* particleSystem;
};

#endif // EMITTERNODE_HPP
