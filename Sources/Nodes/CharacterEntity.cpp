#include "CharacterEntity.hpp"
#include "PickupNode.hpp"
#include "EmitterNode.hpp"

#include <Interaction/Properties.hpp>
#include <Action/CommandQueue.hpp>

#include <DAL/ResourceHolder.hpp>

#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <cmath>

LOGCAT("CharacterEntity");

CharacterEntity::CharacterEntity(CharacterEntityData& data):
	EntityNode(data),
	maxSpeed(data.speed),
	weapon(new WeaponNode()) {
	
	pack.addAnimation(data.packData);
	pack.setDrawnAnimation(Type::Standing);
	
	Node::centerOrigin(pack);
}

bool CharacterEntity::isAllied() const {
	return type == Type::Character::CharacterBlue;
}

float CharacterEntity::getMaxSpeed() const {
	return maxSpeed;
}

WeaponNode & CharacterEntity::getWeapon() { return *weapon; }

void CharacterEntity::setWeapon(WeaponNode::Ptr weapon) { this->weapon = weapon; }

void CharacterEntity::onRemoveEntity() {
	EntityNode::onRemoveEntity();
	Controllable::removeController();
	pack.createNode(Type::Animation::Dead, getWorldPosition());
}

WorldPartition::PartitionLayer CharacterEntity::getPartitionLayer() { return WorldPartition::HighPartition; }

Hitbox & CharacterEntity::getHitbox() {
	hitbox.setTransform(this);
	hitbox.setPosition(getWorldPosition());
	hitbox.setSize(pack.getGlobalBounds());
	hitbox.setOrigin(pack.getOrigin());
	return hitbox; 
}

void CharacterEntity::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(pack, states);
	target.draw(hitbox, states);
}

void CharacterEntity::updateCurrent(sf::Time dt, CommandQueue & commands) {
	updateActions(this);
	if (isAllied() && getVelocity().x != 0.f && getVelocity().y != 0.f) setVelocity(getVelocity() / std::sqrt(2.f));
	pack.update(dt, this);
	EntityNode::updateCurrent(dt, commands);
}
