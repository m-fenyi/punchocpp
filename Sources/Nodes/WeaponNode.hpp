#ifndef WEAPONNODE_HPP
#define WEAPONNODE_HPP


#include "EntityNode.hpp"
#include "Weapon.hpp"

#include <Interaction/Hitbox.hpp>

#include <DAL/DataType.hpp>

class CharacterEntity;
class WeaponNode: public EntityNode {
public:
	using Ptr = std::shared_ptr<WeaponNode>;

	WeaponNode();
	WeaponNode(WeaponData& data);
	WeaponNode(WeaponData& data, ProjectileData& projectile);
	
	EntityNode* getShooter();
	Weapon& getWeapon();
	sf::Vector2f getProjectileSpawn();
	void setWeapon(const WeaponData& data);

	void kickback(sf::Vector2f strength);

	virtual void pickup(CharacterEntity& player) const;

	WorldPartition::PartitionLayer getPartitionLayer() override;

	Hitbox& getHitbox() override;

private:
	void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;
	void updateCurrent(sf::Time dt, CommandQueue& commands) override;

private:
	RectangleHitbox hitbox;
	Weapon weapon;
	Type::Weapon type;
	sf::Sprite sprite;
};


#endif // WEAPONNODE_HPP