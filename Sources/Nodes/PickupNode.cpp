#include "PickupNode.hpp"
#include <Action/CommandQueue.hpp>
#include <Audio/AudioPlayer.hpp>
#include <Utilities/Utilities.hpp>
#include <DAL/ResourceHolder.hpp>

#include <SFML/Graphics/RenderTarget.hpp>

LOGCAT("PickupNode");

PickupNode::PickupNode(PickupData& data):
	EntityNode(data),
	data(data),
	sprite(Context::get().textures.get(data.sprite)) {

	Node::centerOrigin(sprite);
}


void PickupNode::pickup(CharacterEntity& entity) const {
	//data.action(entity);
	Context::get().audio.play(SoundEffect::CollectPickup, getWorldPosition());
}

WorldPartition::PartitionLayer PickupNode::getPartitionLayer() { return WorldPartition::LowPartition; }

Hitbox & PickupNode::getHitbox() { 
	hitbox.setTransform(this); 
	return hitbox; 
}

void PickupNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(sprite, states);
	target.draw(hitbox, states);
}

