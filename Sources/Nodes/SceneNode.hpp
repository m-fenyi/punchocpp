#ifndef SCENENODE_HPP
#define SCENENODE_HPP

#include "Category.hpp"

#include <GUI/State.hpp>

#include <World/WorldPartition.hpp>

#include <SFML/System/NonCopyable.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>

#include <vector>
#include <set>
#include <memory>
#include <utility>


struct Command;
class CommandQueue;
class TextureWrapper;


class SceneNode: public sf::Transformable, public sf::Drawable, private sf::NonCopyable {
public:
	enum Layer {
		Background,
		LowerAir,
		UpperAir,
		Foreground,

		LayerCount
	};

	using Ptr  = std::shared_ptr<SceneNode>;
	using NodeLayers = std::array<SceneNode*, SceneNode::LayerCount>;

public:
	explicit				SceneNode(Category::Type category = Category::NoCategory);

	virtual void			attachChild(Ptr child);
	static void			    attachTo(Layer layer, Ptr child);
	virtual Ptr				detachChild(const SceneNode& node);

	SceneNode*              getParent() const;

	void					update(sf::Time dt, CommandQueue& commands);

	sf::Vector2f			getWorldPosition() const;
	sf::Transform			getWorldTransform() const;
	
	void					onCommand(const Command& command, sf::Time dt);
	virtual unsigned int	getCategory() const;

	virtual void            onRemoveEntity();
	void					removeDestroyedEntities();
	virtual sf::FloatRect	getBoundingRect() const;
	virtual bool			isMarkedForRemoval();
	virtual bool			isDestroyed() const;

	size_t                  countChild();
	
	static void             createNodeLayers(SceneNode& root);
private:
	virtual void			updateCurrent(sf::Time dt, CommandQueue& commands);
	void					updateChildren(sf::Time dt, CommandQueue& commands);

	void        			draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	void					drawChildren(sf::RenderTarget& target, sf::RenderStates states) const;
	void					drawBoundingRect(sf::RenderTarget& target, sf::RenderStates states) const;
	
protected:
	SceneNode*				parent;

private:
	std::vector<Ptr>		children;

	Category::Type			category;
};

#endif // SCENENODE_HPP
