#include "SpriteNode.hpp"
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RenderTarget.hpp>


SpriteNode::SpriteNode(const sf::Sprite& sprite): sprite(sprite) {}

SpriteNode::SpriteNode(Textures::Sprites::ID type): sprite(Context::get().textures.get(type)) {}

void SpriteNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const { target.draw(sprite, states); }