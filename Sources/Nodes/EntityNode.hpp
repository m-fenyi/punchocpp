#ifndef ENTITYNODE_HPP
#define ENTITYNODE_HPP

#include "SceneNode.hpp"
#include <Interaction/Entity.hpp>
#include <World/WorldPartition.hpp>
#include <Utilities/Type.hpp>

class Hitbox;

class EntityNode: public SceneNode, public Entity {
protected:
	EntityNode();
	EntityNode(EntityData& data);
	
public:
	virtual Hitbox& getHitbox() = 0;

	virtual sf::FloatRect getBoundingRect();

	virtual void remove();
	bool isDestroyed() const override;

	void		 setVelocity(sf::Vector2f velocity);
	void		 setVelocity(float vx, float vy);
	void		 accelerate(sf::Vector2f velocity);
	void		 accelerate(float vx, float vy);
	sf::Vector2f getVelocity() const;
	

	virtual void move(const sf::Vector2f& offset);
	void         addToGrid();
	void         removeFromGrid();

	void onRemoveEntity() override;

	virtual WorldPartition::PartitionLayer getPartitionLayer();
	static void setPartition(WorldPartition::Ptr partition);

protected:
	void updateCurrent(sf::Time dt, CommandQueue& commands) override;

	bool         isInGrid;

private:
	sf::Vector2f velocity;
};

#endif // ENTITYNODE_HPP
