#ifndef ENVIRONMENTNODE_HPP
#define ENVIRONMENTNODE_HPP

#include "EntityNode.hpp"

#include <DAL/DataType.hpp>
#include <DAL/Datatables.hpp>

#include <Display/AnimationPack.hpp>
#include <Interaction/Hitbox.hpp>

class EnvironmentNode: public EntityNode {
public:
	using Ptr = std::shared_ptr<EntityNode>;
	EnvironmentNode(EnvironmentData& data);
	
	WorldPartition::PartitionLayer getPartitionLayer() override;
	
	Hitbox& getHitbox() override;

private:
	void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;
	void updateCurrent(sf::Time dt, CommandQueue& commands) override;

private:
	RectangleHitbox hitbox;
	Type::Environment type;
	
	AnimationPack pack;
};

#endif // ENVIRONMENTNODE_HPP