#include "SceneNode.hpp"
#include <Action/Command.hpp>
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <array>

LOGCAT("SceneNode");

namespace {
	SceneNode::NodeLayers layers;
}

SceneNode::SceneNode(Category::Type category):
	children(),
	parent(nullptr),
	category(category) {}


void SceneNode::attachChild(Ptr child) {
	child->parent = this;
	children.push_back(std::move(child));
}

void SceneNode::attachTo(Layer layer, Ptr child) {
	if (!layers.at(layer)) {
		LOGW("Could not add child to layer " << layer);
		return;
	}
	layers.at(layer)->attachChild(child);
}

SceneNode::Ptr SceneNode::detachChild(const SceneNode& node) {
	auto found = std::find_if(children.begin(), children.end(), [&](Ptr& p) { return p.get() == &node; });
	assert(found != children.end());

	(*found)->parent = nullptr;
	(*found)->onRemoveEntity();
	
	children.erase(found);
	return *found;
}

SceneNode * SceneNode::getParent() const {
	return parent;
}

void SceneNode::update(sf::Time dt, CommandQueue& commands) {
	updateCurrent(dt, commands);
	updateChildren(dt, commands);
}

void SceneNode::updateCurrent(sf::Time, CommandQueue&) {}

void SceneNode::updateChildren(sf::Time dt, CommandQueue& commands) {
	for (Ptr& child : children) child->update(dt, commands);
}

void SceneNode::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	states.transform *= getTransform();
	
	drawCurrent(target, states);
	drawChildren(target, states);

#ifdef _DEBUG
	drawBoundingRect(target, states);
#endif // _DEBUG
}

void SceneNode::drawCurrent(sf::RenderTarget&, sf::RenderStates) const {}

void SceneNode::drawChildren(sf::RenderTarget& target, sf::RenderStates states) const {
	for (const Ptr& child : children) child->draw(target, states);
}

void SceneNode::drawBoundingRect(sf::RenderTarget& target, sf::RenderStates) const {
	sf::FloatRect rect = getBoundingRect();

	sf::RectangleShape shape;
	shape.setPosition(sf::Vector2f(rect.left, rect.top));
	shape.setSize(sf::Vector2f(rect.width, rect.height));
	shape.setFillColor(sf::Color::Transparent);
	shape.setOutlineColor(sf::Color::Green);
	shape.setOutlineThickness(1.f);

	target.draw(shape);
}

sf::Vector2f SceneNode::getWorldPosition() const {
	return getWorldTransform() * sf::Vector2f();
}

sf::Transform SceneNode::getWorldTransform() const {
	sf::Transform transform = sf::Transform::Identity;

	for (const SceneNode* node = this; node != nullptr; node = node->parent) 
		transform = node->getTransform() * transform;

	return transform;
}

void SceneNode::onCommand(const Command& command, sf::Time dt) {
	if (command.category & getCategory()) command.action(*this);

	for (Ptr& child : children) child->onCommand(command, dt);
}

unsigned int SceneNode::getCategory() const {
	return category;
}

void SceneNode::onRemoveEntity() {}

void SceneNode::removeDestroyedEntities() {
	for (auto it = children.begin(); it != children.end();) {
		if ((*it)->isMarkedForRemoval()) {
			(*it)->onRemoveEntity();
			it = children.erase(it);
		} else {
			(*it)->removeDestroyedEntities();
			++it;
		}
	}
}

sf::FloatRect SceneNode::getBoundingRect() const {
	return sf::FloatRect();
}

bool SceneNode::isMarkedForRemoval() {
	return isDestroyed();
}

bool SceneNode::isDestroyed() const {
	return false;
}

size_t SceneNode::countChild() {
	size_t childCount = children.size();
	for (Ptr& child : children) childCount += child->countChild();
	return childCount;
}

void SceneNode::createNodeLayers(SceneNode & root) {
	for (size_t i = 0; i < SceneNode::LayerCount; ++i) {
		Category::Type category = (i == SceneNode::LowerAir) ? Category::SceneAirLayer : Category::NoCategory;

		SceneNode::Ptr layer(new SceneNode(category));
		layers[i] = layer.get();

		root.attachChild(std::move(layer));
	}
}
