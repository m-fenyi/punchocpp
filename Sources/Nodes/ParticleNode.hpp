#ifndef PARTICLENODE_HPP
#define PARTICLENODE_HPP

#include "SceneNode.hpp"

#include <Display/Particle.hpp>

#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/DataStruct.hpp>
#include <DAL/TextureWrapper.hpp>

#include <SFML/Graphics/VertexArray.hpp>

#include <deque>


class ParticleNode: public SceneNode {
public:
	ParticleNode(ParticleData& data);

	void					addParticle(sf::Vector2f position);
	
	ParticleData&			getParticleData() const;

	unsigned int	        getCategory() const override;

private:
	void         			updateCurrent(sf::Time dt, CommandQueue& commands) override;
	void         			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;

	void					addVertex(float worldX, float worldY, float texCoordX, float texCoordY, const sf::Color& color) const;
	void					computeVertices() const;


private:
	std::deque<Particle>	particles;
	const sf::Texture&		texture;
	ParticleData& data;

	mutable sf::VertexArray	vertexArray;
	mutable bool			needsVertexUpdate;
};

#endif // PARTICLENODE_HPP
