#ifndef ANIMATIONNODE_HPP
#define ANIMATIONNODE_HPP

#include "SceneNode.hpp"
#include <Display/Animation.hpp>

class AnimationNode: public SceneNode {
public:
	explicit AnimationNode(Animation& animation);
	explicit AnimationNode(Textures::Animations::ID type);

	void center();

	bool isMarkedForRemoval() override;

private:
	void updateCurrent(sf::Time dt, CommandQueue& commands) override;
	void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;


private:
	Animation animation;
};

#endif // ANIMATIONNODE_HPP