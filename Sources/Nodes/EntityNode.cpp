#include "EntityNode.hpp"

#include <Interaction/Properties.hpp>
#include <Interaction/Abilities.hpp>
#include <Interaction/Hitbox.hpp>

#include <Utilities/Utilities.hpp>

#include <cassert>

LOGCAT("EntityNode");


namespace {
	WorldPartition::Ptr worldPartition;
}

EntityNode::EntityNode(): isInGrid(false) {}

EntityNode::EntityNode(EntityData& data): Entity(data), isInGrid(false) {}

sf::FloatRect EntityNode::getBoundingRect() {
	return getHitbox().getBoundingRect();
}

void EntityNode::remove() { removeFromGrid(); }

bool EntityNode::isDestroyed() const { 
	if (hasProperty(Property::Damageable))
		return getProperty<DamageableProperty>(Property::Damageable).isDestroyed();
	return false;
}

void EntityNode::updateCurrent(sf::Time dt, CommandQueue&) {
	move(velocity * dt.asSeconds());
}


void EntityNode::setVelocity(sf::Vector2f velocity) { this->velocity = velocity; }
void EntityNode::setVelocity(float vx, float vy) { velocity.x = vx; velocity.y = vy; }
sf::Vector2f EntityNode::getVelocity() const { return velocity; }

void EntityNode::move(const sf::Vector2f& offset) {
	float oldX = getBoundingRect().left;
	float oldY = getBoundingRect().top;
	Transformable::move(offset);

	if (isInGrid && worldPartition) worldPartition->moveNode(this, oldX, oldY);
}

void EntityNode::addToGrid() {
	if (worldPartition) {
		worldPartition->attachNode(this);
		isInGrid = true;
	} else LOGW("Could not add node to grid");
}

void EntityNode::removeFromGrid() {
	if (worldPartition) {
		worldPartition->detachNode(this);
		isInGrid = false;
	} else LOGW("Could not remove node to grid");
}

void EntityNode::onRemoveEntity() { 
	SceneNode::onRemoveEntity();
	removeFromGrid();
}

WorldPartition::PartitionLayer EntityNode::getPartitionLayer() { return WorldPartition::NoPartition; }
void EntityNode::setPartition(WorldPartition::Ptr partition) { ::worldPartition = partition; }

void EntityNode::accelerate(sf::Vector2f velocity) { this->velocity += velocity; }

void EntityNode::accelerate(float vx, float vy) { velocity.x += vx; velocity.y += vy; }