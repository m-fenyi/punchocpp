#include "Weapon.hpp"
#include "WeaponNode.hpp"

#include <Audio/AudioPlayer.hpp>
#include <Utilities/Utilities.hpp>

LOGCAT("Weapon");


Weapon::Weapon(WeaponNode & owner, WeaponData & data, ProjectileData & projectile) :
	owner(owner),
	data(data),
	projectile(projectile),
	cooldown(sf::Time::Zero),
	ammo(100) {}
Weapon::Weapon(WeaponNode& owner, WeaponData& data):
	Weapon(owner, data, ProjectileData()) {}
Weapon::Weapon(WeaponNode& owner):
	Weapon(owner, WeaponData(), ProjectileData()) {}


void Weapon::setData(const WeaponData& weaponData, const ProjectileData & projectileData) { 
	setWeaponData(weaponData);
	setProjectileData(projectileData);
}
void Weapon::setWeaponData(const WeaponData& data) { this->data = data; }
const WeaponData& Weapon::getWeaponData() const { return data; }

void Weapon::setProjectileData(const ProjectileData & data) { projectile = data; }
const ProjectileData& Weapon::getProjectileData() const { return projectile; }

int Weapon::getAmmo() { return ammo; }

void Weapon::addAmmo(unsigned ammo) { this->ammo += ammo; }
void Weapon::setAmmo(unsigned ammo) { this->ammo  = ammo; }

void Weapon::update(sf::Time dt) { if (!canFire()) cooldown -= dt; }

bool Weapon::canFire() { return cooldown <= sf::Time::Zero && ammo > 0; }

void Weapon::fire() {
	if (canFire()) {
		for (int i = 0; i < data.spread; i++) {
			shoot();
		}
		kickback();
		Context::get().audio.play(data.soundID);
		cooldown += data.rateOfFire;
		ammo--;
	}
}


void Weapon::shoot() {
	std::unique_ptr<ProjectileNode> projectile(new ProjectileNode(projectile));
	
	projectile->setPosition(owner.getProjectileSpawn());
	float rotation = Math::randomReal<float>(-data.accuracy, data.accuracy) + owner.getRotation();
	projectile->setRotation(rotation);

	sf::Vector2f v = Math::toVector(rotation);
	v *= projectile->getMaxSpeed();
	v += owner.getVelocity();
	projectile->setVelocity(v);
	
	projectile->setShooter(owner.getShooter());

	projectile->addToGrid();
  	SceneNode::attachTo(SceneNode::LowerAir, std::move(projectile));
}

void Weapon::kickback() {
	sf::Vector2f kb = Math::toVector(owner.getRotation() + 180);
	kb *= data.kickback;
	owner.kickback(kb);
}
