#ifndef CHARACTERENTITY_HPP
#define CHARACTERENTITY_HPP

#include "EntityNode.hpp"
#include "WeaponNode.hpp"
#include "ProjectileNode.hpp"
#include "TextNode.hpp"

#include <Interaction/Hitbox.hpp>

#include <Action/Controller.hpp>
#include <Action/Controllable.hpp>

#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/TextureWrapper.hpp>

#include <Display/AnimationPack.hpp>

#include <SFML/Graphics/Sprite.hpp>

class CharacterEntity: public EntityNode, public Controllable {
public:
	using Ptr = std::shared_ptr<CharacterEntity>;

	CharacterEntity(CharacterEntityData& data);

	bool					isAllied() const;
	float					getMaxSpeed() const;
	
	WeaponNode&             getWeapon();
	void                    setWeapon(WeaponNode::Ptr weapon);

	void                    onRemoveEntity() override;

	WorldPartition::PartitionLayer getPartitionLayer() override;

	Hitbox&                 getHitbox() override;
	
private:
	void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;
	void updateCurrent(sf::Time dt, CommandQueue& commands) override;


private:
	RectangleHitbox hitbox;
	Type::Character type;
	AnimationPack pack;
	float maxSpeed;

	WeaponNode::Ptr weapon;
};

#endif // CHARACTERENTITY_HPP
