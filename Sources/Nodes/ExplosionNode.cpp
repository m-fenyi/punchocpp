#include "ExplosionNode.hpp"
#include <Interaction/Properties.hpp>
#include <Utilities/Utilities.hpp>
#include <Audio/AudioPlayer.hpp>

ExplosionNode::ExplosionNode():
	animation(Context::get().textures.get(Textures::Animations::Explosion)) {

	animation.setRepeating(false);

	Node::centerOrigin(*this);
	hitbox.setSize(animation.getLocalBounds());

	SoundEffect::ID soundEffect = (Math::randomBool()) ? SoundEffect::Explosion1 : SoundEffect::Explosion2;
	Context::get().audio.getSoundPlayer().play(soundEffect, getWorldPosition());
}

int ExplosionNode::getDamage() const {
	return 10;
}

WorldPartition::PartitionLayer ExplosionNode::getPartitionLayer() { return WorldPartition::LowPartition; }

Hitbox & ExplosionNode::getHitbox() { 
	hitbox.setTransform(this);
	return hitbox;
}

void ExplosionNode::updateCurrent(sf::Time dt, CommandQueue & commands) {
	animation.update(dt);
	if (animation.isFinished()) this->remove();
}

void ExplosionNode::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const {
	target.draw(animation, states);
	target.draw(hitbox, states);
}
