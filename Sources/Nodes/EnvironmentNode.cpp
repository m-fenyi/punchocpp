#include "EnvironmentNode.hpp"

#include <DAL/DataStruct.hpp>
#include <Interaction/Properties.hpp>
#include <Utilities/Type.hpp>


EnvironmentNode::EnvironmentNode(EnvironmentData& data):
	EntityNode(data) {

	pack.addAnimation(data.packData);
	pack.setDrawnAnimation(Type::Idle);
}

WorldPartition::PartitionLayer EnvironmentNode::getPartitionLayer() { return WorldPartition::BackgroundPartition; }

Hitbox & EnvironmentNode::getHitbox() {
	hitbox.setTransform(this);
	hitbox.setSize(pack.getLocalBounds());
	hitbox.setRotation(pack.getDrawnTransfom().getRotation());
	return hitbox;
}

void EnvironmentNode::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const {
	target.draw(pack, states);
	target.draw(hitbox, states);
}

void EnvironmentNode::updateCurrent(sf::Time dt, CommandQueue & commands) {
	pack.update(dt, this);
}
