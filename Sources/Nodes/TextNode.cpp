#include "TextNode.hpp"
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RenderTarget.hpp>

LOGCAT("TextNode");

TextNode::TextNode(const std::string& text) {
	this->text.setFont(Context::get().fonts.get(Fonts::Main));
	this->text.setFillColor(sf::Color::Black);

	this->text.setCharacterSize(20);
	setString(text);
	Node::centerOrigin(this->text);
}

void TextNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(text, states);
}

void TextNode::setString(const std::string& text) {
	this->text.setString(text);
	Node::centerOrigin(this->text);
}