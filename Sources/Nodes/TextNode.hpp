#ifndef TEXTNODE_HPP
#define TEXTNODE_HPP

#include "SceneNode.hpp"

#include <DAL/ResourceHolder.hpp>
#include <DAL/ResourceIdentifiers.hpp>

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>


class TextNode: public SceneNode {
public:
	explicit	 TextNode(const std::string& text);
	void		 setString(const std::string& text);

private:
	void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;


private:
	sf::Text text;
};

#endif // TEXTNODE_HPP
