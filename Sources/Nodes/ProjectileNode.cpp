#include "ProjectileNode.hpp"
#include "EmitterNode.hpp"

#include <Interaction/Properties.hpp>

#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <cmath>
#include <cassert>

LOGCAT("ProjectileNode");


ProjectileNode::ProjectileNode(ProjectileData& data):
	EntityNode(data),
	data(data),
	sprite(Context::get().textures.get(data.sprite)),
	life(sf::Time::Zero),
	targetDirection() {

	Node::centerOrigin(sprite);
	hitbox.setSize(sprite.getLocalBounds());
}

void ProjectileNode::guideTowards(sf::Vector2f position) {
	assert(isGuided());
	targetDirection = Math::unitVector(position - getWorldPosition());
}

bool ProjectileNode::isGuided() const {
	return data.homing;
}

void ProjectileNode::updateCurrent(sf::Time dt, CommandQueue& commands) {
	life += dt;
	if (life > data.lifetime) {
		remove();
	}

	if (isGuided()) {
		const float approachRate = 200.f;

		sf::Vector2f newVelocity = Math::unitVector(approachRate * dt.asSeconds() * targetDirection + getVelocity());
		newVelocity *= getMaxSpeed();
		float angle = std::atan2(newVelocity.y, newVelocity.x);

		setRotation(Math::toDegree(angle));
		setVelocity(newVelocity);
	}

	EntityNode::updateCurrent(dt, commands);
}

void ProjectileNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(sprite, states);
	target.draw(hitbox, states);
}

float ProjectileNode::getMaxSpeed() const {
	return data.speed;
}

int ProjectileNode::getDamage() const {
	return data.damage;
}

void ProjectileNode::setShooter(EntityNode * shooter) { getAbility<DamagerAbility>(Ability::Damager).setSource(shooter); }
EntityNode * ProjectileNode::getShooter() { return getAbility<DamagerAbility>(Ability::Damager).getSource(); }

WorldPartition::PartitionLayer ProjectileNode::getPartitionLayer() { return WorldPartition::LowPartition; }

Hitbox & ProjectileNode::getHitbox() {
	hitbox.setTransform(this);
	hitbox.setPosition(getWorldPosition());
	sf::Vector2f scale = sprite.getScale();
	scale.x *= getScale().x;
	scale.y *= getScale().y;
	hitbox.setScale(scale);
	hitbox.setRotation(getRotation() + sprite.getRotation());
	hitbox.setSize(sprite.getLocalBounds());
	hitbox.setOrigin(sprite.getOrigin());
	return hitbox; 
}
