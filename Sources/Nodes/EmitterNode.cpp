#include "EmitterNode.hpp"
#include "ParticleNode.hpp"

#include <Action/CommandQueue.hpp>
#include <Action/Command.hpp>
#include <Utilities/Log.hpp>

LOGCAT("EmitterNode");

EmitterNode::EmitterNode(ParticleData& data):
	SceneNode(),
	accumulatedTime(sf::Time::Zero),
	data(data),
	particleSystem(nullptr) {}

void EmitterNode::updateCurrent(sf::Time dt, CommandQueue& commands) {
	if (particleSystem) {
		emitParticles(dt);
	} else {
		auto finder = [this](ParticleNode& container) {
			if (&container.getParticleData() == &data) 
				particleSystem = &container;
		};

		Command command;
		command.category = Category::ParticleSystem;
		command.action = derivedAction<ParticleNode>(finder);

		commands.push(command);
	}
}

void EmitterNode::emitParticles(sf::Time dt) {
	const float emissionRate = 30.f;
	const sf::Time interval = sf::seconds(1.f) / emissionRate;

	accumulatedTime += dt;

	while (accumulatedTime > interval) {
		accumulatedTime -= interval;
		particleSystem->addParticle(getWorldPosition());
	}
}
