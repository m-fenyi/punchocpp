#ifndef EXPLOSIONNODE_HPP
#define EXPLOSIONNODE_HPP


#include "EntityNode.hpp"
#include <Interaction/Hitbox.hpp>


class ExplosionNode: public EntityNode {
public:
	ExplosionNode();

	int getDamage() const;
	
	WorldPartition::PartitionLayer getPartitionLayer() override;

	Hitbox& getHitbox() override;

private:
	void updateCurrent(sf::Time dt, CommandQueue& commands) override;
	void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;

private:
	RectangleHitbox hitbox;
	Animation animation;
};

#endif // EXPLOSIONNODE_HPP