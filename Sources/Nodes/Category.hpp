#ifndef CATEGORY_HPP
#define CATEGORY_HPP


namespace Category {
	using Flag = unsigned int;
	enum Type {
		NoCategory     = 0,
		SceneAirLayer  = 1 << 0,
		PlayerEntity   = 1 << 1,
		AlliedEntity   = 1 << 2,
		EnemyEntity    = 1 << 3,
		Pickup         = 1 << 4,
		Projectile     = 1 << 5,

		Explosion      = 1 << 7,
		Environment    = 1 << 8,
		

		ParticleSystem = 1 << 15,
		SoundEffect    = 1 << 16,


		Interactible   = 1 << 31,

		Entity         = PlayerEntity | AlliedEntity | EnemyEntity,
		
		All            = -1u,
	};
}

#endif // CATEGORY_HPP
