#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include "GameAction.hpp"
#include "Command.hpp"

#include <Input/InputHandler.hpp>

#include <SFML\System\Time.hpp>

#include <set>
#include <list>
#include <vector>



class Controllable;
class CharacterEntity;

class Controller {
public:
	virtual void update(sf::Time dt) = 0;

	virtual void addNode(Controllable* node) = 0;
	virtual void removeNode(Controllable* node) = 0;
};

class SetController: public Controller {
public:
	void addNode(Controllable* node) override;
	void removeNode(Controllable* node) override;

protected:
	std::set<Controllable*> controlled;
};

class PlayerController: public Controller, public InputHandler<GameActionID> {
public:
	PlayerController();

	void update(sf::Time dt) override {}
	void addNode(Controllable* node) override;
	void removeNode(Controllable* node) override;

	CharacterEntity& getControlled();

protected:
	//bool activate(GameActionID actionId, const Input& input) const override;

protected:
	CharacterEntity* controlled;
};

struct Direction {
	Direction(float angle, int milliseconds): angle(angle), duration(sf::milliseconds(milliseconds)) {}

	float angle;
	sf::Time duration;
};

class PatternController: public SetController {
public:
	PatternController(std::vector<Direction>& directions);

	void update(sf::Time dt) override;

private:
	std::vector<Direction> directions;

	sf::Time travelTime;
	std::size_t directionIndex;
};

class RandomPatternController: public Controller {
public:
	RandomPatternController(int minDuration, int maxDuration);

	void addNode(Controllable* node) override;
	void removeNode(Controllable* node) override;

	void update(sf::Time dt) override;

private:
	struct AddedNode {
		CharacterEntity* controlled;
		Direction direction;
	};

	std::list<AddedNode> controlled;

	int minDuration;
	int maxDuration;
};

#endif // CONTROLLER_HPP