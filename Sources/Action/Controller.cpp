#include "Controller.hpp"
#include "Controllable.hpp"

#include <Nodes/CharacterEntity.hpp>

#include <Utilities/Utilities.hpp>

LOGCAT("Controller");

void SetController::addNode(Controllable* node) {
	controlled.emplace(node);
	node->setController(this);
}

void SetController::removeNode(Controllable* node) {
	controlled.erase(node);
}




PlayerController::PlayerController(): controlled(nullptr) {
	/*
	setAction(GameActionID::MoveUp     , derivedAction<CharacterEntity>(EntityAccelerator(0, -1)));
	setAction(GameActionID::MoveDown   , derivedAction<CharacterEntity>(EntityAccelerator(0, +1)));
	setAction(GameActionID::MoveLeft   , derivedAction<CharacterEntity>(EntityAccelerator(-1, 0)));
	setAction(GameActionID::MoveRight  , derivedAction<CharacterEntity>(EntityAccelerator(+1, 0)));
	setAction(GameActionID::PrimaryFire, derivedAction<CharacterEntity>(WeaponShooter()));
	setAction(GameActionID::Aim        , derivedAction<CharacterEntity>(WeaponShooter()));
	*/
}

void PlayerController::addNode(Controllable* node) {
	CharacterEntity* controllable = nullptr;
	if (!(controllable = static_cast<CharacterEntity*>(node))) {
		LOGW("Cannot add node, bad type. Expected CharacterEntity");
		return;
	}
	controlled = controllable;
	node->setController(this);
}

void PlayerController::removeNode(Controllable* node) {
	controlled = nullptr;
}

CharacterEntity& PlayerController::getControlled() {
	return *controlled;
}

/*bool PlayerController::activate(GameActionID actionId, const Input& input) const {
	controlled->pushAction(getAction(actionId));
	return true;
}*/




PatternController::PatternController(std::vector<Direction>& directions): 
	SetController(), 
	directions(directions), 
	travelTime(sf::Time::Zero), 
	directionIndex(0) {}

void PatternController::update(sf::Time dt) {
	if (controlled.empty()) return;
	if (travelTime > directions[directionIndex].duration) {
		directionIndex = (directionIndex + 1) % directions.size();
		travelTime = sf::Time::Zero;
	}

	for (auto controllable : controlled) {
		CharacterEntity* node(nullptr);
		if (!(node = static_cast<CharacterEntity*>(controllable))) {
			LOGW("Non CharacterEntity in PatternController");
			return;
		}

		float x = std::cos(Math::toRadian(directions[directionIndex].angle)) * node->getMaxSpeed();
		float y = std::sin(Math::toRadian(directions[directionIndex].angle)) * node->getMaxSpeed();

		node->pushAction(derivedAction<EntityNode>(EntitySpeedSetter(x, y)));
	}
	travelTime += dt;
}




RandomPatternController::RandomPatternController(int minDuration, int maxDuration):
	minDuration(minDuration),
	maxDuration(maxDuration) {}

void RandomPatternController::addNode(Controllable* node) {
	CharacterEntity* controllable = nullptr;
	if (!(controllable = static_cast<CharacterEntity*>(node))) {
		LOGW("Cannot add node, bad type. Expected CharacterEntity");
		return;
	}
	controlled.emplace_back(AddedNode{ controllable, Direction(-1, 1500) });
	node->setController(this);
	node->pushAction(derivedAction<EntityNode>(EntitySpeedSetter(0, 0)));
}

void RandomPatternController::removeNode(Controllable* controllable) {
	for (auto it = controlled.begin(); it != controlled.end(); ) {
		if (it->controlled != controllable) {
			++it;
			continue;
		}

		it = controlled.erase(it);
	}
}

void RandomPatternController::update(sf::Time dt) {
	for (auto& pair : controlled) {
		pair.direction.duration -= dt;
		if (pair.direction.duration < sf::Time::Zero) {
			if (Math::randomBool(3)) {
				pair.direction.angle = -1;
				pair.controlled->pushAction(derivedAction<EntityNode>(EntitySpeedSetter(0, 0)));
			}
			else {
				pair.direction.angle = Math::randomReal<float>(360);
			}
			pair.direction.duration = sf::milliseconds(Math::randomInt(minDuration, maxDuration));
		}
		if (pair.direction.angle < 0) continue;
		//pair.controlled->setRotation(pair.second.angle);

		float x = std::cos(Math::toRadian(pair.direction.angle)) * pair.controlled->getMaxSpeed();
		float y = std::sin(Math::toRadian(pair.direction.angle)) * pair.controlled->getMaxSpeed();

		pair.controlled->pushAction(derivedAction<EntityNode>(EntitySpeedSetter(x, y)));
	}
}
