#include "Controllable.hpp"
#include "Controller.hpp"


Controllable::Controllable(): controller(nullptr) {}

void Controllable::pushAction(Command::GameAction action) {
	actions.push(action);
}

void Controllable::updateActions(SceneNode* controlled) {
	while (!actions.empty()) {
		actions.front()(*controlled);
		actions.pop();
	}
}

void Controllable::removeController() {
	if (controller) {
		controller->removeNode(this);
	}
}

void Controllable::setController(Controller * controller) {
	this->controller = controller;
}

