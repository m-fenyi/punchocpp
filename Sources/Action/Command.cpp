#include "Command.hpp"
#include <Nodes/CharacterEntity.hpp>
#include <Utilities/Node.hpp>

Command::Command(): action(), category(Category::NoCategory) {}



void EntitySpeedSetter::operator()(EntityNode& node) const {
	node.setVelocity(velocity);
}

void EntityAccelerator::operator()(EntityNode & node) const {
	node.accelerate(velocity);
}

void WeaponShooter::operator()(CharacterEntity & entity) const {
	entity.getWeapon().getWeapon().fire();
}

void WeaponAimer::operator()(CharacterEntity & node) const {
	node.getWeapon().setRotation(factor);
	Node::flipHorizontal(node.getWeapon());
}

void WeaponPointAimer::operator()(CharacterEntity& node) const {
	Math::toRotation(point - node.getPosition());
	Node::flipHorizontal(node.getWeapon());
}
