#ifndef CONTROLLABLE_HPP
#define CONTROLLABLE_HPP

#include <queue>
#include "Command.hpp"

class Controller;

class Controllable {
public:
	Controllable();

	void pushAction(Command::GameAction action);
	void updateActions(SceneNode* controlled);

	void removeController();
	void setController(Controller* controller);

protected:
	Controller* controller;

private:
	std::queue<Command::GameAction> actions;
};

#endif // CONTROLLABLE_HPP