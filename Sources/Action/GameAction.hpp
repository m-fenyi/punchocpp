#ifndef GAMEACTION_HPP
#define GAMEACTION_HPP

enum GameActionID {
	MoveUp,
	MoveDown,
	MoveLeft,
	MoveRight,

	Interact,

	PrimaryFire,
	SecondaryFire,
	SwitchWeapon,
	SpecialMove,

	Aim,

	GameActionCount
};

#endif //GAMEACTION_HPP