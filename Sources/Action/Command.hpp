#ifndef COMMAND_HPP
#define COMMAND_HPP

#include <Utilities/Time.hpp>
#include <Nodes/Category.hpp>

#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>

#include <functional>
#include <cassert>


class SceneNode;
class EntityNode;
class CharacterEntity;
class Input;

template<class Node>
class GameCommand {
public:
	virtual void update(Node& node) = 0;
	virtual void update(sf::Time& dt) = 0;
};

template<class Node>
class NodeGameCommand {
public:
	NodeGameCommand():
		NodeGameCommand(Category::All) {}
	NodeGameCommand(Category::Flag flag):
		category(flag) {}

private:
	Category::Flag category;
};

template<class Node>
class TransitionGameCommand: public GameCommand<Node> {
public:
	TransitionGameCommand(Time::Transition<float>& transition):
		transition(transition) {}

	void update(Node& node) override {}
	void update(sf::Time& dt) override {
		transition.update(dt);
	}

private:
	Time::Transition<float>& transition;
};

struct Command {
	using GameAction = std::function<void(SceneNode&)>;

	Command();

	GameAction   action;
	unsigned int category;
};

template <typename DerivedNode, typename Function>
Command::GameAction derivedAction(Function fn) {
	return [=](SceneNode& node) {
		DerivedNode* derivedNode = dynamic_cast<DerivedNode*>(&node);
		assert(derivedNode != nullptr);

		fn(*derivedNode);
	};
}


struct EntityAccelerator {
	EntityAccelerator(float vx, float vy) : velocity(vx, vy) {}

	void operator() (EntityNode& node) const;
	sf::Vector2f velocity;
};

struct EntitySpeedSetter {
	EntitySpeedSetter(float vx, float vy) : velocity(vx, vy) {}

	void operator() (EntityNode& node) const;
	sf::Vector2f velocity;
};

struct WeaponShooter {
	WeaponShooter() {}

	void operator() (CharacterEntity& entity) const;
};

struct WeaponAimer {
	WeaponAimer(float factor) : factor(factor) {}

	void operator() (CharacterEntity& node) const;
	float factor;
};


struct WeaponPointAimer {
	WeaponPointAimer(sf::Vector2f point) : point(point) {}

	void operator() (CharacterEntity& node) const;
	sf::Vector2f point;
};

#endif // COMMAND_HPP
