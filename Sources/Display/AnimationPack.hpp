#ifndef ANIMATIONPACK_HPP
#define ANIMATIONPACK_HPP

#include "Animation.hpp"

#include <DAL/DataType.hpp>
#include <DAL/DataStruct.hpp>
#include <DAL/ResourceIdentifiers.hpp>

#include <SFML\Graphics\Sprite.hpp>

#include <variant>
#include <map>

class EntityNode;

class AnimationPack: public sf::Drawable, public sf::Transformable {
private:
	class AnimationInfo {
	public:
		Animation* toAnim() const;
		sf::Sprite* toSprite() const;

		bool isAnim() const;
		bool isSprite() const;

	public:
		DrawableInfo info;
		sf::Drawable* drawn = nullptr;
	};

public:
	AnimationPack();
	~AnimationPack();
	
	void addAnimation(AnimationPackData& data);
	void addAnimation(Type::Animation type, sf::Drawable& drawable);

	void setDrawnAnimation(Type::Animation type);

	void createNode(Type::Animation type, sf::Vector2f position) const;

	sf::Transformable& getDrawnTransfom() const;
	sf::FloatRect getLocalBounds() const;
	sf::FloatRect getGlobalBounds() const;

	void update(sf::Time dt, EntityNode* node = nullptr);

private:
	void addAnimation(Type::Animation type, Textures::Sprites::ID id);
	void addAnimation(Type::Animation type, Textures::Animations::ID id);

	void createSpriteNode(Type::Animation type, sf::Vector2f position) const;
	void createAnimNode(Type::Animation type, sf::Vector2f position) const;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	std::map<Type::Animation, AnimationInfo> pack;
	Type::Animation current;
};

#endif // ANIMATIONPACK_HPP