#include "Camera.hpp"
#include <Nodes/SceneNode.hpp>

#include <Utilities/Utilities.hpp>

#include <SFML/System/Time.hpp>

LOGCAT("Camera");


Camera::Camera(State::Context & context):
	view(context.window.getWindow().getDefaultView()),
	reticuleSprite(context.textures.get(Textures::Sprites::Projectile1)),
	reticuleLerp(25),
	cameraLerp(0) {

	context.window.setCamera(*this);
	Node::centerOrigin(reticuleSprite);
}

void Camera::update(sf::Time dt) {
	/*
	sf::Vector2f subjectPosition = subject->getWorldPosition();
	sf::Vector2f pointerPosition = getWorldPointerPosition();
	sf::Vector2f objective = (pointerPosition + 4.f * subjectPosition) / 5.f;

	view.setCenter(lerp(view.getCenter(), objective, cameraLerp, dt));
	*/
	for (auto effect : effects) effect.update(dt);
}

void Camera::draw(sf::RenderTarget & target, sf::RenderStates states) const {
	target.draw(reticuleSprite);
	for (auto effect : effects) effect.draw();
}

sf::View & Camera::getView() { return view; }

sf::FloatRect Camera::getViewBounds() const {
	return sf::FloatRect(view.getCenter() - view.getSize() / 2.f, view.getSize());
}

sf::Vector2f Camera::getCenter() const { return view.getCenter(); }

void Camera::setCenter(sf::Vector2f position) { view.setCenter(position); }

void Camera::setCameraLerp(float lerp) { cameraLerp = lerp; }

void Camera::setReticuleLerp(float lerp) { reticuleLerp = lerp; }

void Camera::setZoom(float zoom) { view.zoom(zoom); }

sf::Sprite& Camera::getReticule() { return reticuleSprite; }

float Camera::getReticuleLerp() { return reticuleLerp; }

void Camera::setReticule(ReticuleType type) { Node::centerOrigin(reticuleSprite); }

void Camera::setReticulePosition(sf::Vector2f position) { reticuleSprite.setPosition(position); }
