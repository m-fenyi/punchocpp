#ifndef BLOOMEFFECT_HPP
#define BLOOMEFFECT_HPP

#include "PostEffect.hpp"
#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/ResourceHolder.hpp>

#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/Shader.hpp>

#include <array>


class BloomEffect: public PostEffect {
private:
	using RenderTextureArray = std::array<sf::RenderTexture, 2>;


public:
	BloomEffect();

	void apply(const sf::RenderTexture& input, sf::RenderTarget& output) override;
				 
private:		 
	void		 prepareTextures(sf::Vector2u size);
				 
	void		 filterBright(const sf::RenderTexture& input, sf::RenderTexture& output);
	void		 blurMultipass(RenderTextureArray& renderTextures);
	void		 blur(const sf::RenderTexture& input, sf::RenderTexture& output, sf::Vector2f offsetFactor);
	void		 downsample(const sf::RenderTexture& input, sf::RenderTexture& output);
	void		 add(const sf::RenderTexture& source, const sf::RenderTexture& bloom, sf::RenderTarget& target);


private:
	ShaderHolder		shaders;

	sf::RenderTexture	brightnessTexture;
	RenderTextureArray	firstPassTextures;
	RenderTextureArray	secondPassTextures;
};

#endif // BLOOMEFFECT_HPP
