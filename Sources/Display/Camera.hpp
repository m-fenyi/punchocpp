#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <SFML/Graphics/View.hpp>
#include <SFML/System/Time.hpp>
#include <GUI/State.hpp>

#include <list>

class SceneNode;

class Camera: public sf::Drawable {
public:
	class Effect {
	public:
		Effect(sf::View& view, float strength = 1.f, int durationMilli = 1000): 
			view(view), duration(sf::milliseconds(durationMilli)), strength(strength) {}
		virtual void update(sf::Time dt) {}
		virtual void draw() {}

	private:
		sf::View& view;
		sf::Time duration;
		float strength;
	};

	enum ReticuleType {
		Dot,
		CrossHair,
		Target,

		Count
	};

	Camera(State::Context& context);
	void update(sf::Time dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	sf::View& getView();
	sf::FloatRect getViewBounds() const;
	sf::Vector2f getCenter() const;
	void setCenter(sf::Vector2f position);

	void setCameraLerp(float lerp);
	void setReticuleLerp(float lerp);
	void setZoom(float zoom);


	sf::Sprite& getReticule();
	float getReticuleLerp();
	void setReticule(ReticuleType type);
	void setReticulePosition(sf::Vector2f position);

private:
	std::list<Effect> effects;

	sf::View view;

	sf::Sprite reticuleSprite;

	float      reticuleLerp;
	float      cameraLerp;
};

#endif  // CAMERA_HPP