#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Time.hpp>
#include <Utilities/Time.hpp>

class Animation: public sf::Drawable, public sf::Transformable {
public:
	Animation();
	explicit 			Animation(const sf::Texture& texture);

	void 				setTexture(const sf::Texture& texture);
	const sf::Texture* 	getTexture() const;

	void 				setTextureRect(const sf::IntRect& rectangle);
	const sf::IntRect& 	getTextureRect() const;
	const sf::IntRect& 	getFrameRect() const;

	void 				setColor(const sf::Color& color);
	const sf::Color& 	getColor() const;

	void 				setFrameSize(sf::Vector2i frameSize);
	sf::Vector2i		getFrameSize() const;

	void 				setFrameCount(std::size_t numFrames);
	std::size_t 		getFrameCount() const;

	void 				setDuration(sf::Time duration);
	sf::Time 			getDuration() const;

	void 				setRepeating(bool flag);
	bool 				isRepeating() const;

	void 				restart();
	bool 				isFinished() const;

	void				showNextFrame();
	void				showPreviousFrame();
	void				showFirstFrame();
	void				showLastFrame();
	void				showFrame(size_t frameNum);

	sf::FloatRect 		getLocalBounds() const;
	sf::FloatRect 		getGlobalBounds() const;

	void 				update(sf::Time& dt);

private:
	void 				draw(sf::RenderTarget& target, sf::RenderStates states) const;


private:
	sf::Sprite     sprite;
	sf::IntRect    animRect;
	sf::Vector2i   frameSize;
	size_t         numFrames;
	size_t         currentFrame;
	Time::Duration duration;
	bool           repeat;
};

#endif // ANIMATION_HPP
