#include "Animation.hpp"
#include <Utilities/Log.hpp>
#include <Utilities/Math.hpp>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <SFML/System/Time.hpp>

LOGCAT("Animation");

Animation::Animation():
	sprite(),
	frameSize(),
	numFrames(0),
	currentFrame(0),
	duration(sf::Time::Zero),
	repeat(false) {}

Animation::Animation(const sf::Texture& texture):
	sprite(texture),
	frameSize(texture.getSize()),
	numFrames(0),
	currentFrame(0),
	duration(sf::seconds(1)),
	repeat(false) {}

void Animation::setTexture(const sf::Texture& texture) { sprite.setTexture(texture); }
const sf::Texture* Animation::getTexture() const { return sprite.getTexture(); }

void Animation::setTextureRect(const sf::IntRect & rectangle) { animRect = rectangle; }
const sf::IntRect & Animation::getTextureRect() const { return animRect; }
const sf::IntRect & Animation::getFrameRect() const { return sprite.getTextureRect(); }

void Animation::setColor(const sf::Color & color) { sprite.setColor(color); }
const sf::Color & Animation::getColor() const { return sprite.getColor(); }

void Animation::setFrameSize(sf::Vector2i frameSize) { this->frameSize = frameSize; }
sf::Vector2i Animation::getFrameSize() const { return frameSize; }

void Animation::setFrameCount(std::size_t numFrames) { this->numFrames = numFrames; }
std::size_t Animation::getFrameCount() const { return numFrames; }

void Animation::setDuration(sf::Time duration) { this->duration.setEndTime(duration); }
sf::Time Animation::getDuration() const { return duration.getEndTime(); }

void Animation::setRepeating(bool flag) { repeat = flag; }
bool Animation::isRepeating() const { return repeat; }

void Animation::restart() { currentFrame = 0; }

bool Animation::isFinished() const { return currentFrame >= numFrames; }

void Animation::showNextFrame() { showFrame(currentFrame + 1); }
void Animation::showPreviousFrame() { showFrame(currentFrame - 1); }
void Animation::showFirstFrame() { showFrame(0); }
void Animation::showLastFrame() { showFrame(numFrames); }

void Animation::showFrame(size_t frameNum) {
	currentFrame = Range::between((size_t)0u, numFrames).bound(frameNum);

	sf::Vector2i textureBounds(animRect.left + animRect.width, animRect.top + animRect.height);
	sf::IntRect frameRect = getFrameRect();

	frameRect.left += frameRect.width;

	if (frameRect.left + frameRect.width > textureBounds.x) {
		frameRect.left = animRect.left;
		frameRect.top += frameRect.height;
	}

	if (repeat) {
		currentFrame = (currentFrame + 1) % numFrames;

		if (currentFrame == 0) frameRect = sf::IntRect(animRect.left, animRect.top, frameSize.x, frameSize.y);
	}
	else {
		++currentFrame;
	}
}


sf::FloatRect Animation::getLocalBounds() const {
	return sf::FloatRect(getOrigin(), static_cast<sf::Vector2f>(getFrameSize()));
}
sf::FloatRect Animation::getGlobalBounds() const {
	return getTransform().transformRect(getLocalBounds());
}

void Animation::update(sf::Time& dt) {
	sf::Time timePerFrame = duration.getEndTime() / static_cast<float>(numFrames);
	duration.update(dt);

	sf::Vector2i textureBounds(animRect.left + animRect.width, animRect.top + animRect.height);
	sf::IntRect frameRect = getFrameRect();

	if (currentFrame == 0) frameRect = sf::IntRect(animRect.left, animRect.top, frameSize.x, frameSize.y);

	while (duration.getCurrentTime() >= timePerFrame && (currentFrame <= numFrames || repeat)) {
		showNextFrame();
	}

	sprite.setTextureRect(frameRect);
}

void Animation::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	states.transform *= getTransform();
	target.draw(sprite, states);
}
