#include "AnimationPack.hpp"
#include <Nodes/EntityNode.hpp>
#include <Nodes/AnimationNode.hpp>
#include <Nodes/SpriteNode.hpp>

#include <Utilities/Node.hpp>
#include <Utilities/Context.hpp>
#include <Utilities/Log.hpp>

#include <SFML\Graphics\RenderTarget.hpp>

LOGCAT("AnimationPack");

AnimationPack::AnimationPack(): current(Type::Standing) {}

AnimationPack::~AnimationPack() {
	for (auto pair : pack) {
		delete pair.second.drawn;
	}
}

void AnimationPack::addAnimation(AnimationPackData & packData) {
	for (auto data : packData) {
		switch (data.info.kind) {
		default:
		case Type::SpriteKind:
			addAnimation(data.animation, data.info.id.sprite);
			break;
		case Type::AnimationKind:
			addAnimation(data.animation, data.info.id.anim);
			break;
		}
	}
}

void AnimationPack::addAnimation(Type::Animation type, Textures::Sprites::ID id) {
	pack[type].info.kind = Type::SpriteKind;
	pack[type].info.id.sprite = id;
	pack[type].drawn = new sf::Sprite(Context::get().textures.get(id));
}
void AnimationPack::addAnimation(Type::Animation type, Textures::Animations::ID id) { 
	pack[type].info.kind = Type::AnimationKind;
	pack[type].info.id.anim = id;
	pack[type].drawn = new Animation(Context::get().textures.get(id));
}
void AnimationPack::addAnimation(Type::Animation type, sf::Drawable& drawable) { 
	pack[type].info.kind = Type::GraphicKindCount;
	pack[type].drawn = &drawable;
}

void AnimationPack::setDrawnAnimation(Type::Animation type) { current = type; }

void AnimationPack::createNode(Type::Animation type, sf::Vector2f position) const {
	if (!pack.count(type)) return;
	switch (pack.at(type).info.kind) {
	case Type::SpriteKind:
		createSpriteNode(type, position);
		break;
	case Type::AnimationKind:
		createAnimNode(type, position);
		break;
	default:
		break;
	}
}

sf::Transformable & AnimationPack::getDrawnTransfom() const {
	switch (pack.at(current).info.kind) {
	default:
		LOGW("Return Unknown Transform");
	case Type::SpriteKind:
		return *pack.at(current).toSprite();
	case Type::AnimationKind:
		return *pack.at(current).toAnim();
	}
}

sf::FloatRect AnimationPack::getLocalBounds() const {
	switch (pack.at(current).info.kind) {
	case Type::SpriteKind:
		return pack.at(current).toSprite()->getLocalBounds();
	case Type::AnimationKind:
		return pack.at(current).toAnim()->getLocalBounds();
	default:
		LOGW("Return empty Local bound");
		return sf::FloatRect();
	}
}
sf::FloatRect AnimationPack::getGlobalBounds() const { return getTransform().transformRect(getLocalBounds()); }

void AnimationPack::update(sf::Time dt, EntityNode* node) {
	Type::Animation animID = Node::getAnimationType(node);
	if (animID != Type::Animation::AnimationCount && animID != current && pack.count(animID)) {
		current = animID;
		if (pack.at(current).isAnim()) {
			pack.at(current).toAnim()->restart();
		}
	} else {
		if (pack.at(current).isAnim()) {
			pack.at(current).toAnim()->update(dt);
		}
	}
}

void AnimationPack::createSpriteNode(Type::Animation type, sf::Vector2f position) const {
	SpriteNode::Ptr sprite(new SpriteNode(pack.at(type).info.id.sprite));
	sprite->setPosition(position);
	SceneNode::attachTo(SceneNode::LowerAir, std::move(sprite));
}
void AnimationPack::createAnimNode(Type::Animation type, sf::Vector2f position) const {
	AnimationNode::Ptr anim(new AnimationNode(pack.at(type).info.id.anim));
	anim->setPosition(position);
	SceneNode::attachTo(SceneNode::LowerAir, std::move(anim));
}

void AnimationPack::draw(sf::RenderTarget & target, sf::RenderStates states) const {
	if (!pack.at(current).drawn) return;
	states.transform *= getTransform();
	target.draw(*pack.at(current).drawn, states);
}

Animation * AnimationPack::AnimationInfo::toAnim() const { return static_cast<Animation*>(drawn); }
sf::Sprite * AnimationPack::AnimationInfo::toSprite() const { return static_cast<sf::Sprite*>(drawn); }
bool AnimationPack::AnimationInfo::isAnim() const { return info.kind == Type::AnimationKind; }
bool AnimationPack::AnimationInfo::isSprite() const { return info.kind == Type::SpriteKind; }
