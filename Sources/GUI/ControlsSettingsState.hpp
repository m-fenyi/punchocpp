#ifndef CONTROLSSETTINGSSTATE_HPP
#define CONTROLSSETTINGSSTATE_HPP

#include "GUIState.hpp"
#include "VerticalContainer.hpp"
#include "InputChooser.hpp"
#include "Button.hpp"
#include "Switch.hpp"

#include <array>

class ControlsSettingsState: public GUIState {
public:
	ControlsSettingsState(StateStack& stack, Context& context);

	void onPop(Context& context) override;
private:
	void updateInputChoosers();
	void addInputChooser(GameActionID action, Context context);


private:
	std::array<GUI::Settings::InputChooser::Ptr, GameActionID::GameActionCount>	bindingInputChooser;
	GUI::Settings::Switch<std::string>::Ptr controlsSwitch;
	GUI::Settings::Switch<std::string>::Ptr controllerTypeSwitch;
};

#endif // CONTROLSSETTINGSSTATE_HPP