#ifndef GUI_SCROLLINGBAR_HPP
#define GUI_SCROLLINGBAR_HPP

#include "Container.hpp"

#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/ResourceHolder.hpp>

#include <SFML/Graphics/Text.hpp>


namespace GUI {

	class ScrollingBar: public Container {
	public:
		using Ptr = std::shared_ptr<ScrollingBar>;


	public:
		ScrollingBar(State::Context& context);

	private:
		void		 draw(sf::RenderTarget& target, sf::RenderStates states) const;
	};

}

#endif // GUI_SCROLLINGBAR_HPP