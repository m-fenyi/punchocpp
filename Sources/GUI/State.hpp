#ifndef STATE_HPP
#define STATE_HPP

#include "StateIdentifiers.hpp"
#include "UIAction.hpp"

#include <DAL/TextureWrapper.hpp>
#include <DAL/ResourceHolder.hpp>

#include <Input/Input.hpp>
#include <Input/InputHandler.hpp>

#include <SFML/System/Time.hpp>
#include <SFML/Window/Event.hpp>

#include <memory>


namespace sf {
	class RenderWindow;
}

class StateStack;
class AudioPlayer;
class ConfigurationRepository;
class GameDataRepository;
class TextureWrapper;
class WindowWrapper;

class State {
public:
	using Ptr = std::unique_ptr<State>;

	struct Context {
		Context(WindowWrapper& window, TextureWrapper& textures, FontHolder& fonts, AudioPlayer& audio, ConfigurationRepository& config, GameDataRepository& gameRepo);

		WindowWrapper&           window;
		TextureWrapper&		     textures;
		FontHolder&			     fonts;
		AudioPlayer&		     audio;
		ConfigurationRepository& config;
		GameDataRepository&      gameRepo;
	};

protected:
	State(StateStack& stack, Context& context);

public:
	virtual		 ~State();
	
	virtual void draw(sf::RenderWindow& window) = 0;
	virtual bool update(sf::Time dt) = 0;
	virtual void update() {};
	virtual bool handleInput(const Input& input);
	
	virtual void onPop(Context& context) {};
	
protected:
	void		 requestStackPush(States::ID stateID);
	void		 requestStackPop();
	void		 requestStackClear();

	void		 requestStackUpdate();

protected:
	InputHandler<UIActionID> handler;

private:
	StateStack*	stack;
	Context		context;
};

#endif // STATE_HPP
