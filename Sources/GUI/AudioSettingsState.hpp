#ifndef AUDIOSETTINGSSTATE_HPP
#define AUDIOSETTINGSSTATE_HPP

#include "GUIState.hpp"
#include "Slider.hpp"



class AudioSettingsState: public GUIState {
public:
	AudioSettingsState(StateStack& stack, Context context);
	
	void onPop(Context& context) override;

private:
	GUI::Settings::Slider::Ptr	masterSlider;
	GUI::Settings::Slider::Ptr	musicSlider;
	GUI::Settings::Slider::Ptr	soundSlider;
};

#endif // AUDIOSETTINGSSTATE_HPP