#ifndef GUI_CONTAINER_HPP
#define GUI_CONTAINER_HPP

#include "Component.hpp"
#include "State.hpp"
#include "UIAction.hpp"

#include <Input/InputHandler.hpp>
#include <Utilities/Type.hpp>
#include <Utilities/Helper.hpp>


#include <vector>
#include <memory>

class SoundPlayer;

namespace GUI {
	enum Alignment {
		None             = 0,
		Left             = 1 << 0,
		Right            = 1 << 1,
		Top              = 1 << 2,
		Bottom           = 1 << 3,
		CenterHorizontal = Left | Right,
		CenterVertical   = Top | Bottom,
		Center           = CenterHorizontal | CenterVertical,

		Horizontal       = Left | Right,
		Vertical         = Top | Bottom
	};

	class Container: public Component {
	public:
		using Ptr = std::shared_ptr<Container>;


	public:
		Container(State::Context& context);

		virtual void pack(Component::Ptr component);

		bool isSelectable() const override;
		bool isActivable() const override;
		bool isPressable() const override;
		void update(sf::Time) override;
		void update() override;
		bool handleInput(const Input& input) override;

		sf::FloatRect getLocalBounds() const override;

		void updatePosition() override {}

		void setPadding(float top, float left, float bottom, float right);
		void setPaddingTop   (float top);
		void setPaddingBottom(float bottom);
		void setPaddingLeft  (float left);
		void setPaddingRight (float right);

		void setAlignment(Alignment align);


	protected:
		Alignment getHorizontalFlag() const;
		Alignment getVerticalFlag() const;

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		bool		 hasSelection() const;
		void		 selectAt(sf::Vector2f position);
		void		 select(std::size_t index);
		void		 select();
		void		 selectNext();
		void		 selectPrevious();
		void		 deselect();
		void         activate();
		void         press();
		void         unpress();

	protected:
		std::vector<Component::Ptr> children;
		sf::FloatRect containerPadding;
		Flag<Alignment> alignmentFlag;

	private:
		InputHandler<UIActionID> inputHandler;

		size_t selectedChild;
		uint   selectableCount;

		SoundPlayer& sounds;
		float        pitch;
	};

}

#endif // CONTAINER_HPP
