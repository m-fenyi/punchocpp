#include "GameState.hpp"

#include <Audio/MusicPlayer.hpp>
#include <Audio/AudioPlayer.hpp>
#include <Utilities/Utilities.hpp>

#include <DAL/GameDataRepository.hpp>

#include <SFML/Graphics/RenderWindow.hpp>

LOGCAT("GameState");

GameState::GameState(StateStack& stack, Context& context):
	State(stack, context),
	world(context, context.gameRepo.get(Type::WorldAnger)) {

	context.audio.play(Music::MissionTheme);

	handler.setListener<void>(UIActionID::Return, [this] { requestStackPush(States::Pause); });
}

void GameState::draw(sf::RenderWindow& window) {
	world.draw(window);
}

bool GameState::update(sf::Time dt) {
	world.update(dt);

	return false;
}

bool GameState::handleInput(const Input& input) {
	if (handler.handleInput(input)) return false;
	if (world.handleInput(input)) return false;

	return false;
}