#ifndef GUISTATE_HPP
#define GUISTATE_HPP

#include "State.hpp"
#include "Container.hpp"

#include <memory>

class GUIState : public State {
public:
	GUIState(StateStack& stack, Context context, GUI::Container* container);

	void draw(sf::RenderWindow& window) override;
	bool update(sf::Time dt) override;
	void update() override;

	bool handleInput(const Input& input) override;


protected:
	std::unique_ptr<GUI::Container> GUIContainer;

private:

};

#endif // GUISTATE_HPP

