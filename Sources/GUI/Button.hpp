#ifndef GUI_BUTTON_HPP
#define GUI_BUTTON_HPP

#include "Component.hpp"
#include "State.hpp"
#include <DAL/ResourceIdentifiers.hpp>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include <vector>
#include <string>
#include <memory>
#include <functional>

namespace GUI {

	class Button: public Component {
	public:
		using Ptr  = std::shared_ptr<Button>;
		using Callback = std::function<void()>;

	public:
		Button(State::Context& context);

		void		 setCallback(Callback callback);
		void		 setText(const std::string& tag);
		void		 setToggle(bool flag);
		
		bool isSelectable() const override;
		void select() override;
		void deselect() override;
		
		bool isPressable() const override;
		void press() override;
		void unpress() override;

		bool isActivable() const override;
		void activate() override;
		void deactivate() override;
		
		void update() override;
		void update(sf::Time) override;
		bool handleInput(const Input& input) override;

		void updatePosition() override;

		sf::FloatRect getLocalBounds() const override;

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		void		 changeTexture(Textures::Sprites::ID type);


	private:
		Callback	 callback;
		sf::Sprite	 sprite;
		sf::Text	 text;
		std::string  tag;

		bool		 isToggle;
	};

}

#endif // GUI_BUTTON_HPP