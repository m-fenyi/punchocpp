#include "SettingsComponent.hpp"

namespace GUI {
	namespace Settings {
		SettingsComponent::SettingsComponent(State::Context & context):
			sprite(context.textures.get(Textures::Sprites::UIButtonNormal)),
			title("", context.fonts.get(Fonts::Main), 16) {
			
			sf::FloatRect bounds = sprite.getLocalBounds();
			title.setPosition(bounds.width / 2.f, bounds.height / 2.f);
			Node::centerOrigin(title);
		}

		bool SettingsComponent::isSelectable() const {
			return true;
		}

		void SettingsComponent::select() {
			Component::select();
			changeTexture(Textures::Sprites::UIButtonSelected);
		}

		void SettingsComponent::deselect() {
			Component::deselect();
			changeTexture(Textures::Sprites::UIButtonNormal);
		}

		void SettingsComponent::press() {
			Component::press();
			changeTexture(Textures::Sprites::UIButtonPressed);
		}

		void SettingsComponent::unpress() {
			Component::unpress();
			if (isSelected()) changeTexture(Textures::Sprites::UIButtonSelected);
			else changeTexture(Textures::Sprites::UIButtonNormal);
		}

		void SettingsComponent::activate() {
			Component::activate();

			changeTexture(Textures::Sprites::UIButtonPressed);
		}

		void SettingsComponent::deactivate() {
			Component::deactivate();

			if (isSelected()) changeTexture(Textures::Sprites::UIButtonSelected);
			else changeTexture(Textures::Sprites::UIButtonNormal);
		}
		
		void SettingsComponent::update() {
			setTitle(buttonTag);
		}

		sf::FloatRect SettingsComponent::getLocalBounds() const {
			return sprite.getLocalBounds() + componentMargin;
		}

		void SettingsComponent::updatePosition() {
			sprite.setPosition(sprite.getPosition().x + componentMargin.left, sprite.getPosition().y + componentMargin.top);
			title.setPosition(title.getPosition().x + componentMargin.left, title.getPosition().y + componentMargin.top);
		}

		void SettingsComponent::setTitle(const std::string& tag) {
			this->buttonTag = tag;
			this->title.setString(GUI::Translation().getTranslation(tag));
			Node::centerOrigin(this->title);
		}

		void SettingsComponent::draw(sf::RenderTarget & target, sf::RenderStates states) const {
			states.transform *= getTransform();
			target.draw(sprite, states);
			target.draw(title, states);
		}

		void SettingsComponent::changeTexture(Textures::Sprites::ID type) {
			sprite.setTextureRect(Context::get().textures.get(type).getTextureRect());
		}
	}
}