#ifndef GUI_SETTINGS_INPUTCHOOSER_HPP
#define GUI_SETTINGS_INPUTCHOOSER_HPP

#include "SettingsComponent.hpp"
#include "State.hpp"

#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/ResourceHolder.hpp>

#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Time.hpp>

namespace GUI {
	namespace Settings {
		class InputChooser: public SettingsComponent {
		public:
			using Ptr = std::shared_ptr<InputChooser>;
		public:
			InputChooser(State::Context& context, GameActionID action);

			void update(sf::Time) override;
			bool handleInput(const Input& input) override;

			bool isActivable() const override;
			bool isPressable() const override;

			sf::FloatRect getLocalBounds() const override;

			void updatePosition() override;

			void switchInputGroupShown();

			void updateInputText(bool showKeyboard);
			void updateInputText();

		private:
			void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		private:
			sf::Text	 inputText;
			sf::Sprite   inputSprite;
			GameActionID action;

			bool         showingKeyboard;
		};
	}
}

#endif // GUI_SETTINGS_INPUTCHOOSER_HPP