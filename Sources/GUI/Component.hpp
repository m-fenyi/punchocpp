#ifndef GUI_COMPONENT_HPP
#define GUI_COMPONENT_HPP

#include <DAL/TranslationTable.hpp>

#include <Input/Input.hpp>

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>

#include <memory>
#include <map>
#include <string>

namespace sf {
	class Time;
}

namespace sf {
	class Event;
}

namespace GUI {
	void setTranslationTable(TranslationTable& translationTable);
	TranslationTable& Translation();


	class SimpleComponent : public sf::Drawable, public sf::Transformable, private sf::NonCopyable {};

	class SelectableComponent {
	protected:
		SelectableComponent();

	public:
		virtual bool isSelectable() const = 0;
		virtual bool isSelected() const;
		virtual void select();
		virtual void deselect();
	private:
		bool selected;
	};

	class ActivableComponent {
	protected:
		ActivableComponent();

	public:
		virtual bool isActivable() const = 0;
		virtual bool isActive() const;
		virtual void activate();
		virtual void deactivate();
	private:
		bool active;
	};

	class PressableComponent {
	protected:
		PressableComponent();

	public:
		virtual bool isPressable() const = 0;
		virtual bool isPressed() const;
		virtual void press();
		virtual void unpress();
	private:
		bool pressed;
	};

	class MarginComponent {
	protected:
		MarginComponent();

	public:
		void setMargin(float top, float left, float bottom, float right);
		void setMarginTop(float top);
		void setMarginBottom(float bottom);
		void setMarginLeft(float left);
		void setMarginRight(float right);

		virtual void updatePosition() = 0;
	protected:
		sf::FloatRect componentMargin;
	};

	class Component: public SimpleComponent, public SelectableComponent, public ActivableComponent, public PressableComponent , public MarginComponent{
	public:
		using Ptr = std::shared_ptr<Component>;

	protected:
		Component();

	public:
		virtual		 ~Component() {}

		virtual void update() = 0;
		virtual void update(sf::Time dt) = 0;
		virtual bool handleInput(const Input& input) = 0;

		virtual sf::FloatRect getLocalBounds() const = 0;
		virtual sf::FloatRect getGlobalBounds() const;
	};

}

#endif // COMPONENT_HPP
