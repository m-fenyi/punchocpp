#include "StateStack.hpp"
#include <Utilities/Log.hpp>

#include <cassert>

LOGCAT("StateStack");

StateStack::StateStack(State::Context context):
	stack(),
	pendingList(),
	context(context),
	factories() {}

void StateStack::update(sf::Time dt) {
	for (auto itr = stack.rbegin(); itr != stack.rend(); ++itr) {
		if (!(*itr)->update(dt)) break;
	}

	applyPendingChanges();
}

void StateStack::draw(sf::RenderWindow& window) {
	for (State::Ptr& state : stack) state->draw(window);
}

void StateStack::handleInput(const Input& input) {
	for (auto state = stack.rbegin(); state != stack.rend(); ++state) {
		if (!(*state)->handleInput(input)) break;
	}

	applyPendingChanges();
}

void StateStack::update() {
	for (State::Ptr& state : stack) state->update();
}

void StateStack::pushState(States::ID stateID) {
	pendingList.push_back(PendingChange(Push, stateID));
}

void StateStack::popState() {
	pendingList.push_back(PendingChange(Pop));
}

void StateStack::clearStates() {
	pendingList.push_back(PendingChange(Clear));
}

bool StateStack::isEmpty() const {
	return stack.empty();
}

State::Ptr StateStack::createState(States::ID stateID) {
	auto found = factories.find(stateID);
	assert(found != factories.end());

	return found->second();
}

void StateStack::applyPendingChanges() {
	for (PendingChange change : pendingList) {
		switch (change.action) {
			case Push:
				stack.push_back(createState(change.stateID));
				break;
			case Pop:
				stack.back()->onPop(context);
				stack.pop_back();
				break;
			case Clear:
				stack.clear();
				break;
		}
	}

	pendingList.clear();
}

StateStack::PendingChange::PendingChange(Action action, States::ID stateID): action(action), stateID(stateID) {}
