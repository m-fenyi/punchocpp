#ifndef GUI_SETTINGS_SETTINGSCOMPONENT_HPP
#define GUI_SETTINGS_SETTINGSCOMPONENT_HPP

#include "Component.hpp"
#include "State.hpp"

#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/ResourceHolder.hpp>

#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Time.hpp>

namespace GUI {
	namespace Settings {
		
		class SettingsComponent: public GUI::Component {
		protected:
			SettingsComponent(State::Context& context);

		public:
			bool isSelectable() const override;
			void select() override;
			void deselect() override;

			void press() override;
			void unpress() override;

			void activate() override;
			void deactivate() override;

			void update() override;

			sf::FloatRect getLocalBounds() const override;

			void updatePosition() override;

			void setTitle(const std::string& tag);

		protected:
			void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
			void changeTexture(Textures::Sprites::ID type);

		protected:
			sf::Sprite	 sprite;
			sf::Text	 title;
			std::string  buttonTag;

		};
	}
}

#endif // GUI_SETTINGS_SETTINGSCOMPONENT_HPP