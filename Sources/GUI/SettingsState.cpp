#include "SettingsState.hpp"

#include "VerticalContainer.hpp"
#include "Button.hpp"

#include <Utilities/Utilities.hpp>
#include <DAL/ResourceHolder.hpp>
#include <DAL/TranslationTag.hpp>

#include <SFML/Graphics/RenderWindow.hpp>

LOGCAT("SettingsState");

SettingsState::SettingsState(StateStack& stack, Context& context):
	GUIState(stack, context, new GUI::VerticalContainer(context)) {

	GUIContainer->setPaddingLeft(270);
	GUIContainer->setPaddingTop(250);
	GUIContainer->setPaddingBottom(645);

	auto gameButton = std::make_shared<GUI::Button>(context);
	auto audioButton = std::make_shared<GUI::Button>(context);
	auto graphicsButton = std::make_shared<GUI::Button>(context);
	auto controlsButton = std::make_shared<GUI::Button>(context);

	audioButton->setMarginTop(5);
	graphicsButton->setMarginTop(5);
	controlsButton->setMarginTop(5);

	gameButton->setText(TranslationTag::Button::game);
	audioButton->setText(TranslationTag::Button::audio);
	graphicsButton->setText(TranslationTag::Button::graphics);
	controlsButton->setText(TranslationTag::Button::controls);

	gameButton->setCallback([this]() { requestStackPush(States::GameSettings); });
	audioButton->setCallback([this]() { requestStackPush(States::AudioSettings); });
	graphicsButton->setCallback([this]() { requestStackPush(States::GraphicsSettings); });
	controlsButton->setCallback([this]() { requestStackPush(States::ControlsSettings); });

	GUIContainer->pack(gameButton);
	GUIContainer->pack(audioButton);
	GUIContainer->pack(graphicsButton);
	GUIContainer->pack(controlsButton);

	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setText(TranslationTag::Button::back);
	backButton->setCallback([this]() { requestStackPop(); });

	GUIContainer->pack(backButton);
}
