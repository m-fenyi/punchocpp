#include "Container.hpp"
#include "Button.hpp"
#include <Audio/AudioPlayer.hpp>

#include <DAL/ConfigurationRepository.hpp>

#include <Utilities/Utilities.hpp>

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include <Input/MouseInput.hpp>

LOGCAT("GUI::Container");

namespace GUI {

	Container::Container(State::Context& context):
		children(),
		selectedChild(0), 
		selectableCount(0),
		alignmentFlag(Alignment::None),
		inputHandler(context.config.getSystemConfiguration().getInputHandler()),
		sounds(context.audio.getSoundPlayer()),
		pitch(1.f),
		containerPadding(0, 0, static_cast<float>(context.window.getWindow().getSize().x), static_cast<float>(context.window.getWindow().getSize().y)) {

		inputHandler.setListener<void>(UIActionID::Up      , [this] { selectPrevious(); });
		inputHandler.setListener<void>(UIActionID::Down    , [this] { selectNext(); });
		inputHandler.setListener<void>(UIActionID::Select  , [this] { select(); });
		inputHandler.setListener<void>(UIActionID::Press   , [this] { press(); });
		inputHandler.setListener<void>(UIActionID::Unpress , [this] { unpress(); });
		inputHandler.setListener<void>(UIActionID::Accept  , [this] { activate(); });
		inputHandler.setListener<void>(UIActionID::Reject  , [this] { deselect(); });
		inputHandler.setListener<MouseInput>(UIActionID::Mouse   , [this] (const MouseInput& input) { selectAt((sf::Vector2f)input.getPosition()); });
	}

	void Container::pack(Component::Ptr component) {
		component->setPosition(containerPadding.left, containerPadding.top);
		children.push_back(component);

		if (component->isSelectable()) {
			++selectableCount;
		}
	}

	bool Container::isSelectable() const { return false; }
	bool Container::isActivable() const { return false; }
	bool Container::isPressable() const { return false; }

	void Container::update(sf::Time dt){
		for (const Component::Ptr& child : children) child->update(dt);
	}

	void Container::update() {
		for (const Component::Ptr& child : children) child->update();
	}
	
	bool Container::handleInput(const Input& input) {
		bool consumed = inputHandler.handleInput(input);
		if (hasSelection() 
			&& children[selectedChild]->isSelected() 
			&& !consumed) {
			consumed = children[selectedChild]->handleInput(input);
		}
		return consumed;
	}

	sf::FloatRect Container::getLocalBounds() const {
		return containerPadding + componentMargin;
	}

	void Container::setPadding(float top, float left, float bottom, float right) {
		containerPadding.top    = top;
		containerPadding.left   = left;
		containerPadding.height = bottom - top;
		containerPadding.width  = right - left;
	}

	void Container::setPaddingTop(float top)       { containerPadding.top    = top;  setPaddingBottom(containerPadding.height);}
	void Container::setPaddingLeft(float left)     { containerPadding.left   = left; setPaddingRight (containerPadding.width );}
	void Container::setPaddingBottom(float bottom) { containerPadding.height = bottom - containerPadding.top; }
	void Container::setPaddingRight(float right)   { containerPadding.width  = right  - containerPadding.left; }

	void Container::setAlignment(Alignment align) {
		alignmentFlag.set(align);
	}

	Alignment Container::getHorizontalFlag() const {
		return alignmentFlag.mask(Horizontal).get();
	}

	Alignment Container::getVerticalFlag() const {
		return alignmentFlag.mask(Vertical).get();
	}

	void Container::draw(sf::RenderTarget& target, sf::RenderStates states) const {
		states.transform *= getTransform();

		for (const Component::Ptr& child : children) target.draw(*child, states);

#ifdef _DEBUG

		sf::FloatRect rect = getLocalBounds();
		sf::RectangleShape shape;
		shape.setPosition(sf::Vector2f(rect.left, rect.top));
		shape.setSize(sf::Vector2f(rect.width - 1, rect.height - 1));
		shape.setFillColor(sf::Color::Transparent);
		shape.setOutlineColor(sf::Color::Green);
		shape.setOutlineThickness(1.f);

		target.draw(shape);
#endif // _DEBUG
	}

	bool Container::hasSelection() const {
		return children.size() > 0;
	}

	void Container::selectAt(sf::Vector2f position) {
		if (!getGlobalBounds().contains(position)) return;

		int selectable = 0;
		for (uint i = 0; i < children.size(); i++) {
			if (children[i]->isSelectable()) selectable++;
			else continue;
			if (children[i]->getGlobalBounds().contains(position)) {
				if (!children[i]->isSelected()) {
					pitch = 0.9f + 0.1f * selectable;
					select(i);
				}
				break;
			}
		}
	}

	void Container::select(std::size_t index) {
		if (children[index]->isSelectable()) {
			deselect();

			selectedChild = index;
			select();

			sounds.setPitch(pitch);
			sounds.play(SoundEffect::ButtonSelected);
		}
	}

	void Container::select() {
		if (hasSelection() && children[selectedChild]->isSelectable()) {
			children[selectedChild]->select();
		}
	}

	void Container::selectNext() {
		if (!hasSelection()) return;

 		size_t next = selectedChild;
		do {
			next = (next+1) % children.size();
			if (next == 0) pitch = 1.f;
		} while (!children[next]->isSelectable());

		pitch += 0.1f;
		select(next);
	}

	void Container::selectPrevious() {
		if (!hasSelection()) return;

		size_t prev = selectedChild;
		do {
			prev--;
			if (prev >= children.size()) {
				pitch = 0.9f + 0.1f * selectableCount;
				prev = children.size() - 1;
			}
		} while (!children[prev]->isSelectable());

		pitch -= 0.1f;
		select(prev);
	}

	void Container::deselect() {
		if (hasSelection()) {
			children[selectedChild]->unpress();
			children[selectedChild]->deselect();
		}
	}

	void Container::activate() {
		if (hasSelection() && children[selectedChild]->isActivable()) {
			sounds.play(SoundEffect::ButtonPressed);
			children[selectedChild]->activate();
		}
	}
	void Container::press() {
		if (hasSelection() && children[selectedChild]->isPressable()) {
			children[selectedChild]->press();
		}
	}
	void Container::unpress() {
		if (hasSelection() && children[selectedChild]->isPressable()) {
			children[selectedChild]->unpress();
		}
	}
}
