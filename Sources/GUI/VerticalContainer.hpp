#ifndef GUI_VERTICALCONTAINER_HPP
#define GUI_VERTICALCONTAINER_HPP

#include "Container.hpp"
#include "State.hpp"

namespace GUI {
	class VerticalContainer:public  Container {
	public:
		using Ptr = std::shared_ptr<VerticalContainer>;

	public:
		VerticalContainer(State::Context& context);

		void pack(Component::Ptr component) override;

	private:
		float getHorizontalPosition(size_t index);
		float getVerticalPosition();

		void repositionAll();

	private:
		float totalHeight;
	};
}

#endif // GUI_VERTICALCONTAINER_HPP