#ifndef GUI_SETTINGS_SLIDER_HPP
#define GUI_SETTINGS_SLIDER_HPP

#include "SettingsComponent.hpp"
#include "State.hpp"

#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/ResourceHolder.hpp>

#include <Input/InputHandler.hpp>

#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

namespace GUI {
	namespace Settings {
		class Slider: public SettingsComponent {
		public:
			using Ptr = std::shared_ptr<Slider>;
		public:
			Slider(State::Context& context, float steps = 100.f);

			void update(sf::Time dt) override;
			bool handleInput(const Input& input) override;

			bool isActivable() const override;
			bool isPressable() const override;

			sf::FloatRect getLocalBounds() const override;

			void updatePosition() override;

			void setSliderPosition(float position);
			float getSliderPosition() const;

		private:
			void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
			
		private:
			InputHandler<UIActionID> inputHandler;
			sf::RectangleShape sliderBackground;
			sf::RectangleShape sliderForeground;
			sf::Text           sliderValue;

			float sliderPosition;
			float sliderSteps;
		};
	}
}

#endif // GUI_SETTINGS_SLIDER_HPP