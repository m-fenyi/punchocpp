#include "Button.hpp"

#include <Audio/SoundPlayer.hpp>
#include <Audio/AudioPlayer.hpp>
#include <DAL/ResourceHolder.hpp>
#include <Utilities/Utilities.hpp>

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

LOGCAT("GUI::Button");

namespace GUI {

	Button::Button(State::Context& context):
		callback(),
		sprite(context.textures.get(Textures::Sprites::UIButtonNormal)),
		text("", context.fonts.get(Fonts::Main), 16),
		isToggle(false){

		sf::FloatRect bounds = sprite.getLocalBounds();
		text.setPosition(bounds.width / 2.f, bounds.height / 2.f);
	}

	void Button::setCallback(Callback callback) {
		this->callback = std::move(callback);
	}

	void Button::setText(const std::string& tag) {
		this->tag = tag;
		this->text.setString(GUI::Translation().getTranslation(tag));
		Node::centerOrigin(this->text);
	}

	void Button::setToggle(bool flag) {
		isToggle = flag;
	}

	bool Button::isSelectable() const {
		return true;
	}

	void Button::select() {
		Component::select();

		changeTexture(Textures::Sprites::UIButtonSelected);
	}

	void Button::deselect() {
		Component::deselect();

		changeTexture(Textures::Sprites::UIButtonNormal);
	}

	bool Button::isPressable() const { return true; }

	void Button::press() {
		Component::press();
		changeTexture(Textures::Sprites::UIButtonPressed);
	}

	void Button::unpress() {
		Component::unpress();
		if (isSelected()) changeTexture(Textures::Sprites::UIButtonSelected);
		else changeTexture(Textures::Sprites::UIButtonNormal);
	}

	bool Button::isActivable() const {
		return true;
	}

	void Button::activate() {
		Component::activate();

		if (callback) callback();

		if (isToggle) changeTexture(Textures::Sprites::UIButtonPressed);
		else deactivate();
	}

	void Button::deactivate() {
		Component::deactivate();

		if (isToggle) {
			if (isSelected()) changeTexture(Textures::Sprites::UIButtonSelected);
			else changeTexture(Textures::Sprites::UIButtonNormal);
		}
	}

	void Button::update() {
		setText(tag);
	}

	void Button::update(sf::Time){}

	bool Button::handleInput(const Input&) { return false; }

	void Button::updatePosition() {
		sprite.setPosition(sprite.getPosition().x + componentMargin.left, sprite.getPosition().y + componentMargin.top);
		text.setPosition  (text.getPosition().x   + componentMargin.left, text.getPosition().y   + componentMargin.top);
	}

	sf::FloatRect Button::getLocalBounds() const {
		return sprite.getLocalBounds() + componentMargin;
	}

	void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const {
		states.transform *= getTransform();
		target.draw(sprite, states);
		target.draw(text, states);
	}

	void Button::changeTexture(Textures::Sprites::ID type) {
		sprite.setTextureRect(Context::get().textures.get(type).getTextureRect());
	}

}
