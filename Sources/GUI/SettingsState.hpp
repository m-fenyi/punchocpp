#ifndef SETTINGSSTATE_HPP
#define SETTINGSSTATE_HPP

#include "GUIState.hpp"


class SettingsState: public GUIState {
public:
	SettingsState(StateStack& stack, Context& context);
};

#endif // SETTINGSSTATE_HPP
