#include "State.hpp"
#include "StateStack.hpp"
#include <Utilities/Log.hpp>
#include <Audio/AudioPlayer.hpp>
#include <DAL/ConfigurationRepository.hpp>

#include <Window/WindowWrapper.hpp>

LOGCAT("State");

State::Context::Context(WindowWrapper& window, TextureWrapper& textures, FontHolder& fonts, AudioPlayer& audio, ConfigurationRepository& config, GameDataRepository& gameRepo):
	window(window),
	textures(textures),
	fonts(fonts),
	audio(audio),
	config(config),
	gameRepo(gameRepo) {}

State::State(StateStack& stack, Context& context):
	stack(&stack),
	context(context),
	handler(context.config.getSystemConfiguration().getInputHandler()) {
	handler.setListener<void>(UIActionID::Quit, [this] { requestStackClear(); });
	handler.setListener<void>(UIActionID::Return, [this] { requestStackPop(); });
}

State::~State() {}

bool State::handleInput(const Input & input) {
	return handler.handleInput(input);
}

void State::requestStackPush(States::ID stateID) { stack->pushState(stateID); }

void State::requestStackPop() { stack->popState(); }

void State::requestStackClear() { stack->clearStates(); }

void State::requestStackUpdate() { stack->update(); }
