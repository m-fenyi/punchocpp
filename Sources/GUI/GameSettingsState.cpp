#include "GameSettingsState.hpp"

#include "VerticalContainer.hpp"

#include <Utilities/Utilities.hpp>
#include <DAL/ResourceHolder.hpp>
#include <DAL/TranslationTag.hpp>

#include <SFML/Graphics/RenderWindow.hpp>

LOGCAT("GameSettingsState");

GameSettingsState::GameSettingsState(StateStack& stack, Context& context):
	GUIState(stack, context, new GUI::VerticalContainer(context)) {
	GUIContainer->setPaddingLeft(490);
	GUIContainer->setPaddingTop(300);

	localeSwitch = std::make_shared<GUI::Settings::Switch<std::string>>(context);
	localeSwitch->setChoices(GUI::Translation().getLocales());
	localeSwitch->setChoice(context.config.getConfiguration().getGeneralConfiguration().getLocale());
	localeSwitch->setTitle(TranslationTag::Switch::locale);

	GUIContainer->pack(localeSwitch);


	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setText(TranslationTag::Button::back);
	backButton->setCallback([this]() { requestStackPop(); });

	GUIContainer->pack(backButton);
}

void GameSettingsState::onPop(Context & context) {
	auto& config = context.config.getConfiguration().getGeneralConfiguration();
	config.setLocale(localeSwitch->getChoice());
	GUI::Translation().setLocale(localeSwitch->getChoice());
	requestStackUpdate();
}
