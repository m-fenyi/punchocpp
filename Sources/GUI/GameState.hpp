#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

#include "State.hpp"
#include <World/World.hpp>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class GameState: public State {
public:
	GameState(StateStack& stack, Context& context);

	void draw(sf::RenderWindow& window) override;
	bool update(sf::Time dt) override;
	
	bool handleInput(const Input& input) override;


private:
	World	world;
};

#endif // GAMESTATE_HPP
