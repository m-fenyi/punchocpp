#ifndef UIACTION_HPP
#define UIACTION_HPP

enum UIActionID {
	Quit,

	Up,
	Down,
	Left,
	Right,

	Select,
	Press,
	Unpress,
	Accept,
	Reject,
	Return,

	Mouse
};


#endif // !UIACTION_HPP