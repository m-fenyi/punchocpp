#include "GUIState.hpp"
#include "Button.hpp"
#include <DAL/TranslationTag.hpp>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

GUIState::GUIState(StateStack & stack, Context context, GUI::Container * container) : 
	State(stack, context), 
	GUIContainer(container) { }


void GUIState::draw(sf::RenderWindow & window) {
	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 100));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(backgroundShape);
	window.draw(*GUIContainer.get());
}

bool GUIState::update(sf::Time dt) {
	GUIContainer->update(dt);
	return true;
}

void GUIState::update() {
	GUIContainer->update();
}

bool GUIState::handleInput(const Input & input) {
	if (State::handleInput(input)) return false;
	GUIContainer->handleInput(input);
	return false;
}
