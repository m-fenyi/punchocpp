#ifndef LOADINGSTATE_HPP
#define LOADINGSTATE_HPP

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>

#include "State.hpp"
#include "VerticalContainer.hpp"
#include "Label.hpp"
#include "ProgressBar.hpp"

#include <Thread/ParallelTask.hpp>

class LoadingState: public State {
public:
	LoadingState(StateStack& stack, Context& context);

	void draw(sf::RenderWindow& window) override;
	bool update(sf::Time dt) override;
	void update() override;
	
	bool handleInput(const Input& input) override;

	void setCompletion(float percent);

private:
	GUI::VerticalContainer GUIContainer;
	GUI::ProgressBar* progressBar;

	ParallelTask loadingTask;
};

#endif // LOADINGSTATE_HPP
