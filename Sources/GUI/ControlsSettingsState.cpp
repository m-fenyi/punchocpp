#include "ControlsSettingsState.hpp"
#include "VerticalContainer.hpp"

#include <DAL/TranslationTag.hpp>

#include <SFML/Graphics/RenderWindow.hpp>

LOGCAT("ConstrolsSettingsState");

ControlsSettingsState::ControlsSettingsState(StateStack& stack, Context& context):
	GUIState(stack, context, new GUI::VerticalContainer(context)) {
	GUIContainer->setPaddingLeft(490);
	GUIContainer->setPaddingTop(150);

	controlsSwitch = std::make_shared<GUI::Settings::Switch<std::string>>(context);
	controlsSwitch->setTitle(TranslationTag::Switch::controlsType);
	controlsSwitch->setChoices(TranslationTag::Switch::getChoices(TranslationTag::Switch::controlsType, 2));

	controllerTypeSwitch = std::make_shared<GUI::Settings::Switch<std::string>>(context);
	controllerTypeSwitch->setTitle(TranslationTag::Switch::controllerType);
	controllerTypeSwitch->setChoices(TranslationTag::Switch::getChoices(TranslationTag::Switch::controllerType, 2));
	controllerTypeSwitch->setCurrent(!context.textures.getXboxController());

	GUIContainer->pack(controlsSwitch);
	GUIContainer->pack(controllerTypeSwitch);

	for (std::size_t action = 0; action < GameActionID::GameActionCount; ++action) {
		addInputChooser(static_cast<GameActionID>(action), context);
	}

	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setText(TranslationTag::Button::back);
	backButton->setCallback([this]() { requestStackPop(); });

	GUIContainer->pack(backButton);
}

void ControlsSettingsState::onPop(Context & context) {}

void ControlsSettingsState::updateInputChoosers() {
	for (auto inputChooser : bindingInputChooser)
		inputChooser->updateInputText(true);
}

void ControlsSettingsState::addInputChooser(GameActionID action, Context context) {
	bindingInputChooser[action] = std::make_shared<GUI::Settings::InputChooser>(context, action);
	GUIContainer->pack(bindingInputChooser[action]);
}
