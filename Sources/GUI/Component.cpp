#include "Component.hpp"

namespace {
	TranslationTable* UITranslation;
}

namespace GUI {

	Component::Component() {}

	sf::FloatRect Component::getGlobalBounds() const {
		sf::FloatRect bounds = getLocalBounds();
		bounds.left += getPosition().x;
		bounds.top += getPosition().y;
		return bounds;
	}


	SelectableComponent::SelectableComponent(): selected(false) {}
	bool SelectableComponent::isSelected() const { return selected; }
	void SelectableComponent::select() { selected = true; }
	void SelectableComponent::deselect() { selected = false; }


	ActivableComponent::ActivableComponent(): active(false) {}
	bool ActivableComponent::isActive() const { return active; }
	void ActivableComponent::activate() { active = true; }
	void ActivableComponent::deactivate() { active = false; }


	PressableComponent::PressableComponent(): pressed(false) {}
	bool PressableComponent::isPressed() const { return pressed; }
	void PressableComponent::press() { pressed = true; }
	void PressableComponent::unpress() { pressed = false; }


	MarginComponent::MarginComponent() {}
	void MarginComponent::setMargin(float top, float left, float bottom, float right) {
		componentMargin.top    = top;
		componentMargin.left   = left;
		componentMargin.height = bottom;
		componentMargin.width  = right;
		updatePosition();
	}
	void MarginComponent::setMarginTop(float top)       { componentMargin.top    = top;    updatePosition();}
	void MarginComponent::setMarginLeft(float left)     { componentMargin.left   = left;   updatePosition();}
	void MarginComponent::setMarginBottom(float bottom) { componentMargin.height = bottom; updatePosition();}
	void MarginComponent::setMarginRight(float right)   { componentMargin.width  = right;  updatePosition();}
	

	void setTranslationTable(TranslationTable& translationTable) {
		::UITranslation = &translationTable;
	}
	TranslationTable & Translation() {
		return *UITranslation;
	}
}