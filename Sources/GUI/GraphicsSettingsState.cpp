#include "GraphicsSettingsState.hpp"
#include "Button.hpp"
#include "VerticalContainer.hpp"

#include <DAL/ConfigurationRepository.hpp>

#include <DAL/ResourceHolder.hpp>
#include <DAL/TranslationTag.hpp>
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RenderWindow.hpp>

LOGCAT("GraphicsSettingsState");

GraphicsSettingsState::GraphicsSettingsState(StateStack& stack, Context& context):
	GUIState(stack, context, new GUI::VerticalContainer(context)) {

	GUIContainer->setPaddingLeft(490);
	GUIContainer->setPaddingTop(300);

	vSyncSwitch = std::make_shared<GUI::Settings::Switch<std::string>>(context);
	vSyncSwitch->setTitle(TranslationTag::Switch::vsync);
	vSyncSwitch->setChoices({ TranslationTag::General::off, TranslationTag::General::on });
	vSyncSwitch->setCurrent(context.window.isVsyncEnabled());

	windowStyleSwitch = std::make_shared<GUI::Settings::Switch<std::string>>(context);
	windowStyleSwitch->setTitle(TranslationTag::Switch::windowStyle);
	windowStyleSwitch->setChoices(TranslationTag::Switch::getChoices(TranslationTag::Switch::windowStyle, 3));
	windowStyleSwitch->setCurrent(context.window.getWindowMode());

	antialiasingSlider = std::make_shared<GUI::Settings::Slider>(context, 8.f);
	antialiasingSlider->setTitle(TranslationTag::Slider::antialiasing);
	antialiasingSlider->setSliderPosition(static_cast<float>(context.window.getAntiAliasingLevel()));

	resolutionSwitch = std::make_shared<GUI::Settings::Switch<Resolution>>(context);
	resolutionSwitch->setTitle(TranslationTag::Switch::resolution);

	resolutionSwitch->setChoices(context.config.getSystemConfiguration().getResolutions());
	resolutionSwitch->setChoice(context.window.getResolution());
	
	GUIContainer->pack(windowStyleSwitch);
	GUIContainer->pack(resolutionSwitch);
	GUIContainer->pack(antialiasingSlider);
	GUIContainer->pack(vSyncSwitch);

	auto closeAction = [this] { requestStackPop(); };

	handler.setListener<void>(UIActionID::Return, closeAction);

	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setText(TranslationTag::Button::back);
	backButton->setCallback(closeAction);

	GUIContainer->pack(backButton);
}

void GraphicsSettingsState::onPop(Context & context) {
	auto& config = context.config.getConfiguration().getGraphicsConfiguration();
	config.setVSync(vSyncSwitch->getCurrent());
	config.setAALevel((int)antialiasingSlider->getSliderPosition());
	config.setResolution(resolutionSwitch->getChoice());
	config.setWindowMode((WindowMode)windowStyleSwitch->getCurrent());
	context.window.reloadConfiguration();
}
