#include "LoadingState.hpp"

#include <DAL/TranslationTag.hpp>
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

LOGCAT("LoadingState");

LoadingState::LoadingState(StateStack& stack, Context& context):
	State(stack, context),
	GUIContainer(context),
	loadingTask(context){
	GUIContainer.setAlignment(GUI::Alignment::Center);

	auto label = std::make_shared<GUI::Label>(TranslationTag::Label::loading, context.fonts);
	auto progress = std::make_shared<GUI::ProgressBar>(450.f, 15.f);
	
	progress->setMarginTop(50);

	GUIContainer.pack(label);
	GUIContainer.pack(progress);

	progressBar = progress.get();
	progressBar->setCompletion(0.f);

	loadingTask.execute();
}

void LoadingState::draw(sf::RenderWindow& window) {
	window.setView(window.getDefaultView());

	window.draw(GUIContainer);
}

bool LoadingState::update(sf::Time dt) {
	if (loadingTask.isFinished()) {
		requestStackPop();
		requestStackPush(States::Game);
		requestStackPush(States::Menu);
	} else {
		setCompletion(loadingTask.getCompletion() / 100.f);
	}
	GUIContainer.update(dt);
	return false;
}

void LoadingState::update() {
	GUIContainer.update();
}

bool LoadingState::handleInput(const Input& input) {
	return false;
}

void LoadingState::setCompletion(float percent) {
	progressBar->setCompletion(percent);
}