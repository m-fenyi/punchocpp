#include "ProgressBar.hpp"
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/System/Time.hpp>

LOGCAT("GUI::ProgressBar");

namespace GUI {
	ProgressBar::ProgressBar(float width, float height):
		completion(0),
		updateSpeed(1) {
		progressBarBackground.setFillColor(sf::Color::White);
		progressBarBackground.setSize(sf::Vector2f(width, height));

		progressBar.setFillColor(sf::Color(100, 100, 100));
		progressBar.setSize(sf::Vector2f(200, height));
	}

	bool ProgressBar::isSelectable() const {
		return false;
	}

	void ProgressBar::update() {}

	void ProgressBar::update(sf::Time dt){}
	
	bool ProgressBar::handleInput(const Input&) { return false; }

	void ProgressBar::setCompletion(float value) {
		if (value > 1.f) value = 1.f;
		if (value < 0.f) value = 0.f;
		completion = value;

		progressBar.setSize(sf::Vector2f(progressBarBackground.getSize().x * completion, progressBar.getSize().y));
	}

	float ProgressBar::getCompletion() const {
		return completion;
	}

	bool ProgressBar::isActivable() const { return false; }
	bool ProgressBar::isPressable() const { return false; }

	void ProgressBar::setColor(sf::Color color) { progressBar.setFillColor(color); }

	void ProgressBar::setBackgroundColor(sf::Color color) { progressBarBackground.setFillColor(color); }

	sf::FloatRect ProgressBar::getLocalBounds() const {
		return progressBarBackground.getLocalBounds() + componentMargin;
	}

	void ProgressBar::updatePosition() {
		progressBarBackground.setPosition(progressBarBackground.getPosition() + componentMargin);
		progressBar.setPosition(progressBarBackground.getPosition());
	}

	void ProgressBar::draw(sf::RenderTarget& target, sf::RenderStates states) const {
		states.transform *= getTransform();
		target.draw(progressBarBackground, states);
		target.draw(progressBar, states);
	}
	
}