#include "MenuState.hpp"
#include "Button.hpp"
#include "Label.hpp"
#include "VerticalContainer.hpp"

#include <Audio/MusicPlayer.hpp>
#include <Audio/AudioPlayer.hpp>
#include <DAL/ResourceHolder.hpp>
#include <DAL/TranslationTag.hpp>
#include <Utilities/Utilities.hpp>

#include <Input/InputMatcherBuilder.hpp>
#include <Input/InputListenerFactory.hpp>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

LOGCAT("MenuState");

MenuState::MenuState(StateStack& stack, Context& context):
	GUIState(stack, context, new GUI::VerticalContainer(context)) {
	GUIContainer->setPadding(150, 50, 600, 300);

	auto gameLabel      = std::make_shared<GUI::Label>(TranslationTag::gameTitle, context.fonts, 72);
	auto playButton     = std::make_shared<GUI::Button>(context);
	auto settingsButton = std::make_shared<GUI::Button>(context);
	auto exitButton     = std::make_shared<GUI::Button>(context);

	playButton->setMarginTop(5);
	settingsButton->setMarginTop(5);
	exitButton->setMarginTop(5);

	playButton->setText(TranslationTag::Button::play);
	settingsButton->setText(TranslationTag::Button::settings);
	exitButton->setText(TranslationTag::Button::exit);

	playButton->setCallback([this]() {
		requestStackPop();
		requestStackPush(States::Game);
	});
	settingsButton->setCallback([this]() {
		requestStackPush(States::Settings);
	});
	exitButton->setCallback([this]() {
		requestStackClear();
	});

	gameLabel->setTextColor(sf::Color(96, 63, 37));

	GUIContainer->pack(gameLabel);
	GUIContainer->pack(playButton);
	GUIContainer->pack(settingsButton);
	GUIContainer->pack(exitButton);

	context.audio.getMusicPlayer().play(Music::MenuTheme);

	handler.setListener<void>(UIActionID::Return, [this] { requestStackClear(); });

}

