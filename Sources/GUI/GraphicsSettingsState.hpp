#ifndef GRAPHICSSETTINGSSTATE_HPP
#define GRAPHICSSETTINGSSTATE_HPP

#include "GUIState.hpp"
#include "Switch.hpp"
#include "Slider.hpp"



class GraphicsSettingsState: public GUIState {
public:
	GraphicsSettingsState(StateStack& stack, Context& context);

	void onPop(Context& context) override;
	
private:
	GUI::Settings::Switch<std::string>::Ptr vSyncSwitch;
	GUI::Settings::Switch<std::string>::Ptr windowStyleSwitch;
	GUI::Settings::Switch<Resolution>::Ptr resolutionSwitch;

	GUI::Settings::Slider::Ptr antialiasingSlider;
};

#endif // GRAPHICSSETTINGSSTATE_HPP