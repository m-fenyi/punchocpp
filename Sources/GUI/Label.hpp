#ifndef GUI_LABEL_HPP
#define GUI_LABEL_HPP

#include "Component.hpp"
#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/ResourceHolder.hpp>

#include <SFML/Graphics/Text.hpp>


namespace GUI {

	class Label: public Component {
	public:
		using Ptr = std::shared_ptr<Label>;


	public:
		Label(const std::string& tag, const FontHolder& fonts, int fontSize = 16);

		bool isSelectable() const override;
		void		 setText(const std::string& tag);

		void         setTextColor(sf::Color color);

		void update() override;
		void update(sf::Time) override;
		bool handleInput(const Input& input) override;
		
		virtual bool isActivable() const override;
		virtual bool isPressable() const override;

		sf::FloatRect getLocalBounds() const override;

		void updatePosition() override;

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;


	private:
		sf::Text text;
		std::string tag;

	};

}

#endif // GUI_LABEL_HPP