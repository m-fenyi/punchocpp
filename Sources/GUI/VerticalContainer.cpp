#include "VerticalContainer.hpp"
#include <Utilities/Log.hpp>

LOGCAT("GUI::VerticalContainer");

GUI::VerticalContainer::VerticalContainer(State::Context & context): 
	Container(context),
	totalHeight(0){}

void GUI::VerticalContainer::pack(Component::Ptr component) {
	Container::pack(component);
	
	totalHeight += component->getLocalBounds().top  + component->getLocalBounds().height;
	if (totalHeight > containerPadding.height)
		LOGW("Padding overflow - Padding height: " << containerPadding.height << ", Components height: " << totalHeight);

	component->setPosition(getHorizontalPosition(children.size() - 1), 
						   getVerticalPosition());
}

float GUI::VerticalContainer::getHorizontalPosition(size_t index) {
	Component::Ptr component = children[index];
	
	switch(getHorizontalFlag()){
	default:
	case GUI::Alignment::None:
	case GUI::Alignment::Left:
		return getLocalBounds().left;
	case GUI::Alignment::Right:
		return containerPadding.left + containerPadding.width - component->getLocalBounds().width - component->getLocalBounds().left;
	case GUI::Alignment::CenterHorizontal:
		return containerPadding.left + (containerPadding.width - component->getLocalBounds().width - component->getLocalBounds().left) / 2;
	}
}

float GUI::VerticalContainer::getVerticalPosition() {
	Component::Ptr component = children.back();

	switch (getVerticalFlag()) {
	default:
	case GUI::Alignment::None:
	case GUI::Alignment::Top:
		if(children.size() == 1) return getLocalBounds().top;
		else return children[children.size() - 2]->getPosition().y + children[children.size() - 2]->getLocalBounds().top + children[children.size() - 2]->getLocalBounds().height;
	case GUI::Alignment::Bottom:
		repositionAll();
		return component->getPosition().y;
	case GUI::Alignment::CenterVertical:
		repositionAll();
		return component->getPosition().y;
	}
}

void GUI::VerticalContainer::repositionAll() {
	float positionTop;
	if (getVerticalFlag() == GUI::Alignment::CenterVertical) {
		positionTop = containerPadding.top + (containerPadding.height - totalHeight) / 2;
	} else {
		positionTop = containerPadding.top + containerPadding.height - totalHeight;
	}
	children.front()->setPosition(getHorizontalPosition(0), positionTop);
	for (unsigned int i = 1; i < children.size(); ++i) {
		children[i]->setPosition(getHorizontalPosition(i),
								 children[i - 1]->getPosition().y + children[i - 1]->getLocalBounds().top + children[i - 1]->getLocalBounds().height);
	}
}
