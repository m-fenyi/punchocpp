#ifndef GUI_SETTINGS_SWITCH_HPP
#define GUI_SETTINGS_SWITCH_HPP

#include "SettingsComponent.hpp"
#include "State.hpp"

#include <DAL/ConfigurationRepository.hpp>
#include <DAL/ResourceIdentifiers.hpp>
#include <DAL/ResourceHolder.hpp>

#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Time.hpp>

// LOGCAT("GUI::Settings::Switch")

namespace GUI {
	namespace Settings {
		template<class T>
		class Switch: public SettingsComponent {
		public:
			using Ptr = std::shared_ptr<Switch<T>>;
		public:
			Switch(State::Context& context):
				SettingsComponent(context),
				choiceText("", context.fonts.get(Fonts::Main), 16),
				choices(),
				current(0),
				inputHandler(context.config.getSystemConfiguration().getInputHandler()) {

				choiceText.setPosition(sprite.getPosition().x + 250, title.getPosition().y);

				inputHandler.setListener<void>(UIActionID::Left , [this] { setCurrent(getCurrent() - 1u); });
				inputHandler.setListener<void>(UIActionID::Right, [this] { setCurrent(getCurrent() + 1u); });
			}

			void update() {
				SettingsComponent::update();
				updateTextChoice();
			}
			void update(sf::Time) {}
			
			bool handleInput(const Input& input) override {
				return inputHandler.handleInput(input);
			}

			bool isActivable() const override { return false; }
			bool isPressable() const override { return false; }

			sf::FloatRect getLocalBounds() const override {
				sf::FloatRect spriteRect = SettingsComponent::getLocalBounds();
				spriteRect.width += 50 + choiceText.getLocalBounds().width;
				return spriteRect;
			}

			void updatePosition() override {
				SettingsComponent::updatePosition();
				choiceText.setPosition(choiceText.getPosition() + componentMargin);
			}
			
			void setChoices(std::vector<T> labels) {
				if (current >= labels.size()) current = labels.size() - 1;
				choices = labels;
				updateTextChoice();
			}

			void setChoice(const T& choice) {
				int i = 0;
				for (auto c : choices) {
					if (c == choice) {
						setCurrent(i);
						break;
					}
					++i;
				}
			}
			T    getChoice() const {
				return choices[current];
			}

			size_t getCurrent() const { 
				return current;
			}
			void setCurrent(size_t newCurrent) {
				current = Range::between((size_t)0u, (size_t)(choices.size() - 1u)).bound(newCurrent);
				updateTextChoice();
			}

			
		private:
			void draw(sf::RenderTarget& target, sf::RenderStates states) const override {
				SettingsComponent::draw(target, states);
				states.transform *= getTransform();
				target.draw(choiceText, states);
			}

			void updateTextChoice() {
				choiceText.setString(toString(choices[current]));
			}

		private:
			InputHandler<UIActionID> inputHandler;
			sf::Text	 choiceText;
			std::vector<T> choices;

			size_t current;
		};

		template<>
		void Switch<std::string>::updateTextChoice() {
			choiceText.setString(GUI::Translation().getTranslation(choices[current]));
		}
	}
}

#endif // GUI_SETTINGS_SWITCH_HPP