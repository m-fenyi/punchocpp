#include "Label.hpp"
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

LOGCAT("GUI::Label");

namespace GUI {
	Label::Label(const std::string& tag, const FontHolder& fonts, int fontSize): tag(tag), text(GUI::Translation().getTranslation(tag), fonts.get(Fonts::Main), fontSize) {}

	bool Label::isSelectable() const {
		return false;
	}

	void Label::update() {
		setText(tag);
	}

	void Label::update(sf::Time){}

	bool Label::handleInput(const Input&) { return false; }

	bool Label::isActivable() const { return false; }
	bool Label::isPressable() const { return false; }

	sf::FloatRect Label::getLocalBounds() const { return text.getLocalBounds() + componentMargin; }

	void Label::updatePosition() {
		setPosition(getPosition() + componentMargin);
	}

	void Label::draw(sf::RenderTarget& target, sf::RenderStates states) const {
		states.transform *= getTransform();
		target.draw(text, states);
	}


	void Label::setText(const std::string& tag) {
		this->tag = tag;
		this->text.setString(GUI::Translation().getTranslation(tag));
	}

	void Label::setTextColor(sf::Color color) { text.setFillColor(color); }

}