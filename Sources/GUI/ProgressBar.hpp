#ifndef GUI_PROGRESSBAR_HPP
#define GUI_PROGRESSBAR_HPP

#include "Component.hpp"

#include <SFML/Graphics/RectangleShape.hpp>


namespace GUI {

	class ProgressBar: public Component {
	public:
		using Ptr = std::shared_ptr<ProgressBar>;


	public:
		ProgressBar(float width, float height);
		bool isSelectable() const override;
		
		void update() override;
		void update(sf::Time) override;
		bool handleInput(const Input& input) override;

		bool isActivable() const override;
		bool isPressable() const override;
		
		void         setCompletion(float value);
		float        getCompletion() const;

		void         setColor(sf::Color color);
		void         setBackgroundColor(sf::Color color);

		sf::FloatRect getLocalBounds() const override;

		void updatePosition() override;

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	private:
		sf::RectangleShape progressBarBackground;
		sf::RectangleShape progressBar;

		float completion;
		unsigned short int updateSpeed;

	};

}

#endif // GUI_PROGRESSBAR_HPP