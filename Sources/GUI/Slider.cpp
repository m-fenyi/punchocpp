#include "Slider.hpp"
#include <Utilities/Log.hpp>
#include <DAL/ConfigurationRepository.hpp>

LOGCAT("GUI::Settings::Slider");

namespace GUI {
	namespace Settings {
		Slider::Slider(State::Context& context, float steps):
			SettingsComponent(context),
			sliderBackground(),
			sliderForeground(),
			sliderValue("", context.fonts.get(Fonts::Main), 16),
			sliderPosition(0),
			sliderSteps(steps),
			inputHandler(context.config.getSystemConfiguration().getInputHandler()) {

			sf::FloatRect bounds = sprite.getLocalBounds();
			sliderBackground.setPosition(bounds.width + 50, bounds.height - 20.f);
			sliderBackground.setFillColor(sf::Color::White);
			sliderBackground.setSize(sf::Vector2f(200, 15));

			sliderForeground.setPosition(sliderBackground.getPosition());
			sliderForeground.setFillColor(sf::Color(100, 100, 100));
			sliderForeground.setSize(sf::Vector2f(0, 15));
			
			sliderValue.setPosition(sliderBackground.getPosition().x + sliderBackground.getSize().x / 2.f, bounds.top + 20.f);
			sliderValue.setString("0");
			Node::centerOrigin(sliderValue);
			
			inputHandler.setListener<void>(UIActionID::Left , [this] {
				--sliderPosition;
				setSliderPosition(sliderPosition); 
			});
			inputHandler.setListener<void>(UIActionID::Right, [this] {
				++sliderPosition;
				setSliderPosition(sliderPosition);
			});
		}


		void Slider::update(sf::Time dt) {}
		
		bool Slider::handleInput(const Input& input) { 
			return inputHandler.handleInput(input);
		}

		sf::FloatRect Slider::getLocalBounds() const {
			sf::FloatRect spriteRect = SettingsComponent::getLocalBounds();
			spriteRect.width += 50 + sliderBackground.getSize().x;
			return spriteRect;
		}

		void Slider::updatePosition() {
			SettingsComponent::updatePosition();
			sliderBackground.setPosition(sliderBackground.getPosition() + componentMargin);
			sliderForeground.setPosition(sliderBackground.getPosition());
			sliderValue.setPosition(sliderBackground.getPosition().x + sliderBackground.getSize().x / 2.f, sprite.getLocalBounds().top + 20.f);
			Node::centerOrigin(sliderValue);
		}

		void Slider::setSliderPosition(float position) {
			if (position > sliderSteps) position = sliderSteps;
			else if (position < 0) position = 0;
			sliderPosition = position;
			sliderForeground.setSize(sf::Vector2f(sliderBackground.getSize().x * sliderPosition / sliderSteps, 15));
			
			sliderValue.setString(toString(static_cast<int>(sliderPosition)));
			Node::centerOrigin(sliderValue);
		}

		float Slider::getSliderPosition() const {
			return sliderPosition;
		}

		void Slider::draw(sf::RenderTarget& target, sf::RenderStates states) const {
			SettingsComponent::draw(target, states);
			states.transform *= getTransform();
			target.draw(sliderBackground, states);
			target.draw(sliderForeground, states);
			target.draw(sliderValue, states);
		}
		bool Slider::isActivable() const { return false; }
		bool Slider::isPressable() const { return false; }
	}
}
