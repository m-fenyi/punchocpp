#ifndef GUI_HORIZONTALCONTAINER_HPP
#define GUI_HORIZONTALCONTAINER_HPP

#include "Container.hpp"
#include "State.hpp"

namespace GUI {
	class HorizontalContainer: public Container {
	public:
		using Ptr = std::shared_ptr<HorizontalContainer>;

	public:
		HorizontalContainer(State::Context& context);

		void pack(Component::Ptr component) override;

	private:
		float getHorizontalPosition();
		float getVerticalPosition();

		void repositionAll();

	private:
		float totalWidth;
	};
}

#endif // GUI_HORIZONTALCONTAINER_HPP