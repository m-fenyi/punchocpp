#ifndef GAMESETTINGSSTATE_HPP
#define GAMESETTINGSSTATE_HPP

#include "GUIState.hpp"
#include "VerticalContainer.hpp"
#include "Button.hpp"
#include "Switch.hpp"

class GameSettingsState: public GUIState {
public:
	GameSettingsState(StateStack& stack, Context& context);
		
	void onPop(Context& context) override;
private:
	GUI::Settings::Switch<std::string>::Ptr  localeSwitch;
};

#endif // GAMESETTINGSSTATE_HPP