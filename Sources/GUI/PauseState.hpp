#ifndef PAUSESTATE_HPP
#define PAUSESTATE_HPP

#include "GUIState.hpp"
#include "VerticalContainer.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class PauseState: public GUIState {
public:
	PauseState(StateStack& stack, Context& context);

	void onPop(Context& context) override;
};

#endif // PAUSESTATE_HPP
