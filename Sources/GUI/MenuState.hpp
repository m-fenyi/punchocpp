#ifndef MENUSTATE_HPP
#define MENUSTATE_HPP

#include "GUIState.hpp"



class MenuState: public GUIState {
public:
	MenuState(StateStack& stack, Context& context);
};

#endif // MENUSTATE_HPP
