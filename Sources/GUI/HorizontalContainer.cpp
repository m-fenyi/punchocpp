#include "HorizontalContainer.hpp"
#include <Utilities/Log.hpp>

LOGCAT("GUI::HorizontalContainer");

GUI::HorizontalContainer::HorizontalContainer(State::Context & context): Container(context), totalWidth(0.f){}

void GUI::HorizontalContainer::pack(Component::Ptr component) {
	Container::pack(component);
	
	totalWidth += component->getLocalBounds().left + component->getLocalBounds().width;
	if (totalWidth > containerPadding.width) 
		LOGW("Padding overflow - Padding width: " << containerPadding.width << ", Components width: " << totalWidth);

	component->setPosition(getHorizontalPosition(), getVerticalPosition());
}

float GUI::HorizontalContainer::getHorizontalPosition() {
	Component::Ptr component = children.back();

	switch (getHorizontalFlag()) {
	default:
	case GUI::Alignment::None:
	case GUI::Alignment::Left:
		return containerPadding.left;
	case GUI::Alignment::Right:
		return containerPadding.left + containerPadding.width - component->getLocalBounds().width - component->getLocalBounds().left;
	case GUI::Alignment::CenterHorizontal:
		return containerPadding.left;
	}
}

float GUI::HorizontalContainer::getVerticalPosition() {
	Component::Ptr component = children.back();

	switch (getVerticalFlag()) {
	default:
	case GUI::Alignment::None:
	case GUI::Alignment::Top:
		return containerPadding.left;
	case GUI::Alignment::Bottom:
		repositionAll();
		return containerPadding.left + containerPadding.width - component->getLocalBounds().width - component->getLocalBounds().left;
	case GUI::Alignment::CenterVertical:
		return containerPadding.left;
	}
}

void GUI::HorizontalContainer::repositionAll() {
	/* Default
	for (unsigned int i = 0; i < children.size(); ++i)
	children[i]->setPosition(containerPadding.left + containerPadding.width * i / children.size(), containerPadding.top);*/
}
