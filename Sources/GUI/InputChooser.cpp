#include "InputChooser.hpp"

#include <DAL/TranslationTag.hpp>
#include <Utilities/Utilities.hpp>

LOGCAT("GUI::Settings::InputChooser");

namespace GUI {
	namespace Settings {
	
		InputChooser::InputChooser(State::Context & context, GameActionID action):
			SettingsComponent(context),
			inputText("", context.fonts.get(Fonts::Main), 16),
			action(action),
			showingKeyboard(true) {

			setTitle(TranslationTag::Action::prefix + toString(action));

			inputText.setPosition(sprite.getPosition().x + 270, title.getPosition().y);
			inputSprite.setPosition(inputText.getPosition());
			updateInputText();
		}

		void InputChooser::update(sf::Time) {}

		bool InputChooser::handleInput(const Input& input) {
			if (!isActive()) return false;
			/*
			
			Context::get().player.getBindings().assignInput(action, event);*/
			updateInputText();
			deactivate();
			return true;
		}

		bool InputChooser::isActivable() const { return true; }
		bool InputChooser::isPressable() const { return true; }

		sf::FloatRect InputChooser::getLocalBounds() const {
			sf::FloatRect rect = SettingsComponent::getLocalBounds();
			rect.width += inputText.getGlobalBounds().width + 70;
			return rect;
		}

		void InputChooser::updatePosition() {
			SettingsComponent::updatePosition();
			inputText.setPosition(inputText.getPosition() + componentMargin);
			inputSprite.setPosition(inputText.getPosition());
		}

		void InputChooser::updateInputText(bool showKeyboard) {
			showingKeyboard = showKeyboard;
			updateInputText();
		}

		void InputChooser::switchInputGroupShown() {
			showingKeyboard = !showingKeyboard;
			updateInputText();
		}

		void InputChooser::draw(sf::RenderTarget & target, sf::RenderStates states) const {
			SettingsComponent::draw(target, states);
			states.transform *= getTransform();
			if (showingKeyboard) target.draw(inputText, states);
			else target.draw(inputSprite, states);
		}

		void InputChooser::updateInputText() {
			// TODO
			/*if (showingKeyboard) {
				std::string text;
				if (Context::get().player.getBindings().isKeyboardInput(action))
					 text = toString(Context::get().player.getBindings().getAssignedInput<sf::Keyboard::Key>(action));
				else text = toString(Context::get().player.getBindings().getAssignedInput<sf::Mouse::Button>(action));
				inputText.setString(text);
				Node::centerOrigin(inputText);
			} else {
				if (Context::get().player.getBindings().isJoystickAxisInput(action))
					 inputSprite = Context::get().textures.get(InputUtility::toSpriteID(Context::get().player.getBindings().getAssignedInput<JoystickAxis>(action)    , Context::get().textures.getXboxController()));
				else inputSprite = Context::get().textures.get(InputUtility::toSpriteID(Context::get().player.getBindings().getAssignedInput<Joystick::Button>(action), Context::get().textures.getXboxController()));
				inputSprite.setPosition(inputText.getPosition());
				Node::centerOrigin(inputSprite);
			}*/
		}
	}
}
