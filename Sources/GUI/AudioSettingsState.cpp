#include "AudioSettingsState.hpp"

#include "VerticalContainer.hpp"
#include "Button.hpp"

#include <Audio/AudioPlayer.hpp>
#include <DAL/ConfigurationRepository.hpp>
#include <DAL/ResourceHolder.hpp>
#include <DAL/TranslationTag.hpp>
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RenderWindow.hpp>

LOGCAT("AudioSettingsState");

AudioSettingsState::AudioSettingsState(StateStack& stack, Context context):
	GUIState(stack, context, new GUI::VerticalContainer(context)) {
	GUIContainer->setPaddingTop(300);
	GUIContainer->setPaddingLeft(490);

	masterSlider = std::make_shared<GUI::Settings::Slider>(context, 100.f);
	musicSlider  = std::make_shared<GUI::Settings::Slider>(context, 100.f);
	soundSlider  = std::make_shared<GUI::Settings::Slider>(context, 100.f);

	masterSlider->setTitle(TranslationTag::Slider::masterVolume);
	musicSlider->setTitle(TranslationTag::Slider::musicVolume);
	soundSlider->setTitle(TranslationTag::Slider::soundsVolume);

	masterSlider->setSliderPosition((float)context.audio.getMasterVolume());
	musicSlider->setSliderPosition( (float)context.audio.getMusicVolume());
	soundSlider->setSliderPosition( (float)context.audio.getSoundVolume());

	GUIContainer->pack(masterSlider);
	GUIContainer->pack(musicSlider);
	GUIContainer->pack(soundSlider);

	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setText(TranslationTag::Button::back);
	backButton->setCallback([this]() { requestStackPop(); });

	GUIContainer->pack(backButton);
}

void AudioSettingsState::onPop(Context & context) {
	auto& config = context.config.getConfiguration().getAudioConfiguration();
	config.setMasterVolume((int)masterSlider->getSliderPosition());
	config.setMusicVolume( (int)musicSlider->getSliderPosition());
	config.setSoundVolume( (int)soundSlider->getSliderPosition());
	context.audio.reloadConfiguration();
}
