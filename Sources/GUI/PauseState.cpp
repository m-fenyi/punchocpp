#include "PauseState.hpp"
#include "Button.hpp"
#include "Label.hpp"

#include <Audio/MusicPlayer.hpp>
#include <Audio/AudioPlayer.hpp>
#include <DAL/ResourceHolder.hpp>
#include <DAL/TranslationTag.hpp>
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

LOGCAT("PauseState");

PauseState::PauseState(StateStack& stack, Context& context):
	GUIState(stack, context, new GUI::VerticalContainer(context)) {

	GUIContainer->setAlignment(GUI::Alignment::Center);

	auto pauseLabel       = std::make_shared<GUI::Label> (TranslationTag::Label::pause, context.fonts, 70);
	auto returnButton     = std::make_shared<GUI::Button>(context);
	auto settingsButton   = std::make_shared<GUI::Button>(context);
	auto backToMenuButton = std::make_shared<GUI::Button>(context);

	pauseLabel->setMarginBottom(20);
	
	returnButton->setText(TranslationTag::Button::resume);
	settingsButton->setText(TranslationTag::Button::settings);
	backToMenuButton->setText(TranslationTag::Button::menu);

	returnButton->setCallback([this]() { requestStackPop(); });
	settingsButton->setCallback([this]() { requestStackPush(States::Settings); });
	backToMenuButton->setCallback([this]() {
		requestStackClear();
		requestStackPush(States::Game);
		requestStackPush(States::Menu);
	});

	GUIContainer->pack(pauseLabel);
	GUIContainer->pack(returnButton);
	GUIContainer->pack(settingsButton);
	GUIContainer->pack(backToMenuButton);

	context.audio.getMusicPlayer().setPaused(true);
}

void PauseState::onPop(Context & context) {
	context.audio.getMusicPlayer().setPaused(false);
}
