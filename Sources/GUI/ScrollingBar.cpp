#include "ScrollingBar.hpp"

namespace GUI {
	ScrollingBar::ScrollingBar(State::Context& context): Container(context) {}
	
	void ScrollingBar::draw(sf::RenderTarget& target, sf::RenderStates states) const {
		states.transform *= getTransform();
	}

}