#include "GameOverState.hpp"
#include "Button.hpp"
#include "Label.hpp"

#include <DAL/ResourceHolder.hpp>
#include <DAL/TranslationTag.hpp>
#include <Utilities/Utilities.hpp>

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

#include <string>

LOGCAT("GameOverState");

GameOverState::GameOverState(StateStack& stack, Context& context):
	GUIState(stack, context, new GUI::VerticalContainer(context)) {

	GUIContainer->setAlignment(GUI::Alignment::Center);

	auto gameOverLabel = std::make_shared<GUI::Label>(TranslationTag::Label::gameover, context.fonts, 70);
	auto replayButton = std::make_shared<GUI::Button>(context);
	auto menuButton = std::make_shared<GUI::Button>(context);

	gameOverLabel->setMarginBottom(20);

	replayButton->setText(TranslationTag::Button::replay);
	menuButton-> setText(TranslationTag::Button::menu);

	replayButton->setCallback([this]() {
		requestStackClear();
		requestStackPush(States::Game);
	});
	menuButton->setCallback([this]() {
		requestStackClear();
		requestStackPush(States::Menu);
	});

	GUIContainer->pack(gameOverLabel);
	GUIContainer->pack(replayButton);
	GUIContainer->pack(menuButton);

	handler.setListener<void>(UIActionID::Return, [this] { requestStackClear(); requestStackPush(States::Menu); });
}

