#ifndef GAMEOVERSTATE_HPP
#define GAMEOVERSTATE_HPP

#include "GUIState.hpp"
#include "VerticalContainer.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class GameOverState: public GUIState {
public:
	GameOverState(StateStack& stack, Context& context);
};

#endif // GAMEOVERSTATE_HPP
