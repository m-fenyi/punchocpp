#include "AudioPlayer.hpp"
#include <Utilities/Log.hpp>

LOGCAT("AudioPlayer");


AudioPlayer::AudioPlayer(): masterVolume(100), musicVolume(100), soundVolume(100), sounds(), music(){}

MusicPlayer& AudioPlayer::getMusicPlayer(){ return music; }

SoundPlayer& AudioPlayer::getSoundPlayer(){ return sounds; }

void AudioPlayer::play(Music::ID theme) { music.play(theme); }
void AudioPlayer::pauseMusic() { music.setPaused(true); }
void AudioPlayer::unpauseMusic() { music.setPaused(false); }
void AudioPlayer::stopMusic() { music.stop(); }
void AudioPlayer::play(SoundEffect::ID effect) { sounds.play(effect); }
void AudioPlayer::play(SoundEffect::ID effect, sf::Vector2f position) { sounds.play(effect, position); }

void AudioPlayer::setConfiguration(const AudioConfiguration & config) {
	configuration = &config;
	reloadConfiguration();
}

void AudioPlayer::reloadConfiguration() {
	if (!configuration) return;
	setMasterVolume(configuration->getMasterVolume());
	setMusicVolume (configuration->getMusicVolume());
	setSoundVolume (configuration->getSoundVolume());
}

void AudioPlayer::setMasterVolume(suint vol){
	if (vol > 100) vol = 100;
	masterVolume = vol;

	music.setVolume((masterVolume * musicVolume) / 100);
	sounds.setVolume((masterVolume * soundVolume) / 100);
}

void AudioPlayer::setMusicVolume(suint vol){
	if (vol > 100) vol = 100;
	musicVolume = vol;

	music.setVolume((masterVolume * musicVolume) / 100);
}

void AudioPlayer::setSoundVolume(suint vol){
	if (vol > 100) vol = 100;
	soundVolume = vol;

	sounds.setVolume((masterVolume * soundVolume) / 100);
}

int AudioPlayer::getMasterVolume(){
	return masterVolume;
}

int AudioPlayer::getMusicVolume(){
	return musicVolume;
}

int AudioPlayer::getSoundVolume(){
	return soundVolume;
}
