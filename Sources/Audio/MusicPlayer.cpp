#include "MusicPlayer.hpp"
#include <DAL/Filepath.hpp>
#include <Utilities/Log.hpp>

LOGCAT("MusicPlayer");

MusicPlayer::MusicPlayer():
	music(),
	filenames(),
	volume(100) {

	filenames[Music::MenuTheme] = FilePath::Audio::Music::menuTheme;
	filenames[Music::MissionTheme] = FilePath::Audio::Music::missionTheme;
}

void MusicPlayer::play(Music::ID theme) {
	std::string filename = filenames[theme];

	if (!music.openFromFile(filename)) throw std::runtime_error("Music " + filename + " could not be loaded.");

	music.setVolume(volume);
	music.setLoop(true);
	music.play();
}

void MusicPlayer::stop() {
	music.stop();
}

void MusicPlayer::setVolume(unsigned int volume) {
	if (volume > 100) this->volume = 100;
	else this->volume = volume;

	music.setVolume(this->volume);
}

int MusicPlayer::getVolume(){
	return volume;
}

void MusicPlayer::setPaused(bool paused) {
	if (paused) music.pause();
	else music.play();
}
