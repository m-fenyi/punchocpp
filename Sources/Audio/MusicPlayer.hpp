#ifndef MUSICPLAYER_HPP
#define MUSICPLAYER_HPP

#include <DAL/ResourceHolder.hpp>
#include <DAL/ResourceIdentifiers.hpp>

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Audio/Music.hpp>

#include <map>
#include <string>


class MusicPlayer: private sf::NonCopyable {
public:
	MusicPlayer();

	void play(Music::ID theme);
	void stop();
		 
	void setPaused(bool paused);
	void setVolume(unsigned int volume);
	int  getVolume();


private:
	sf::Music						 music;
	std::map<Music::ID, std::string> filenames;
	unsigned short int				 volume;
};

#endif // MUSICPLAYER_HPP
