#include "SoundPlayer.hpp"
#include <Utilities/Log.hpp>

#include <DAL/Filepath.hpp>

#include <SFML/Audio/Listener.hpp>

#include <cmath>

LOGCAT("SoundPlayer");

namespace {
	const float ListenerZ = 300.f;
	const float Attenuation = 8.f;
	const float MinDistance2D = 200.f;
	const float MinDistance3D = std::sqrt(MinDistance2D*MinDistance2D + ListenerZ*ListenerZ);
}

SoundPlayer::SoundPlayer(): soundBuffers(), sounds(), pitch(1.f), volume(100) {

	soundBuffers.load(SoundEffect::AlliedGunfire , FilePath::Audio::Sound::alliedGunfire);
	soundBuffers.load(SoundEffect::EnemyGunfire  , FilePath::Audio::Sound::enemyGunfire);
	soundBuffers.load(SoundEffect::Explosion1    , FilePath::Audio::Sound::explosion1);
	soundBuffers.load(SoundEffect::Explosion2    , FilePath::Audio::Sound::explosion2);
	soundBuffers.load(SoundEffect::LaunchMissile , FilePath::Audio::Sound::launchMissile);
	soundBuffers.load(SoundEffect::CollectPickup , FilePath::Audio::Sound::collectPickup);
	soundBuffers.load(SoundEffect::ButtonPressed , FilePath::Audio::Sound::button);
	soundBuffers.load(SoundEffect::ButtonSelected, FilePath::Audio::Sound::button);

	sf::Listener::setDirection(0.f, 0.f, -1.f);
}

void SoundPlayer::play(SoundEffect::ID effect) {
	play(effect, getListenerPosition());
}

void SoundPlayer::play(SoundEffect::ID effect, sf::Vector2f position) {
	sounds.push_back(sf::Sound());
	sf::Sound& sound = sounds.back();

	sound.setBuffer(soundBuffers.get(effect));
	sound.setPosition(position.x, -position.y, 0.f);
	sound.setAttenuation(Attenuation);
	sound.setMinDistance(MinDistance3D);
	sound.setPitch(pitch);
	sound.setVolume(volume);
	resetPitch();
	
	sound.play();
}

void SoundPlayer::setPitch(float value) { pitch = value; }

void SoundPlayer::risePitch(float value) { pitch += value; }

void SoundPlayer::lowerPitch(float value) { pitch -= value; }

void SoundPlayer::resetPitch() { setPitch(1.f); }

void SoundPlayer::setVolume(unsigned int volume){
	if (volume > 100) this->volume = 100;
	else this->volume = volume;
}

short SoundPlayer::getVolume(){
	return volume;
}

void SoundPlayer::removeStoppedSounds() {
	sounds.remove_if([](const sf::Sound& s) {
		return s.getStatus() == sf::Sound::Stopped;
	});
}

void SoundPlayer::setListenerPosition(sf::Vector2f position) {
	sf::Listener::setPosition(position.x, -position.y, ListenerZ);
}

sf::Vector2f SoundPlayer::getListenerPosition() const {
	sf::Vector3f position = sf::Listener::getPosition();
	return sf::Vector2f(position.x, -position.y);
}
