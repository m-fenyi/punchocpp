#ifndef SOUNDPLAYER_HPP
#define SOUNDPLAYER_HPP

#include <DAL/ResourceHolder.hpp>
#include <DAL/ResourceIdentifiers.hpp>

#include <SFML/System/Vector2.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>

#include <list>


class SoundPlayer: private sf::NonCopyable {
public:
	SoundPlayer();

	void		 play(SoundEffect::ID effect);
	void		 play(SoundEffect::ID effect, sf::Vector2f position);

	void		 setPitch(float value);
	void		 risePitch(float value = 0.1f);
	void		 lowerPitch(float value = 0.1f);
	void		 resetPitch();

	void		 setVolume(unsigned int volume);
	short		 getVolume();
				 
	void		 removeStoppedSounds();
	void		 setListenerPosition(sf::Vector2f position);
	sf::Vector2f getListenerPosition() const;


private:
	float pitch;
	unsigned short int volume;
	SoundBufferHolder	 soundBuffers;
	std::list<sf::Sound> sounds;
};

#endif // SOUNDPLAYER_HPP
