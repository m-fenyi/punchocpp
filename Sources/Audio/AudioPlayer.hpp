#ifndef AUDIOPLAYER_HPP
#define AUDIOPLAYER_HPP

#include "MusicPlayer.hpp"
#include "SoundPlayer.hpp"

#include <Configuration/AudioConfiguration.hpp>

using suint = short unsigned int;

class AudioPlayer{
public:
	AudioPlayer();

	MusicPlayer& getMusicPlayer();
	SoundPlayer& getSoundPlayer();

	void play(SoundEffect::ID effect);
	void play(SoundEffect::ID effect, sf::Vector2f position);
	void play(Music::ID theme);
	void pauseMusic();
	void unpauseMusic();
	void stopMusic();

	void setConfiguration(const AudioConfiguration& config);
	void reloadConfiguration();

	void setMasterVolume(suint vol);
	void setMusicVolume( suint vol);
	void setSoundVolume( suint vol);

	int getMasterVolume();
	int getMusicVolume();
	int getSoundVolume();


private:
	const AudioConfiguration* configuration;

	suint masterVolume;
	suint musicVolume;
	suint soundVolume;

	MusicPlayer				music;
	SoundPlayer				sounds;
};

#endif // AUDIOPLAYER_HPP