#ifndef WINDOWWRAPPER_HPP
#define WINDOWWRAPPER_HPP

#include <Configuration/GraphicsConfiguration.hpp>
#include "Resolution.hpp"
#include "WindowMode.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

class Camera;

class WindowWrapper {
public:
	WindowWrapper();
	~WindowWrapper();

	void setConfiguration(const GraphicsConfiguration& config);
	void reloadConfiguration();

	void setVsyncEnabled(bool vSyncOn);
	void setResolution(Resolution res);
	void setAntiAliasingLevel(int level);
	void setWindowMode(WindowMode mode);

	void setCamera(Camera& camera);
	Camera& getCamera();

	bool       isVsyncEnabled();
	Resolution getResolution();
	int        getAntiAliasingLevel();
	WindowMode getWindowMode();
	sf::RenderWindow& getWindow();

	void recreate();
	void createWindow();

private:
	sf::VideoMode       getVideoMode() const;
	sf::ContextSettings getContextSettings() const;
	unsigned            getWindowStyle() const;

	void                initWindow();

private:
	const GraphicsConfiguration* configuration;

	sf::RenderWindow* window;
	Camera*         camera;

	bool vSyncEnabled;
	int antiAliasingLevel;
	Resolution resolution;
	WindowMode windowMode;
};


#endif // WINDOWWRAPPER_HPP