#include "SFMLEventRetriever.hpp"
#include <boolinq/boolinq.h>

SFMLEventRetriever::SFMLEventRetriever(sf::RenderWindow & window): window(window) {}

bool SFMLEventRetriever::retrieveEvent(sf::Event & event) {
	return window.pollEvent(event);
}

bool SFMLEventRetriever::getMousePosition(sf::Vector2i & mousePosition) {
	mousePosition = sf::Mouse::getPosition();
	return true;
}

bool SFMLEventRetriever::getStickPosition(Joystick::ID joystickId, Joystick::Stick stickId, sf::Vector2f & axis) {
	if (stickId == Joystick::LeftStick) {
		axis.x = sf::Joystick::getAxisPosition(joystickId, (sf::Joystick::Axis)Joystick::LeftHorizontalAxis);
		axis.y = sf::Joystick::getAxisPosition(joystickId, (sf::Joystick::Axis)Joystick::LeftVerticalAxis);
	} else if (stickId == Joystick::RightStick) {
		axis.x = sf::Joystick::getAxisPosition(joystickId, (sf::Joystick::Axis)Joystick::RightHorizontalAxis);
		axis.y = sf::Joystick::getAxisPosition(joystickId, (sf::Joystick::Axis)Joystick::RightVerticalAxis);
	} else {
		return false;
	}
	return true;
}

std::list<unsigned> SFMLEventRetriever::getConnectedJoystick() {
	return linq::range((unsigned)0, (unsigned)sf::Joystick::Count, (unsigned)1)
		.where([](int i) { return sf::Joystick::isConnected(i); })
		.toList();
}

bool SFMLEventRetriever::isPressed(sf::Keyboard::Key key) {
	return sf::Keyboard::isKeyPressed(key);
}

bool SFMLEventRetriever::isPressed(sf::Mouse::Button button) {
	return sf::Mouse::isButtonPressed(button);
}

bool SFMLEventRetriever::isPressed(Joystick::ID joystickId, Joystick::Button button) {
	switch (button) {
	case Joystick::DpadUp:
		return sf::Joystick::getAxisPosition(joystickId, sf::Joystick::PovY) > 0.f;
	case Joystick::DpadDown:
		return sf::Joystick::getAxisPosition(joystickId, sf::Joystick::PovY) < 0.f;

	case Joystick::DpadRight:
		return sf::Joystick::getAxisPosition(joystickId, sf::Joystick::PovX) > 0.f;
	case Joystick::DpadLeft:
		return sf::Joystick::getAxisPosition(joystickId, sf::Joystick::PovX) < 0.f;

	case Joystick::RightTrigger:
		return sf::Joystick::getAxisPosition(joystickId, sf::Joystick::Z) > 0.f;
	case Joystick::LeftTrigger:
		return sf::Joystick::getAxisPosition(joystickId, sf::Joystick::Z) < 0.f;
	default:
		return sf::Joystick::isButtonPressed(joystickId, button);
	}
}
