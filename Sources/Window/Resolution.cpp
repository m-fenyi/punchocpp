#include "Resolution.hpp"

bool operator==(const Resolution& lhs, const Resolution& rhs) {
	return lhs.height == rhs.height && lhs.width == rhs.width;
}