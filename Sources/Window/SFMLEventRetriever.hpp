#ifndef SFMLEVENTRETRIEVER_HPP
#define SFMLEVENTRETRIEVER_HPP

#include <Input/EventRetriever.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

class SFMLEventRetriever : public EventRetriever {
public:
	SFMLEventRetriever(sf::RenderWindow& window);

	bool retrieveEvent(sf::Event & event) override;
	bool getMousePosition(sf::Vector2i & mousePosition) override;
	bool getStickPosition(Joystick::ID joystickId, Joystick::Stick stickId, sf::Vector2f & axis) override;
	std::list<unsigned> getConnectedJoystick() override;

	bool isPressed(sf::Keyboard::Key key) override;
	bool isPressed(sf::Mouse::Button button) override;
	bool isPressed(Joystick::ID joystickId, Joystick::Button button) override;

private:
	sf::RenderWindow& window;


	// Inherited via EventRetriever

};

#endif // SFMLEVENTRETRIEVER_HPP