#ifndef WINDOWMODE_HPP
#define WINDOWMODE_HPP



enum WindowMode {
	Fullscreen,
	Borderless,
	Windowed,

	WindowModeCount
};


#endif // WINDOWMODE_HPP