#ifndef RESOLUTION_HPP
#define RESOLUTION_HPP


struct Resolution {
	unsigned width, height;
};

bool operator==(const Resolution& lhs, const Resolution& rhs);

#endif // RESOLUTION_HPP