#include "WindowWrapper.hpp"

#include <Display/Camera.hpp>

static const std::string GameTitle = "Puncho"; // TODO Use configuration

WindowWrapper::WindowWrapper():
	window(nullptr),
	camera(nullptr),
	vSyncEnabled(false),
	antiAliasingLevel(0),
	windowMode(Windowed),
	resolution { 1024, 768 } {}

WindowWrapper::~WindowWrapper() {
	if (window) delete window;
}

void WindowWrapper::setConfiguration(const GraphicsConfiguration & config) {
	configuration = &config;
	reloadConfiguration();
}

void WindowWrapper::reloadConfiguration() {
	setVsyncEnabled(configuration->getVSync());
	setAntiAliasingLevel(configuration->getAALevel());
	setResolution(configuration->getResolution());
	setWindowMode(configuration->getWindowMode());
	if (window) recreate();
}

void WindowWrapper::setVsyncEnabled(bool enable) {
	vSyncEnabled = enable;
	if(window) window->setVerticalSyncEnabled(enable);
}

void WindowWrapper::setResolution(Resolution res) { resolution = res; }
void WindowWrapper::setAntiAliasingLevel(int level) { antiAliasingLevel = level; }
void WindowWrapper::setWindowMode(WindowMode mode) { windowMode = mode; }
void WindowWrapper::setCamera(Camera & camera) { this->camera = &camera; }

bool WindowWrapper::isVsyncEnabled() { return vSyncEnabled; }
Resolution WindowWrapper::getResolution() { return resolution; }
int WindowWrapper::getAntiAliasingLevel() { return antiAliasingLevel; }
WindowMode WindowWrapper::getWindowMode() { return windowMode; }
Camera & WindowWrapper::getCamera() { return *camera; }
sf::RenderWindow & WindowWrapper::getWindow() { return *window; }

void WindowWrapper::recreate() {
	window->create(getVideoMode(), GameTitle, getWindowStyle(), getContextSettings());
	initWindow();
}

void WindowWrapper::createWindow() {
	window = new sf::RenderWindow(getVideoMode(), GameTitle, getWindowStyle(), getContextSettings());
	initWindow();
}

sf::VideoMode WindowWrapper::getVideoMode() const {
	return sf::VideoMode(resolution.width, resolution.height);
}

sf::ContextSettings WindowWrapper::getContextSettings() const {
	sf::ContextSettings settings;
	settings.antialiasingLevel = antiAliasingLevel;
	return settings;
}

unsigned WindowWrapper::getWindowStyle() const {
	unsigned style;
	
	switch (windowMode) {
	default:
	case Fullscreen:
		style = sf::Style::Fullscreen;
		break;
	case Borderless:
		style = sf::Style::None;
		break;
	case Windowed:
		style = sf::Style::Titlebar | sf::Style::Close;
		break;
	}

	return style;
}

void WindowWrapper::initWindow() {
	window->setJoystickThreshold(0.1f);
	window->setKeyRepeatEnabled(false);
	window->setMouseCursorVisible(true);

	window->setVerticalSyncEnabled(vSyncEnabled);

	if (windowMode != Windowed)
		window->setSize(sf::Vector2u(resolution.width, resolution.width));
}
