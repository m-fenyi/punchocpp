#ifndef GRIDPARTITIONNODE_HPP
#define GRIDPARTITIONNODE_HPP

#include <list>
#include <set>
#include <vector>
#include <memory>

#include <SFML\Graphics\Rect.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

#include <Utilities/Collision.hpp>


class EntityNode;

class GridPartition {
public:
	using Ptr = std::shared_ptr<GridPartition>;

	using Cell = std::list<EntityNode*>;
	using Column = std::vector<Cell>;
	using Grid = std::vector<Column>;

	using CellCache = std::pair<int, int>;
	using Cache = std::set<CellCache>;

	GridPartition(float width, float height, int horizontalCellCount, int verticalCellCount, bool cache = true);
	GridPartition(float width, float height, float cellWidth, float cellHeight, bool cache = true);

	void clearGrid();

	void attachNode(EntityNode* node);
	void detachNode(EntityNode* node);
	void moveNode(EntityNode* node, float oldX, float oldY);

	std::set<CollisionData> getCollisionData(std::initializer_list<GridPartition*> subPartitions = {});
	std::set<CollisionData> getCollisionDataWith(std::initializer_list<GridPartition*> subPartitions);

	sf::FloatRect	getBoundingRect() const;

	void			draw(sf::RenderTarget& target) const;

private:
	float getCellWidth() const;
	float getCellHeight() const;

	void collidesWithCellule(std::set<CollisionData>& collisions, EntityNode* node, Cell& cellule, bool skipDuplicate = false);

	void addNode(EntityNode* node, sf::IntRect bounds);
	void addNode(EntityNode* node, int cellX, int cellY);
	
	void moveNode(EntityNode* node, sf::IntRect oldBounds, sf::IntRect newBounds);

	void removeNode(EntityNode* node, sf::IntRect bounds);
	void removeNode(EntityNode* node, int cellX, int cellY);

	void cacheNode(EntityNode* node, int cellX, int cellY);
	void uncacheNode(EntityNode* node, int cellX, int cellY);

	bool isInGrid(int x, int y);
	bool shouldCache(int x, int y);
	bool canCache(int x, int y);

	sf::IntRect getGridBounds(sf::FloatRect bounds);

private:
	Grid gridPartition;
	bool caching;
	Cache nonEmptyCache;

	float width;
	float height;
	
	int horizontalCellCount;
	int verticalCellCount;
};

#endif // GRIDPARTITIONNODE_HPP