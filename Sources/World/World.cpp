#include "World.hpp"

#include <Nodes/ProjectileNode.hpp>
#include <Nodes/Weapon.hpp>
#include <Nodes/PickupNode.hpp>
#include <Nodes/EnvironmentNode.hpp>
#include <Nodes/ParticleNode.hpp>
#include <Nodes/ExplosionNode.hpp>
#include <Nodes/AnimationNode.hpp>
#include <Nodes/SpriteNode.hpp>

#include <Interaction/CollisionManager.hpp>

#include <DAL/ConfigurationRepository.hpp>

#include <Audio/AudioPlayer.hpp>
#include <Utilities/Utilities.hpp>

#include <SFML/System/Sleep.hpp>

#include <algorithm>
#include <cmath>
#include <limits>



LOGCAT("World");

World::World(State::Context& context, WorldData& data):
	gameData(context.gameRepo),
	sceneTexture(),
	camera(context),
	randomController(1000, 3000),
	playerController(),
	sceneGraph(),
	worldPartition(nullptr),
	worldBounds(0.f, 0.f, data.dimension.x, data.dimension.y),
	spawnPosition(worldBounds.width / 2.f, worldBounds.height - 400),
	playerEntity(nullptr),
	enemySpawnPoints(),
	enemies(data.enemies),
	activeEnemies() {

	worldPartition = std::make_shared<WorldPartition>(worldBounds.width, worldBounds.height, 100.f, 100.f);

	sceneTexture.create(context.window.getWindow().getSize().x, context.window.getWindow().getSize().y);

	context.config.getConfiguration().getControlsConfiguration().getInputConfiguration().createHandler(playerController);

	buildScene();

	camera.setCameraLerp(data.cameraLerp);
	camera.setReticuleLerp(data.playerLerp);
	camera.setZoom(data.zoom);
}

bool World::handleInput(const Input & input) {
	return playerController.handleInput(input);
}

void World::update(sf::Time dt) {
	destroyEntitiesOutsideView();
	guideMissiles();

	while (!commandQueue.isEmpty()) 
		sceneGraph.onCommand(commandQueue.pop(), dt);

	handleCollisions();

	sceneGraph.removeDestroyedEntities();
	spawnEnemies();

	sceneGraph.update(dt, commandQueue);
	adaptPlayerVelocity();
	adaptPlayerPosition();

	randomController.update(dt);
	playerController.update(dt);

	updateSounds(dt);
	updateCamera(dt);
}

void World::draw(sf::RenderWindow& window) {
	if (PostEffect::isSupported()) {
		sceneTexture.clear();
		sceneTexture.setView(camera.getView());
		sceneTexture.draw(sceneGraph);
		sceneTexture.draw(camera);
		//worldPartition->draw(sceneTexture);
		
		sceneTexture.display();
		bloomEffect.apply(sceneTexture, window);
		
	} else {
		window.setView(camera.getView());
		window.draw(sceneGraph);
		window.draw(camera);
	}

#ifdef _DEBUG
	
	sf::Text entity = sf::Text();
	entity.setPosition(5.f, 50.f);
	entity.setFont(Context::get().fonts.get(Fonts::Main));
	entity.setCharacterSize(15u);
	entity.setString("Entity: " + toString(sceneGraph.countChild()));

	sceneTexture.setView(sceneTexture.getDefaultView());
	sceneTexture.draw(entity);

	window.setView(window.getDefaultView());
	window.draw(entity);
#endif // _DEBUG

}

CommandQueue& World::getCommandQueue() {
	return commandQueue;
}

bool World::hasAlivePlayer() const {
	return !playerEntity->isMarkedForRemoval();
}

bool World::hasPlayerWon() const {
	return hasAlivePlayer() && enemySpawnPoints.empty() && activeEnemies.empty();
}

void World::adaptPlayerPosition() {
	const float borderDistanceX = playerEntity->getBoundingRect().width  / 2 + 1;
	const float borderDistanceY = playerEntity->getBoundingRect().height / 2 + 1;

	sf::Vector2f position = playerEntity->getPosition();
	position.x = std::max(position.x, worldBounds.left + borderDistanceX);
	position.x = std::min(position.x, worldBounds.left + worldBounds.width - borderDistanceX);
	position.y = std::max(position.y, worldBounds.top + borderDistanceY);
	position.y = std::min(position.y, worldBounds.top + worldBounds.height - borderDistanceY);
	playerEntity->setPosition(position);
}

void World::adaptPlayerVelocity() {
	sf::Vector2f velocity = playerEntity->getVelocity();

	if (velocity != sf::Vector2f(0.f, 0.f)) 
		playerEntity->setVelocity(velocity / std::sqrt(2.f));
}

void World::handleCollisions() {
	std::set<CollisionData> collisionData = worldPartition->getCollisionData();
	CollisionManager::instance().handle(collisionData);
}

void World::updateSounds(sf::Time& dt) {
	Context::get().audio.getSoundPlayer().setListenerPosition(playerEntity->getWorldPosition());
	Context::get().audio.getSoundPlayer().removeStoppedSounds();
}

void World::updateCamera(sf::Time& dt) {
	if (playerEntity) camera.setCenter(playerEntity->getPosition());
	camera.update(dt);
}

void World::buildScene() {
	EntityNode::setPartition(worldPartition);
	SceneNode::createNodeLayers(sceneGraph);

	CharacterEntity::Ptr player(new CharacterEntity(gameData.get(Type::CharacterBlue)));
	playerEntity = player;
	playerEntity->setPosition(spawnPosition);
	WeaponData weaponData = gameData.get(Type::AlliedGun);
	ProjectileData projectileData = gameData.get(weaponData.projectileID);
	WeaponNode::Ptr weapon(new WeaponNode(weaponData, projectileData));
	playerEntity->setWeapon(weapon);
	playerEntity->attachChild(weapon);
	playerEntity->addToGrid();
	playerController.addNode(playerEntity.get());
	SceneNode::attachTo(SceneNode::UpperAir, std::move(player));
	
	for (int i = 0; i < Type::WeaponCount; ++i) {
		WeaponData weaponData = gameData.get((Type::Weapon)i);
		ProjectileData projectileData = gameData.get(weaponData.projectileID);
		WeaponNode::Ptr weapon(new WeaponNode(weaponData, projectileData));
		weapon->setPosition(i * 200.f, spawnPosition.y - 200);
		weapon->setRotation(45.f + i * 90.f);
		weapon->addToGrid();
		SceneNode::attachTo(SceneNode::LowerAir, std::move(weapon));
	}
	for (int i = 0; i < Type::EnvironmentCount; ++i) {
		EnvironmentNode::Ptr env(new EnvironmentNode(gameData.get((Type::Environment)i)));
		env->setPosition(i * 150.f, spawnPosition.y + 200);
		env->addToGrid();
		SceneNode::attachTo(SceneNode::LowerAir, std::move(env));
	}

	sf::IntRect textureRect(worldBounds);
	sf::Sprite background(Context::get().textures.get(Textures::GroundDesert));
	background.setTextureRect(textureRect);
	std::unique_ptr<SpriteNode> groundSprite(new SpriteNode(background));
	groundSprite->setPosition(worldBounds.left, worldBounds.top);
	SceneNode::attachTo(SceneNode::Background, std::move(groundSprite));

	std::unique_ptr<ParticleNode> smokeNode(new ParticleNode(gameData.get(Type::Particle::Smoke)));
	SceneNode::attachTo(SceneNode::LowerAir, std::move(smokeNode));
	
	std::unique_ptr<ParticleNode> propellantNode(new ParticleNode(gameData.get(Type::Particle::Propellant)));
	SceneNode::attachTo(SceneNode::LowerAir, std::move(propellantNode));

	addEnemies();
}

void World::addEnemies() {
	for (auto enemy : enemies) {
		addEnemy(enemy.type, enemy.position.x, enemy.position.y);
	}
}

void World::addEnemy(Type::Character type, float relX, float relY) {
	SpawnPoint spawn(type, spawnPosition.x + relX, spawnPosition.y - relY);
	enemySpawnPoints.push_back(spawn);
}

void World::spawnEnemies() {
	for (auto it = enemySpawnPoints.begin(); it != enemySpawnPoints.end(); ) {
		SpawnPoint spawn = *it;
		//if (!getBattlefieldBounds().contains(spawn.coord)) {
		//	++it;
		//	continue;
		//}

		CharacterEntity::Ptr enemy(new CharacterEntity(gameData.get(spawn.type)));
		enemy->setPosition(spawn.coord);
		enemy->addToGrid();
		
		WeaponData weaponData = gameData.get(Type::EnemyGun);
		ProjectileData projectileData = gameData.get(weaponData.projectileID);
		WeaponNode::Ptr weapon(new WeaponNode(weaponData, projectileData));

		enemy->setWeapon(weapon);
		
		SceneNode::attachTo(SceneNode::UpperAir, enemy);
		enemy->attachChild(weapon);
		
		randomController.addNode(enemy.get());
		
		it = enemySpawnPoints.erase(it);
	}
}

void World::destroyEntitiesOutsideView() {
	Command command;
	command.category = Category::Projectile;// | Category::EnemyEntity;
	command.action = derivedAction<EntityNode>([this](EntityNode& e) {
		if (!getBattlefieldBounds().intersects(e.getBoundingRect())) e.remove();
	});

	commandQueue.push(command);
}

void World::guideMissiles() {
	activeEnemies.clear();
	Command enemyCollector;
	enemyCollector.category = Category::EnemyEntity;
	enemyCollector.action = derivedAction<CharacterEntity>([this](CharacterEntity& enemy) {
		if (!enemy.isDestroyed()) activeEnemies.push_back(&enemy);
	});

	Command missileGuider;
	missileGuider.category = Category::Projectile;
	missileGuider.action = derivedAction<ProjectileNode>([this](ProjectileNode& missile) {
		if (!missile.isGuided()) return;

		float minDistance = std::numeric_limits<float>::max();
		CharacterEntity* closestEnemy = nullptr;

		for (CharacterEntity* enemy : activeEnemies) {
			float enemyDistance = Node::distance(missile, *enemy);

			if (enemyDistance < minDistance) {
				closestEnemy = enemy;
				minDistance = enemyDistance;
			}
		}

		if (closestEnemy) missile.guideTowards(closestEnemy->getWorldPosition());
	});

	commandQueue.push(enemyCollector);
	commandQueue.push(missileGuider);
}

sf::FloatRect World::getBattlefieldBounds() const {
	sf::FloatRect bounds = camera.getViewBounds();
	bounds.top  -= 100.f;
	bounds.left -= 100.f;
	bounds.height += 200.f;
	bounds.width  += 200.f;

	return bounds;
}
