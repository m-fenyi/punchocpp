#ifndef WORLD_HPP
#define WORLD_HPP

#include <DAL/DataType.hpp>
#include <DAL/GameDataRepository.hpp>
#include <Action/Controller.hpp>
#include <Nodes/SceneNode.hpp>
#include <Nodes/CharacterEntity.hpp>
#include <Action/CommandQueue.hpp>
#include <Action/Command.hpp>
#include <Display/Camera.hpp>
#include <Display/BloomEffect.hpp>
#include <Audio/SoundPlayer.hpp>
#include <GUI/State.hpp>

#include "WorldPartition.hpp"

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/Texture.hpp>



class World: private sf::NonCopyable {
private:
	struct SpawnPoint {
		SpawnPoint(Type::Character type, float x, float y):
			type(type),
			coord(x, y) {}

		Type::Character type;
		sf::Vector2f coord;
	};

public:
	World(State::Context& context, WorldData& data);

	bool            handleInput(const Input& input);
	void			update(sf::Time dt);
	void			draw(sf::RenderWindow& window);

	CommandQueue&	getCommandQueue();

	bool 			hasAlivePlayer() const;
	bool 			hasPlayerWon() const;

private:
	void			adaptPlayerPosition();
	void			adaptPlayerVelocity();
	void			handleCollisions();
	void			updateSounds(sf::Time& dt);
	void			updateCamera(sf::Time& dt);

	void			buildScene();
	void			addEnemies();
	void			addEnemy(Type::Character type, float relX, float relY);
	void			spawnEnemies();
	void			destroyEntitiesOutsideView();
	void			guideMissiles();
	sf::FloatRect	getBattlefieldBounds() const;

private:
	GameDataRepository&                 gameData;

	sf::RenderTexture					sceneTexture;

	RandomPatternController             randomController;
	PlayerController                    playerController;

	SceneNode							sceneGraph;
	WorldPartition::Ptr                 worldPartition;
	CommandQueue						commandQueue;

	sf::FloatRect						worldBounds;
	sf::Vector2f						spawnPosition;
	CharacterEntity::Ptr				playerEntity;

	std::list<SpawnPoint>				enemySpawnPoints;
	std::vector<CharacterEntity*>		activeEnemies;

	WorldData::EnemyPackData            enemies;

	Camera                              camera;

	BloomEffect							bloomEffect;
};

#endif // WORLD_HPP
