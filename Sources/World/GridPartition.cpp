#include "GridPartition.hpp"

#include <Utilities/Math.hpp>
#include <Utilities/Log.hpp>
#include <Utilities/String.hpp>
#include <Nodes/EntityNode.hpp>

#include <SFML\Graphics\RectangleShape.hpp>
#include <SFML\Graphics\RenderTarget.hpp>
#include <SFML\Graphics\RenderStates.hpp>

#include <sstream>

LOGCAT("GridPartition");

GridPartition::GridPartition(float width, float height, int horizontalCellCount, int verticalCellCount, bool cache):
	width(width), height(height), horizontalCellCount(horizontalCellCount), verticalCellCount(verticalCellCount), gridPartition(horizontalCellCount), caching(cache) {
	
	clearGrid();
}

GridPartition::GridPartition(float width, float height, float cellWidth, float cellHeight, bool cache):
	width(width), height(height), horizontalCellCount((int)(width/cellWidth)), verticalCellCount((int)(height/cellHeight)), caching(cache) {
	gridPartition = Grid(horizontalCellCount);
	
	clearGrid();
}

void GridPartition::clearGrid() {
	for (int i = 0; i < horizontalCellCount; ++i) {
		gridPartition[i] = Column(verticalCellCount);
		for (int j = 0; j < verticalCellCount; ++j) {
			gridPartition[i][j] = Cell();
		}
	}
}

void GridPartition::attachNode(EntityNode* node) {
	sf::IntRect bounds = getGridBounds(node->getBoundingRect());
	addNode(node, bounds);
	cacheNode(node, bounds.left, bounds.top);
}

void GridPartition::detachNode(EntityNode* node) {
	sf::IntRect bounds = getGridBounds(node->getBoundingRect());
	removeNode(node, bounds);
	uncacheNode(node, bounds.left, bounds.top);
}

void GridPartition::moveNode(EntityNode* node, float oldX, float oldY) {
	sf::IntRect newBounds = getGridBounds(node->getBoundingRect());
	sf::IntRect oldBounds = getGridBounds(sf::FloatRect(oldX, oldY, node->getBoundingRect().width, node->getBoundingRect().height));
	
	if (newBounds != oldBounds) {
		moveNode(node, oldBounds, newBounds);

		uncacheNode(node, oldBounds.left, oldBounds.top);
		cacheNode(node, newBounds.left, newBounds.top);
	}
}

std::set<CollisionData> GridPartition::getCollisionData(std::initializer_list<GridPartition*> subPartitions) {
	std::set<CollisionData> collisions;
	for (auto& cell : nonEmptyCache) {
		for (auto nodePtr : gridPartition[cell.first][cell.second]) {
			int maxX = (int) ((nodePtr->getBoundingRect().left + nodePtr->getBoundingRect().width) / getCellWidth());
			int maxY = (int) ((nodePtr->getBoundingRect().top + nodePtr->getBoundingRect().height) / getCellHeight());
			maxX = std::min(maxX, horizontalCellCount - 1);
			maxY = std::min(maxY, verticalCellCount - 1);

			for (int k = cell.first; k <= maxX; k++) {
				for (int l = cell.second; l <= maxY; l++) {

					collidesWithCellule(collisions, nodePtr, gridPartition[k][l], true);
					for (auto partition : subPartitions) {

						collidesWithCellule(collisions, nodePtr, partition->gridPartition[k][l]);
					}

				}
			}

		}

	}
	return collisions;
}

std::set<CollisionData> GridPartition::getCollisionDataWith(std::initializer_list<GridPartition*> subPartitions) {
	std::set<CollisionData> collisions;
	for (auto& cell : nonEmptyCache) {

		for (auto nodePtr : gridPartition[cell.first][cell.second]) {
			int maxX = (int) ((nodePtr->getBoundingRect().left + nodePtr->getBoundingRect().width) / getCellWidth());
			int maxY = (int) ((nodePtr->getBoundingRect().top + nodePtr->getBoundingRect().height) / getCellHeight());
			maxX = std::min(maxX, horizontalCellCount - 1);
			maxY = std::min(maxY, verticalCellCount - 1);

			for (int k = cell.first; k <= maxX; k++) {
				for (int l = cell.second; l <= maxY; l++) {

					for (auto partition : subPartitions) {
						collidesWithCellule(collisions, nodePtr, partition->gridPartition[k][l]);
					}

				}
			}

		}

	}
	return collisions;
}

sf::FloatRect GridPartition::getBoundingRect() const {
	return sf::FloatRect(0, 0, width, height);
}

void GridPartition::draw(sf::RenderTarget& target) const {
#if 1
	float cellWidth = getCellWidth();
	float cellHeight = getCellHeight();
	
	sf::RectangleShape shape;
	shape.setSize(sf::Vector2f(cellWidth, cellHeight));
	shape.setFillColor(sf::Color::Transparent);
	shape.setOutlineThickness(1.f);
	for (int i = 0; i < horizontalCellCount; ++i) {
		for (int j = 0; j < verticalCellCount; ++j) {
			shape.setPosition(sf::Vector2f(i * cellWidth, j * cellHeight));
			if (gridPartition[i][j].empty()) shape.setOutlineColor(sf::Color::Transparent);
			else shape.setOutlineColor(sf::Color::Red);

			target.draw(shape);
		}
	}
#endif // 1
}

float GridPartition::getCellWidth() const { return width / horizontalCellCount; }
float GridPartition::getCellHeight() const { return height / verticalCellCount; }

void GridPartition::collidesWithCellule(std::set<CollisionData>& collisions, EntityNode * node, Cell & cellule, bool skipDuplicate) {
	auto start = cellule.begin();
	if (skipDuplicate) {
		//TODO Improve
		start = std::find(cellule.begin(), cellule.end(), node);
		if (start == cellule.end()) start = cellule.begin();
		else ++start;
	}
	for (auto testNode : cellule) {
		//LOGD("Check collision");

		if (!Collision::canCollide(node, testNode)) continue;
		//LOGD("\t nodes can collision");

		CollisionData data = Collision::collision(*node, *testNode);
		if (data.collision) {
			//LOGD("\t\tCollision detected");

			data.pair = std::minmax(node, testNode);
			collisions.insert(data);
		} else {
			//LOGD("\t\tCollision not detectted");

		}
	}
}


void GridPartition::addNode(EntityNode * node, sf::IntRect bounds) {
	for (int i = bounds.left; i < bounds.left + bounds.width; ++i) {
		for (int j = bounds.top; j < bounds.top + bounds.height; ++j) {
			addNode(node, i, j);
		}
	}
}
void GridPartition::addNode(EntityNode * node, int cellX, int cellY) {
	if (isInGrid(cellX, cellY)) {
		gridPartition[cellX][cellY].emplace_back(node);
		gridPartition[cellX][cellY].unique();
	}
}

void GridPartition::moveNode(EntityNode * node, sf::IntRect oldBounds, sf::IntRect newBounds) {
	removeNode(node, oldBounds);
	addNode(node, newBounds);
}

void GridPartition::removeNode(EntityNode * node, sf::IntRect bounds) {
	for (int i = bounds.left; i < bounds.left + bounds.width; ++i) {
		for (int j = bounds.top; j < bounds.top + bounds.height; ++j) {
			removeNode(node, i, j);
		}
	}
}

void GridPartition::removeNode(EntityNode * node, int cellX, int cellY) {
	if (isInGrid(cellX, cellY)) {
		gridPartition[cellX][cellY].remove(node);
	}
}

void GridPartition::cacheNode(EntityNode * node, int cellX, int cellY) {
	if (canCache(cellX, cellY)) {
		nonEmptyCache.insert(std::make_pair(cellX, cellY));
	}
}

void GridPartition::uncacheNode(EntityNode * node, int cellX, int cellY) {
	if (canCache(cellX, cellY)) {
		nonEmptyCache.erase(std::make_pair(cellX, cellY));
	}
}

bool GridPartition::isInGrid(int x, int y) {
	return Range::between(0, horizontalCellCount).contains(x) && 
		Range::between(0, verticalCellCount).contains(y);
}

bool GridPartition::shouldCache(int x, int y) {
	return caching && !gridPartition[x][y].empty();
}

bool GridPartition::canCache(int x, int y) {
	return isInGrid(x, y) && shouldCache(x, y);
}

sf::IntRect GridPartition::getGridBounds(sf::FloatRect bounds) {
	int left = (int) (bounds.left / getCellWidth());
	int top = (int) (bounds.top / getCellHeight());
	int right = (int) ((bounds.left + bounds.width) / getCellWidth());
	int bottom = (int) ((bounds.top + bounds.height) / getCellHeight());

	int width  = right  - left + 1;
	int height = bottom - top  + 1;


	sf::IntRect gridBounds(left, top, width, height);
	return gridBounds;
}
