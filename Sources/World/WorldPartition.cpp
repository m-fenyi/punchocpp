#include "WorldPartition.hpp"

#include <Nodes/CharacterEntity.hpp>
#include <Utilities/Log.hpp>

LOGCAT("WorldPartition");

// Partitions:
// - High      : Entities interacting between themselves and lower partitions
// - Low       : Entities interacting with lower partitions
// - BackGround: Entities interacting with nothing

WorldPartition::WorldPartition(float width, float height, int horizontalCellCount, int verticalCellCount): 
	highPartition(width, height, horizontalCellCount, verticalCellCount), 
	lowPartition (width, height, horizontalCellCount, verticalCellCount),
	backPartition(width, height, horizontalCellCount, verticalCellCount, false) {
	LOGI("Building world partition with cell count! Grid: " << width << ", " << height << ". Width: " << horizontalCellCount << ", Height : " << verticalCellCount);
}

WorldPartition::WorldPartition(float width, float height, float cellWidth, float cellHeight): 
	highPartition(width, height, cellWidth, cellHeight), 
	lowPartition (width, height, cellWidth, cellHeight),
	backPartition(width, height, cellWidth, cellHeight, false) {
	LOGI("Building world partition with cell size ! Grid: " << width << ", " << height << ". Cell: " << cellWidth << ", " << cellHeight);
}


void WorldPartition::attachNode(EntityNode * node) { getNodePartition(node).attachNode(node); }
void WorldPartition::detachNode(EntityNode * node) { getNodePartition(node).detachNode(node); }
void WorldPartition::moveNode(EntityNode * node, float oldX, float oldY) { getNodePartition(node).moveNode(node, oldX, oldY); }


std::set<CollisionData> WorldPartition::getCollisionData() {
	std::set<CollisionData> highCollisions = highPartition.getCollisionData({ &lowPartition, &backPartition });
	std::set<CollisionData> lowCollisions  = lowPartition.getCollisionDataWith({ &backPartition });

	highCollisions.merge(lowCollisions);
	return highCollisions;
}


sf::FloatRect WorldPartition::getBoundingRect() const { return highPartition.getBoundingRect(); }

void WorldPartition::draw(sf::RenderTarget & target) const {
	highPartition.draw(target);
	lowPartition.draw(target);
	backPartition.draw(target);
}

GridPartition & WorldPartition::getNodePartition(EntityNode* node) {
	switch (node->getPartitionLayer()) {
	case HighPartition:
		return highPartition;
	case LowPartition:
		return lowPartition;
	default:
	case BackgroundPartition:
		return backPartition;
	case NoPartition:
		LOGE("Node cannot be in partition");
		assert(false);
		return backPartition;
	}
}
