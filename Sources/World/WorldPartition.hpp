#ifndef WORLDPARTITION_HPP
#define WORLDPARTITION_HPP

#include "GridPartition.hpp"

#include <Utilities/Collision.hpp>

#include <memory>

class EntityNode;
class ProjectileNode;

class WorldPartition {
public:
	using Ptr = std::shared_ptr<WorldPartition>;

	enum PartitionLayer {
		HighPartition,
		LowPartition,
		BackgroundPartition,

		PartitionCount,

		NoPartition
	};

	WorldPartition(float width, float height, int horizontalCellCount, int verticalCellCount);
	WorldPartition(float width, float height, float cellWidth, float cellHeight);

	void attachNode(EntityNode* node);
	void detachNode(EntityNode* node);
	void moveNode(EntityNode* node, float oldX, float oldY);
	
	std::set<CollisionData> getCollisionData();

	sf::FloatRect getBoundingRect() const;

	void draw(sf::RenderTarget& target) const;

private:
	GridPartition & getNodePartition(EntityNode* node);

private:
	GridPartition highPartition;
	GridPartition lowPartition;
	GridPartition backPartition;
};


#endif // WORLDPARTITION_HPP