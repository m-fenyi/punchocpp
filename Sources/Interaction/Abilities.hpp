#ifndef ABILITIES_HPP
#define ABILITIES_HPP

#include <DAL/DataType.hpp>

enum Ability {
	NoAbility    = 0,
	Damager      = 1 << 0,
	Interacter   = 1 << 1,
	Burner       = 1 << 2,
	Pickuper     = 1 << 3,
	Blocker      = 1 << 4,
};
class EntityNode;



class BaseAbility {
public:
	virtual Ability getAbility() const = 0;
};



class DamagerAbility : public BaseAbility {
public:
	DamagerAbility(int hitpoints = 0, bool destroyOnDamage = true);

	void setSource(EntityNode* source);
	EntityNode* getSource() const;

	Ability getAbility() const override;

public:
	unsigned int hitpoints;
	bool destroyOnDamage;

private:
	EntityNode* damageSource;
};



class BurnerAbility : public BaseAbility {
public:
	BurnerAbility(Type::Effect burnEffect = Type::SlowBurn);

	Type::Effect getBurnType() const;

	Ability getAbility() const override;

public:
	Type::Effect burnEffect;
};
#endif // ABILITIES_HPP