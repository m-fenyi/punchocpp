#include "CollisionManager.hpp"

#include <Nodes/EntityNode.hpp>
#include <Utilities/Log.hpp>

LOGCAT("CollisionManager");

bool matchRequirements(CollisionManager::Requirements require, EntityNode* abilityNode, EntityNode* propertyNode) {
	return require.ability & abilityNode->getAbilities() && require.property & propertyNode->getProperties();
}
void blockerCollision(EntityNode* abilityNode, EntityNode* propertyNode, const sf::Vector2f mtv);
void damageCollision(EntityNode* abilityNode, EntityNode* propertyNode);
void pickupCollision(EntityNode* abilityNode, EntityNode* propertyNode);

// ### MANAGER ###

CollisionManager::CollisionManager() : handlers() {
	Handler blockerHandler(Requirements{ Ability::Blocker , Property::Blockable  }, &blockerCollision);
	Handler damageHandler (Requirements{ Ability::Damager , Property::Damageable }, &damageCollision);
	Handler pickupHandler (Requirements{ Ability::Pickuper, Property::Pickupable }, &pickupCollision);

	handlers.push_back(blockerHandler);
	handlers.push_back(damageHandler);
	handlers.push_back(pickupHandler);
}

void CollisionManager::handle(std::set<CollisionData>& collisions) {
	for (CollisionData data : collisions) {
		for (auto handler : handlers) {
			if (handler.canHandleCollision(data))
				handler.handleCollision(data);
		}
	}
}


// ### HANDLER ###

CollisionManager::Handler::Handler(Requirements require, Action handler) : requirements(require), handler(handler) {}
CollisionManager::Handler::Handler(Requirements require, ActionMTV handler) : requirements(require), handlerMTV(handler) {}

bool CollisionManager::Handler::canHandleCollision(CollisionData & collision) {
	if (matchRequirements(requirements, collision.pair.first, collision.pair.second)) {
		return true;
	} else if (matchRequirements(requirements, collision.pair.second, collision.pair.first)) {
		std::swap(collision.pair.first, collision.pair.second);
		return true;
	}
	return false;
}

void CollisionManager::Handler::handleCollision(CollisionData & collision) {
	if (handlerMTV) {
		handlerMTV(collision.pair.first, collision.pair.second, collision.mtv);
	} else if (handler) {
		handler(collision.pair.first, collision.pair.second);
	}
}


// ### HANDLER FUNCTIONS ###

void blockerCollision(EntityNode * abilityNode, EntityNode * propertyNode, const sf::Vector2f mtv) {
	propertyNode->move(mtv);
	if (propertyNode->getProperty<BlockableProperty>(Property::Blockable).destroyOnImpact) {
		propertyNode->remove();
	}
}

void burnerCollision(EntityNode * abilityNode, EntityNode * propertyNode) {
	Type::Effect burnEffect = abilityNode->getAbility<BurnerAbility>(Ability::Burner).burnEffect;
	if (propertyNode->hasEffect(Effect::Burning)) {
		propertyNode->getEffect<BurnEffect>(Effect::Burning).setEffect(burnEffect);
	} else {
		propertyNode->addEffect(new BurnEffect(*propertyNode, burnEffect));
	}
}


void damageCollision(EntityNode * abilityNode, EntityNode * propertyNode) {
	DamagerAbility& ability = abilityNode->getAbility<DamagerAbility>(Ability::Damager);
	DamageableProperty& property = propertyNode->getProperty<DamageableProperty>(Property::Damageable);
	
	if (ability.getSource() == propertyNode) return;

	property.damage(ability.hitpoints);

	if (ability.destroyOnDamage) {
		abilityNode->remove();
	}
}

void pickupCollision(EntityNode * abilityNode, EntityNode * propertyNode) {
	propertyNode->getProperty<PickupableProperty>(Property::Pickupable).action(*abilityNode);
}
