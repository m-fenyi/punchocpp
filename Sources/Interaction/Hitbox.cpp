#include "Hitbox.hpp"
#include <Utilities/Log.hpp>
#include <Utilities/String.hpp>
#include <boolinq/boolinq.h>

#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/VertexArray.hpp>

LOGCAT("Hitbox");


Polygon::Polygon(std::vector<sf::Vector2f>& points): points(points) {}

std::vector<sf::Vector2f> Polygon::getPoints() const {
	return points;
}

std::vector<sf::Vector2f> Polygon::getAxes() const {
	std::vector<sf::Vector2f> result;

	sf::Vector2f axis, edge;
	for (unsigned int i = 0; i < points.size(); ++i) {
		edge.x = points[(i + 1) % points.size()].x - points[i].x;
		edge.y = points[(i + 1) % points.size()].y - points[i].y;

		float length = sqrt(edge.x * edge.x + edge.y * edge.y);

		edge.x /= length;
		edge.y /= length;

		result.push_back(sf::Vector2f(-edge.y, edge.x));
	}

	return result;
}

void Hitbox::setTransform(sf::Transformable * node) {
	setPosition(node->getPosition());
	setOrigin(node->getOrigin());
	setScale(node->getScale());
	setRotation(node->getRotation());
}


RectangleHitbox::RectangleHitbox(): rectangle(0, 0, 1, 1) {}

RectangleHitbox::RectangleHitbox(sf::FloatRect rectangle): rectangle(rectangle) {}

RectangleHitbox::RectangleHitbox(sf::Vector2f offset, sf::Vector2f size): rectangle(offset, size) {}

void RectangleHitbox::setOffset(sf::Vector2f offset) {
	rectangle.left = offset.x;
	rectangle.top  = offset.y;
}

void RectangleHitbox::setSize(sf::Vector2f size) {
	rectangle.width  = size.x;
	rectangle.height = size.y;
}

void RectangleHitbox::setSize(sf::FloatRect size) {
	rectangle.width = size.width;
	rectangle.height = size.height;
}

Polygon RectangleHitbox::getPolygon() const {
	std::vector<sf::Vector2f> points = getPoints();

	for (unsigned i = 0; i < points.size(); ++i) {
		points[i] = getTransform().transformPoint(points[i]);
	}
	
	return Polygon(points);
}

sf::FloatRect RectangleHitbox::getBoundingRect() const {
	return getTransform().transformRect(rectangle);
}

void RectangleHitbox::draw(sf::RenderTarget & target, sf::RenderStates states) const {
#ifdef _DEBUG
	/*
	sf::FloatRect rect = getBoundingRect();
	sf::RectangleShape shape;
	shape.setPosition(getPosition());
	shape.setSize(sf::Vector2f(rectangle.width, rectangle.height));
	shape.setOrigin(getOrigin());
	shape.setScale(getScale());
	shape.setRotation(getRotation());

	shape.setFillColor(sf::Color::Transparent);
	shape.setOutlineColor(sf::Color::Red);
	shape.setOutlineThickness(1.f);
	target.draw(shape);
	*/
	/*
	sf::RectangleShape bound;
	bound.setPosition(rect.left, rect.top);
	bound.setSize(sf::Vector2f(rect.width, rect.height));
	
	bound.setFillColor(sf::Color::Transparent);
	bound.setOutlineColor(sf::Color::Blue);
	bound.setOutlineThickness(1.f);
	target.draw(bound);
	*/
	sf::ConvexShape polygon;
	Polygon points = getPolygon();
	polygon.setPointCount(points.getPoints().size());
	for (unsigned i = 0; i < points.getPoints().size(); ++i) 
		polygon.setPoint(i, points.getPoints().at(i));
	polygon.setFillColor(sf::Color::Transparent);
	polygon.setOutlineColor(sf::Color::Magenta);
	polygon.setOutlineThickness(1.f);

	target.draw(polygon);
#endif // _DEBUG
}

std::vector<sf::Vector2f> RectangleHitbox::getPoints() const {
	static std::vector<sf::Vector2f> points;

	float right  = rectangle.left + rectangle.width;
	float bottom = rectangle.top + rectangle.height;
	points = { sf::Vector2f(rectangle.left, rectangle.top), sf::Vector2f(right, rectangle.top),
		       sf::Vector2f(right, bottom), sf::Vector2f(rectangle.left, bottom) };

	return points;
}
