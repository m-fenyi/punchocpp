#ifndef HITBOX_HPP
#define HITBOX_HPP

#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/System/Vector2.hpp>

#include <vector>

class EntityNode;

class Polygon {
public:
	Polygon(std::vector<sf::Vector2f>& points);

	std::vector<sf::Vector2f> getPoints() const;
	std::vector<sf::Vector2f> getAxes() const;

private:
	std::vector<sf::Vector2f> points;
};

class Hitbox: public sf::Transformable, public sf::Drawable {
public:
	void setTransform(sf::Transformable* node);
	virtual Polygon getPolygon() const = 0;

	virtual sf::FloatRect getBoundingRect() const = 0;
};

class RectangleHitbox: public Hitbox {
public:
	RectangleHitbox();
	RectangleHitbox(sf::FloatRect rectangle);
	RectangleHitbox(sf::Vector2f offset, sf::Vector2f size);

	void setOffset(sf::Vector2f offset);
	void setSize(sf::Vector2f size);
	void setSize(sf::FloatRect size);

	Polygon getPolygon() const override;

	sf::FloatRect getBoundingRect() const override;

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	std::vector<sf::Vector2f> getPoints() const;

private:
	sf::FloatRect rectangle;
};


#endif // HITBOX_HPP