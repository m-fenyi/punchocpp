#include "Entity.hpp"


// ### ABILITY ###
AbilityEntity::AbilityEntity(uint abilitiesID) : abilitiesID(abilitiesID) {}
AbilityEntity::AbilityEntity(AbilityEntityData & data) : abilitiesID(data.abilitiesID), abilities(data.abilities) {}

uint AbilityEntity::getAbilities() const { return abilitiesID; }
BaseAbility* AbilityEntity::getAbility(Ability abilityID) const { return abilities.at(abilityID); }
bool AbilityEntity::hasAbility(Ability abilityID) const {
	return abilitiesID & abilityID;
}
void AbilityEntity::addAbility(Ability abilityID) {
	addAbility(abilityID, nullptr);
}
void AbilityEntity::addAbility(BaseAbility* ability) {
	addAbility(ability->getAbility(), ability);
}
void AbilityEntity::addAbility(Ability abilityID, BaseAbility * ability) {
	abilitiesID |= abilityID;
	abilities[abilityID] = ability;
}
void AbilityEntity::removeAbility(Ability abilityID) {
	abilitiesID ^= abilityID;
	abilities.erase(abilityID);
}

// ### PROPERTY ###

PropertyEntity::PropertyEntity(uint propertiesID): propertiesID(propertiesID) {}
PropertyEntity::PropertyEntity(PropertyEntityData & data) : propertiesID(data.propertiesID), properties(data.properties) {}

uint PropertyEntity::getProperties() const { return propertiesID; }
BaseProperty* PropertyEntity::getProperty(Property propertyID) const { return properties.at(propertyID); }
bool PropertyEntity::hasProperty(Property propertyID) const {
	return propertiesID & propertyID;
}
void PropertyEntity::addProperty(Property propertyID) {
	addProperty(propertyID, nullptr);
}
void PropertyEntity::addProperty(BaseProperty* property) {
	addProperty(property->getProperty(), property);
}
void PropertyEntity::addProperty(Property propertyID, BaseProperty * property) {
	propertiesID |= propertyID;
	properties[propertyID] = property;
}
void PropertyEntity::removeProperty(Property propertyID) {
	propertiesID ^= propertyID;
	properties.erase(propertyID);
}


// ### EFFECT ###

EffectEntity::EffectEntity(uint effectsID) : effectsID(effectsID) {}
EffectEntity::EffectEntity(EffectEntityData & data) : effectsID(data.effectsID), effects(data.effects) {}

uint EffectEntity::getEffects() const { return effectsID; }
BaseEffect* EffectEntity::getEffect(Effect effectID) const { return effects.at(effectID); }
bool EffectEntity::hasEffect(Effect effectID) const {
	return effectsID & effectID;
}
void EffectEntity::addEffect(Effect effectID) {
	addEffect(effectID, nullptr);
}
void EffectEntity::addEffect(BaseEffect * effect) {
	addEffect(effect->getEffect(), effect);
}
void EffectEntity::addEffect(Effect effectID, BaseEffect* effect) {
	effectsID |= effectID;
	effects[effectID] = effect; 
}
void EffectEntity::removeEffect(Effect effectID) {
	effectsID ^= effectID;
	effects.erase(effectID);
}

void EffectEntity::updateEffects(sf::Time dt) {
	for (auto& effectPair : effects) {
		effectPair.second->update(dt);
	}
}

// ### ENTITY ###

Entity::Entity(uint abilitiesID, uint propertiesID, uint effectsID): 
	AbilityEntity(abilitiesID), 
	PropertyEntity(propertiesID), 
	EffectEntity(effectsID) {}

Entity::Entity(EntityData& data): AbilityEntity(data), PropertyEntity(data), EffectEntity(data) {}
