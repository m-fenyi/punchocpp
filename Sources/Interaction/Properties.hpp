#ifndef PROPERTIES_HPP
#define PROPERTIES_HPP

#include <functional>


enum Property {
	NoProperty   = 0,
	Damageable   = 1 << 0,
	Interactible = 1 << 1,
	Burnable     = 1 << 2,
	Pickupable   = 1 << 3,
	Blockable    = 1 << 4,

};


class BaseProperty {
public:
	virtual Property getProperty() const = 0;
};



class DamageableProperty : public BaseProperty {
public:
	DamageableProperty();
	DamageableProperty(int hitpoints);


	void damage(int points);
	void heal(int points);
	void destroy();
	bool isDestroyed() const;

	int getHitpoints() const;
	void setHitpoints(int hitpoints);

	Property getProperty() const override;

public:
	int hitpoints;
};



class BlockableProperty : public BaseProperty {
public:
	BlockableProperty(bool destroyOnImpact = false);

	Property getProperty() const override;

public:
	bool destroyOnImpact;
};



class EntityNode;
class PickupableProperty : public BaseProperty {
	using Action = std::function<void(EntityNode&)>;
public:
	PickupableProperty();

	void action(EntityNode& pickuperNode);

	Property getProperty() const override;

public:
	Action pickupAction;
};

#endif // PROPERTIES_HPP