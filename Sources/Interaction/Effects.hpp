#ifndef EFFECTS_HPP
#define EFFECTS_HPP

#include <SFML/System/Time.hpp>
#include <DAL/DataType.hpp>

enum Effect {
	NoEffect,
	Burning,

};



class EntityNode;

class BaseEffect {
public:
	enum State {
		Ready,
		Running,
		Paused,
		Stoped
	};

protected:
	BaseEffect(EntityNode& subject, Type::Effect effectType = Type::NoEffect);

public:
	void start();
	void stop();
	void pause();
	void unpause();
	void setState(State state);

	void setEffect(Type::Effect effectType);

	virtual void update(sf::Time& dt) = 0;

	virtual Effect getEffect() const = 0;

protected:
	virtual void onStart() = 0;
	virtual void onStop() = 0;
	virtual void onPause() {};
	virtual void onUnpause() {};

protected:
	EntityNode& subject;
	State effectState;
	Type::Effect effectType;
};


class TimedEffect : public BaseEffect {
public:
	TimedEffect(EntityNode& subject, Type::Effect effectType = Type::NoEffect);
	TimedEffect(EntityNode& subject, sf::Time duration, sf::Time cooldown = sf::Time::Zero);

	void setEffect(Type::Effect effectType);

	void setDuration(sf::Time duration);
	sf::Time& getTimeLeft();

	void resetCooldown(sf::Time newCooldown);
	bool isInCooldown() const;

	void update(sf::Time& dt) override;

protected:
	sf::Time timeLeft;
	sf::Time cooldown;
};

class BurnEffect : public TimedEffect {
public:
	BurnEffect(EntityNode& subject, Type::Effect effect = Type::SlowBurn);
	
	void update(sf::Time& dt) override;

	Effect getEffect() const override;

protected:
	void onStart() override;
	void onStop() override;
};

#endif // EFFECTS_HPP