#include "Effects.hpp"
#include <Nodes/EntityNode.hpp>


BaseEffect::BaseEffect(EntityNode& subject, Type::Effect effectType): subject(subject), effectType(effectType), effectState(Ready) {}

void BaseEffect::start()   { effectState = Running; onStart(); }
void BaseEffect::stop()    { effectState = Stoped;  onStop(); }
void BaseEffect::pause()   { effectState = Paused;  onPause(); }
void BaseEffect::unpause() { effectState = Running; onUnpause(); }
void BaseEffect::setState(State state) {
	switch (state) {
		case Running:
			switch (effectState) {
			case Ready:
			case Stoped:
				start();
				break;
			case Paused:
				unpause();
				break;
			default:
				break;
			}
			break;
		case Stoped:
			stop();
			break;
		case Paused:
			pause();
			break;
		default:
			break;
	}
}
void BaseEffect::setEffect(Type::Effect effectType) { this->effectType = effectType; }



TimedEffect::TimedEffect(EntityNode& subject, Type::Effect effectType) : BaseEffect(subject, effectType), timeLeft(sf::Time::Zero), cooldown(sf::Time::Zero) {}// TODO
TimedEffect::TimedEffect(EntityNode& subject, sf::Time duration, sf::Time cooldown) : BaseEffect(subject), timeLeft(duration), cooldown(cooldown) {}
void TimedEffect::setEffect(Type::Effect effectType) { setDuration(sf::Time::Zero); }// TODO
void TimedEffect::setDuration(sf::Time duration) { timeLeft = duration; }
sf::Time& TimedEffect::getTimeLeft() { return timeLeft; }
void TimedEffect::resetCooldown(sf::Time newCooldown) { cooldown += newCooldown; }
bool TimedEffect::isInCooldown() const { return cooldown > sf::Time::Zero; }
void TimedEffect::update(sf::Time& dt) {
	if (effectState != Running) return;
	timeLeft -= dt;
	cooldown -= dt;
	if (timeLeft <= sf::Time::Zero) {
		stop();
	}
}



BurnEffect::BurnEffect(EntityNode & subject, Type::Effect effect): TimedEffect(subject, effect) {}
void BurnEffect::update(sf::Time & dt) {
	TimedEffect::update(dt);
	if (isInCooldown()) return;

	if (subject.hasProperty(Property::Damageable)) {
		subject.getProperty<DamageableProperty>(Property::Damageable).damage(1);
	}
}
Effect BurnEffect::getEffect() const { return Effect::Burning; }
void BurnEffect::onStart() {
	subject.addAbility(Ability::Burner);
}
void BurnEffect::onStop() {
	subject.removeAbility(Ability::Burner);
}
