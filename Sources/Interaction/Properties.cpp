#include "Properties.hpp"

#include <Nodes/EntityNode.hpp>



BlockableProperty::BlockableProperty(bool destroyOnImpact) : destroyOnImpact(destroyOnImpact) {}
Property BlockableProperty::getProperty() const { return Property::Blockable; }



PickupableProperty::PickupableProperty() {}
void PickupableProperty::action(EntityNode& pickuperNode) {
	if (pickupAction) pickupAction(pickuperNode);
}
Property PickupableProperty::getProperty() const { return Property::Pickupable; }



DamageableProperty::DamageableProperty(): DamageableProperty(0) {}
DamageableProperty::DamageableProperty(int hitpoints) : hitpoints(hitpoints) {}
void DamageableProperty::damage(int points)          { hitpoints -= points; }
void DamageableProperty::heal(int points)            { hitpoints += points; }
void DamageableProperty::destroy()                   { setHitpoints(0); }
bool DamageableProperty::isDestroyed() const         { return hitpoints <= 0; }
int DamageableProperty::getHitpoints() const         { return hitpoints; }
void DamageableProperty::setHitpoints(int hitpoints) { this->hitpoints = hitpoints; }
Property DamageableProperty::getProperty() const     { return Property::Damageable; }
