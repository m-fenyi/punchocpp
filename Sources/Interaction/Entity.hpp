#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "Properties.hpp"
#include "Abilities.hpp"
#include "Effects.hpp"

#include <DAL/DataStruct.hpp>
#include <Utilities/Type.hpp>

#include <SFML/System/Time.hpp>

#include <map>

class BaseAbility;
class BaseEffect;
class BaseProperty;

class AbilityEntity {
public:
	AbilityEntity(uint abilitiesID = Ability::NoAbility);
	AbilityEntity(AbilityEntityData& data);

	uint                 getAbilities() const;
	BaseAbility*         getAbility(Ability abilityID) const;
	template<class T> T& getAbility(Ability abilityID) const { return *static_cast<T*>(getAbility(abilityID)); }

	bool                 hasAbility(Ability abilityID) const;

	void addAbility(Ability abilityID);
	void addAbility(BaseAbility* ability);
	void removeAbility(Ability abilityID);

private:
	void addAbility(Ability abilityID, BaseAbility* ability);

	std::map<Ability, BaseAbility*> abilities;
	uint abilitiesID;
};

class PropertyEntity {
public:
	PropertyEntity(uint propertiesID = Property::NoProperty);
	PropertyEntity(PropertyEntityData& data);

	uint                 getProperties() const;
	BaseProperty*        getProperty(Property propertyID) const;
	template<class T> T& getProperty(Property propertyID) const { return *static_cast<T*>(getProperty(propertyID)); }

	bool                 hasProperty(Property propertyID) const;

	void addProperty(Property propertyID);
	void addProperty(BaseProperty* property);
	void removeProperty(Property propertyID);

private:
	void addProperty(Property propertyID, BaseProperty* property);

	std::map<Property, BaseProperty*> properties;
	uint propertiesID;
};

class EffectEntity {
public:
	EffectEntity(uint effectsID = Effect::NoEffect);
	EffectEntity(EffectEntityData& data);

	uint                 getEffects() const;
	BaseEffect*          getEffect(Effect effectID) const;
	template<class T> T& getEffect(Effect effectID) const { return *static_cast<T*>(getEffect(effectID)); }

	bool                 hasEffect(Effect effectID) const;

	void addEffect(Effect effectID);
	void addEffect(BaseEffect* effect);
	void removeEffect(Effect effectID);

	void updateEffects(sf::Time dt);

private:
	void addEffect(Effect effectID, BaseEffect* effect);

	std::map<Effect, BaseEffect*> effects;
	uint effectsID;
};

class Entity: public AbilityEntity, public PropertyEntity, public EffectEntity {
public:
	Entity(uint abilitiesID = Ability::NoAbility, uint propertiesID = Property::NoProperty, uint effectsID = Effect::NoEffect);
	Entity(EntityData& data);
};

#endif // ENTITY_HPP