#ifndef COLLISIONHANDLER_HPP
#define COLLISIONHANDLER_HPP

#include "Properties.hpp"
#include "Abilities.hpp"

#include <Utilities/Collision.hpp>
#include <Utilities/Singleton.hpp>

#include <SFML/System/Vector2.hpp>

#include <utility>
#include <functional>

class EntityNode;

class CollisionManager: public Singleton<CollisionManager> {
public:
	struct Requirements {
		Ability ability;
		Property property;
	};

	class Handler {
	public:
		using Action    = std::function<void(EntityNode*, EntityNode*)>;
		using ActionMTV = std::function<void(EntityNode*, EntityNode*, const sf::Vector2f)>;

		Handler(Requirements require, Action handler);
		Handler(Requirements require, ActionMTV handler);

		bool canHandleCollision(CollisionData& collision);
		void handleCollision(CollisionData& collision);

	private:
		Requirements requirements;
		Action handler;
		ActionMTV handlerMTV;
	};

	CollisionManager();

	void handle(std::set<CollisionData>& collisions);

private:
	std::vector<CollisionManager::Handler> handlers;
};

#endif // COLLISIONHANDLER_HPP