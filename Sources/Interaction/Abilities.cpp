#include "Abilities.hpp"



DamagerAbility::DamagerAbility(int hitpoints, bool destroyOnDamage) : hitpoints(hitpoints), damageSource(nullptr), destroyOnDamage(destroyOnDamage) {}
void DamagerAbility::setSource(EntityNode * source) {
	damageSource = source;
}
EntityNode * DamagerAbility::getSource() const { return damageSource; }
Ability DamagerAbility::getAbility() const { return Ability::Damager; }



BurnerAbility::BurnerAbility(Type::Effect burnEffect): burnEffect(burnEffect) {}
Type::Effect BurnerAbility::getBurnType() const { return burnEffect; }
Ability BurnerAbility::getAbility() const { return Ability::Burner; }
