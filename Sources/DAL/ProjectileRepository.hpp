#ifndef PROJECTILEREPOSITORY_HPP
#define PROJECTILEREPOSITORY_HPP


#include "JsonStructRepository.hpp"
#include "DataStruct.hpp"
#include "DataType.hpp"

class ProjectileRepository : public ReadOnlyJsonStructRepository<Type::Projectile, ProjectileData> {
public:
	JsonStructInfo<ProjectileData> getStructInfo(ProjectileData& data) const override;
	void loadDefault() noexcept override;
	const std::string& getFileName() const override;
};

#endif // PROJECTILEREPOSITORY_HPP

