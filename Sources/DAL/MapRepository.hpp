#ifndef MAPREPOSITORY_HPP
#define MAPREPOSITORY_HPP

#include "Repository.hpp"

#include <map>

template<class DataID, class Data>
class BaseMapRepository {
protected:
	std::map<DataID, Data> repository;
};


template<class DataID, class Data>
class ReadOnlyMapRepository : virtual public BaseMapRepository<DataID, Data>, public SimpleReadOnlyRepository<DataID, Data> {
public:
	std::vector<Data> getAll() const override {
		std::vector<Data> allData;
		std::transform(repository.begin(), repository.end(), 
					   std::back_inserter(allData),
					   [](const std::pair<DataID, Data>& pair) -> Data { return pair.second; });
		return allData;
	}

	std::vector<DataID> getAllID() const {
		std::vector<DataID> allId;
		std::transform(repository.begin(), repository.end(),
					   std::back_inserter(allId),
					   [](const std::pair<DataID, Data>& pair) -> DataID { return pair.first; });
		return allId;
	}

	Data& get(DataID id) override { return repository.at(id); }
	const Data& get(DataID id) const override { return repository.at(id); }
	bool has(DataID id) const override { return repository.count(id); }
	bool isLoaded() const noexcept override { return repository.size() > 0; }

protected:
	virtual const std::string& getFileName() const = 0;
};

template<class DataID, class Data>
class WriteOnlyMapRepository : virtual public BaseMapRepository<DataID, Data>, public SimpleWriteOnlyRepository<DataID, Data> {
public:
	void add(DataID id, Data& data) override { repository[id] = data; }
	void update(DataID id, Data& data) override { repository[id] = data; }
	void remove(DataID id) override { repository.erase(id); }
};

template<class DataID, class Data>
class MapRepository : 
	public ReadOnlyMapRepository<DataID, Data>, 
	public WriteOnlyMapRepository<DataID, Data> {};

#endif // JSONREPOSITORY_HPP