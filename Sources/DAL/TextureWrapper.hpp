#ifndef TEXTUREWRAPPER_HPP
#define TEXTUREWRAPPER_HPP

#include "ResourceIdentifiers.hpp"
#include <Display/Animation.hpp>

#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\Sprite.hpp>

class TextureWrapper {
public:
	TextureWrapper();

	sf::Texture& get(Textures::ID id);
	sf::Sprite&  get(Textures::Sprites::ID id);
	Animation&   get(Textures::Animations::ID id);

	const sf::Texture& get(Textures::ID id) const;
	const sf::Sprite&  get(Textures::Sprites::ID id) const;
	const Animation&   get(Textures::Animations::ID id) const;

	void load(Textures::ID id, const std::string& filename);
	void load(Textures::ID id, const std::string & filename, sf::IntRect rect);
	void insert(Textures::Sprites::ID id, std::unique_ptr<sf::Sprite> ptr);
	void insert(Textures::Animations::ID id, std::unique_ptr<Animation> ptr);

	TextureHolder&   getTextures();
	SpriteHolder&    getSprites();
	AnimationHolder& getAnimations();

	void setXboxController(bool xboxController);
	bool getXboxController();

private:
	TextureHolder   textures;
	SpriteHolder    sprites;
	AnimationHolder animations;

	bool xboxController;
};

#endif // TEXTUREWRAPPER_HPP