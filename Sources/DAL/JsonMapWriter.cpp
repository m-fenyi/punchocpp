#include "JsonMapWriter.hpp"

#include <Utilities/File.hpp>

void JsonMapWriter::writeFile(const StringMap & map, const std::string & fileName) {
	Json::Value jsonValue = writeJson(map);
	File::JSON::save(fileName, jsonValue);
}

Json::Value JsonMapWriter::writeJson(const StringMap & map) {
	Json::Value jsonValue;

	for (auto pair : map) {
		File::JSON::getValue(jsonValue, pair.first) = pair.second;
	}

	return jsonValue;
}

