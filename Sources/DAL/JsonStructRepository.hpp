#ifndef JSONSTRUCTREPOSITORY_HPP
#define JSONSTRUCTREPOSITORY_HPP


#include "MapRepository.hpp"

#include "JsonStructParser.hpp"
#include "JsonStructWriter.hpp"

using NumID = unsigned;

template<class Data>
class BaseJsonStructRepository {
protected:
	virtual JsonStructInfo<Data> getStructInfo(Data& data) const = 0;
};

template<class DataID, class Data>
class ReadOnlyJsonStructRepository : virtual public BaseJsonStructRepository<Data>, public ReadOnlyMapRepository<DataID, Data> {
public:
	void loadData() override {
		Data dataStruct;
		JsonStructInfo<Data> structInfo = getStructInfo(dataStruct);
		const std::string& fileName = getFileName();

		repository = JsonStructParser::parseToMap<DataID>(structInfo, fileName);
	}
};

template<class DataID, class Data>
class WriteOnlyJsonStructRepository : virtual public BaseJsonStructRepository<Data>, public WriteOnlyMapRepository<DataID, Data> {
public:
	void save() const override {
		Data dataStruct;
		JsonStructInfo<Data> structInfo = getStructInfo(dataStruct);
		const std::string& fileName = getFileName();

		JsonStructWriter::writeFile<DataID>(structInfo, repository, fileName);
	}
};

template<class DataID, class Data>
class JsonStructRepository :
	public ReadOnlyJsonStructRepository<DataID, Data>,
	public WriteOnlyJsonStructRepository<DataID, Data> {};


#endif // JSONSTRUCTREPOSITORY_HPP
