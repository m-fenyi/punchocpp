#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include "ResourceIdentifiers.hpp"
#include "DataType.hpp"

#include <Interaction/Abilities.hpp>
#include <Interaction/Properties.hpp>
#include <Interaction/Effects.hpp>

#include <Utilities/Type.hpp>

#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Rect.hpp>

#include <functional>
#include <vector>

class EntityNode;
class BaseAbility;
class BaseProperty;
class BaseEffect;

struct WorldData {
	struct EnemyData {
		Type::Character type;
		sf::Vector2f position;
	};
	using EnemyPackData = std::vector<EnemyData>;
	float         zoom;
	float         cameraLerp;
	float         playerLerp;
	sf::Vector2f  dimension;
	EnemyPackData enemies;
};

struct DrawableInfo {
	union DrawableID {
		Textures::Sprites::ID sprite;
		Textures::Animations::ID anim;
	};
	Type::GraphicKind kind;
	DrawableID id;
};
struct AnimationData {
	Type::Animation animation;
	DrawableInfo info;
};

using AnimationPackData = std::vector<AnimationData>;

struct AbilityEntityData {
	std::map<Ability, BaseAbility*> abilities;
	uint abilitiesID;
};

struct PropertyEntityData {
	std::map<Property, BaseProperty*> properties;
	uint propertiesID;
};

struct EffectEntityData {
	std::map<Effect, BaseEffect*> effects;
	uint effectsID;
};

struct EntityData : AbilityEntityData, PropertyEntityData, EffectEntityData {};

struct CharacterEntityData : EntityData{
	int               hitpoints;
	float             speed;
	AnimationPackData packData;
};

struct WeaponData {
	Type::Projectile      projectileID;
	Textures::Sprites::ID spriteID;
	SoundEffect::ID       soundID;
	sf::Time              rateOfFire;
	int                   spread;
	float                 accuracy;
	float                 kickback;
};

struct ProjectileData : EntityData {
	Textures::Sprites::ID sprite;
	int      damage;
	float    speed;
	float    aeraofeffect;
	sf::Time lifetime;
	float    bounciness;

	bool     homing;
	bool     explosive;
};

struct PickupData : EntityData {
	using Action = std::function<void(EntityNode&)>;
	enum ActionID {
		Heal,
		Ammo,
	};

	Action                action;
	Textures::Sprites::ID sprite;
};

struct ParticleData {
	sf::Color color;
	sf::Time  lifetime;
};

struct EnvironmentData : EntityData {
	AnimationPackData packData;
};


struct EffectData { };
struct TimedEffectData : EffectData {
	sf::Time duration;
};
struct BurnEffectData : TimedEffectData {
	sf::Time rate;
	int damage;
};


namespace Graphics {
	struct TextureData {
		Textures::ID textureID;
		std::string filename;
		sf::IntRect rect;
		bool smooth;
		bool repeated;
		bool sRGB;
	};

	struct SpriteData {
		Textures::Sprites::ID spriteID;
		Textures::ID textureID;
		sf::IntRect spriteRect;
		sf::Vector2f origin;
		sf::Vector2f scale;
		sf::Color color;
		float rotation;
	};

	struct AnimationData {
		Textures::Animations::ID animID;
		Textures::ID textureID;
		sf::IntRect animRect;
		sf::Vector2i frameSize;
		unsigned frameCount;
		sf::Time duration;
		bool repeating;
		sf::Vector2f origin;
		sf::Vector2f scale;
		sf::Color color;
		float rotation;
	};
}

#endif // DATASTRUCT_HPP