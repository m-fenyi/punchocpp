#include "CharacterRepository.hpp"
#include "Filepath.hpp"

JsonStructInfo<CharacterEntityData> CharacterRepository::getStructInfo(CharacterEntityData & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo( "speed"        , &data.speed    , true , new FloatRule()),
		JsonFieldInfo( "animationPack", &data.packData , true , new VectorRule(new AnimationDataRule())),
		JsonFieldInfo( "abilities"    , &data          , false, new AbilityRule()),
		JsonFieldInfo( "properties"   , &data          , false, new PropertyRule()),
		JsonFieldInfo( "effects"      , &data          , false, new EffectRule())
	};
	return JsonStructInfo<CharacterEntityData>(fields, &data);
}

void CharacterRepository::loadDefault() noexcept {}

const std::string & CharacterRepository::getFileName() const {
	return FilePath::Json::entities;
}
