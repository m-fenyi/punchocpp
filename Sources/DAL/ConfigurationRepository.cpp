#include "ConfigurationRepository.hpp"
#include "Filepath.hpp"
#include "JsonStructParser.hpp"
#include "JsonStructWriter.hpp"

void ConfigurationRepository::loadDefault() noexcept {}

GameConfiguration & ConfigurationRepository::getConfiguration() { 
	if (has("Custom"))
		return get("Custom");
	else
		return get("Default");
}
SystemConfiguration & ConfigurationRepository::getSystemConfiguration() { return systemConf; }

void ConfigurationRepository::loadData() {
	loadConfig();
	loadSystem();
}
void ConfigurationRepository::save() const {
	saveConfig();
	saveSystem();
}

const std::string & ConfigurationRepository::getFileName() const { return FilePath::Json::config; }
const std::string & ConfigurationRepository::getSystemFileName() const { return FilePath::Json::system; }

void ConfigurationRepository::loadConfig() {
	GameConfiguration_t dataStruct;
	auto structInfo = getStructInfo(dataStruct);
	auto fileName = getFileName();

	// Multiple Named Configuration in config.json
	//auto parseConfig = JsonStructParser::parseToNameMap(structInfo, fileName);
	//for (auto config : parseConfig) {
	//	repository[config.first].load(config.second);
	//}
	
	JsonStructParser::parseToStruct(structInfo, fileName);
	repository["Custom"].load(dataStruct);
}
void ConfigurationRepository::loadSystem() {
	SystemConfiguration_t systemConfig;
	auto systemStructInfo = getSystemStructInfo(systemConfig);
	auto systemFileName = getSystemFileName();
	JsonStructParser::parseToStruct(systemStructInfo, systemFileName);
	systemConf.load(systemConfig);
}

void ConfigurationRepository::saveConfig() const {
	GameConfiguration_t dataStruct;
	JsonStructInfo<GameConfiguration_t> structInfo = getStructInfo(dataStruct);
	const std::string& fileName = getFileName();

	std::map<std::string, GameConfiguration_t> configRepo;
	for (auto config : repository) {
		configRepo[config.first] = repository.at(config.first).save();
	}

	JsonStructWriter::writeNameMap(structInfo, configRepo, fileName);
}
void ConfigurationRepository::saveSystem() const {
	SystemConfiguration_t systemConfig;
	auto systemStructInfo = getSystemStructInfo(systemConfig);
	auto systemFileName = getSystemFileName();

	JsonStructWriter::writeStruct(systemStructInfo, systemConf.save(), systemFileName);
}

JsonStructInfo<AudioConfiguration_t> ConfigurationRepository::getAudioStructInfo(AudioConfiguration_t & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo("Volume.Master", &data.masterVolume, true, new IntegerRule(Range::between(0, 101), 100)),
		JsonFieldInfo("Volume.Music" , &data.musicVolume , true, new IntegerRule(Range::between(0, 101), 100)),
		JsonFieldInfo("Volume.Sound" , &data.soundVolume , true, new IntegerRule(Range::between(0, 101), 100))
	};
	return JsonStructInfo<AudioConfiguration_t>(fields, &data);
}

JsonStructInfo<ControlsConfiguration_t> ConfigurationRepository::getControlsStructInfo(ControlsConfiguration_t & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo("Bindings"       , &data.inputConf     , true, new ControlBindingRule(true)),
		JsonFieldInfo("PreferedInput"  , &data.preferedInput , true, new IntegerRule(Range::between(0,4), 0)),
		JsonFieldInfo("JoystickSetup"  , &data.joystickSetup , true, new IntegerRule(Range::between(0,2), 0)),
		JsonFieldInfo("KeyboardSetup"  , &data.keyboardSetup , true, new IntegerRule(Range::between(0,4), 0)),
		JsonFieldInfo("XboxController" , &data.xboxController, true, new BoolRule())
	};
	return JsonStructInfo<ControlsConfiguration_t>(fields, &data);
}

JsonStructInfo<GeneralConfiguration_t> ConfigurationRepository::getGeneralStructInfo(GeneralConfiguration_t & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo("Locale", &data.locale, false, new StringRule("English", false))
	};
	return JsonStructInfo<GeneralConfiguration_t>(fields, &data);
}

JsonStructInfo<GraphicsConfiguration_t> ConfigurationRepository::getGraphicsStructInfo(GraphicsConfiguration_t & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo( "VSync"      , &data.vsync            , true, new BoolRule()),
		JsonFieldInfo( "AALevel"    , &data.antiAliasingLevel, true, new IntegerRule(Range::above(0), 0)),
		JsonFieldInfo( "Resolution" , &data.resolution       , true, new ResolutionRule()),
		JsonFieldInfo( "WindowMode" , &data.windowMode       , true, new IntegerRule(Range::between(0, (int)WindowMode::WindowModeCount), 0)),
	};
	return JsonStructInfo<GraphicsConfiguration_t>(fields, &data);
}

JsonStructInfo<GameConfiguration_t> ConfigurationRepository::getStructInfo(GameConfiguration_t & data) const {
	auto audioStructInfo    = getAudioStructInfo(data.audioConf);
	auto controlsStructInfo = getControlsStructInfo(data.controlsConf);
	auto generalStructInfo  = getGeneralStructInfo(data.generalConf);
	auto graphicsStructInfo = getGraphicsStructInfo(data.graphicsConf);
	JsonFieldsInfo fields = {
		JsonFieldInfo( "Audio"   , &data.audioConf   , true, new ComplexRule<AudioConfiguration_t>(audioStructInfo)),
		JsonFieldInfo( "Controls", &data.controlsConf, true, new ComplexRule<ControlsConfiguration_t>(controlsStructInfo)),
		JsonFieldInfo( "General" , &data.generalConf , true, new ComplexRule<GeneralConfiguration_t>(generalStructInfo)),
		JsonFieldInfo( "Graphics", &data.graphicsConf, true, new ComplexRule<GraphicsConfiguration_t>(graphicsStructInfo))
	};
	return JsonStructInfo<GameConfiguration_t>(fields, &data);
}

JsonStructInfo<SystemConfiguration_t> ConfigurationRepository::getSystemStructInfo(SystemConfiguration_t & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo("Version"    , &data.version    , true, new StringRule()),
		JsonFieldInfo("UIBindings" , &data.uiInput    , true, new ControlBindingRule(false)),
		JsonFieldInfo("Resolutions", &data.resolutions, true, new VectorRule(new ResolutionRule()))
	};
	return JsonStructInfo<SystemConfiguration_t>(fields, &data);
}

