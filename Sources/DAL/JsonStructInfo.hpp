#ifndef JSONSTRUCTINFO_HPP
#define JSONSTRUCTINFO_HPP

#include <string>
#include <vector>
#include <memory>

class JsonFieldRule;

struct JsonFieldInfo {
	template<class Rule>
	JsonFieldInfo(const std::string& name, void* field, bool required, Rule* rule) 
		: name(name), field(field), required(required), rule(std::shared_ptr<Rule>(rule)) {}
	
	// TODO: Field Info should have FieldRule for field class type
	//template<class Value>
	//JsonFieldInfo(const std::string& name, Value* field, bool required, JsonFieldClassRule<Value>* rule)
	//	: name(name), field(field), required(required), rule(std::shared_ptr<JsonFieldClassRule<Value>>(rule)) {}

	std::string name;
	void* field;
	bool required;
	std::shared_ptr<JsonFieldRule> rule;
};

using JsonFieldsInfo = std::vector<JsonFieldInfo>;

template<class T>
struct JsonStructInfo {
	JsonStructInfo(JsonFieldsInfo& fields, T* data) : fieldInfos(fields), structData(data) {}

	JsonFieldsInfo fieldInfos;
	T* structData;
};

#endif // JSONSTRUCTINFO_HPP

