#ifndef PARTICLEREPOSITORY_HPP
#define PARTICLEREPOSITORY_HPP


#include "JsonStructRepository.hpp"
#include "DataStruct.hpp"
#include "DataType.hpp"

class ParticleRepository : public ReadOnlyJsonStructRepository<Type::Particle, ParticleData> {
public:
	JsonStructInfo<ParticleData> getStructInfo(ParticleData& data) const override;
	void loadDefault() noexcept override;
	const std::string& getFileName() const override;
};

#endif // PARTICLEREPOSITORY_HPP

