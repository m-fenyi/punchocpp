#include "TextureWrapper.hpp"
#include <Utilities/Log.hpp>

LOGCAT("TexureWrapper");

TextureWrapper::TextureWrapper(): xboxController(true) {}

sf::Texture& TextureWrapper::get(Textures::ID id) { return textures.get(id); }
sf::Sprite&  TextureWrapper::get(Textures::Sprites::ID id) { return sprites.get(id); }
Animation&   TextureWrapper::get(Textures::Animations::ID id) { return animations.get(id); }

const sf::Texture& TextureWrapper::get(Textures::ID id) const { return textures.get(id); }
const sf::Sprite&  TextureWrapper::get(Textures::Sprites::ID id) const { return sprites.get(id); }
const Animation&   TextureWrapper::get(Textures::Animations::ID id) const { return animations.get(id); }

void TextureWrapper::load(Textures::ID id, const std::string & filename) { textures.load(id, filename); }
void TextureWrapper::load(Textures::ID id, const std::string & filename, sf::IntRect rect) { textures.load(id, filename, rect); }
void TextureWrapper::insert(Textures::Sprites::ID id, std::unique_ptr<sf::Sprite> ptr) { sprites.insertResource(id, std::move(ptr)); }
void TextureWrapper::insert(Textures::Animations::ID id, std::unique_ptr<Animation> ptr) { animations.insertResource(id, std::move(ptr)); }

TextureHolder & TextureWrapper::getTextures() { return textures; }
SpriteHolder & TextureWrapper::getSprites() { return sprites; }
AnimationHolder & TextureWrapper::getAnimations() { return animations; }

void TextureWrapper::setXboxController(bool xboxController) { this->xboxController = xboxController; }
bool TextureWrapper::getXboxController() { return xboxController; }
