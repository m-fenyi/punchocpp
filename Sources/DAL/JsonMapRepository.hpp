#ifndef JSONMAPREPOSITORY_HPP
#define JSONMAPREPOSITORY_HPP


#include "MapRepository.hpp"

template<class Data>
class BaseJsonMapRepository {
protected:
	
};

template<class Data>
class ReadOnlyJsonMapRepository : virtual public BaseJsonMapRepository<Data>, public ReadOnlyMapRepository<std::string, Data> {
protected:
	void loadData() override {
		const std::string& fileName = getFileName();

		repository = JSONMapParser::parseFile(fileName);
	}
};

template<class Data>
class WriteOnlyJsonMapRepository : virtual public BaseJsonMapRepository<Data>, public WriteOnlyMapRepository<std::string, Data> {
protected:
	void save() const override {
		const std::string& fileName = getFileName();

		JSONMapParser::writeFile(repository, fileName);
	}
};

template<class Data>
class JsonMapRepository :
	public ReadOnlyJsonMapRepository<Data>,
	public WriteOnlyJsonMapRepository<Data> {};


#endif // JSONMAPREPOSITORY_HPP
