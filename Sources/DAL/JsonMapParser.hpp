#ifndef JSONMAPPARSER_HPP
#define JSONMAPPARSER_HPP


#include <string>
#include <map>

#include <Utilities/StaticClass.hpp>

#include <json/json.h>

class JsonMapParser : public StaticClass {
	using StringMap = std::map<std::string, std::string>;
public:
	static StringMap parseFile(const std::string& fileName);
	static StringMap parseJson(Json::Value& value);

private:
	static void parseValue(Json::Value& value, const std::string& prefix, StringMap& map);
};


#endif // JSONMAPPARSER_HPP

