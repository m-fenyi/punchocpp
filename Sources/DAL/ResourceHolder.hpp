#ifndef RESOURCEHOLDER_HPP
#define RESOURCEHOLDER_HPP

#include <map>
#include <string>
#include <memory>
#include <stdexcept>
#include <cassert>
#include <Utilities/Log.hpp>


template <typename Resource, typename Identifier>
class ResourceHolder {
public:
	template <typename Parameter>
	void			load(Identifier id, const std::string& filename, const Parameter& secondParam);
	void			load(Identifier id, const std::string& filename);

	Resource&		get(Identifier id);
	const Resource&	get(Identifier id) const;

	void			insertResource(Identifier id, std::unique_ptr<Resource> resource);


private:
	std::map<Identifier, std::unique_ptr<Resource>>	resourceMap;
};

#include "ResourceHolder.inl"
#endif // RESOURCEHOLDER_HPP
