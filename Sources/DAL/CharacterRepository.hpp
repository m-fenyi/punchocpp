#ifndef CHARACTERREPOSITORY_HPP
#define CHARACTERREPOSITORY_HPP


#include "JsonStructRepository.hpp"
#include "DataStruct.hpp"
#include "DataType.hpp"

class CharacterRepository : public ReadOnlyJsonStructRepository<Type::Character, CharacterEntityData> {
public:
	void loadDefault() noexcept override;

protected:
	JsonStructInfo<CharacterEntityData> getStructInfo(CharacterEntityData& data) const override;
	const std::string& getFileName() const override;
};

#endif // CHARACTERREPOSITORY_HPP

