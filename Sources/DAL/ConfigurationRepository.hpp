#ifndef CONFIGURATIONREPOSITORY_HPP
#define CONFIGURATIONREPOSITORY_HPP

#include "JsonStructRepository.hpp"
#include <Configuration/GameConfiguration.hpp>
#include <Configuration/SystemConfiguration.hpp>

class ConfigurationRepository : public MapRepository<std::string, GameConfiguration> {
public:
	void loadDefault() noexcept override;
	void save() const override;

	GameConfiguration& getConfiguration();
	SystemConfiguration& getSystemConfiguration();

protected:
	void loadData() override;

	const std::string& getFileName() const override;
	const std::string& getSystemFileName() const;

private:
	void loadConfig();
	void loadSystem();

	void saveConfig() const;
	void saveSystem() const;

	inline JsonStructInfo<AudioConfiguration_t> getAudioStructInfo(AudioConfiguration_t& data) const;
	inline JsonStructInfo<ControlsConfiguration_t> getControlsStructInfo(ControlsConfiguration_t& data) const;
	inline JsonStructInfo<GeneralConfiguration_t> getGeneralStructInfo(GeneralConfiguration_t& data) const;
	inline JsonStructInfo<GraphicsConfiguration_t> getGraphicsStructInfo(GraphicsConfiguration_t& data) const;
	
	inline JsonStructInfo<GameConfiguration_t> getStructInfo(GameConfiguration_t& data) const;
	inline JsonStructInfo<SystemConfiguration_t> getSystemStructInfo(SystemConfiguration_t& data) const;

private:
	SystemConfiguration systemConf;
};

#endif // CONFIGURATIONREPOSITORY_HPP
