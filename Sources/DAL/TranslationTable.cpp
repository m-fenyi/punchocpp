#include "TranslationTable.hpp"
#include <Utilities/Log.hpp>

LOGCAT("TranslationTable");

TranslationTable::TranslationTable():locale("default") {}

void TranslationTable::addTranslation(const std::string & locale, const std::string & tag, const std::string & translation) {
	this->translation[locale][tag] = translation;
}

void TranslationTable::setLocale(const std::string & locale) {
	if(translation.find(locale) != translation.end()) this->locale = locale;
}

const std::string& TranslationTable::getTranslation(const std::string& tag) {
	if (translation[locale].find(tag) != translation[locale].end()) return translation[locale][tag];
	else if (translation["default"].find(tag) != translation["default"].end()) return translation["default"][tag];
	return tag;
}

const std::string& TranslationTable::getLocale() {
	return locale;
}

std::vector<std::string> TranslationTable::getLocales() {
	std::vector<std::string> locales;
	for (auto pair : translation) locales.emplace_back(pair.first);
	return locales;
}
