#ifndef PICKUPREPOSITORY_HPP
#define PICKUPREPOSITORY_HPP


#include "JsonStructRepository.hpp"
#include "DataStruct.hpp"
#include "DataType.hpp"

class PickupRepository : public ReadOnlyJsonStructRepository<Type::Pickup, PickupData> {
public:
	JsonStructInfo<PickupData> getStructInfo(PickupData& data) const override;
	void loadDefault() noexcept override;
	const std::string& getFileName() const override;
};

#endif // PICKUPREPOSITORY_HPP

