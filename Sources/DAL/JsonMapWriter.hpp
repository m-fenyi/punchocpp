#ifndef JSONMAPWRITER_HPP
#define JSONMAPWRITER_HPP

#include <string>
#include <map>

#include <Utilities/StaticClass.hpp>

#include <json/json.h>

class JsonMapWriter : public StaticClass {
	using StringMap = std::map<std::string, std::string>;
public:
	static void writeFile(const StringMap& map, const std::string& fileName);
	static Json::Value writeJson(const StringMap& map);
};

#endif // JSONMAPWRITER_HPP

