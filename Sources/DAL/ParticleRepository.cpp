#include "ParticleRepository.hpp"
#include "Filepath.hpp"

JsonStructInfo<ParticleData> ParticleRepository::getStructInfo(ParticleData & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo( "color"   , &data.color   , true, new ColorRule()),
		JsonFieldInfo( "lifetime", &data.lifetime, true, new TimeRule())
	};
	return JsonStructInfo<ParticleData>(fields, &data);
}

void ParticleRepository::loadDefault() noexcept {}

const std::string & ParticleRepository::getFileName() const {
	return FilePath::Json::particle;
}
