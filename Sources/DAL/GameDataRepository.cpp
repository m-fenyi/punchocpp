#include "GameDataRepository.hpp"

void GameDataRepository::loadDefault() noexcept {
	characterRepo.loadDefault();
	environmentRepo.loadDefault();
	particleRepo.loadDefault();
	pickupRepo.loadDefault();
	projectileRepo.loadDefault();
	weaponRepo.loadDefault();
	worldRepo.loadDefault();
}

bool GameDataRepository::isLoaded() const noexcept {
	bool loaded = true;
	loaded &= characterRepo.isLoaded();
	loaded &= environmentRepo.isLoaded();
	loaded &= particleRepo.isLoaded();
	loaded &= pickupRepo.isLoaded();
	loaded &= projectileRepo.isLoaded();
	loaded &= weaponRepo.isLoaded();
	loaded &= worldRepo.isLoaded();
	return loaded;
}

#define GET(DATA, TYPE, REPO) DATA& GameDataRepository::get(TYPE id) { return REPO.get(id); }
#define CONST_GET(DATA, TYPE, REPO) const DATA& GameDataRepository::get(TYPE id) const { return REPO.get(id); }
#define HAS(TYPE, REPO) bool GameDataRepository::has(TYPE id) const { return REPO.has(id); }
#define METHOD(DATA, TYPE, REPO) \
	GET(DATA, TYPE, REPO) \
	CONST_GET(DATA, TYPE, REPO) \
	HAS(TYPE, REPO)

METHOD(CharacterEntityData, Type::Character  , characterRepo)
METHOD(EnvironmentData    , Type::Environment, environmentRepo)
METHOD(ParticleData       , Type::Particle   , particleRepo)
METHOD(PickupData         , Type::Pickup     , pickupRepo)
METHOD(ProjectileData     , Type::Projectile , projectileRepo)
METHOD(WeaponData         , Type::Weapon     , weaponRepo)
METHOD(WorldData          , Type::World      , worldRepo)

void GameDataRepository::loadData() {
	characterRepo.loadData();
	environmentRepo.loadData();
	particleRepo.loadData();
	pickupRepo.loadData();
	projectileRepo.loadData();
	weaponRepo.loadData();
	worldRepo.loadData();
}
