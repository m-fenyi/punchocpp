#include "JsonStructWriter.hpp"

void JsonStructWriter::makeField(JsonFieldInfo & fieldInfo, Json::Value & value) {
	bool valid = fieldInfo.rule->matches(fieldInfo.field);
	
	if (valid) {
		fieldInfo.rule->save(fieldInfo.field, value);
	} else if (fieldInfo.required) {
		throw WriteException(fieldInfo.name, "Could not be written, value is required & does not match rule");
	}
}
