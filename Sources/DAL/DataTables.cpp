#include "DataTables.hpp"

#include "Filepath.hpp"
#include "TranslationTag.hpp"
#include "JSONStructParser.hpp"

#include <Nodes/CharacterEntity.hpp>
#include <Nodes/ProjectileNode.hpp>
#include <Nodes/PickupNode.hpp>
#include <Nodes/EnvironmentNode.hpp>

#include <Display/Particle.hpp>
#include <World/World.hpp>

#include <Utilities/Utilities.hpp>


#include <json\json.h>

LOGCAT("DataTables");




TranslationTable& initializeUITranslationData() {
	UITranslationData = TranslationTable();

	Json::Value UITranslation;
	if (!File::JSON::load(FilePath::Json::translationUI, UITranslation)) {
		LOGW("Default UI Translation data returned");
		return UITranslationData;
	}

	for (auto key : UITranslation.getMemberNames()) {
		initializeUITranslationData(key, UITranslation[key]);
	}

	UITranslationData.setLocale("English");
	LOGI("Json UI Translation data returned");
	return UITranslationData;
}

void initializeUITranslationData(std::string& locale, Json::Value& translation) {
	initializeUITranslationData(locale, std::string(), translation);
}

void initializeUITranslationData(std::string & locale, std::string & tagPrefix, Json::Value& translation) {
	if (!tagPrefix.empty()) tagPrefix += '.';
	for (auto key : translation.getMemberNames()) {
		if (translation[key].isObject()) {
			initializeUITranslationData(locale, tagPrefix + key, translation[key]);
		} else {
			UITranslationData.addTranslation(locale, tagPrefix + key, translation.get(key, "Unknown").asString());
		}
	}
}

