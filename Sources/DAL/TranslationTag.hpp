#ifndef TRANSLATIONTAG_HPP
#define TRANSLATIONTAG_HPP

#include <string>
#include <vector>

namespace TranslationTag {
	const std::string gameTitle = "GameTitle";
	
	namespace General {
		const std::string prefix = "General.";

		const std::string on  = prefix + "On";
		const std::string off = prefix + "Off";
	}

	namespace Label {
		const std::string prefix = "Label.";

		const std::string gameover = prefix + "GameOver";
		const std::string pause    = prefix + "Pause";
		const std::string loading  = prefix + "Loading";
	}

	namespace Button {
		const std::string prefix = "Button.";

		const std::string play     = prefix + "Play";
		const std::string exit     = prefix + "Exit";
		const std::string back     = prefix + "Back";
		const std::string settings = prefix + "Settings";

		const std::string game     = prefix + "Game";
		const std::string audio    = prefix + "Audio";
		const std::string graphics = prefix + "Graphics";
		const std::string controls = prefix + "Controls";

		const std::string resume   = prefix + "Resume";
		const std::string replay   = prefix + "Replay";
		const std::string menu     = prefix + "Menu";
	}

	namespace Switch {
		const std::string prefix = "Switch.";

		const std::string locale           = prefix + "Locale";
		const std::string controlsType     = prefix + "ControlsType";
		const std::string controllerType   = prefix + "ControllerType";
		const std::string vsync            = prefix + "VSync";
		const std::string windowStyle      = prefix + "WindowStyle";
		const std::string resolution       = prefix + "Resolution";

		std::string getChoice(const std::string& switchTag, int index);

		std::vector<std::string> getChoices(const std::string& switchTag, int choiceCount);
	}

	namespace Slider {
		const std::string prefix = "Slider.";

		const std::string masterVolume = prefix + "MasterVolume";
		const std::string musicVolume  = prefix + "MusicVolume";
		const std::string soundsVolume = prefix + "SoundsVolume";

		const std::string antialiasing = prefix + "Antialiasing";
	}

	namespace Action {
		const std::string prefix = "Action.";

		const std::string up     	    = prefix + "Up";
		const std::string down   	    = prefix + "Down";
		const std::string left   	    = prefix + "Left";
		const std::string right  	    = prefix + "Right";
		const std::string interact      = prefix + "Interact";
		const std::string primaryFire   = prefix + "Shoot1";
		const std::string secondaryFire = prefix + "Shoot2";
		const std::string switchWeapon  = prefix + "Switch";
		const std::string specialMove   = prefix + "Special";
	}
}

#endif // TRANSLATIONTAG_HPP

