#ifndef REPOSITORY_HPP
#define REPOSITORY_HPP

#include <vector>

#include "JSONStructParser.hpp"

class ReadOnlyRepository {
public:
	void loadOrDefault() noexcept {
		try {
			load();
		} catch (...) {
			loadDefault();
		}
	}
	virtual void load() {
		if (!isLoaded()) {
			loadData();
		}
	}
	virtual void loadDefault() noexcept = 0;
	virtual void loadData() = 0;

	virtual bool isLoaded() const noexcept = 0;

};


class WriteOnlyRepository {
public:
	virtual void save() const = 0;
};


template<class DataID, class Data>
class SimpleReadOnlyRepository : public ReadOnlyRepository {
public:
	virtual std::vector<Data> getAll() const = 0;
	virtual Data& get(DataID id) = 0;
	virtual const Data& get(DataID id) const = 0;
	virtual bool has(DataID id) const = 0;
};


template<class DataID, class Data>
class SimpleWriteOnlyRepository : public WriteOnlyRepository{
public:
	virtual void add(DataID id, Data& data) = 0;
	virtual void update(DataID id, Data& data) = 0;
	virtual void remove(DataID id) = 0;
};

template<class DataID, class Data>
class SimpleRepository: 
	public SimpleReadOnlyRepository<DataID, Data>, 
	public SimpleWriteOnlyRepository<DataID, Data> {};


#endif // REPOSITORY_HPP
