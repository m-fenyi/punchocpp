#include "JsonFieldRule.hpp"

#include "DataStruct.hpp"
#include "JsonStructParser.hpp"

#include <Window/WindowWrapper.hpp>
#include <Utilities/Math.hpp>

#include <limits>


BoolRule::BoolRule() : default(false) {}
BoolRule::BoolRule(bool default) : default(default) {}
void BoolRule::loadDefault(bool * field) {
	(*field) = default;
}
void BoolRule::load(bool* field, Json::Value & value) {
	(*field) = value.asBool();
}
void BoolRule::save(bool* field, Json::Value & value) {
	value = *field;
}
bool BoolRule::matchesJson(Json::Value & value) {
	if (!value.isBool() && !value.isNumeric()) return false;

	return true;
}




ColorRule::ColorRule() : default(sf::Color::Black) {}
ColorRule::ColorRule(sf::Color default) : default(default) {}
void ColorRule::loadDefault(sf::Color* field) {
	(*field) = default;
}
void ColorRule::load(sf::Color* field, Json::Value& value) {
	int r = value[0].asInt();
	int g = value[1].asInt();
	int b = value[2].asInt();
	int a;
	if (value.size() == 4) {
		a = value[3].asInt();
	} else {
		a = default.a;
	}

	sf::Color jsonColor(r, g, b, a);

	(*field) = jsonColor;
}
void ColorRule::save(sf::Color* field, Json::Value & value) {
	sf::Color color = *field;
	value.append(color.r);
	value.append(color.g);
	value.append(color.b);
	value.append(color.a);
}
bool ColorRule::matchesJson(Json::Value& value) {
	int size = value.size();
	if (size != 3 && size != 4) return false;
	for (int i = 0; i < size; ++i) {
		bool validRange = Range::between(0, 256).contains(value[i].asInt());
		if (!value[i].isUInt() || !validRange) {
			return false;
		}
	}

	return true;
}




StringRule::StringRule(std::string default, bool allowEmpty) : default(default), allowEmpty(allowEmpty) {}
void StringRule::loadDefault(std::string* field) {
	(*field) = default;
}
void StringRule::load(std::string* field, Json::Value& value) {
	(*field) = value.asString();
}
void StringRule::save(std::string* field, Json::Value & value) {
	value = *field;
}
bool StringRule::matchesJson(Json::Value& value) {
	if (!value.isString()) return false;
	if (!allowEmpty && value.asString().empty()) return false;

	return true;
}
bool StringRule::matches(std::string* field) {
	return allowEmpty || !field->empty();
}




void ResolutionRule::loadDefault(Resolution* field) {}
void ResolutionRule::load(Resolution* field, Json::Value & value) {
	field->width = value[0].asUInt();
	field->height = value[1].asUInt();
}
void ResolutionRule::save(Resolution* field, Json::Value & value) {
	value.append(field->width);
	value.append(field->height);
}
bool ResolutionRule::matchesJson(Json::Value & value) {
	if (value.size() != 2) return false;
	if (!value[0].isUInt()) return false;
	if (!value[1].isUInt()) return false;
	return true;
}





void AnimationDataRule::loadDefault(AnimationData * field) { (*field) = AnimationData(); }
void AnimationDataRule::load(AnimationData * field, Json::Value & value) {
	int id;
	field->animation = (Type::Animation)  value[0].asInt();
	field->info.kind = (Type::GraphicKind)value[1].asInt();
	id = value[2].asInt();

	if (field->info.kind == Type::GraphicKind::AnimationKind) field->info.id.anim = (Textures::Animations::ID)id;
	else field->info.id.sprite = (Textures::Sprites::ID)id;
}
void AnimationDataRule::save(AnimationData * field, Json::Value & value) {
	value.append(field->animation);
	value.append(field->info.kind);
	if (field->info.kind == Type::GraphicKind::AnimationKind) value.append(field->info.id.anim);
	else value.append(field->info.id.sprite);
}
bool AnimationDataRule::matchesJson(Json::Value & value) {
	if (!value.isArray() || value.size() != 3) return false;
	for (int i = 0; i < 3; ++i) {
		if (!value[i].isUInt()) return false;
	}

	bool validAnimation = Range::between(0, (int)Type::AnimationCount).contains(value[0].asInt());
	bool validGraphicKind = Range::between(0, (int)Type::GraphicKindCount).contains(value[1].asInt());
	if (!validGraphicKind || !validAnimation) return false;
	return true;
}




void EnemyDataRule::loadDefault(WorldData::EnemyData * field) { (*field) = WorldData::EnemyData(); }
void EnemyDataRule::load(WorldData::EnemyData * field, Json::Value & value) {
	field->type = static_cast<Type::Character>(value["type"].asInt());
	field->position.x = value["x"].asFloat();
	field->position.y = value["y"].asFloat();
}
void EnemyDataRule::save(WorldData::EnemyData * field, Json::Value & value) {
	value["type"] = field->type;
	value["x"]    = field->position.x;
	value["y"]    = field->position.y;
}
bool EnemyDataRule::matchesJson(Json::Value & value) {
	if (!(value.isMember("type") && value["type"].isInt())) return false;
	if (!(value.isMember("x")    && value["x"].isNumeric())) return false;
	if (!(value.isMember("y")    && value["y"].isNumeric())) return false;
	return true;
}




void InputRule::loadDefault(Input::ID * field) { (*field) = Input::ID(); }
void InputRule::load(Input::ID * field, Json::Value & value) {
	if (value.size() == 1) {
		(*field) = Input::ID((Input::Source)value[0].asInt());
	} else if (value.size() == 2) {
		(*field) = Input::ID((Input::Source)value[0].asInt(), value[1].asInt());
	} else if (value.size() == 3) {
		(*field) = Input::ID((Input::Source)value[0].asInt(), value[1].asInt(), value[2].asInt());
	}
}
void InputRule::save(Input::ID * field, Json::Value & value) {
	if (field->hasAdditionalCode()) {
		value.append(field->getSource());
		value.append(field->getInputCode());
		value.append(field->getAdditionalCode());
	} else if (field->hasCode()) {
		value.append(field->getSource());
		value.append(field->getInputCode());
	} else {
		value.append(field->getSource());
	}
}
bool InputRule::matchesJson(Json::Value & value) {
	if (!value.isArray()) return false;
	if (!Range::between(1, 4).contains(value.size())) return false;
	for (auto jsonInput : value) {
		if (!jsonInput.isNumeric()) return false;
	}
	
	return true;
}





ActionRule::ActionRule() : actionID(0), efficiency(0) {}
ActionRule::ActionRule(uint actionID, int efficiency) : actionID(actionID), efficiency(efficiency) {}
void ActionRule::loadDefault(PickupData::Action* field) { loadAction(field); }
void ActionRule::load(PickupData::Action* field, Json::Value & value) {
	efficiency = value.get("efficieny", 0).asUInt();
	actionID = value.get("ID", PickupData::Heal).asUInt();
	loadAction(field);
}
void ActionRule::loadAction(PickupData::Action* field) {
	int actionEfficiency = efficiency;

	switch (actionID) {
	case PickupData::Heal:
		*field = [actionEfficiency](EntityNode& entity) { /*entity.repair(actionEfficiency);*/ };
	case PickupData::Ammo:
		*field = [actionEfficiency](EntityNode& entity) { /*entity.getWeapon().addAmmo(actionEfficiency);*/ };
	}
}
void ActionRule::save(PickupData::Action* field, Json::Value & value) {} // TODO not savable
bool ActionRule::matchesJson(Json::Value & value) {
	if (!value.isObject()) return false;

	if (!(value.isMember("ID") && value["ID"].isUInt())) return false;
	if (!(value.isMember("efficiency") && value["efficiency"].isUInt())) return false;

	return true;
}
bool ActionRule::matches(PickupData::Action* field) { return false; }





AbilityRule::AbilityRule() {}
void AbilityRule::loadDefault(void * field) {
	AbilityEntityData* data = (AbilityEntityData*)(field);
	data->abilitiesID = Ability::NoAbility;
}
void AbilityRule::load(void * field, Json::Value & value) {
	AbilityEntityData* data = (AbilityEntityData*)(field);
	for (auto ability : value) {
		Ability id = (Ability)ability.get("ID", Ability::NoAbility).asUInt();
		data->abilitiesID |= id;
		data->abilities[id] = getAbility(id, ability["data"]);
	}
}
void AbilityRule::save(void * field, Json::Value & value) {}
bool AbilityRule::matchesJson(Json::Value & value) {
	if (!value.isArray()) return false;

	for (auto ability : value) {
		if (!ability.isObject()) return false;
		if (!ability.isMember("ID")) return false;
	}

	return true;
}
bool AbilityRule::matches(void * field) {
	return true;
}
BaseAbility* AbilityRule::getAbility(Ability abilityID, Json::Value & value) {
	BaseAbility* ability;
	switch (abilityID) {
	case Ability::NoAbility:
	case Ability::Interacter:
	case Ability::Pickuper:
	case Ability::Blocker:
	default:
		ability = nullptr;
		break;
	case Ability::Burner:
	{
		ability = new BurnerAbility();
		JsonFieldsInfo fields = {
			JsonFieldInfo("effect", &((BurnerAbility*)ability)->burnEffect, true, new IntegerRule(false))
		};
		JsonStructInfo<BurnerAbility> structInfo(fields, (BurnerAbility*)ability);
		JsonStructParser::jsonToStruct(structInfo, value);
		break;
	}
	case Ability::Damager:
	{
		ability = new DamagerAbility();
		JsonFieldsInfo fields = {
			JsonFieldInfo("hitpoints"      , &((DamagerAbility*)ability)->hitpoints      , true , new IntegerRule()),
			JsonFieldInfo("destroyOnDamage", &((DamagerAbility*)ability)->destroyOnDamage, false, new BoolRule(true)),
		};
		JsonStructInfo<DamagerAbility> structInfo(fields, (DamagerAbility*)ability);
		JsonStructParser::jsonToStruct(structInfo, value);
		break;
	}
	}
	return ability;
}



PropertyRule::PropertyRule() {}
void PropertyRule::loadDefault(void * field) {
	PropertyEntityData* data = (PropertyEntityData*)(field);
	data->propertiesID = Property::NoProperty;
}
void PropertyRule::load(void * field, Json::Value & value) {
	PropertyEntityData* data = (PropertyEntityData*)(field);
	for (auto property : value) {
		Property id = (Property)property.get("ID", Property::NoProperty).asUInt();
		data->propertiesID |= id;
		data->properties[id] = getProperty(id, property["data"]);
	}
}
void PropertyRule::save(void * field, Json::Value & value) {}
bool PropertyRule::matchesJson(Json::Value & value) {
	if (!value.isArray()) return false;

	for (auto property : value) {
		if (!property.isObject()) return false;
		if (!property.isMember("ID")) return false;
	}

	return true;
}
bool PropertyRule::matches(void * field) {
	return true;
}
BaseProperty* PropertyRule::getProperty(Property propertyID, Json::Value & value) {
	BaseProperty* property;
	switch (propertyID) {
	case Property::NoProperty:
	case Property::Interactible:
	case Property::Burnable:
	default:
		return nullptr;
	case Property::Damageable:
	{
		property = new DamageableProperty();
		JsonFieldsInfo fields = {
			JsonFieldInfo("hitpoints", &((DamageableProperty*)property)->hitpoints, true, new IntegerRule())
		};
		JsonStructInfo<DamageableProperty> structInfo(fields, (DamageableProperty*)property);
		JsonStructParser::jsonToStruct(structInfo, value);
		break;
	}
	case Property::Pickupable:
	{
		property = new PickupableProperty();
		JsonFieldsInfo fields = {
			JsonFieldInfo("action", &((PickupableProperty*)property)->pickupAction, true, new ActionRule())
		};
		JsonStructInfo<PickupableProperty> structInfo(fields, (PickupableProperty*)property);
		JsonStructParser::jsonToStruct(structInfo, value);
		break;
	}
	case Property::Blockable:
	{
		property = new BlockableProperty();
		JsonFieldsInfo fields = {
			JsonFieldInfo("destroyOnImpact", &((BlockableProperty*)property)->destroyOnImpact, false, new BoolRule(false))
		};
		JsonStructInfo<BlockableProperty> structInfo(fields, (BlockableProperty*)property);
		JsonStructParser::jsonToStruct(structInfo, value);
		break;
	}
	}
	return property;
}



EffectRule::EffectRule() {}
void EffectRule::loadDefault(void * field) {
	EffectEntityData* data = (EffectEntityData*)(field);
	data->effectsID = Effect::NoEffect;
}
void EffectRule::load(void * field, Json::Value & value) {
	EffectEntityData* data = (EffectEntityData*)(field);
	for (auto effect : value) {
		Effect id = (Effect)effect.get("ID", Effect::NoEffect).asUInt();
		data->effectsID |= id;
		data->effects[id] = getEffect(id, effect["data"]);
	}
}
void EffectRule::save(void * field, Json::Value & value) {}
bool EffectRule::matchesJson(Json::Value & value) {
	if (!value.isArray()) return false;

	for (auto effect : value) {
		if (!effect.isObject()) return false;
		if (!effect.isMember("ID")) return false;
	}

	return true;
}
bool EffectRule::matches(void * field) {
	return true;
}
BaseEffect* EffectRule::getEffect(Effect effectID, Json::Value & value) {
	BaseEffect* effect;
	switch (effectID) {
	case Effect::NoEffect:
	case Effect::Burning:
	default:
		effect = nullptr;
		break;
	}
	return effect;
}

ControlBindingRule::ControlBindingRule(bool pressedDefault) : bindingStruct() {
	ActionProperty& actionProperty = bindingStruct.property;
	JsonFieldsInfo actionField = {
		JsonFieldInfo("strength", &actionProperty.strength      , false, new RangeRule<float>(Range::positive<float>())),
		JsonFieldInfo("duration", &actionProperty.duration      , false, new RangeRule<sf::Time>(Range::positive<sf::Time>())),
		JsonFieldInfo("pressed" , &actionProperty.requirePressed, false, new BoolRule(pressedDefault))
	};
	JsonFieldsInfo bindingFields = {
		JsonFieldInfo("actionId", &bindingStruct.actionId, true , new IntegerRule(false)),
		JsonFieldInfo("inputs"  , &bindingStruct.inputs  , true , new VectorRule<Input::ID>(new InputRule())),
		JsonFieldInfo("property", &bindingStruct.property, false, new ComplexRule<ActionProperty>(JsonStructInfo(actionField, &actionProperty)))
	};
	JsonStructInfo<BindingConfiguration> bindingStructInfo(bindingFields, &bindingStruct);
	subRule = std::unique_ptr<ComplexRule<BindingConfiguration>>(new ComplexRule(JsonStructInfo(bindingFields, &bindingStruct)));
}
