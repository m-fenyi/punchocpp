#ifndef DATATABLES_HPP
#define DATATABLES_HPP

#include "TranslationTable.hpp"

#include "DataStruct.hpp"


#include <vector>


namespace Json {
	class Value;
}

namespace {
	TranslationTable UITranslationData;
}


TranslationTable& initializeUITranslationData();
void initializeUITranslationData(std::string& locale, Json::Value& translation);
void initializeUITranslationData(std::string& locale, std::string& tagPrefix, Json::Value& translation);

#endif // DATATABLES_HPP
