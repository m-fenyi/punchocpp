#ifndef NODEREPOSITORY_HPP
#define NODEREPOSITORY_HPP
#pragma once

#include "Repository.hpp"

#include "CharacterRepository.hpp"
#include "EnvironmentRepository.hpp"
#include "ParticleRepository.hpp"
#include "PickupRepository.hpp"
#include "ProjectileRepository.hpp"
#include "WeaponRepository.hpp"
#include "WorldRepository.hpp"

class GameDataRepository : public ReadOnlyRepository {
public:
	void loadDefault() noexcept override;
	bool isLoaded() const noexcept override;

	CharacterEntityData& get(Type::Character);
	EnvironmentData&     get(Type::Environment);
	ParticleData&        get(Type::Particle);
	PickupData&          get(Type::Pickup);
	ProjectileData&      get(Type::Projectile);
	WeaponData&          get(Type::Weapon);
	WorldData&           get(Type::World);

	const CharacterEntityData& get(Type::Character) const;
	const EnvironmentData&     get(Type::Environment) const;
	const ParticleData&        get(Type::Particle) const;
	const PickupData&          get(Type::Pickup) const;
	const ProjectileData&      get(Type::Projectile) const;
	const WeaponData&          get(Type::Weapon) const;
	const WorldData&           get(Type::World) const;

	bool has(Type::Character) const;
	bool has(Type::Environment) const;
	bool has(Type::Particle) const;
	bool has(Type::Pickup) const;
	bool has(Type::Projectile) const;
	bool has(Type::Weapon) const;
	bool has(Type::World) const;

protected:
	void loadData() override;

private:
	CharacterRepository   characterRepo;
	EnvironmentRepository environmentRepo;
	ParticleRepository    particleRepo;
	PickupRepository      pickupRepo;
	ProjectileRepository  projectileRepo;
	WeaponRepository      weaponRepo;
	WorldRepository       worldRepo;
};


#endif // NODEREPOSITORY_HPP
