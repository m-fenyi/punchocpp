#ifndef JSONFIELDRULE_HPP
#define JSONFIELDRULE_HPP

#include <Utilities/Type.hpp>
#include <Utilities/Math.hpp>

#include <Configuration/InputConfiguration.hpp>

#include <Interaction/Entity.hpp>

#include <Input/Input.hpp>

#include "JsonStructInfo.hpp"

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Time.hpp>

#include <json/json.h>


struct Resolution;

class JsonFieldRule {
public:
	virtual void loadDefault(void* field) = 0;
	virtual void load(void* field, Json::Value& value) = 0;
	virtual void save(void* field, Json::Value& value) = 0;
	virtual bool matchesJson(Json::Value& value) = 0;
	virtual bool matches(void* field) = 0;
};

template<class FieldClass>
class JsonFieldClassRule : public JsonFieldRule {
public:
	void loadDefault(void* field) override {
		FieldClass* fieldClass = static_cast<FieldClass*>(field);
		loadDefault(fieldClass);
	}
	void load(void* field, Json::Value& value) override {
		FieldClass* fieldClass = static_cast<FieldClass*>(field);
		load(fieldClass, value);
	}
	void save(void* field, Json::Value& value) override {
		FieldClass* fieldClass = static_cast<FieldClass*>(field);
		save(fieldClass, value);
	}
	bool matches(void* field) override {
		FieldClass* fieldClass = static_cast<FieldClass*>(field);
		return matches(fieldClass);
	}
	virtual bool matchesJson(Json::Value& value) = 0;
protected:
	virtual void loadDefault(FieldClass* field) = 0;
	virtual void load(FieldClass* field, Json::Value& value) = 0;
	virtual void save(FieldClass* field, Json::Value& value) = 0;
	virtual bool matches(FieldClass* field) { return true; }
};


namespace {
	template<class Value = void>
	Value getValue(const Json::Value& value) {}
	template<>
	int getValue<int>(const Json::Value& value) { return value.asInt(); }
	template<>
	float getValue<float>(const Json::Value& value) { return value.asFloat(); }
	template<>
	sf::Time getValue<sf::Time>(const Json::Value& value) { return sf::milliseconds(value.asInt()); }

	template<class Value>
	void setValue(Json::Value& jsonValue, const Value& value) { jsonValue = value; }
	template<>
	void setValue<sf::Time>(Json::Value& jsonValue, const sf::Time& value) { jsonValue = value.asMilliseconds(); }
}


// ### Simple Rules ###

class BoolRule : public JsonFieldClassRule<bool> {
public:
	BoolRule();
	BoolRule(bool default);
protected:
	void loadDefault(bool* field) override;
	void load(bool* field, Json::Value& value) override;
	void save(bool* field, Json::Value& value) override;
	bool matchesJson(Json::Value& value) override;
private:
	bool default;
};

class ColorRule : public JsonFieldClassRule<sf::Color> {
public:
	ColorRule();
	ColorRule(sf::Color default);
protected:
	void loadDefault(sf::Color* field) override;
	void load(sf::Color* field, Json::Value& value) override;
	void save(sf::Color* field, Json::Value& value) override;
	bool matchesJson(Json::Value& value) override;
private:
	sf::Color default;
};

template<class Value, class BoundValue>
class BaseBoundedRule : public JsonFieldClassRule<Value>{
public:
	BaseBoundedRule(bool isSigned = true) : bounds(Range::any<BoundValue>()), default() { if (!isSigned) bounds = Range::positive<BoundValue>(); }
	BaseBoundedRule(Value default) : bounds(Range::any<BoundValue>()), default(default) {}
	BaseBoundedRule(Math::Range<BoundValue> bounds, Value default) : bounds(bounds), default(default) {}
protected:
	void loadDefault(Value* field) override { (*field) = default; }
	bool matches(Value* field) override { return bounds.contains(*field); }
protected:
	Math::Range<BoundValue> bounds;
	Value default;
};

template<class Value>
class BoundedRule : public BaseBoundedRule<Value, Value> {
public:
	using BaseBoundedRule<Value, Value>::BaseBoundedRule;
};

template<class Value>
class NumericRule : public BoundedRule<Value> {
public:
	using BoundedRule<Value>::BoundedRule;
protected:
	void load(Value* field, Json::Value& value) override { (*field) = getValue<Value>(value); }
	void save(Value* field, Json::Value& value) override { value = *field; }
	bool matchesJson(Json::Value& value) override {
		if (!value.isNumeric()) return false;
		if (!bounds.contains(getValue<Value>(value))) return false;
		return true;
	}
};
using IntegerRule = NumericRule<int>;
using FloatRule = NumericRule<float>;
class TimeRule : public BoundedRule<sf::Time> {
public:
	using BoundedRule<sf::Time>::BoundedRule;
protected:
	void load(sf::Time* field, Json::Value& value) override { (*field) = sf::milliseconds(getValue<int>(value)); }
	void save(sf::Time* field, Json::Value& value) override { value = field->asMilliseconds(); }
	bool matchesJson(Json::Value& value) override {
		if (!value.isNumeric()) return false;
		if (!bounds.contains(sf::milliseconds(getValue<int>(value)))) return false;
		return true;
	}
};

template<class Value>
class PointRule : public BoundedRule<sf::Vector2<Value>> {
public:
	using BoundedRule<sf::Vector2<Value>>::BoundedRule;
protected:
	void load(sf::Vector2<Value>* field, Json::Value& value) override {
		field->x = getValue<Value>(value[0]);
		field->y = getValue<Value>(value[1]);
	}
	void save(sf::Vector2<Value>* field, Json::Value& value) override {
		value.append(field->x);
		value.append(field->y);
	}
	bool matchesJson(Json::Value& value) override {
		if (value.size() != 2) return false;
		for (int i = 0; i < 2; ++i) {
			if (!value[i].isNumeric()) return false;
		}
		sf::Vector2<Value> point(getValue<Value>(value[0]), getValue<Value>(value[1]));
		if (!bounds.contains(point)) return false;
		return true;
	}
};
using IntPointRule = PointRule<int>;
using FloatPointRule = PointRule<float>;

template<class Value>
class RangeRule : public BaseBoundedRule<Math::Range<Value>, Value> {
public:
	using BaseBoundedRule<Math::Range<Value>, Value>::BaseBoundedRule;
protected:
	void load(Math::Range<Value>* field, Json::Value& value) override {
		if (value.isMember("min")) field->setMin(getValue<Value>(value["min"]));
		if (value.isMember("max")) field->setMax(getValue<Value>(value["max"]));
	}
	void save(Math::Range<Value>* field, Json::Value& value) override {
		if (field->hasMin()) setValue<Value>(value["min"], field->getMin());
		if (field->hasMax()) setValue<Value>(value["max"], field->getMax());
	}
	bool matchesJson(Json::Value& value) override {
		if (!value.isMember("min") && !value.isMember("max")) return false;
		if (value.isMember("min") && !value.isNumeric()) return false;
		if (value.isMember("max") && !value.isNumeric()) return false;

		Math::Range<Value> range;
		if (value.isMember("min")) range.setMin(getValue<Value>(value["min"]));
		if (value.isMember("max")) range.setMax(getValue<Value>(value["max"]));
		if (!bounds.contains(range)) return false;

		return true;
	}
};

class ResolutionRule : public JsonFieldClassRule<Resolution> {
protected:
	void loadDefault(Resolution * field) override;
	void load(Resolution * field, Json::Value & value) override;
	void save(Resolution * field, Json::Value & value) override;
	bool matchesJson(Json::Value & value) override;
};

template<class Value>
class RectRule : public BoundedRule<sf::Rect<Value>> {
public:
	using BoundedRule<sf::Rect<Value>>::BoundedRule;
protected:
	void load(sf::Rect<Value>* field, Json::Value& value) override {
		field->left   = getValue<Value>(value[0]);
		field->top    = getValue<Value>(value[1]);
		field->width  = getValue<Value>(value[2]);
		field->height = getValue<Value>(value[3]);
	}
	void save(sf::Rect<Value>* field, Json::Value& value) override {
		value.append(field->left);
		value.append(field->top);
		value.append(field->width);
		value.append(field->height);
	}
	bool matchesJson(Json::Value& value) override {
		if (value.size() != 4) return false;
		for (int i = 0; i < 4; ++i) {
			if (!value[i].isNumeric()) return false;
		}
		sf::Rect<Value> rect;
		rect.left   = getValue<Value>(value[0]);
		rect.top    = getValue<Value>(value[1]);
		rect.width  = getValue<Value>(value[2]);
		rect.height = getValue<Value>(value[3]);
		if (!bounds.contains(rect)) return false;

		return true;
	}
};
using IntRectRule = RectRule<int>;
using FloatRectRule = RectRule<float>;

class StringRule : public JsonFieldClassRule<std::string> {
public:
	StringRule(std::string default = "", bool allowEmpty = true);
protected:
	void loadDefault(std::string* field) override;
	void load(std::string* field, Json::Value& value) override;
	void save(std::string* field, Json::Value& value) override;
	bool matchesJson(Json::Value& value) override;
	bool matches(std::string* field) override;
private:
	bool allowEmpty;
	std::string default;
};

class AnimationDataRule : public JsonFieldClassRule<AnimationData> {
protected:
	void loadDefault(AnimationData* field) override;
	void load(AnimationData* field, Json::Value& value) override;
	void save(AnimationData* field, Json::Value& value) override;
	bool matchesJson(Json::Value& value) override;
};

class EnemyDataRule : public JsonFieldClassRule<WorldData::EnemyData> {
protected:
	void loadDefault(WorldData::EnemyData* field) override;
	void load(WorldData::EnemyData* field, Json::Value& value) override;
	void save(WorldData::EnemyData* field, Json::Value& value) override;
	bool matchesJson(Json::Value& value) override;
};

class ActionRule : public JsonFieldClassRule<PickupData::Action> {
public:
	ActionRule();
	ActionRule(uint actionID, int efficiency);
protected:
	void loadDefault(PickupData::Action* field) override;
	void load(PickupData::Action* field, Json::Value& value) override;
	void save(PickupData::Action* field, Json::Value& value) override;
	bool matchesJson(Json::Value& value) override;
	bool matches(PickupData::Action* field) override;
private:
	void loadAction(PickupData::Action* field);
private:
	uint actionID;
	int efficiency;
};

class InputRule : public JsonFieldClassRule<Input::ID> {
protected:
	void loadDefault(Input::ID* field) override;
	void load(Input::ID* field, Json::Value & value) override;
	void save(Input::ID* field, Json::Value & value) override;
	bool matchesJson(Json::Value & value) override;
};

// ### Complex Rules ###

template<class Complex>
class ComplexRule : public JsonFieldClassRule<Complex> {
public:
	ComplexRule(JsonStructInfo<Complex>& structInfo) : structInfo(structInfo) {}

protected:
	void loadDefault(Complex* field) override {
		for (JsonFieldInfo& field : structInfo.fieldInfos) {
			field.rule->loadDefault(field.field);
		}
		*(field) = *structInfo.structData;
	}
	void load(Complex* field, Json::Value & value) override {
		for (JsonFieldInfo& field : structInfo.fieldInfos) {
			Json::Value subValue = File::JSON::getValue(value, field.name);
			if (field.rule->matchesJson(subValue)) {
				field.rule->load(field.field, subValue);
			} else {
				field.rule->loadDefault(field.field);
			}
		}
		*(field) = *structInfo.structData;
	}
	void save(Complex* field, Json::Value & value) override {
		*structInfo.structData = *field;
		for (JsonFieldInfo& field : structInfo.fieldInfos) {
			field.rule->save(field.field, File::JSON::getValue(value, field.name));
		}
	}
	bool matchesJson(Json::Value & value) override {
		for (JsonFieldInfo& fieldInfo : structInfo.fieldInfos) {
			if (fieldInfo.rule->matchesJson(File::JSON::getValue(value, fieldInfo.name))) {
				return true;
			}
		}
		return false;
	}
	bool matches(Complex* field) override {
		for (JsonFieldInfo& fieldInfo : structInfo.fieldInfos) {
			if (!fieldInfo.rule->matches(fieldInfo.field)) {
				return false;
			}
		}
		return true;
	}
		
private:
	JsonStructInfo<Complex> structInfo;
};

template<class Value>
class VectorRule : public JsonFieldClassRule<std::vector<Value>> {
protected:
	VectorRule(size_t defaultSize = 0) : subRule(), defaultSize(defaultSize) {}
public:
	VectorRule(JsonFieldClassRule<Value>* rule, size_t defaultSize = 0) : subRule(rule), defaultSize(defaultSize) {}

protected:
	void loadDefault(std::vector<Value>* field) override {
		field->clear();
		for (size_t i = 0; i < defaultSize; ++i) {
			Value subValue;
			subRule->loadDefault((void*)&subValue);
			field->push_back(subValue);
		}
	}
	void load(std::vector<Value>* field, Json::Value & value) override {
		field->clear();
		for (Json::Value& jsonValue : value) {
			if(subRule->matchesJson(jsonValue)) {
				Value subValue;
				subRule->load((void*)&subValue, jsonValue);
				field->push_back(subValue);
			}
		}
	}
	void save(std::vector<Value>* field, Json::Value & value) override {
		for (Value& subValue : *field) {
			if (subRule->matches((void*)&subValue)) {
				value.append(Json::Value());
				subRule->save((void*)&subValue, value[value.size() - 1]);
			}
		}
	}
	bool matchesJson(Json::Value & value) override {
		if (!value.isArray()) return false;
		for (Json::Value& jsonValue : value) {
			if (subRule->matchesJson(jsonValue)) {
				return true;
			}
		}
		return false;
	}
	bool matches(std::vector<Value>* field) override {
		for (Value& value : *field) {
			if (subRule->matches((void*)&value)) {
				return true;
			}
		}
		return false;
	}
protected:
	std::unique_ptr<JsonFieldClassRule<Value>> subRule;
private:
	size_t defaultSize;
};

class ControlBindingRule : public VectorRule<BindingConfiguration> {
public:
	ControlBindingRule(bool pressedDefault);

private:
	BindingConfiguration bindingStruct;
};


// ### Tree Rules ###
class AbilityRule : public JsonFieldRule {
public:
	AbilityRule();
	void loadDefault(void* field) override;
	void load(void* field, Json::Value& value) override;
	void save(void* field, Json::Value& value) override;
	bool matchesJson(Json::Value& value) override;
	bool matches(void* field) override;
private:
	BaseAbility* getAbility(Ability abilityID, Json::Value& value);
};

class PropertyRule : public JsonFieldRule {
public:
	PropertyRule();
	void loadDefault(void* field) override;
	void load(void* field, Json::Value& value) override;
	void save(void* field, Json::Value& value) override;
	bool matchesJson(Json::Value& value) override;
	bool matches(void* field) override;
private:
	BaseProperty* getProperty(Property propertyID, Json::Value& value);
};

class EffectRule : public JsonFieldRule {
public:
	EffectRule();
	void loadDefault(void* field) override;
	void load(void* field, Json::Value& value) override;
	void save(void* field, Json::Value& value) override;
	bool matchesJson(Json::Value& value) override;
	bool matches(void* field) override;
private:
	BaseEffect* getEffect(Effect effectID, Json::Value& value);
};

#endif // JSONFIELDRULE_HPP
