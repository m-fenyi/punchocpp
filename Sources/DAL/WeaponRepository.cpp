#include "WeaponRepository.hpp"
#include "Filepath.hpp"

JsonStructInfo<WeaponData> WeaponRepository::getStructInfo(WeaponData & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo( "projectileID", &data.projectileID, true, new IntegerRule()),
		JsonFieldInfo( "spriteID"    , &data.spriteID    , true, new IntegerRule()),
		JsonFieldInfo( "soundID"     , &data.soundID     , true, new IntegerRule()),
		JsonFieldInfo( "rateoffire"  , &data.rateOfFire  , true, new TimeRule()),
		JsonFieldInfo( "spread"      , &data.spread      , true, new IntegerRule()),
		JsonFieldInfo( "accuracy"    , &data.accuracy    , true, new FloatRule()),
		JsonFieldInfo( "kickback"    , &data.kickback    , true, new FloatRule()),
	};
	return JsonStructInfo<WeaponData>(fields, &data);
}

void WeaponRepository::loadDefault() noexcept {}

const std::string & WeaponRepository::getFileName() const {
	return FilePath::Json::weapon;
}
