#ifndef DATATYPE_HPP
#define DATATYPE_HPP

namespace Type {
	enum Animation {
		Idle,
		Standing,
		Moving,
		Attacking,
		Dead,

		AnimationCount
	};

	enum GraphicKind {
		SpriteKind,
		AnimationKind,

		GraphicKindCount
	};

	enum Character {
		CharacterBlue,
		CharacterRed,
		CharacterGreen,

		CharacterCount
	};

	enum Effect {
		FastBurn,
		SlowBurn,

		EffectCount,
		NoEffect,
	};

	enum Environment {
		Rock,
		TNTBox,
		WoodenBox,
		WoodLog,
		Cactus,
		Fire,

		EnvironmentCount,
		UnknownEnvironment
	};

	enum Explosion {
		ExplosionNormal,
		ExplosionEntity,

		ExplosionCount
	};
	
	enum Particle {
		Propellant,
		Smoke,

		ParticleCount
	};

	enum Pickup {
		HealthRefill,
		MissileRefill,

		PickupCount
	};

	enum Projectile {
		Bullet,
		Missile,

		ProjectileCount
	};

	enum Weapon {
		AlliedGun,
		EnemyGun,
		MissileLauncher,

		WeaponCount
	};

	enum World {
		WorldBonfire,
		WorldAnger,

		WorldCount
	};
}

#endif // DATATYPE_HPP