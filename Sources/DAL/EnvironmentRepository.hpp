#ifndef ENVIRONMENTREPOSITORY_HPP
#define ENVIRONMENTREPOSITORY_HPP


#include "JsonStructRepository.hpp"
#include "DataStruct.hpp"
#include "DataType.hpp"

class EnvironmentRepository : public ReadOnlyJsonStructRepository<Type::Environment, EnvironmentData> {
public:
	JsonStructInfo<EnvironmentData> getStructInfo(EnvironmentData& data) const override;
	void loadDefault() noexcept override;
	const std::string& getFileName() const override;
};

#endif // ENVIRONMENTREPOSITORY_HPP

