#include "JsonMapParser.hpp"

#include <Utilities/File.hpp>

JsonMapParser::StringMap JsonMapParser::parseFile(const std::string & fileName) {
	Json::Value jsonValue;
	if (!File::JSON::load(fileName, jsonValue)) {
		//TODO throw
	}
	return parseJson(jsonValue);
}

JsonMapParser::StringMap JsonMapParser::parseJson(Json::Value & value) {
	std::map<std::string, std::string> map;

	for (auto field : value.getMemberNames()) {
		if (value[field].isObject()) {
			parseValue(value[field], field, map);
		} else if (value[field].isString()) {
			map[field] = value[field].asString();
		}
	}

	return map;
}

void JsonMapParser::parseValue(Json::Value & value, const std::string & prefix, StringMap & map) {
	for (auto field : value.getMemberNames()) {
		if (value[field].isObject()) {
			parseValue(value[field], prefix + '.' + field, map);
		} else if (value[field].isString()) {
			map[prefix + '.' + field] = value[field].asString();
		}
	}
}

