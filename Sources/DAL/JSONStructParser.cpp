#include "JsonStructParser.hpp"

void JsonStructParser::readField(JsonFieldInfo & fieldInfo, Json::Value & value) {
	bool valid = fieldInfo.rule->matchesJson(value);
	bool existsValid = !value.isNull() && valid;
	bool missingNotRequired = value.isNull() && !fieldInfo.required;
	bool existsNotValid = !value.isNull() && !valid;

	if (existsValid) {
		fieldInfo.rule->load(fieldInfo.field, value);
	} else if (missingNotRequired || existsNotValid) {
		fieldInfo.rule->loadDefault(fieldInfo.field);
	} else {
		throw ParseException(fieldInfo.name, "Could not be initialized, value is required");
	}
}
