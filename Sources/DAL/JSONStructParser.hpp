#ifndef JSONSTRUCTPARSER_HPP
#define JSONSTRUCTPARSER_HPP

#include <string>
#include <map>

#include <json/json.h>

#include "JsonStructInfo.hpp"
#include "JsonFieldRule.hpp"

#include <Utilities/StaticClass.hpp>
#include <Utilities/File.hpp>
#include <Utilities/Exceptions.hpp>
#include <Utilities/Log.hpp>


class JsonStructParser : public StaticClass {
public:
	template<class ID, class T>
	static std::map<ID, T> parseToMap(JsonStructInfo<T>& structInfo, const std::string& fileName) {
		Json::Value jsonValue;
		if (!File::JSON::load(fileName, jsonValue)) {
			//TODO throw
		}
		return jsonToMap<ID, T>(structInfo, jsonValue);
	}
	template<class T>
	static std::map<std::string, T> parseToNameMap(JsonStructInfo<T>& structInfo, const std::string& fileName) {
		Json::Value jsonValue;
		if (!File::JSON::load(fileName, jsonValue)) {
			//TODO throw
		}
		return jsonToNameMap<T>(structInfo, jsonValue);
	}
	
	template<class T>
	static std::vector<T> parseToVector(JsonStructInfo<T>& structInfo, const std::string& fileName) {
		Json::Value jsonValue;
		if (!File::JSON::load(fileName, jsonValue)) {
			//TODO throw
		}
		return jsonToVector<T>(structInfo, jsonValue);
	}
	
	template<class T>
	static T parseToStruct(JsonStructInfo<T>& structInfo, const std::string& fileName) {
		Json::Value jsonValue;
		if (!File::JSON::load(fileName, jsonValue)) {
			//TODO throw
		}
		jsonToStruct<T>(structInfo, jsonValue);
		return *structInfo.structData;
	}





	template<class ID, class T>
	static std::map<ID, T> jsonToMap(JsonStructInfo<T>& structInfo, Json::Value& value) {
		std::map<ID, T> structs;

		for (unsigned i = 0; i < value.size(); ++i) {
			ID structID = (ID)i;
			jsonToStruct(structInfo, value[i], &structID);
			structs[structID] = *structInfo.structData;
		}

		return structs;
	}
	
	template<class T>
	static std::map<std::string, T> jsonToNameMap(JsonStructInfo<T>& structInfo, Json::Value& value) {
		std::map<std::string, T> structs;
		
		for (auto memberName : value.getMemberNames()) {
			std::string structID = memberName;
			jsonToStruct(structInfo, value[memberName]);
			structs[structID] = *structInfo.structData;
		}

		return structs;
	}

	template<class T>
	static std::vector<T> jsonToVector(JsonStructInfo<T>& structInfo, Json::Value& value) {
		std::vector<T> structs(value.size());

		for (unsigned i = 0; i < value.size(); ++i) {
			jsonToStruct(structInfo, value[i]);
			structs[i] = *structInfo.structData;
		}

		return structs;
	}

	template<class ID = unsigned, class T>
	static void jsonToStruct(JsonStructInfo<T>& structInfo, Json::Value& value, ID* id) {
		if (value.isMember("ID")) {
			*id = static_cast<ID>(value.get("ID", 0).asUInt());
		}
		jsonToStruct(structInfo, value);
	}

	template<class T>
	static void jsonToStruct(JsonStructInfo<T>& structInfo, Json::Value& value) {
		*structInfo.structData = T();
		for (JsonFieldInfo& fieldInfo : structInfo.fieldInfos) {
			try {
				readField(fieldInfo, File::JSON::getValue(value, fieldInfo.name));
			} catch (const ParseException& e) {
				Log::e() << "JsonStructParser::parseStruct() : " << e.getMessage() << std::endl;
			}
		}
	}

private:
	static void readField(JsonFieldInfo& fieldInfo, Json::Value& value);
};

#endif // JSONSTRUCTPARSER_HPP