#include "PickupRepository.hpp"
#include "Filepath.hpp"

JsonStructInfo<PickupData> PickupRepository::getStructInfo(PickupData & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo( "spriteID"  , &data.sprite, true , new IntegerRule()),
		JsonFieldInfo( "abilities" , &data       , false, new AbilityRule()),
		JsonFieldInfo( "properties", &data       , false, new PropertyRule()),
		JsonFieldInfo( "effects"   , &data       , false, new EffectRule())
	};
	return JsonStructInfo<PickupData>(fields, &data);
}

void PickupRepository::loadDefault() noexcept {}

const std::string & PickupRepository::getFileName() const {
	return FilePath::Json::pickup;
}
