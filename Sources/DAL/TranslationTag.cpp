#include "TranslationTag.hpp"
#include <Utilities/Utilities.hpp>

std::string TranslationTag::Switch::getChoice(const std::string& switchTag, int index) {
	return switchTag + "." + toString(index);
}

std::vector<std::string> TranslationTag::Switch::getChoices(const std::string& switchTag, int choiceCount) {
	std::vector<std::string> choices;
	for (int i = 1; i <= choiceCount; ++i) {
		choices.push_back(getChoice(switchTag, i));
	}
	return choices;
}
