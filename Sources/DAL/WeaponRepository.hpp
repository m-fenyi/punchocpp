#ifndef WEAPONREPOSITORY_HPP
#define WEAPONREPOSITORY_HPP


#include "JsonStructRepository.hpp"
#include "DataStruct.hpp"
#include "DataType.hpp"

class WeaponRepository : public ReadOnlyJsonStructRepository<Type::Weapon, WeaponData> {
public:
	JsonStructInfo<WeaponData> getStructInfo(WeaponData& data) const override;
	void loadDefault() noexcept override;
	const std::string& getFileName() const override;
};

#endif // WEAPONREPOSITORY_HPP

