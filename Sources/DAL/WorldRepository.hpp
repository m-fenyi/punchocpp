#ifndef WORLDREPOSITORY_HPP
#define WORLDREPOSITORY_HPP


#include "JsonStructRepository.hpp"
#include "DataStruct.hpp"
#include "DataType.hpp"

class WorldRepository : public ReadOnlyJsonStructRepository<Type::World, WorldData> {
public:
	JsonStructInfo<WorldData> getStructInfo(WorldData& data) const override;
	void loadDefault() noexcept override;
	const std::string& getFileName() const override;
};

#endif // WORLDREPOSITORY_HPP

