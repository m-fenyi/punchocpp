#include "EnvironmentRepository.hpp"
#include "Filepath.hpp"


JsonStructInfo<EnvironmentData> EnvironmentRepository::getStructInfo(EnvironmentData & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo( "animationPack", &data.packData    , true , new VectorRule(new AnimationDataRule())),
		JsonFieldInfo( "abilities"    , &data             , false, new AbilityRule()),
		JsonFieldInfo( "properties"   , &data             , false, new PropertyRule()),
		JsonFieldInfo( "effects"      , &data             , false, new EffectRule())
	};
	return JsonStructInfo<EnvironmentData>(fields, &data);
}

void EnvironmentRepository::loadDefault() noexcept {}

const std::string & EnvironmentRepository::getFileName() const {
	return FilePath::Json::environment;
}
