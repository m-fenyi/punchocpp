#ifndef TRANSLATIONTABLE_HPP
#define TRANSLATIONTABLE_HPP

#include <map>
#include <vector>
#include <string>

using TranslationMap = std::map<std::string, std::map<std::string, std::string>>;

class TranslationTable {
public:
	TranslationTable();

	void addTranslation(const std::string& locale, const std::string& tag, const std::string& translation);
	void setLocale(const std::string& locale);
	
	const std::string& getTranslation(const std::string& tag);
	const std::string& getLocale();

	std::vector<std::string> getLocales();

private:
	TranslationMap translation;
	std::string locale;
};

#endif // TRANSLATIONTABLE_HPP