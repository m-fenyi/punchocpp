#include "ProjectileRepository.hpp"
#include "Filepath.hpp"

JsonStructInfo<ProjectileData> ProjectileRepository::getStructInfo(ProjectileData & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo( "speed"       , &data.speed       , true , new FloatRule()),
		JsonFieldInfo( "areaofeffect", &data.aeraofeffect, true , new FloatRule()),
		JsonFieldInfo( "bounciness"  , &data.bounciness  , true , new FloatRule()),
		JsonFieldInfo( "homing"      , &data.homing      , true , new BoolRule()),
		JsonFieldInfo( "explosive"   , &data.explosive   , true , new BoolRule()),
		JsonFieldInfo( "lifetime"    , &data.lifetime    , true , new TimeRule()),
		JsonFieldInfo( "spriteID"    , &data.sprite      , true , new IntegerRule()),
		JsonFieldInfo( "abilities"   , &data             , false, new AbilityRule()),
		JsonFieldInfo( "properties"  , &data             , false, new PropertyRule()),
		JsonFieldInfo( "effects"     , &data             , false, new EffectRule())
	};
	return JsonStructInfo<ProjectileData>(fields, &data);
}

void ProjectileRepository::loadDefault() noexcept {}

const std::string & ProjectileRepository::getFileName() const {
	return FilePath::Json::projectile;
}
