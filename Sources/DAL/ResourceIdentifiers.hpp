#ifndef RESOURCEIDENTIFIERS_HPP
#define RESOURCEIDENTIFIERS_HPP

#include "ResourceHolder.hpp"

namespace sf {
	class Texture;
	class Sprite;
	class Font;
	class Shader;
	class SoundBuffer;
}
class Animation;


namespace Textures {
	enum ID {
		Buttons           = 0,
		ControllerButtons = 1,

		Entities          = 2,
		Weapons           = 3,
		Environment       = 4,
		Explosion         = 5,

		GroundDesert      = 33,

		Particle1         = 65,
		Particle2         = 66,

		TitleScreen       = 128,

		Count             = 10
	};
	namespace Sprites {
		enum ID {
			UIButtonNormal       = 0,
			UIButtonSelected     = 1,
			UIButtonPressed      = 2,

			XboxControllerIcon1  = 3,
			XboxControllerIcon2  = 5,
			XboxControllerIcon3  = 7,
			XboxControllerIcon4  = 9,
			XboxControllerIcon5  = 11,
			XboxControllerIcon6  = 13,
			XboxControllerIcon7  = 15,
			XboxControllerIcon8  = 17,
			XboxControllerIcon9  = 19,
			XboxControllerIcon10 = 21,
			XboxControllerIcon11 = 23,
			XboxControllerIcon12 = 25,
			XboxControllerIcon13 = 27,
			XboxControllerIcon14 = 29,

			DSControllerIcon1    = 4,
			DSControllerIcon2    = 6,
			DSControllerIcon3    = 8,
			DSControllerIcon4    = 10,
			DSControllerIcon5    = 12,
			DSControllerIcon6    = 14,
			DSControllerIcon7    = 16,
			DSControllerIcon8    = 18,
			DSControllerIcon9    = 20,
			DSControllerIcon10   = 22,
			DSControllerIcon11   = 24,
			DSControllerIcon12   = 26,
			DSControllerIcon13   = 28,
			DSControllerIcon14   = 30,
			
			JoystickIconLD       = 31,
			JoystickIconLL       = 32,
			JoystickIconLR       = 33,
			JoystickIconLU       = 34,
			JoystickIconLB       = 35,
			JoystickIconRD       = 36,
			JoystickIconRL       = 37,
			JoystickIconRR       = 38,
			JoystickIconRU       = 39,
			JoystickIconRB       = 40,

			Character1Dead       = 70,
			Character2Dead       = 71,
			Character3Dead       = 72,
			Animal1Dead          = 73,

			Pickup1              = 74,
			Pickup2              = 75,

			Projectile1          = 76,
			Projectile2          = 77,

			Weapon1              = 78,
			Weapon2              = 79,
			Weapon3              = 80,
			Weapon4              = 81,

			Rock                 = 84,
			TNTBox               = 85,
			WoodenBox            = 86,
			WoodLog              = 87,
			Cactus               = 88,


			TitleScreen          = 255,

			Count = 59
		};
	}
	namespace Animations{
		enum ID {
			Explosion          = 0,
			Fire               = 1,

			Character1Standing = 2,
			Character2Standing = 3,
			Character3Standing = 4,
			Animal1Standing    = 5,

			Character1Moving   = 6,
			Character2Moving   = 7,
			Character3Moving   = 8,
			Animal1Moving      = 9,

			Animal1Attacking   = 10,

			Character1Sitting  = 11,

			Count              = 12
		};
	}
}

namespace Shaders {
	enum ID {
		BrightnessPass,
		DownSamplePass,
		GaussianBlurPass,
		AddPass,
		NoisePass
	};
}

namespace Fonts {
	enum ID {
		Main,
	};
}

namespace SoundEffect {
	enum ID {
		AlliedGunfire,
		EnemyGunfire,
		Explosion1,
		Explosion2,
		LaunchMissile,
		CollectPickup,
		ButtonPressed,
		ButtonSelected
	};
}

namespace Music {
	enum ID {
		MenuTheme,
		MissionTheme,
	};
}



using TextureHolder     = ResourceHolder<sf::Texture, Textures::ID>;
using SpriteHolder      = ResourceHolder<sf::Sprite, Textures::Sprites::ID>;
using AnimationHolder   = ResourceHolder<Animation, Textures::Animations::ID>;
using FontHolder        = ResourceHolder<sf::Font, Fonts::ID>;
using ShaderHolder      = ResourceHolder<sf::Shader, Shaders::ID>;
using SoundBufferHolder = ResourceHolder<sf::SoundBuffer, SoundEffect::ID>;

#endif // RESOURCEIDENTIFIERS_HPP
