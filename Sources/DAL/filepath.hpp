#ifndef FILEPATH_HPP
#define FILEPATH_HPP

#include <string>
namespace FilePath{
	const std::string prefix                = "../data";
	namespace Json {
		const std::string prefix            = FilePath::prefix + "/Data";

		const std::string animations        = prefix + "/animation.json";
		const std::string config            = prefix + "/config.json";
		const std::string entities          = prefix + "/entities.json";
		const std::string environment       = prefix + "/environment.json";
		const std::string particle          = prefix + "/particle.json";
		const std::string pickup            = prefix + "/pickup.json";
		const std::string projectile        = prefix + "/projectile.json";
		const std::string sprites           = prefix + "/sprite.json";
		const std::string system            = prefix + "/system.json";
		const std::string textures          = prefix + "/textures.json";
		const std::string translationUI     = prefix + "/translationUI.json";
		const std::string weapon            = prefix + "/weapons.json";
		const std::string world             = prefix + "/world.json";
	}
	namespace Audio {
		namespace Music {
			const std::string prefix        = FilePath::prefix + "/Music";

			const std::string menuTheme     = prefix + "/MenuTheme.ogg";
			const std::string missionTheme  = prefix + "/MissionTheme.ogg";
		}
		namespace Sound {
			const std::string prefix        = FilePath::prefix + "/Sound";

			const std::string alliedGunfire = prefix + "/AlliedGunfire.wav";
			const std::string enemyGunfire  = prefix + "/EnemyGunfire.wav";
			const std::string explosion1    = prefix + "/Explosion1.wav";
			const std::string explosion2    = prefix + "/Explosion2.wav";
			const std::string launchMissile = prefix + "/LaunchMissile.wav";
			const std::string collectPickup = prefix + "/CollectPickup.wav";
			const std::string button        = prefix + "/Button.wav";
		}
	}
	namespace Shaders {
		const std::string prefix            = FilePath::prefix + "/Shaders";
		namespace Vert {

			const std::string fullpass      = prefix + "/Fullpass.vert";
			const std::string noise         = prefix + "/Noise.vert";
		}
		namespace Frag {
			const std::string brightness    = prefix + "/Brightness.frag";
			const std::string downSample    = prefix + "/DownSample.frag";
			const std::string gaussianBlur  = prefix + "/GaussianBlur.frag";
			const std::string noise         = prefix + "/Noise.frag";
			const std::string add           = prefix + "/Add.frag";
		}
	}
	namespace Textures {
		const std::string prefix = FilePath::prefix + "/Textures";
	}
	namespace Font {
		const std::string prefix            = FilePath::prefix + "/Fonts";

		const std::string main              = prefix + "/Durango Western.otf";
	}
}


#endif // FILEPATH_HPP