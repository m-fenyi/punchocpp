#include "WorldRepository.hpp"
#include "Filepath.hpp"

JsonStructInfo<WorldData> WorldRepository::getStructInfo(WorldData & data) const {
	JsonFieldsInfo fields = {
		JsonFieldInfo( "enemies"    , &data.enemies   , true , new VectorRule(new EnemyDataRule())),
		JsonFieldInfo( "zoom"       , &data.zoom      , true , new FloatRule()),
		JsonFieldInfo( "cameralerp" , &data.cameraLerp, true , new FloatRule()),
		JsonFieldInfo( "playerlerp" , &data.playerLerp, true , new FloatRule()),
		JsonFieldInfo( "dimension"  , &data.dimension , true , new FloatPointRule()),
	};
	return JsonStructInfo<WorldData>(fields, &data);
}

void WorldRepository::loadDefault() noexcept {}

const std::string & WorldRepository::getFileName() const {
	return FilePath::Json::world;
}
