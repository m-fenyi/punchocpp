#ifndef JSONSTRUCTWRITER_HPP
#define JSONSTRUCTWRITER_HPP

#include <string>
#include <map>

#include <json/json.h>

#include "JsonStructInfo.hpp"
#include "JsonFieldRule.hpp"

#include <Utilities/StaticClass.hpp>
#include <Utilities/File.hpp>
#include <Utilities/Exceptions.hpp>
#include <Utilities/Log.hpp>


class JsonStructWriter : public StaticClass {
public:
	template<class T, class ID>
	static void writeMap(JsonStructInfo<T>& structInfo, std::map<ID, T> dataMap, const std::string& fileName) {
		Json::Value jsonValue = mapToJson(structInfo, dataMap);
		File::JSON::save(fileName, jsonValue);
	}
	template<class T>
	static void writeNameMap(JsonStructInfo<T>& structInfo, std::map<std::string, T> dataMap, const std::string& fileName) {
		Json::Value jsonValue = nameMapToJson(structInfo, dataMap);
		File::JSON::save(fileName, jsonValue);
	}
	template<class T>
	static void writeVector(JsonStructInfo<T>& structInfo, std::vector<T> dataVector, const std::string& fileName) {
		Json::Value jsonValue = vectorToJson(structInfo, dataVector);
		File::JSON::save(fileName, jsonValue);
	}
	template<class T>
	static void writeStruct(JsonStructInfo<T>& structInfo, T& data, const std::string& fileName) {
		Json::Value jsonValue = structToJson(structInfo, data);
		File::JSON::save(fileName, jsonValue);
	}

	



	
	template<class T, class ID>
	static Json::Value mapToJson(JsonStructInfo<T>& structInfo, std::map<ID, T> dataMap) {
		Json::Value jsonStructs;
		for (auto data : dataMap) {
			Json::Value jsonStruct = structToJson(structInfo, data.second, data.first);
			jsonStructs.append(jsonStruct);
		}
		return jsonStructs;
	}
	template<class T, class ID>
		static Json::Value nameMapToJson(JsonStructInfo<T>& structInfo, std::map<ID, T> dataMap) {
		Json::Value jsonStructs;
		for (auto data : dataMap) {
			Json::Value jsonStruct = structToJson(structInfo, data.second);
			jsonStructs[data.first] = jsonStruct;
		}
		return jsonStructs;
	}
	template<class T>
	static Json::Value mapToJson(JsonStructInfo<T>& structInfo, std::vector<T> dataVector) {
		Json::Value jsonStructs;
		for (auto data : dataVector) {
			Json::Value jsonStruct = structToJson(structInfo, data);
			jsonStructs.append(jsonStruct);
		}
		return jsonStructs;
	}
	template<class T, class ID>
	static Json::Value structToJson(JsonStructInfo<T>& structInfo, T& data, ID id) {
		Json::Value jsonStruct = structToJson(structInfo, data);
		jsonStruct["ID"] = id;
	}

	template<class T>
	static Json::Value structToJson(JsonStructInfo<T>& structInfo, T& data) {
		Json::Value jsonStruct;
		*structInfo.structData = data;
		for (JsonFieldInfo& fieldInfo : structInfo.fieldInfos) {
			try {
				Json::Value& jsonField = File::JSON::getValue(jsonStruct, fieldInfo.name);
				makeField(fieldInfo, jsonField);
			} catch (const WriteException& e) {
				Log::e() << "JsonStructWriter::writeStruct() : " << e.getMessage() << std::endl;
			}
		}
		return jsonStruct;
	}

private:
	static void makeField(JsonFieldInfo& fieldInfo, Json::Value& value);
};

#endif // JSONSTRUCTWRITER_HPP

