#include "File.hpp"
#include "Log.hpp"
#include <fstream>

LOGCAT("Utilities.File");

namespace File::JSON {
	bool load(std::string filename, Json::Value& jsonData) {
		LOGV("Loading JSON file: " << filename);

		Json::CharReaderBuilder rbuilder;
		rbuilder["collectComments"] = false;
		std::string errs;
		std::ifstream in(filename);
		bool ok = Json::parseFromStream(rbuilder, in, &jsonData, &errs);

		return ok;
	}

	void save(std::string filename, Json::Value & jsonData) {
		LOGV("Saving JSON file: " << filename);
		Json::StreamWriterBuilder wbuilder;
		wbuilder["indentation"] = "  ";
		std::string document = Json::writeString(wbuilder, jsonData);
		return;
		//Saving disabled to preserve readability
		std::ofstream out(filename, std::ofstream::out);
		out << document;
		out.close();
	}

	Json::Value& getValue(Json::Value& root, const std::string& name) {
		size_t dotPos = name.find_first_of('.');
		if (dotPos >= name.length()) {
			return root[name];
		} else {
			std::string branchName = name.substr(0, dotPos);
			std::string subBranchName = name.substr(dotPos + 1);
			return getValue(root[branchName], subBranchName);
		}
	}
}