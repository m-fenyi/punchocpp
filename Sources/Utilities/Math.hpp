#ifndef MATHUTILITY_HPP
#define MATHUTILITY_HPP

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Time.hpp>

#include <limits>
#include <random>
#include <functional>
#include <optional>

sf::FloatRect operator+(sf::FloatRect lhs, sf::FloatRect rhs);
sf::Vector2f  operator+(sf::Vector2f lhs, sf::FloatRect rhs);

template<class T> inline bool operator<(const sf::Rect<T>& lhs, const sf::Rect<T>& rhs) { return lhs.left < rhs.left && lhs.top < rhs.top && lhs.width < rhs.width && lhs.height < rhs.height; }
template<class T> inline bool operator>(const sf::Rect<T>& lhs, const sf::Rect<T>& rhs) { return lhs.left > rhs.left && lhs.top > rhs.top && lhs.width > rhs.width && lhs.height > rhs.height; }
template<class T> inline bool operator<=(const sf::Rect<T>& lhs, const sf::Rect<T>& rhs) { return lhs.left <= rhs.left && lhs.top <= rhs.top && lhs.width <= rhs.width && lhs.height <= rhs.height; }
template<class T> inline bool operator>=(const sf::Rect<T>& lhs, const sf::Rect<T>& rhs) { return lhs.left >= rhs.left && lhs.top >= rhs.top && lhs.width >= rhs.width && lhs.height >= rhs.height; }

template<class T> inline bool operator<(const sf::Vector2<T>& lhs, const sf::Vector2<T>& rhs) { return lhs.x < rhs.x && lhs.y < rhs.y; }
template<class T> inline bool operator>(const sf::Vector2<T>& lhs, const sf::Vector2<T>& rhs) { return lhs.x > rhs.x && lhs.y > rhs.y; }
template<class T> inline bool operator<=(const sf::Vector2<T>& lhs, const sf::Vector2<T>& rhs) { return lhs.x <= rhs.x && lhs.y <= rhs.y; }
template<class T> inline bool operator>=(const sf::Vector2<T>& lhs, const sf::Vector2<T>& rhs) { return lhs.x >= rhs.x && lhs.y >= rhs.y; }



namespace Math {
	template<class T> inline T& min(T& lhs, T&rhs) { return (lhs < rhs) ? lhs : rhs; }
	template<class T> inline T& max(T& lhs, T&rhs) { return (lhs > rhs) ? lhs: rhs; }

	std::mt19937 getRandomEngine();

	float			toDegree(float radian);
	float			toRadian(float degree);
	float			toDegree(sf::Vector2i& vector);
	float			toDegree(sf::Vector2f& vector);
	float           toRotation(sf::Vector2i& vector);
	float           toRotation(sf::Vector2f& vector);

	sf::Vector2f    toVector(float degree);

	template<class Int = int>
	inline Int randomInt(Int min, Int max) {
		std::uniform_int_distribution<Int> distr(min, max);
		return distr(getRandomEngine());
	}
	template<class Real = double>
	inline Real randomReal(Real min, Real max) {
		std::uniform_real_distribution<Real> distr(min, max);
		return distr(getRandomEngine());
	}

	template<class Int = int>
	inline Int randomInt(Int max) { return randomInt<Int>(0, max); }
	template<class Real = double>
	inline Real randomReal(Real max) { return randomReal<Real>(0.0, max); }
	bool randomBool(unsigned probability = 2);

	// Vector operations
	float			length(sf::Vector2f vector);
	float			length(sf::Vector2i vector);
	sf::Vector2f	unitVector(sf::Vector2f vector);
	sf::Vector2f	unitVector(sf::Vector2i vector);

	template<class T>
	class Range {
	public:
		Range(T minBound, T maxBound, T step, bool strictMin = false, bool strictMax = true) 
			: minBound(minBound), maxBound(maxBound), step(step),
			  strictMin(strictMin), strictMax(strictMax) {}
		Range(T minBound, T maxBound, bool strictMin = false, bool strictMax = true)
			: minBound(minBound), maxBound(maxBound),
			strictMin(strictMin), strictMax(strictMax) {}
		Range(bool strictMin = false, bool strictMax = true)
			: strictMin(strictMin), strictMax(strictMax) {}

		bool contains(const Range<T>& value) const { 
			if (!hasMin() && !hasMax()) return true;
			if (hasMin() && (!value.hasMin() || !isAboveMin(value.getMin()))) return false;
			if (hasMax() && (!value.hasMax() || !isBelowMax(value.getMax()))) return false;
			return true;
		}
		bool contains(T value) const { return isAboveMin(value) && isBelowMax(value); }

		bool isAboveMax(T value) const { return  hasMax() && (strictMax ? getMax() <= value : getMax() < value ); }
		bool isBelowMax(T value) const { return !hasMax() || (strictMax ? value < getMax() : value <= getMax() ); }

		bool isAboveMin(T value) const { return !hasMin() || (strictMin ? getMin() < value : getMin() <= value); }
		bool isBelowMin(T value) const { return  hasMin() && (strictMin ? value <= getMin() : value < getMin()); }

		T distance() const { return getMax() - getMin(); }
		
		T bound(T value) {
			if (isBelowMin(value)) return getLowest();
			else if (isAboveMax(value)) return getHighest();
			else return value;
		}

		T boundMod(T value) {
			T mod = (value - getMin()) % distance();
			if (hasMin() && value < getMin()) return getMin() - mod;
			else if (hasMax() && getMax() <= value) return getMin() + mod;
			else return value;
		}

		void setMin(T value) { 
			if (!hasMax()) {
				minBound = value;
			} else {
				minBound = Math::min(value, maxBound.value());
				maxBound = Math::max(value, maxBound.value());
			}
		}
		void setMax(T value) { 
			if (!hasMin()) {
				maxBound = value;
			} else {
				maxBound = Math::max(value, minBound.value());
				minBound = Math::min(value, minBound.value());
			}
		}
		void setStep(T value) { step = value; }

		bool hasMin() const { return minBound.has_value(); }
		bool hasMax() const { return maxBound.has_value(); }
		bool hasStep() const { return step.has_value(); }

		void unboundMin() { minBound.reset(); }
		void unboundMax() { maxBound.reset(); }

		T getMin() const { return minBound.value(); }
		T getMax() const { return maxBound.value(); }

		T getLowest() const  { return (strictMin? getMin() + getStep() : getMin()); }
		T getHighest() const { return (strictMax? getMax() - getStep() : getMax()); }

		T getStep() const { return (hasStep()? step.value(): T()); }

		bool isMinStrict() const { return strictMin; }
		bool isMaxStrict() const { return strictMax; }

		void setMinStrict(bool isMinStrict) { strictMin = isMinStrict; }
		void setMaxStrict(bool isMaxStrict) { strictMax = isMaxStrict; }

	private:
		std::optional<T> minBound, maxBound, step;
		bool strictMin, strictMax;
	};


}
namespace Range {
	template<class T> inline Math::Range<T> between(T min, T max) { return Math::Range(Math::min(min, max), Math::max(min, max), false, true); }
	template<class T> inline Math::Range<T> betweenStrict(T min, T max) { return Math::Range(Math::min(min, max), Math::max(min, max), true, true); }
	template<class T> inline Math::Range<T> equal(T value) { return Math::Range(value, value, false, false); }

	template<class T> inline Math::Range<T> above(T min) { 
		Math::Range<T> r(false, false);
		r.setMin(min);
		return r;
	}
	template<class T> inline Math::Range<T> below(T max) {
		Math::Range<T> r(false, true);
		r.setMax(max);
		return r;
	}

	template<class T> inline Math::Range<T> positive() { return above(T()); }
	template<class T> inline Math::Range<T> negative() { return below(T()); }

	template<class T> inline Math::Range<T> any() { return Math::Range<T>(false, false); }
}


#endif // MATHUTILITY_HPP