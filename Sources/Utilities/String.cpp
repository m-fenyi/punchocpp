#include "String.hpp"
#include <sstream>

std::ostream & operator<<(std::ostream & out, const sf::Color & value) {
	out << "(" << (unsigned int)value.r << ", " << (unsigned int)value.g << ", " << (unsigned int)value.b << ", " << (unsigned int)value.a << ")";
	return out;
}

std::ostream & operator<<(std::ostream & out, const Resolution & value) {
	out << value.width << "x" << value.height ;
	return out;
}
