#ifndef UTILITY_TIME_HPP
#define UTILITY_TIME_HPP
#pragma once

#include <SFML/System/Time.hpp>

#include <vector>
#include <memory>
#include <optional>

namespace Time {
	class Duration {
	public:
		Duration();
		Duration(sf::Time endTime);

		void update(sf::Time& dt);
		float getProgress() const;

		sf::Time getCurrentTime() const;
		sf::Time getOverTime() const;
		sf::Time getEndTime() const;
		sf::Time getTimeLeft() const;

		void setEndTime(sf::Time& endTime);

		bool isCompleted() const;

		void reset();
	private:
		sf::Time currentTime;
		std::optional<sf::Time> endTime;
	};

	template<class Value>
	class Transition {
	public:
		using Ptr = std::unique_ptr<Transition<Value>>;

	public:
		Transition():
			duration(),
			ended(false) {}
		Transition(Duration duration):
			duration(duration),
			ended(false) {}
		virtual Value getValue() const = 0;
		virtual void setStartValue(Value value) = 0;

		virtual void update(sf::Time& dt) {
			duration.update(dt);
		}

		virtual bool hasEnded() const {
			return ended || getDuration().isCompleted();
		}
		
		virtual void end() {
			ended = true;
		}

		virtual void reset() {
			duration.reset();
			ended = false;
		}

		Duration getDuration() const {
			return duration;
		}

	private:
		bool ended;
		Duration duration;
	};

	template<class Value>
	class LinearTransition: public Transition<Value> {
	public:
		LinearTransition(Value from, Value to, sf::Time duration):
			Transition(Duration(duration)),
			startValue(from),
			endValue(to) {}

		Value getValue() const override {
			if (getDuration().isCompleted()) {
				return endValue;
			}
			else {
				return startValue + (endValue - startValue) * getDuration().getProgress();
			}
		}

		void setStartValue(Value value) override {
			startValue = value;
		}
		
	private:
		Value startValue;
		Value endValue;
	};

	template<class Value>
	class EndlessTransition: public Transition<Value> {
	public:
		EndlessTransition(Value value): 
			Transition(),
			startValue(value) {}
		EndlessTransition(Value start, Value step): 
			Transition(),
			startValue(start),
			step(step) {}

		Value getValue() const override {
			if (step.has_value()) {
				return startValue + (step.value() * getDuration().getCurrentTime().asSeconds());
			}
			else {
				return startValue;
			}
		}
		
		void setStartValue(Value value) override {
			startValue = value;
		}

	private:
		Value startValue;
		std::optional<Value> step;
	};

	template<class Value>
	class TransitionSequence: public Transition<Value> {
	public:
		TransitionSequence(std::vector<Transition*> transitionVector):
			Transition(),
			transitions(),
			currentTransition(0) {
			for (auto* transition : transitionVector) {
				transitions.push_back(Transition<Value>::Ptr(transition));
			}
		}
		

		Value getValue() const override {
			return getCurrentTransition()->getValue();
		}

		void setStartValue(Value value) override {
			getCurrentTransition()->setStartValue(value);
		}

		void update(sf::Time& dt) override {
			getCurrentTransition()->update(dt);
			while (getCurrentTransition()->hasEnded() && !hasEnded()) {
				sf::Time overtime = getCurrentTransition()->getDuration().getOverTime();
				++currentTransition;
				getCurrentTransition()->update(overtime);
			}
		}

		void end() override {
			Value currentValue = getCurrentTransition()->getValue();
			currentTransition = transitions.size() - 1u;
			getCurrentTransition()->setStartValue(currentValue);
		}

		bool hasEnded() const override {
			return transitions.back()->hasEnded();
		}

		void reset() override {
			Transition<Value>::reset();
			for (auto&& transition : transitions) {
				transition->reset();
			}
			currentTransition = 0u;
		}

	protected:
		Transition* getCurrentTransition() const {
			return transitions.at(currentTransition).get();
		}

	private:
		std::vector<Transition<Value>::Ptr> transitions;
		size_t currentTransition;
	};

}

#endif // UTILITY_TIME_HPP