#ifndef STRINGUTILITY_HPP
#define STRINGUTILITY_HPP

#include <string>
#include <sstream>
#include <SFML/Window/Event.hpp>

#include <Window/WindowWrapper.hpp>
#include <Action/GameAction.hpp>
#include <Nodes/Category.hpp>


template <typename T>
std::string toString(const T& value) {
	std::ostringstream stream;
	stream << value;
	return stream.str();
}

template<typename T>
std::ostream& operator<<(std::ostream& out, const sf::Rect<T>& classObj) {
	out << "(" << value.left << ", " << value.top << ", " << value.width << ", " << value.height << ")";
	return out;
}

template<typename T>
std::ostream& operator<<(std::ostream& out, const sf::Vector2<T>& value) {
	out << "(" << value.x << ", " << value.y << ")";
	return out;
}

std::ostream& operator<<(std::ostream& out, const sf::Color& value);

std::ostream& operator<<(std::ostream& out, const Resolution& value);


#endif // STRINGUTILITY_HPP