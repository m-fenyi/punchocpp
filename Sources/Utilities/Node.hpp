#ifndef NODEUTILITY_HPP
#define NODEUTILITY_HPP

#include <DAL/DataType.hpp>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Time.hpp>

class SceneNode;
class EntityNode;
class Animation;
class AnimationPack;

namespace Node {
	void centerOrigin(SceneNode& node);
	void centerOrigin(sf::Text& text);
	void centerOrigin(sf::Sprite& sprite);
	void centerOrigin(Animation& animation);
	void centerOrigin(AnimationPack& pack);
	sf::Vector2f centerOrigin(const sf::FloatRect& rect, const sf::Vector2f scale = sf::Vector2f(1, 1));

	float distance(const SceneNode& lhs, const SceneNode& rhs);

	sf::Vector2f lerp(const sf::Vector2f& currentPos, const sf::Vector2f& destination, const float factor, const sf::Time& dt);
	sf::Vector2f lerpMove(const sf::Vector2f& currentPos, const sf::Vector2f& destination, const float factor, const sf::Time& dt);

	void flipHorizontal(sf::Transformable& transformable);

	Type::Animation getAnimationType(EntityNode* node);
}
#endif // NODEUTILITY_HPP