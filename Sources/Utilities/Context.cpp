#include "Context.hpp"

namespace {
	State::Context* context;
}

namespace Context {
	void set(State::Context & context) {
		::context = new State::Context(context);
	}

	State::Context & get() {
		return *::context;
	}
}