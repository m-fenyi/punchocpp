#ifndef FILEUTILITY_HPP
#define FILEUTILITY_HPP

#include <Json/json.h>
#include <string>


namespace File {
	namespace JSON {
		bool load(std::string filename, Json::Value& jsonData);
		void save(std::string filename, Json::Value& jsonData);

		Json::Value& getValue(Json::Value& root, const std::string& name);
	}
}
#endif // FILEUTILITY_HPP