#include "Node.hpp"
#include "Math.hpp"
#include "Log.hpp"

#include <Nodes/SceneNode.hpp>
#include <Nodes/EntityNode.hpp>

#include <Display/AnimationPack.hpp>


LOGCAT("Nodeutility");

namespace Node {

	void centerOrigin(SceneNode & node) {
		sf::FloatRect bounds = node.getBoundingRect();
		node.setOrigin(centerOrigin(bounds, node.getScale()));
	}

	void centerOrigin(sf::Text& text) {
		sf::FloatRect bounds = text.getLocalBounds();
		text.setOrigin(centerOrigin(bounds, text.getScale()));
	}

	void centerOrigin(sf::Sprite& sprite) {
		sf::FloatRect bounds = sprite.getLocalBounds();
		sprite.setOrigin(centerOrigin(bounds, sprite.getScale()));
	}

	void centerOrigin(Animation& animation) {
		sf::FloatRect bounds = animation.getLocalBounds();
		animation.setOrigin(centerOrigin(bounds, animation.getScale()));
	}

	void centerOrigin(AnimationPack & pack) {
		sf::FloatRect bounds = pack.getLocalBounds();
		pack.setOrigin(centerOrigin(bounds, pack.getScale()));
	}

	sf::Vector2f centerOrigin(const sf::FloatRect& rect, const sf::Vector2f scale) {
		return sf::Vector2f((rect.left + rect.width / 2.f) * scale.x,
			(rect.top + rect.height / 2.f) * scale.y);
	}

	float distance(const SceneNode& lhs, const SceneNode& rhs) {
		return Math::length(lhs.getWorldPosition() - rhs.getWorldPosition());
	}


	sf::Vector2f lerp(const sf::Vector2f& currentPos, const sf::Vector2f& destination, const float factor, const sf::Time& dt) {
		sf::Vector2f newPos = currentPos;
		newPos.x += (destination.x - newPos.x) * factor * dt.asSeconds();
		newPos.y += (destination.y - newPos.y) * factor * dt.asSeconds();

		return newPos;
	}

	sf::Vector2f lerpMove(const sf::Vector2f & currentPos, const sf::Vector2f & destination, const float factor, const sf::Time & dt) {
		sf::Vector2f lerpPos = lerp(currentPos, destination, factor, dt);
		return lerpPos - currentPos;
	}

	void flipHorizontal(sf::Transformable & transformable) {
		float r = transformable.getRotation();
		sf::Vector2f s = transformable.getScale();
		if (Range::between(90.f, 270.f).contains(r)) {
			if (s.y < 0) return;
		} else {
			if (s.y >= 0) return;
		}
		s.y *= -1;
		transformable.setScale(s);
	}

	Type::Animation getAnimationType(EntityNode * node) {
		if (!node) return Type::Animation::AnimationCount;
		if (Math::length(node->getVelocity()) >= 1.f) return Type::Animation::Moving;
		return Type::Animation::Standing;
	}
}