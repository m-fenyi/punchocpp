#include "Time.hpp"

namespace Time {
	Duration::Duration(): endTime(), currentTime(sf::Time::Zero) {}
	Duration::Duration(sf::Time endTime): endTime(endTime), currentTime(sf::Time::Zero) {}

	void Duration::update(sf::Time& dt) {
		currentTime += dt;
	}

	float Duration::getProgress() const {
		if (!endTime.has_value()) {
			return 0.f;
		}
		else if(endTime <= sf::Time::Zero) {
			return 1.f;
		}
		else {
			return currentTime / endTime.value();
		}
	}

	sf::Time Duration::getCurrentTime() const {
		return currentTime;
	}

	sf::Time Duration::getOverTime() const {
		if (isCompleted()) {
			return getCurrentTime() - getEndTime();
		}
		else {
			return sf::Time::Zero;
		}
	}

	sf::Time Duration::getEndTime() const {
		if (endTime.has_value()) {
			return endTime.value();
		}
		else {
			return sf::Time::Zero;
		}
	}

	sf::Time Duration::getTimeLeft() const {
		if (isCompleted() || !endTime.has_value()) {
			return sf::Time::Zero;
		}
		else {
			return endTime.value() - currentTime;
		}
	}

	void Duration::setEndTime(sf::Time& endTime) {
		this->endTime = endTime;
	}

	bool Duration::isCompleted() const {
		return (endTime.has_value()) &&
			(currentTime >= endTime.value());
	}

	void Duration::reset() {
		currentTime = sf::Time::Zero;
	}

}