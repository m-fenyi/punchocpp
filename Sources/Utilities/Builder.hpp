#ifndef BUILDER_HPP
#define BUILDER_HPP


template<class BuildedClass>
class Builder {
public:
	virtual BuildedClass build() const = 0;
	virtual void clean() = 0;
};

#endif // BUILDER_HPP

