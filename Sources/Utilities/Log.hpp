#ifndef LOG_HPP
#define LOG_HPP

#include <iostream>
#include <string>

#define NLOGCAT(CATEGORY, NAME) static Log::Category NAME = Log::Category { CATEGORY }
#define LOGCAT(CATEGORY) NLOGCAT(CATEGORY, logCat)

#define LOG(MODE, STREAM) Log::MODE (logCat) << STREAM << std::endl

#define LOGTRACE(MODE, STREAM) LOG(MODE, "[" << __FUNCSIG__ << "]" << STREAM)

#define LOGV(STREAM)  LOG(v, STREAM)
#define LOGD(STREAM)  LOG(d, STREAM)
#define LOGST()       LOGTRACE(t, "")
#define LOGT(STREAM)  LOGTRACE(t, ": " << STREAM)
#define LOGI(STREAM)  LOG(i, STREAM)
#define LOGW(STREAM)  LOG(w, STREAM)
#define LOGE(STREAM)  LOG(e, STREAM)


using namespace std;

namespace Log {
	enum Severity {
		Error,
		Warning,
		Info,
		Trace,
		Debug,
		Verbose,

		SeverityCount
	};
	struct Category {
		std::string catTag;
	};

	std::ostream& v(const Log::Category& logCat = Category { "Default" } );
	std::ostream& d(const Log::Category& logCat = Category { "Default" } );
	std::ostream& t(const Log::Category& logCat = Category { "Default" });
	std::ostream& i(const Log::Category& logCat = Category { "Default" } );
	std::ostream& w(const Log::Category& logCat = Category { "Default" } );
	std::ostream& e(const Log::Category& logCat = Category { "Default" } );

	void setLoggingLevel(Severity severity);

	void logToFile();
	void printTimestamp(std::ostream& output);

	void log(char level, const std::string& tag);
};

#endif // LOG_HPP