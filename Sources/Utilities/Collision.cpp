#include "Collision.hpp"
#include "Log.hpp"

#include <Nodes/EntityNode.hpp>
#include <Interaction/Hitbox.hpp>

LOGCAT("Utilities.Collision");

struct Projection {
	float min, max;
};


namespace Collision {
	Projection getProjection(const Polygon& poly, sf::Vector2f axis);
	bool doProjectionsOverlap(const Projection& proj1, const Projection& proj2);
	float calculateOverlap(const Projection& proj1, const Projection& proj2);
	sf::Vector2f calculateMTV(MTVData mtvData);


	bool canCollide(EntityNode * lhs, EntityNode * rhs) {
		//LOGD(lhs << " != " << rhs);
		//LOGD(lhs->getCategory() << " != " << rhs->getCategory());
		//LOGD(lhs->isDestroyed() << " && " << rhs->isDestroyed());

		return  lhs != rhs &&
			//lhs->getCategory() != rhs->getCategory() &&
			!(lhs->isDestroyed() || rhs->isDestroyed());
	}

	bool simpleCollision(EntityNode & lhs, EntityNode & rhs) {
		return collision(lhs.getHitbox().getPolygon(), rhs.getHitbox().getPolygon()).collision;
	}

	CollisionData collision(EntityNode& lhs, EntityNode& rhs) {
		MTVData mtvData = collision(lhs.getHitbox().getPolygon(), rhs.getHitbox().getPolygon());
		CollisionData data;
		data.collision = mtvData.collision;
		if (!data.collision) return data;

		mtvData.directionVector = lhs.getPosition() - rhs.getPosition();
		data.mtv = calculateMTV(mtvData);
		return data;
	}

	MTVData collision(Polygon & lhs, Polygon & rhs) {
		MTVData data;
		data.collision = false;
		data.mtvValue = FLT_MAX;

		std::vector<sf::Vector2f> lhsAxes = lhs.getAxes(), rhsAxes = rhs.getAxes();

		Projection lhsProj, rhsProj;
		float temp;

		for (unsigned int i = 0; i < lhsAxes.size(); ++i) {
			lhsProj = getProjection(lhs, lhsAxes[i]);
			rhsProj = getProjection(rhs, lhsAxes[i]);

			if (!doProjectionsOverlap(lhsProj, rhsProj)) return data;
			else {
				temp = calculateOverlap(lhsProj, rhsProj);

				if (temp < data.mtvValue) {
					data.mtvValue = temp;
					data.mtvAxis = lhsAxes[i];
				}
			}
		}

		for (unsigned int i = 0; i < rhsAxes.size(); ++i) {
			lhsProj = getProjection(lhs, rhsAxes[i]);
			rhsProj = getProjection(rhs, rhsAxes[i]);

			if (!doProjectionsOverlap(lhsProj, rhsProj)) return data;
			else {
				temp = calculateOverlap(lhsProj, rhsProj);

				if (temp < data.mtvValue) {
					data.mtvValue = temp;
					data.mtvAxis = lhsAxes[i];
				}
			}
		}
		data.collision = true;
		return data;
	}
	
	Projection getProjection(const Polygon& poly, sf::Vector2f axis) {
		Projection result;
		auto points = poly.getPoints();

		result.min = points[0].x * axis.x + points[0].y * axis.y;
		result.max = result.min;

		for (unsigned int i = 1; i < points.size(); ++i) {
			float newProj = points[i].x * axis.x + axis.y * points[i].y;

			if (newProj < result.min) result.min = newProj;
			else if (newProj > result.max) result.max = newProj;
		}
		return result;
	}

	bool doProjectionsOverlap(const Projection& proj1, const Projection& proj2) {
		if ((proj1.max <= proj2.min) || (proj2.max <= proj1.min)) return false;
		return true;
	}

	float calculateOverlap(const Projection& proj1, const Projection& proj2) {
		return std::max(0.f, std::min(proj1.max, proj2.max) - std::max(proj1.min, proj2.min));
	}

	sf::Vector2f calculateMTV(MTVData mtvData) {
		float angle = atan(mtvData.mtvAxis.y / mtvData.mtvAxis.x);

		sf::Vector2f result(cos(angle) * mtvData.mtvValue, sin(angle) * mtvData.mtvValue);

		if (result.x * mtvData.directionVector.x + result.y * mtvData.directionVector.y < 0) {
			result.x = -result.x;
			result.y = -result.y;
		}
		return result;
	}
}