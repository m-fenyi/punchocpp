#ifndef STATICCLASS_HPP
#define STATICCLASS_HPP

class StaticClass {
public:
	StaticClass() = delete;
	StaticClass(StaticClass const &) = delete;
	StaticClass& operator=(StaticClass const &) = delete;
};

#endif // !STATICCLASS_HPP

