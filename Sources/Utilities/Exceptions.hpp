#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <exception>
#include <string>

class BaseException : public std::exception {
protected:
	BaseException();
	BaseException(std::string message);

public:
	virtual std::string getExceptionName() const = 0;
	virtual std::string getMessage() const;
	virtual const char* what() const;
protected:
	std::string message;
};


class ParseException : public BaseException {
public:
	ParseException(std::string fieldName, std::string error);
	std::string getExceptionName() const override;
};


class WriteException : public BaseException {
public:
	WriteException(std::string fieldName, std::string error);
	std::string getExceptionName() const override;
};

#endif // EXCEPTIONS_HPP