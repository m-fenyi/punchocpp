#ifndef CONTEXTUTILITY_HPP
#define CONTEXTUTILITY_HPP

#include <GUI/State.hpp>

namespace Context {
	void set(State::Context& context);
	State::Context& get();
}

#endif // CONTEXTUTILITY_HPP