#include "Exceptions.hpp"

BaseException::BaseException() {}
BaseException::BaseException(std::string message) : message(message) {}
std::string BaseException::getMessage() const { return getExceptionName() + ": " + message; }
const char* BaseException::what() const { return getMessage().c_str(); }



ParseException::ParseException(std::string fieldName, std::string error) : BaseException(fieldName + " : " + error) {}
std::string ParseException::getExceptionName() const { return "ParseException"; }


WriteException::WriteException(std::string fieldName, std::string error) : BaseException(fieldName + " : " + error) {}
std::string WriteException::getExceptionName() const { return "WriteException"; }
