#include "Log.hpp"

#include <ctime>
#include <cstdio>
#include <sstream>
#include <fstream>

namespace Log {
	namespace {
		Severity minSeverity = Verbose;
	}
	
	std::ofstream logFile("Puncho.log");

	void logToFile() {
#ifdef _DEBUG
		std::cerr.rdbuf(logFile.rdbuf());
#else
		std::cout.rdbuf(logFile.rdbuf());
#endif // _DEBUG
	}

	std::ostream& v(const Log::Category& logCat){
		if (Log::Severity::Verbose <= minSeverity) {
			log('V', logCat.catTag);
			return std::cout;
		}
		return std::cerr;
	}
	std::ostream& d(const Log::Category& logCat){
		if (Log::Severity::Debug <= minSeverity) {
			log('D', logCat.catTag);
			return std::cout;
		}
		return std::cerr;
	}

	std::ostream & t(const Log::Category & logCat) {
		if (Log::Severity::Trace <= minSeverity) {
			log('T', logCat.catTag);
			return std::cout;
		}
		return std::cerr;
	}

	std::ostream& i(const Log::Category& logCat){
		if (Log::Severity::Info <= minSeverity) {
			log('I', logCat.catTag);
			return std::cout;
		}
		return std::cerr;
	}
	std::ostream& w(const Log::Category& logCat){
		if (Log::Severity::Warning <= minSeverity) {
			log('W', logCat.catTag);
			return std::cout;
		}
		return std::cerr;
	}
	std::ostream& e(const Log::Category& logCat){
		log('E', logCat.catTag);
		return std::cout;
	}

	void setLoggingLevel(Severity severity) {
		minSeverity = severity;
	}

	void printTimestamp(std::ostream& output) {
		time_t now = time(0);
		struct tm tm;
		localtime_s(&tm, &now);
		std::ostringstream timestamp;
		
		timestamp << tm.tm_hour << ":";
		
		if (tm.tm_min < 10) timestamp << '0';
		timestamp << tm.tm_min << ":";
		
		if (tm.tm_sec < 10) timestamp << '0';
		timestamp << tm.tm_sec;

		output << timestamp.str() << " - ";
	}

	static void log(char level, const std::string& tag) {
		printTimestamp(std::cout);
		std::cout << level << " - " << tag << ": ";
	}
}