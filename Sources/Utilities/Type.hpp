#ifndef TYPEUTILITY_HPP
#define TYPEUTILITY_HPP


using uint = unsigned int;
using suint = short unsigned int;
using luint = long unsigned int;
using vsuint = unsigned char;
using vluint = long long unsigned int;

#endif // TYPEUTILITY_HPP