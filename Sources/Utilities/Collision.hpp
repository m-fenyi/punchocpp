#ifndef COLLISIONUTILITY_HPP
#define COLLISIONUTILITY_HPP

#include <SFML/System/Vector2.hpp>
#include <utility>
#include <set>

class EntityNode;
class Polygon;

struct CollisionData {
	using Pair = std::pair<EntityNode*, EntityNode*>;
	bool collision;
	sf::Vector2f mtv;
	Pair pair;
};
inline bool operator<(const CollisionData& lhs, const CollisionData& rhs) {
	return lhs.mtv.x < rhs.mtv.x;
}

struct MTVData {
	bool collision;
	float mtvValue;
	sf::Vector2f mtvAxis;
	sf::Vector2f directionVector;
};

namespace Collision {
	bool canCollide(EntityNode * lhs, EntityNode * rhs);

	bool simpleCollision(EntityNode& lhs, EntityNode& rhs);
	CollisionData collision(EntityNode& lhs, EntityNode& rhs);
	MTVData collision(Polygon& lhs, Polygon& rhs);
}
#endif // COLLISIONUTILITY_HPP