#ifndef UTILITY_HELPER_HPP
#define UTILITY_HELPER_HPP



template<class FlagClass, class FlagValue = unsigned>
class Flag {
public:
	Flag() : flag() {}
	Flag(FlagClass value) : flag() { add(value); }
	Flag(FlagValue value) : flag(value) {}

	void add(FlagClass addFlag) { flag |= addFlag; }
	void toggle(FlagClass toggleFlag) { flag ^= toggleFlag; }
	void remove(FlagClass delFlag) { flag = flag & ~delFlag; }

	bool has(FlagClass testFlag) const { return flag & testFlag; }

	Flag<FlagClass> mask(FlagClass maskFlag) const { return Flag(flag & maskFlag); }

	FlagValue getValue() const { return flag; }
	FlagClass get() const { return static_cast<FlagClass>(flag); }
	void set(FlagClass setFlag) { flag = setFlag; }

public:

	operator FlagValue() const { return flag; }
	operator FlagClass() const { return get(); }

private:
	FlagValue flag;
};


#endif // UTILITY_HELPER_HPP