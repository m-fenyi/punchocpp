#include "Math.hpp"
#include <cmath>
#include <ctime>
#include <random>


namespace Math {
	std::mt19937 getRandomEngine() {
		static std::random_device rd;
		static std::mt19937 gen(rd());
		return gen;
	}

	float toDegree(float radian) {
		return 180.f / 3.141592653589793238462643383f * radian;
	}

	float toDegree(sf::Vector2f& vector) {
		return toDegree(std::atan2(vector.x, vector.y));
	}

	float toRotation(sf::Vector2i& vector) {
		return toRotation((sf::Vector2f) vector);
	}

	float toRadian(float degree) {
		return 3.141592653589793238462643383f / 180.f * degree;
	}

	float toDegree(sf::Vector2i& vector) {
		return toDegree((sf::Vector2f)vector);
	}

	float toRotation(sf::Vector2f& vector) {
		return -(toDegree(vector) + 90);
	}

	sf::Vector2f toVector(float degree) {
		float r = toRadian(degree);
		sf::Vector2f vector(std::cos(r), std::sin(r));
		return vector;
	}

	bool randomBool(unsigned probability) {
		if (probability == 0) return false;
		return randomInt<unsigned>(1, probability) == 1;
	}

	float length(sf::Vector2f vector) {
		return std::sqrt(vector.x * vector.x + vector.y * vector.y);
	}

	float length(sf::Vector2i vector) {
		return length((sf::Vector2f)vector);
	}

	sf::Vector2f unitVector(sf::Vector2f vector) {
		if (vector == sf::Vector2f(0.f, 0.f)) return vector;
		return vector / length(vector);
	}

	sf::Vector2f unitVector(sf::Vector2i vector) {
		return unitVector((sf::Vector2f)vector);
	}


}
sf::FloatRect operator+(sf::FloatRect lhs, sf::FloatRect rhs) {
	return sf::FloatRect(lhs.left + rhs.left,
							lhs.top + rhs.top,
							lhs.width + rhs.width,
							lhs.height + rhs.height);
}

sf::Vector2f operator+(sf::Vector2f lhs, sf::FloatRect rhs) {
	return sf::Vector2f(lhs.x + rhs.left, lhs.y + rhs.top);
}
