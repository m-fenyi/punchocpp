#ifndef PARALLELTASK_HPP
#define PARALLELTASK_HPP

#include <GUI/State.hpp>

#include <SFML/System/Thread.hpp>
#include <SFML/System/Mutex.hpp>
#include <SFML/System/Lock.hpp>
#include <SFML/System/Clock.hpp>

#include <atomic>


class ParallelTask {
public:
	ParallelTask(State::Context& context);
	void execute();
	bool isFinished();
	int  getCompletion();

private:
	void runTask();
	void loadTextures();
	void loadSprites();
	void loadAnimations();

private:
	sf::Thread      thread;
	sf::Clock       elapsedTime;
	sf::Mutex       mutex;

	State::Context& context;
	
	std::atomic<bool>          finished;
	std::atomic<int>           progress;
};


#endif // PARALLELTASK_HPP