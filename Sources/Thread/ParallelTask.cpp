#include "ParallelTask.hpp"

#include <DAL/TextureWrapper.hpp>
#include <DAL/Filepath.hpp>
#include <DAL/JSONStructParser.hpp>
#include <Utilities/Utilities.hpp>

#include <json\json.h>

LOGCAT("ParallelTask");

ParallelTask::ParallelTask(State::Context& context):
	thread(&ParallelTask::runTask, this),
	finished(false),
	context(context),
	progress(0){}

void ParallelTask::execute() {
	finished = false;
	elapsedTime.restart();
	thread.launch();
}

bool ParallelTask::isFinished() {
	return finished.load();
}

int ParallelTask::getCompletion() {
	return progress.load();
}

void ParallelTask::runTask() {

	loadTextures();
	loadSprites();
	loadAnimations();
	
	LOGD("Loading finished");
	finished = true;
}

void ParallelTask::loadTextures() {
	
	std::string prefix = FilePath::Textures::prefix;

	int step = (int)(100.f / (float)Textures::Count);

	LOGI("Loading Textures...");
	Graphics::TextureData textureData;
	JsonFieldsInfo fields = {
		JsonFieldInfo("textureID", &textureData.textureID, true , new IntegerRule(false)),
		JsonFieldInfo("filename" , &textureData.filename , true , new StringRule()),
		JsonFieldInfo("rect"     , &textureData.rect     , false, new IntRectRule(sf::IntRect(-1, -1, -1, -1))),
		JsonFieldInfo("smooth"   , &textureData.smooth   , false, new BoolRule(false)),
		JsonFieldInfo("repeated" , &textureData.repeated , false, new BoolRule(false)),
		JsonFieldInfo("sRGB"     , &textureData.sRGB     , false, new BoolRule(false)),
	};
	JsonStructInfo<Graphics::TextureData> textureParse(fields, &textureData);
	auto texturesData = JsonStructParser::parseToVector(textureParse, FilePath::Json::textures);

	for (auto texture : texturesData) {
		if (texture.filename.empty()) continue;
		std::string filename = prefix + "/" + texture.filename;
		Textures::ID id = texture.textureID;
		if (texture.rect == sf::IntRect(-1, -1, -1, -1)) {
			context.textures.load(id, filename);
		} else {
			context.textures.load(id, filename, texture.rect);
		}
		context.textures.get(id).setSmooth(texture.smooth);
		context.textures.get(id).setRepeated(texture.repeated);
		context.textures.get(id).setSrgb(texture.sRGB);

		progress += step;
	}
	LOGD("Textures Loaded!");
}

void ParallelTask::loadSprites() {
	LOGD("Loading Sprites...");

	Graphics::SpriteData spriteData;
	JsonFieldsInfo fields = {
		JsonFieldInfo("spriteID" , &spriteData.spriteID  , true , new IntegerRule(false)),
		JsonFieldInfo("textureID", &spriteData.textureID , true , new IntegerRule(false)),
		JsonFieldInfo("sprite"   , &spriteData.spriteRect, true , new IntRectRule()),
		JsonFieldInfo("origin"   , &spriteData.origin    , false, new FloatPointRule(sf::Vector2f(0, 0))),
		JsonFieldInfo("scale"    , &spriteData.scale     , false, new FloatPointRule(sf::Vector2f(1.f, 1.f))),
		JsonFieldInfo("rotation" , &spriteData.rotation  , false, new FloatRule(Range::between(0.f, 360.f), 0)),
		JsonFieldInfo("color"    , &spriteData.color     , false, new ColorRule(sf::Color(255,255,255,255))),
	};
	JsonStructInfo<Graphics::SpriteData> spriteParse(fields, &spriteData);
	auto spritesData = JsonStructParser::parseToVector(spriteParse, FilePath::Json::sprites);

	for (auto sprite : spritesData) {
		std::unique_ptr<sf::Sprite> spritePtr(new sf::Sprite(context.textures.get(sprite.textureID)));
		spritePtr->setTextureRect(sprite.spriteRect);
		spritePtr->setColor(sprite.color);
		spritePtr->setOrigin(sprite.origin);
		spritePtr->setScale(sprite.scale);
		spritePtr->setRotation(sprite.rotation);

		context.textures.insert(sprite.spriteID, std::move(spritePtr));
	}
	LOGD("Sprites Loaded!");
}

void ParallelTask::loadAnimations() {
	LOGI("Loading Animations...");

	Graphics::AnimationData animationData;
	JsonFieldsInfo fields = {
		JsonFieldInfo("animationID", &animationData.animID    , true , new IntegerRule(false)),
		JsonFieldInfo("textureID"  , &animationData.textureID , true , new IntegerRule(false)),
		JsonFieldInfo("animation"  , &animationData.animRect  , true , new IntRectRule()),
		JsonFieldInfo("frameSize"  , &animationData.frameSize , true , new IntPointRule()),
		JsonFieldInfo("frameCount" , &animationData.frameCount, true , new IntegerRule(false)),
		JsonFieldInfo("duration"   , &animationData.duration  , true , new TimeRule()),
		JsonFieldInfo("repeating"  , &animationData.repeating , false, new BoolRule(true)),
		JsonFieldInfo("origin"     , &animationData.origin    , false, new FloatPointRule(sf::Vector2f(0, 0))),
		JsonFieldInfo("scale"      , &animationData.scale     , false, new FloatPointRule(sf::Vector2f(1.f, 1.f))),
		JsonFieldInfo("rotation"   , &animationData.rotation  , false, new FloatRule(Range::between(0.f, 360.f), 0)),
		JsonFieldInfo("color"      , &animationData.color     , false, new ColorRule(sf::Color(255,255,255,255)))
	};
	JsonStructInfo<Graphics::AnimationData> animationParse(fields, &animationData);
	auto animationsData = JsonStructParser::parseToVector(animationParse, FilePath::Json::animations);

	for (auto animation : animationsData) {
		std::unique_ptr<Animation> animPtr(new Animation(context.textures.get(animation.textureID)));
		animPtr->setTextureRect(animation.animRect);
		animPtr->setColor(animation.color);
		animPtr->setOrigin(animation.origin);
		animPtr->setScale(animation.scale);
		animPtr->setRotation(animation.rotation);

		animPtr->setFrameSize(animation.frameSize);
		animPtr->setFrameCount(animation.frameCount);
		animPtr->setDuration(animation.duration);
		animPtr->setRepeating(animation.repeating);
		
		animPtr->showFirstFrame();

		context.textures.insert(animation.animID, std::move(animPtr));
	}
	LOGD("Animations Loaded!");
}


