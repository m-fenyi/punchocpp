#ifndef INPUT_INPUTHANDLER_HPP
#define INPUT_INPUTHANDLER_HPP

#include "Input.hpp"
#include "InputListener.hpp"
#include "InputMatcher.hpp"
#include "InputMatcherBuilder.hpp"
#include "InputListenerFactory.hpp"

#include <map>

class BaseInputHandler {
public:
	virtual bool handleInput(const Input& input) const = 0;
};


template<class Identifier>
class InputHandler : public BaseInputHandler {
	struct InputBinding {
		InputMatcher* matcher;
		Identifier actionId;
	};
	using Listener = InputListener<Identifier>*;
	using ListenerPtr = std::unique_ptr<InputListener<Identifier>>;
public:
	bool handleInput(const Input & input) const override {
		bool consumed = false;
		for (auto binding : bindings) {
			if (binding.matcher->matches(input)) {

				consumed = activate(binding.actionId, input);
			}
		}
		return consumed;
	}

	bool hasAction(Identifier actionId) const {
		return listeners.find(actionId) != listeners.end();
	}

	void addMatcher(InputMatcher* matcher, Identifier actionId) {
		InputBinding binding;
		binding.matcher = matcher;
		binding.actionId = actionId;
		bindings.push_back(binding);
	}

	template<class ActionArg, class ActionFunction>
	void setListener(Identifier actionId, ActionFunction function) {
		setListener(actionId, InputListenerFactory<Identifier>::create<ActionFunction>(function));
	}

	void setListener(Identifier actionId, Listener listener) {
		listeners[actionId] = ListenerPtr(listener);
	}

	template<class MatcherParam>
	void addMatcher(MatcherParam matcher, Identifier actionId) {
		addMatcher(InputMatcherFactory::create(matcher), actionId);
	}

protected:
	bool activate(Identifier actionId, const Input& input) const {
		bool consumed = false;
		if (hasAction(actionId)) {
			notify(actionId, input);
			consumed = true;
		}
		return consumed;
	}

	void notify(Identifier actionId, const Input& input) const {
		Listener listener = getListener(actionId);
		if (input.getLifeTime() == sf::Time::Zero) {
			listener->onInputStart(actionId, input);
		}

		listener->onInput(actionId, input);

		if (!input.isRealtime()) {
			listener->onInputEnd(actionId, input);
		}
	}

	Listener getListener(Identifier actionId) const {
		return listeners.at(actionId).get();
	}


private:
	std::map<Identifier, ListenerPtr> listeners;
	std::vector<InputBinding> bindings;
};


#endif // INPUTHANDLER_HPP
