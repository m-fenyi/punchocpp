#include "Input.hpp"

Input::Input() {}
unsigned Input::hashCode() const { return getSource() * 0x10000 + getAdditionalCode() * 0x1000 + getCode(); }
Flag<Input::Source> Input::getSourceFlag() const { return Flag<Source>(getSource()); }
bool Input::isRealtime() const { return false; }
float Input::getStrength() const { return 0.f; }
sf::Time Input::getLifeTime() const { return sf::Time::Zero; }
Input::Code Input::getCode() const { return 0; }
Input::AdditionalCode Input::getAdditionalCode() const { return 0; }
Input::ID Input::getID() const { return ID(getSource(), getCode(), getAdditionalCode()); }



Input::ID::ID(Source source, unsigned inputCode, short additionalCode) :
	source(source),
	inputCode(inputCode),
	additionalCode(additionalCode) {}
Input::ID::ID(Source source, unsigned inputCode) :
	source(source),
	inputCode(inputCode),
	additionalCode() {}
Input::ID::ID(Source source) :
	source(source),
	inputCode(),
	additionalCode() {}
Input::ID::ID() : ID(Input::None) {}
bool Input::ID::hasAdditionalCode() const { return additionalCode.has_value(); }
bool Input::ID::hasCode() const { return inputCode.has_value(); }
Flag<Input::Source> Input::ID::getSourceFlag() const { return Flag<Source>(getSource()); }
Input::Source Input::ID::getSource() const { return source; }
unsigned Input::ID::getInputCode() const { return inputCode.value(); }
short Input::ID::getAdditionalCode() const { return additionalCode.value(); }
bool Input::ID::operator==(const ID & other) {
	bool matchSource = getSourceFlag().has(other.source);
	bool matchICode = !hasCode() || !other.hasCode() || inputCode == other.inputCode;
	bool matchACode = !hasAdditionalCode() || !other.hasAdditionalCode() || additionalCode == other.additionalCode;
	return matchSource && matchICode && matchACode;
}
bool Input::ID::operator!=(const ID & other) { return !(*this == other); }



ButtonInput::ButtonInput() : pressed(true), pressedDuration(sf::Time::Zero) {}
bool ButtonInput::isPressed() const { return pressed; }
bool ButtonInput::isReleased() const { return !isPressed(); }
void ButtonInput::setReleased() { pressed = false; }
float ButtonInput::getStrength() const { return 1.0f; }
sf::Time ButtonInput::getLifeTime() const { return pressedDuration; }
void ButtonInput::update(sf::Time & dt) { pressedDuration += dt; }
bool ButtonInput::isRealtime() const { return isPressed(); }





bool operator==(const Input & lhs, const Input & rhs) {
	return lhs.hashCode() == rhs.hashCode();
}
bool operator>(const Input & lhs, const Input & rhs) {
	return lhs.hashCode() > rhs.hashCode();
}

