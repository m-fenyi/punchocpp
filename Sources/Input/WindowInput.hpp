#ifndef WINDOWINPUT_HPP
#define WINDOWINPUT_HPP


#include "Input.hpp"

class WindowInput : public Input {
public:
	WindowInput(sf::Event::EventType eventType);

	sf::Event::EventType getEventType() const;

	Input::Source getSource() const override;
	Input::Code getCode() const override;

private:
	sf::Event::EventType eventType;
};

class ResizeInput : public WindowInput {
public:
	ResizeInput(sf::Vector2i size);

	sf::Vector2i getResize() const;
	Input::Source getSource() const override;

private:
	sf::Vector2i size;
};


#endif // WINDOWINPUT_HPP