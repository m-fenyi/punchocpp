#ifndef INPUT_HPP
#define INPUT_HPP

#include <SFML/Window/Event.hpp>
#include <SFML/System/Time.hpp>

#include <Utilities/Helper.hpp>

#include <optional>

class Input {
public:
	enum Source {
		None             = 0,
		Resize           = 1 << 0,
		OtherWindow      = 1 << 1,
		Window           = Resize | OtherWindow,

		Text             = 1 << 2,

		Keyboard         = 1 << 3,

		MouseButton      = 1 << 4,
		MouseWheel       = 1 << 5,
		MouseMove        = 1 << 6,
		Mouse            = MouseButton | MouseWheel | MouseMove,
		
		MousePosition    = 1 << 7,

		KeyboardMouse    = Keyboard | Mouse,

		JoystickButton   = 1 << 8,
		JoystickMove     = 1 << 9,
		Joystick         = JoystickButton | JoystickMove,
		
		JoystickStatus   = 1 << 10,
		JoystickPosition = 1 << 11,

		Realtime         = MousePosition | JoystickPosition,

		Button           = Keyboard | MouseButton | JoystickButton,

		Rebindable       = KeyboardMouse | Joystick,

		All              = Window | Text | KeyboardMouse | Joystick | Realtime,

		Unknown          = None,
	};

	enum PreferedSource {
		KeyboardSource,
		JoystickSource,
		AutoSource,
	};

	enum KeyboardSetup {
		WASD,
		ZQSD,
		Arrows,
	};

	using Code = unsigned;
	using AdditionalCode = short;

	class ID {
	public:
		ID(Source source, unsigned inputCode, short additionalCode);
		ID(Source source, unsigned inputCode);
		ID(Source source);
		ID();

		bool hasAdditionalCode() const;
		bool hasCode() const;

		Flag<Source> getSourceFlag() const;
		Source getSource() const;
		unsigned getInputCode() const;
		short getAdditionalCode() const;

		bool operator==(const ID& other);
		bool operator!=(const ID& other);

	private:
		Source source;
		std::optional<unsigned> inputCode;
		std::optional<short>    additionalCode;
	};

protected:
	Input();

public:
	unsigned hashCode() const;
	Flag<Source>           getSourceFlag() const;
	virtual Source         getSource() const = 0;
	virtual Code           getCode() const;
	virtual AdditionalCode getAdditionalCode() const;
	ID getID() const;
	
	virtual float getStrength() const;
	virtual sf::Time getLifeTime() const;
	virtual void update(sf::Time& dt) {}

	virtual bool isRealtime() const;
};
bool operator==(const Input& lhs, const Input& rhs);
bool operator>(const Input& lhs, const Input& rhs);


class ButtonInput : virtual public Input {
protected:
	ButtonInput();
public:

	bool isPressed() const;
	bool isReleased() const;
	void setReleased();

	sf::Time getLifeTime() const override;
	void update(sf::Time& dt) override;

	float getStrength() const override;
	bool isRealtime() const override;

private:
	sf::Time pressedDuration;
	bool pressed;
};


#endif // INPUT_HPP