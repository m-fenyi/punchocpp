#include "InputMatcherBuilder.hpp"
#include "KeyboardInput.hpp"
#include "MouseInput.hpp"
#include "WindowInput.hpp"
#include <Utilities/Math.hpp>

void InputMatcherBuilder::clean() {
	id = Input::ID();
	requirePressed = false;
	strength = Math::Range<float>();
	duration = Math::Range<sf::Time>();
}

InputMatcher* InputMatcherBuilder::build() const {
	if (id.getSourceFlag().has(Input::Button)) {
		return new ButtonInputMatcher(id, strength, duration, requirePressed);
	}
	else {
		return new SimpleInputMatcher(id, strength, duration);
	}

}

InputMatcherBuilder& InputMatcherBuilder::setInputID(Input::ID id) {
	this->id = id;
	return self();
}

InputMatcherBuilder& InputMatcherBuilder::setInputSource(Input::Source source) {
	return setInputID(Input::ID(source));
}

InputMatcherBuilder& InputMatcherBuilder::setEventType(sf::Event::EventType eventType) {
	Input::ID id;
	switch (eventType) {
	case sf::Event::Closed:
	case sf::Event::Resized:
	case sf::Event::LostFocus:
	case sf::Event::GainedFocus:
	case sf::Event::MouseEntered:
	case sf::Event::MouseLeft:
		return setInputID(Input::ID(Input::Window, eventType));
	case sf::Event::MouseMoved:
		return setInputID(Input::ID(Input::MouseMove));
	case sf::Event::JoystickMoved:
		return setInputID(Input::ID(Input::JoystickMove));
	case sf::Event::JoystickConnected:
		return setInputID(Input::ID(Input::JoystickStatus, 1));
	case sf::Event::JoystickDisconnected:
		return setInputID(Input::ID(Input::JoystickStatus, 0));
	default:
		return setInputID(Input::ID(Input::None));
	}
}

InputMatcherBuilder& InputMatcherBuilder::setUnicode(sf::Uint32 unicode) {
	return setInputID(Input::ID(Input::Text, unicode));
}

InputMatcherBuilder& InputMatcherBuilder::setKeyboardKey(sf::Keyboard::Key key) {
	return setInputID(Input::ID(Input::Keyboard, key));
}

InputMatcherBuilder& InputMatcherBuilder::setKeyboardKey(sf::Keyboard::Key key, KeyboardInput::AdditionalKeyFlag additionalKey) {
	return setInputID(Input::ID(Input::Keyboard, key, additionalKey));
}

InputMatcherBuilder& InputMatcherBuilder::setMouseButton(sf::Mouse::Button button) {
	return setInputID(Input::ID(Input::MouseButton, button));
}

InputMatcherBuilder& InputMatcherBuilder::setMouseWheel(sf::Mouse::Wheel wheel) {
	return setInputID(Input::ID(Input::MouseWheel, wheel));
}

InputMatcherBuilder& InputMatcherBuilder::setMouseWheel(sf::Mouse::Wheel wheel, bool scrollUp) {
	return setInputID(Input::ID(Input::MouseWheel, wheel, scrollUp));
}

InputMatcherBuilder& InputMatcherBuilder::setMousePosition() {
	return setInputID(Input::ID(Input::MousePosition));
}

InputMatcherBuilder& InputMatcherBuilder::setJoystickButton(Joystick::Button button) {
	return setInputID(Input::ID(Input::JoystickButton, button));
}

InputMatcherBuilder& InputMatcherBuilder::setJoystickButton(Joystick::Button button, Joystick::ID id) {
	return setInputID(Input::ID(Input::JoystickButton, button, id));
}

InputMatcherBuilder& InputMatcherBuilder::setJoystickStick(Joystick::Stick stick) {
	return setInputID(Input::ID(Input::JoystickMove, stick));
}

InputMatcherBuilder& InputMatcherBuilder::setJoystickStick(Joystick::Stick stick, Joystick::ID id) {
	return setInputID(Input::ID(Input::JoystickMove, stick, id));
}

InputMatcherBuilder& InputMatcherBuilder::setJoystickPosition(Joystick::Stick stick) {
	return setInputID(Input::ID(Input::JoystickPosition, stick));
}

InputMatcherBuilder& InputMatcherBuilder::setJoystickPosition(Joystick::Stick stick, Joystick::ID id) {
	return setInputID(Input::ID(Input::JoystickPosition, stick, id));
}

InputMatcherBuilder& InputMatcherBuilder::setRequirePressed(bool requirePressed) {
	this->requirePressed = requirePressed;
	return self();
}

InputMatcherBuilder& InputMatcherBuilder::setStrength(Math::Range<float> strength) {
	this->strength = strength;
	return self();
}

InputMatcherBuilder& InputMatcherBuilder::setMinStrength(float strength) {
	this->strength.setMin(strength);
	return self();
}

InputMatcherBuilder& InputMatcherBuilder::setMaxStrength(float strength) {
	this->strength.setMax(strength);
	return self();
}

InputMatcherBuilder& InputMatcherBuilder::setDuration(Math::Range<sf::Time> duration) {
	this->duration = duration;
	return self();
}

InputMatcherBuilder& InputMatcherBuilder::setMinDuration(sf::Time duration) {
	this->duration.setMin(duration);
	return self();
}

InputMatcherBuilder& InputMatcherBuilder::setMaxDuration(sf::Time duration) {
	this->duration.setMax(duration);
	return self();
}


InputMatcherBuilder& InputMatcherBuilder::self() {
	return *this;
}
