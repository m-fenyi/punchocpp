#ifndef INPUT_INPUTLISTENERFACTORY_HPP
#define INPUT_INPUTLISTENERFACTORY_HPP

#include "InputListenerWrapper.hpp"
#include <Utilities/StaticClass.hpp>

#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>

template<class ActionIdentifier>
class InputListenerFactory : public StaticClass {
public:
	template<class Action>
	static InputListener<ActionIdentifier>* create(Action action) {
		return new SimpleInputListener<ActionIdentifier>();
	}

	template<class DerivedInput>
	static InputListener<ActionIdentifier>* create(std::function<void(const DerivedInput&)> action) {
		return new DerivedInputListenerWrapper<ActionIdentifier, DerivedInput>(action);
	}

	template<>
	static InputListener<ActionIdentifier>* create(std::function<void(const Input&)> action) {
		return new BaseInputListenerWrapper<ActionIdentifier>(action);
	}

	template<>
	static InputListener<ActionIdentifier>* create<std::function<void()>>(std::function<void()> action) {
		return new SimpleInputListenerWrapper<ActionIdentifier>(action);
	}
	

};

#endif // INPUTLISTENERFACTORY_HPP