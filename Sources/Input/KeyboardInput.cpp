#include "KeyboardInput.hpp"


KeyboardInput::KeyboardInput(sf::Keyboard::Key key, AdditionalKeyFlag additionalKey) : key(key), additionalKey(additionalKey) {}
sf::Keyboard::Key KeyboardInput::getKey() const { return key; }
KeyboardInput::AdditionalKeyFlag KeyboardInput::getAdditionalKey() const { return additionalKey; }
Input::Source KeyboardInput::getSource() const { return Source::Keyboard; }
Input::Code KeyboardInput::getCode() const { return getKey(); }
Input::AdditionalCode KeyboardInput::getAdditionalCode() const { return getAdditionalKey(); }



TextInput::TextInput(sf::Uint32 unicode) : unicode(unicode) {}
sf::Uint32 TextInput::getUnicode() const { return unicode; }
char TextInput::getChar() const { return (char)unicode; }
Input::Source TextInput::getSource() const { return Source::Text; }

