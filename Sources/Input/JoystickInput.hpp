#ifndef JOYSTICK_HPP
#define JOYSTICK_HPP


#include "Input.hpp"

namespace Joystick {
	using ID = unsigned;

	enum Button {
		A = 0,
		B = 1,
		X = 2,
		Y = 3,
		LeftButton = 4,
		RightButton = 5,
		Select = 6,
		Start = 7,
		LeftStickButton = 8,
		RightStickButton = 9,

		LeftTrigger = 10,
		RightTrigger = 11,
		DpadLeft = 12,
		DpadRight = 13,
		DpadUp = 14,
		DpadDown = 15,

		ButtonCount,
		UnknownButton
	};

	enum Axis {
		LeftHorizontalAxis = sf::Joystick::X,
		LeftVerticalAxis = sf::Joystick::Y,
		RightHorizontalAxis = sf::Joystick::U,
		RightVerticalAxis = sf::Joystick::R,

		Trigger = sf::Joystick::Z,
		HorizontalButton = sf::Joystick::PovX,
		VerticalButton = sf::Joystick::PovY,

		UnknownAxis = sf::Joystick::V,
		AxisCount = sf::Joystick::AxisCount
	};

	enum Stick {
		LeftStick = 0,
		RightStick = 1,

		StickCount
	};
}


class JoystickInput : virtual public Input {
protected:
	JoystickInput(Joystick::ID joystickId);

public:
	Joystick::ID getJoystickId() const;
	Input::AdditionalCode getAdditionalCode() const override;

private:
	Joystick::ID joystickId;
};


class JoystickButtonInput : public ButtonInput, public JoystickInput {
public:
	JoystickButtonInput(Joystick::ID joystickId, Joystick::Button button, float strength = 1.f);

	Joystick::Button getButton() const;
	Input::Source getSource() const override;
	float getStrength() const override;
	Input::Code getCode() const override;

private:
	Joystick::Button button;
	float strength;
};

class JoystickMoveInput : public JoystickInput {
public:
	JoystickMoveInput(Joystick::ID joystickId, Joystick::Axis axis, float delta);

	float getDelta() const;
	Joystick::Axis getAxis() const;

	Input::Source getSource() const override;
	Input::Code getCode() const override;
	
private:
	Joystick::Axis axis;
	float delta;
};

class JoystickStatusInput : public JoystickInput {
public:
	JoystickStatusInput(Joystick::ID joystickId, bool connection);

	bool isConnection() const;
	bool isDisconnection() const;
	Input::Source getSource() const override;
	Input::Code getCode() const override;
	
private:
	bool connection;
};


class JoystickPositionInput : public JoystickInput {
public:
	JoystickPositionInput(Joystick::ID joystickId, Joystick::Stick stick, sf::Vector2f position);

	void setPosition(sf::Vector2f newPosition);
	
	Joystick::Stick getStick() const;
	Joystick::Axis getHorizontalAxis() const;
	Joystick::Axis getVerticalAxis() const;

	sf::Vector2f getPosition() const;
	sf::Vector2f getPreviousPosition() const;
	sf::Vector2f getMove() const;

	Input::Source getSource() const override;
	Input::Code getCode() const override;

	bool isRealtime() const override;

private:
	Joystick::Stick stick;
	sf::Vector2f position;
	sf::Vector2f previousPosition;
};

#endif // JOYSTICK_HPP

