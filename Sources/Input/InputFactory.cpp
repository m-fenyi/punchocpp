#include "InputFactory.hpp"
#include "JoystickInput.hpp"
#include "KeyboardInput.hpp"
#include "WindowInput.hpp"
#include "MouseInput.hpp"


Input * InputFactory::produce(sf::Event & event) {
	
	switch (event.type) {
		case sf::Event::Closed:
		case sf::Event::LostFocus:
		case sf::Event::GainedFocus:
		case sf::Event::MouseEntered:
		case sf::Event::MouseLeft:
			return new WindowInput(event.type);
		case sf::Event::Resized:
			return new ResizeInput(sf::Vector2i(event.size.width, event.size.height));
		case sf::Event::TextEntered:
			return new TextInput(event.text.unicode);

		case sf::Event::KeyPressed: {
			unsigned additionalKeys = 0;
			if (event.key.alt)     additionalKeys |= KeyboardInput::Alt;
			if (event.key.control) additionalKeys |= KeyboardInput::Control;
			if (event.key.shift)   additionalKeys |= KeyboardInput::Shift;
			if (event.key.system)  additionalKeys |= KeyboardInput::System;
			return new KeyboardInput(event.key.code, additionalKeys);
		}

		case sf::Event::MouseWheelScrolled:
			return new MouseWheelInput(sf::Vector2i(event.mouseWheelScroll.x, event.mouseWheelScroll.y), event.mouseWheelScroll.wheel, event.mouseWheelScroll.delta);
		case sf::Event::MouseButtonPressed:
			return new MouseButtonInput(sf::Vector2i(event.mouseButton.x, event.mouseButton.y), event.mouseButton.button);

		case sf::Event::MouseMoved:
			return new MouseMoveInput(sf::Vector2i(event.mouseMove.x, event.mouseMove.y));

		case sf::Event::JoystickButtonPressed:
			return new JoystickButtonInput(event.joystickButton.joystickId, (Joystick::Button)event.joystickButton.button);

		case sf::Event::JoystickMoved:
			return processJoystickMove(event);

		case sf::Event::JoystickConnected:
			return new JoystickStatusInput(event.joystickConnect.joystickId, true);
		case sf::Event::JoystickDisconnected:
			return new JoystickStatusInput(event.joystickConnect.joystickId, false);

		default:
		case sf::Event::KeyReleased:
		case sf::Event::MouseButtonReleased:
		case sf::Event::JoystickButtonReleased:

		case sf::Event::MouseWheelMoved:
		case sf::Event::TouchBegan:
		case sf::Event::TouchMoved:
		case sf::Event::TouchEnded:
		case sf::Event::SensorChanged:
			return nullptr;
	}
}

Input * InputFactory::produceMousePosition(sf::Vector2i mousePosition) {
	return new MousePositionInput(mousePosition);
}

Input * InputFactory::produceJoystickPosition(Joystick::ID joystickId, Joystick::Stick stickId, sf::Vector2f joystickPosition) {
	return new JoystickPositionInput(joystickId, stickId, joystickPosition);
}

Input * InputFactory::processJoystickMove(sf::Event & event) {
	switch (event.joystickMove.axis) {
	default:
	case sf::Joystick::X:
	case sf::Joystick::U:
	case sf::Joystick::Y:
	case sf::Joystick::R:
	case sf::Joystick::V:
		return new JoystickMoveInput(event.joystickMove.joystickId, (Joystick::Axis)event.joystickMove.axis, event.joystickMove.position);

	case sf::Joystick::Z: // Trigger
		if (event.joystickMove.position < 0.f)
			return new JoystickButtonInput(event.joystickMove.joystickId, Joystick::LeftTrigger, -event.joystickMove.position);
		else
			return new JoystickButtonInput(event.joystickMove.joystickId, Joystick::RightTrigger, event.joystickMove.position);

	case sf::Joystick::PovX: // Horizontal Dpad
		if (event.joystickMove.position < 0.f)
			return new JoystickButtonInput(event.joystickMove.joystickId, Joystick::DpadLeft, -event.joystickMove.position);
		else
			return new JoystickButtonInput(event.joystickMove.joystickId, Joystick::DpadRight, event.joystickMove.position);

	case sf::Joystick::PovY : // Vertical Dpad
		if (event.joystickMove.position < 0.f)
			return new JoystickButtonInput(event.joystickMove.joystickId, Joystick::DpadDown, -event.joystickMove.position);
		else
			return new JoystickButtonInput(event.joystickMove.joystickId, Joystick::DpadUp, event.joystickMove.position);
	}
}
