#ifndef INPUTFACTORY_HPP
#define INPUTFACTORY_HPP


#include <SFML/Window/Event.hpp>
#include "Input.hpp"
#include "JoystickInput.hpp"

#include <Utilities/StaticClass.hpp>

class InputFactory: public StaticClass {
public:
	
	static Input* produce(sf::Event& event);

	static Input* produceMousePosition(sf::Vector2i mousePosition);
	static Input* produceJoystickPosition(Joystick::ID joystickId, Joystick::Stick stickId, sf::Vector2f joystickPosition);

private:
	static Input* processJoystickMove(sf::Event& event);
};

#endif // INPUTFACTORY_HPP