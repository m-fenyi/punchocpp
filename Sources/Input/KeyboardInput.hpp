#ifndef KEYBOARDINPUT_HPP
#define KEYBOARDINPUT_HPP

#include "Input.hpp"

class TextInput : public Input {
public:
	TextInput(sf::Uint32 unicode);

	sf::Uint32 getUnicode() const;
	char getChar() const;
	Input::Source getSource() const override;

private:
	sf::Uint32 unicode;
};

class KeyboardInput : public ButtonInput {
public:
	using AdditionalKeyFlag = unsigned;
	enum AdditionalKey {
		None    = 0,
		Alt     = 1 << 0,
		Control = 1 << 1,
		Shift   = 1 << 2,
		System  = 1 << 3,
	};
public:
	KeyboardInput(sf::Keyboard::Key key, AdditionalKeyFlag additionalKey = AdditionalKey::None);

	sf::Keyboard::Key getKey() const;
	AdditionalKeyFlag getAdditionalKey() const;
	
	Input::Source getSource() const override;
	Input::Code getCode() const override;
	Input::AdditionalCode getAdditionalCode() const override;

private:
	sf::Keyboard::Key key;
	AdditionalKeyFlag additionalKey;
};

#endif // !KEYBOARDINPUT_HPP
