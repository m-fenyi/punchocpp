#include "InputMatcher.hpp"




SimpleInputMatcher::SimpleInputMatcher(Input::ID inputId, Math::Range<float> strength, Math::Range<sf::Time> duration): inputId(inputId), inputStrength(strength), inputDuration(duration) {}
SimpleInputMatcher::SimpleInputMatcher(Input::ID inputId, Math::Range<float> strength) : SimpleInputMatcher(inputId, strength, Range::any<sf::Time>()) {}
SimpleInputMatcher::SimpleInputMatcher(Input::ID inputId, Math::Range<sf::Time> duration) : SimpleInputMatcher(inputId, Range::any<float>(), duration) {}
SimpleInputMatcher::SimpleInputMatcher(Input::ID inputId): SimpleInputMatcher(inputId, Range::any<float>()) {}
bool SimpleInputMatcher::matches(const Input & input) {
	if (input.getID() != inputId) return false;
	if (!inputStrength.contains(input.getStrength())) return false;
	if (!inputDuration.contains(input.getLifeTime())) return false;
	return true;
}



ButtonInputMatcher::ButtonInputMatcher(Input::ID inputId, Math::Range<float> strength, Math::Range<sf::Time> duration, bool isPressed)
	: SimpleInputMatcher(inputId, strength, duration), isPressed(isPressed) {}
ButtonInputMatcher::ButtonInputMatcher(Input::ID inputId, Math::Range<float> strength, bool isPressed)
	: SimpleInputMatcher(inputId, strength), isPressed(isPressed) {}
ButtonInputMatcher::ButtonInputMatcher(Input::ID inputId, Math::Range<sf::Time> duration, bool isPressed)
	: SimpleInputMatcher(inputId, duration), isPressed(isPressed) {}
ButtonInputMatcher::ButtonInputMatcher(Input::ID inputId, bool isPressed)
	: SimpleInputMatcher(inputId), isPressed(isPressed) {}
bool ButtonInputMatcher::matches(const Input & input) {
	if (!(input.getSourceFlag().has(Input::Button))) return false;
	if (!SimpleInputMatcher::matches(input)) return false;

	const ButtonInput& buttonInput = dynamic_cast<const ButtonInput&>(input);
	if (buttonInput.isPressed() != isPressed) return false;
	return true;
}



TimedInputMatcher::TimedInputMatcher(InputMatcher & matcher, sf::Time matchTime): matcher(matcher), timeFrame(matchTime), countdown(sf::Time::Zero), endless(false) {}
void TimedInputMatcher::startTimed() { countdown = timeFrame; endless = false; }
void TimedInputMatcher::startEndless() { endless = true; }
void TimedInputMatcher::stop() { endless = false; countdown = sf::Time::Zero; }
bool TimedInputMatcher::canMatch() const { return endless || countdown > sf::Time::Zero; }
bool TimedInputMatcher::matches(const Input & input) {
	if (!canMatch()) return false;
	return matcher.matches(input);
}
void TimedInputMatcher::update(const sf::Time & dt) {
	countdown -= dt;
}



SequenceInputMatcher::SequenceInputMatcher(): currentMatcher(0) {}
void SequenceInputMatcher::addMatcher(TimedInputMatcher & matcher) {
	sequence.emplace_back(matcher);
	if (sequence.size() == 1) {
		sequence[0].startEndless();
	}
}
bool SequenceInputMatcher::matches(const Input & input) {
	if (sequence.empty()) return false;
	if (sequence[currentMatcher].matches(input)) {
		currentMatcher++;
		if (currentMatcher == sequence.size()) {
			currentMatcher = 0;
			return true;
		} else {
			sequence[currentMatcher].startTimed();
		}
	}
	return false;
}
void SequenceInputMatcher::update(const sf::Time & dt) {
	sequence[currentMatcher].update(dt);
}
