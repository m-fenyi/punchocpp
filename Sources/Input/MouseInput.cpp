#include "MouseInput.hpp"
#include <Utilities/Math.hpp>

MouseInput::MouseInput(sf::Vector2i position) : position(position) {}
sf::Vector2i MouseInput::getPosition() const { return position; }



MouseButtonInput::MouseButtonInput(sf::Vector2i position, sf::Mouse::Button button) : MouseInput(position), button(button) {}
sf::Mouse::Button MouseButtonInput::getButton() const { return button; }
Input::Source MouseButtonInput::getSource() const { return Source::MouseButton; }
Input::Code MouseButtonInput::getCode() const { return getButton(); }



MouseWheelInput::MouseWheelInput(sf::Vector2i position, sf::Mouse::Wheel wheelId, float delta) : MouseInput(position), wheelId(wheelId), delta(delta) {}
sf::Mouse::Wheel MouseWheelInput::getWheelId() const { return wheelId; }
float MouseWheelInput::getDelta() const { return delta; }
bool MouseWheelInput::isScrollUp() const { return delta >= 0.f; }
float MouseWheelInput::getStrength() const { return 10.f / getDelta(); }
Input::Source MouseWheelInput::getSource() const { return Source::MouseWheel; }
Input::Code MouseWheelInput::getCode() const { return getWheelId(); }
Input::AdditionalCode MouseWheelInput::getAdditionalCode() const { return isScrollUp(); }



MouseMoveInput::MouseMoveInput(sf::Vector2i position) : MouseInput(position) {}
Input::Source MouseMoveInput::getSource() const { return Source::MouseMove; }



MousePositionInput::MousePositionInput(sf::Vector2i position) : MouseInput(position), previousPosition(0, 0) {}
void MousePositionInput::setPosition(sf::Vector2i newPosition) {
	previousPosition = position;
	position = newPosition;
}
sf::Vector2i MousePositionInput::getPreviousPosition() const { return previousPosition; }
sf::Vector2i MousePositionInput::getMove() const { return getPosition() - getPreviousPosition(); }
Input::Source MousePositionInput::getSource() const { return Source::MousePosition; }
bool MousePositionInput::isRealtime() const { return true; }
