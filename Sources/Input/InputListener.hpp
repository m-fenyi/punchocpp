#ifndef INPUT_INPUTLISTENER_HPP
#define INPUT_INPUTLISTENER_HPP
#pragma once

#include "Input.hpp"

template<class InputIdentifier>
class InputListener {
public:
	virtual void onInputStart(const InputIdentifier& identifier, const Input& input) = 0;
	virtual void onInput(const InputIdentifier& identifier, const Input& input) = 0;
	virtual void onInputEnd(const InputIdentifier& identifier, const Input& input) = 0;
};


#endif // INPUTLISTENER_HPP