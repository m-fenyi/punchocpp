#ifndef INPUTMANAGER_HPP
#define INPUTMANAGER_HPP

#include "Input.hpp"
#include "EventRetriever.hpp"

#include <Utilities/Singleton.hpp>

#include <vector>
#include <list>

namespace sf {
	class Event;
}

class InputProducer {
public:
	InputProducer();
	InputProducer(EventRetriever* eventRetriever);

	void setEventRetriever(EventRetriever* eventRetriever);

	const std::vector<Input*> retrieveInputs(sf::Time& dt);
	
private:
	void initRealtimeInput();
	void update(sf::Time& dt);

private:
	EventRetriever* eventRetriever;
	std::list<Input*> realtimeInputs;
};

#endif // INPUTMANAGER_HPP

