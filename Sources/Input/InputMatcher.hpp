#ifndef INPUTMATCHER_HPP
#define INPUTMATCHER_HPP

#include "Input.hpp"

#include <Utilities/Math.hpp>

#include <deque>

class InputMatcher {
public:
	virtual bool matches(const Input& input) = 0;
	virtual void update(const sf::Time& dt) {}
};


class SimpleInputMatcher : public InputMatcher {
public:
	SimpleInputMatcher(Input::ID inputId, Math::Range<float> strength, Math::Range<sf::Time> duration);
	SimpleInputMatcher(Input::ID inputId, Math::Range<float> strength);
	SimpleInputMatcher(Input::ID inputId, Math::Range<sf::Time> duration);
	SimpleInputMatcher(Input::ID inputId);

	bool matches(const Input& input) override;

private:
	const Input::ID inputId;
	const Math::Range<float> inputStrength;
	const Math::Range<sf::Time> inputDuration;
};


class ButtonInputMatcher : public SimpleInputMatcher {
public:
	ButtonInputMatcher(Input::ID inputId, Math::Range<float> strength, Math::Range<sf::Time> duration, bool isPressed = true);
	ButtonInputMatcher(Input::ID inputId, Math::Range<float> strength, bool isPressed = true);
	ButtonInputMatcher(Input::ID inputId, Math::Range<sf::Time> duration, bool isPressed = true);
	ButtonInputMatcher(Input::ID inputId, bool isPressed = true);

	bool matches(const Input& input) override;

private:
	bool isPressed;
};

class TimedInputMatcher : public InputMatcher {
public:
	TimedInputMatcher(InputMatcher& matcher, sf::Time matchTime);

	void startTimed();
	void startEndless();
	void stop();
	bool canMatch() const;

	bool matches(const Input & input) override;
	void update(const sf::Time& dt) override;
private:
	InputMatcher& matcher;
	const sf::Time timeFrame;
	sf::Time countdown;
	bool endless;
};


class SequenceInputMatcher : public InputMatcher {
public:
	SequenceInputMatcher();
	void addMatcher(TimedInputMatcher& matcher);
	bool matches(const Input& input) override;
	void update(const sf::Time& dt) override;

private:
	std::deque<TimedInputMatcher> sequence;
	size_t currentMatcher;
};

#endif // INPUTMATCHER_HPP