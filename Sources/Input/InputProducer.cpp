#include "InputProducer.hpp"
#include "InputFactory.hpp"
#include "JoystickInput.hpp"
#include "KeyboardInput.hpp"
#include "MouseInput.hpp"
#include <boolinq/boolinq.h>

InputProducer::InputProducer() :
	eventRetriever(nullptr),
	realtimeInputs() {}

InputProducer::InputProducer(EventRetriever* eventRetriever) :
	eventRetriever(eventRetriever), 
	realtimeInputs() {

	initRealtimeInput();
}

void InputProducer::setEventRetriever(EventRetriever* eventRetriever) { 
	this->eventRetriever = eventRetriever;
	realtimeInputs.clear();
	initRealtimeInput();
}

void InputProducer::update(sf::Time & dt) {
	realtimeInputs.remove_if([](Input* i) { return !i->isRealtime(); });
	
	for (Input* input : realtimeInputs) {
		switch (input->getSource()) {
		case Input::JoystickButton:
		{
			JoystickButtonInput* joystickInput = dynamic_cast<JoystickButtonInput*>(input);
			if (!eventRetriever->isPressed(joystickInput->getJoystickId(), joystickInput->getButton())) {
				joystickInput->setReleased();
			}
			joystickInput->update(dt);
		}
		break;
		case Input::Keyboard:
		{
			KeyboardInput* keyboardInput = dynamic_cast<KeyboardInput*>(input);
			if (!eventRetriever->isPressed(keyboardInput->getKey())) {
				keyboardInput->setReleased();
			}
			keyboardInput->update(dt);
		}
			break;
		case Input::MouseButton:
		{
			MouseButtonInput* mouseInput = dynamic_cast<MouseButtonInput*>(input);
			if (!eventRetriever->isPressed(mouseInput->getButton())) {
				mouseInput->setReleased();
			}
			mouseInput->update(dt);
		}
			break;


		case Input::MousePosition: {
			sf::Vector2i mousePosition;
			MousePositionInput* mouseInput = dynamic_cast<MousePositionInput*>(input);
			eventRetriever->getMousePosition(mousePosition);
			mouseInput->setPosition(mousePosition);
			break;
		}
		case Input::JoystickPosition: {
			sf::Vector2f joystickPosition;
			JoystickPositionInput* joystickInput = dynamic_cast<JoystickPositionInput*>(input);
			Joystick::ID joystickId = joystickInput->getJoystickId();

			Joystick::Stick stickId = joystickInput->getStick();
			if (eventRetriever->getStickPosition(joystickId, stickId, joystickPosition)) {
				joystickInput->setPosition(joystickPosition);
			}
			break;
		}
		}
	}
}

const std::vector<Input*> InputProducer::retrieveInputs(sf::Time& dt) {
	update(dt);

	std::vector<Input*> inputs;

	inputs.assign(realtimeInputs.begin(), realtimeInputs.end());
	
	int i = 0;
	sf::Event event;
	while (eventRetriever->retrieveEvent(event)) {
		Input* input = InputFactory::produce(event);
		if (!input) continue;
		if (linq::from(realtimeInputs).any([input](Input* i) { return *i == *input; })) continue;

		inputs.push_back(input);
		if (input->isRealtime()) realtimeInputs.push_back(input);
	}

	return inputs;
}

void InputProducer::initRealtimeInput() {
	sf::Vector2i mousePosition;
	bool mouseFound = eventRetriever->getMousePosition(mousePosition);
	if (mouseFound) {
		realtimeInputs.push_back(InputFactory::produceMousePosition(mousePosition));
	}
	
	sf::Vector2f joystickPosition;
	bool stickFound = false;
	for (auto joystickId : eventRetriever->getConnectedJoystick()) {
		stickFound = eventRetriever->getStickPosition(joystickId, Joystick::LeftStick, joystickPosition);
		if (stickFound) realtimeInputs.push_back(InputFactory::produceJoystickPosition(joystickId, Joystick::LeftStick, joystickPosition));

		stickFound = eventRetriever->getStickPosition(joystickId, Joystick::RightStick, joystickPosition);
		if (stickFound) realtimeInputs.push_back(InputFactory::produceJoystickPosition(joystickId, Joystick::RightStick, joystickPosition));
	}
}
