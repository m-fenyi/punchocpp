#ifndef INPUT_INPUTLISTENERWRAPPER_HPP
#define INPUT_INPUTLISTENERWRAPPER_HPP

#include "Input.hpp"
#include "InputListener.hpp"

template<class ActionIdentifier>
class SimpleInputListener: public InputListener<ActionIdentifier> {
public:
	void onInputStart(const ActionIdentifier& identifier, const Input& input) override {
		onInputStart(input);
	}
	void onInput(const ActionIdentifier& identifier, const Input& input) override {
		onInput(input);
	}
	void onInputEnd(const ActionIdentifier& identifier, const Input& input) override {
		onInputEnd(input);
	}

	virtual void onInputStart(const Input& input) {}
	virtual void onInput(const Input& input) {}
	virtual void onInputEnd(const Input& input) {}
};

template<class ActionIdentifier, class... Objects>
class InputListenerWrapper: public SimpleInputListener<ActionIdentifier> {
	using Callback = std::function<void(Objects...)>;
public:
	InputListenerWrapper(Callback callback): callback(callback) {}
protected:
	Callback callback;
};

template<class ActionIdentifier>
class BaseInputListenerWrapper: public InputListenerWrapper<ActionIdentifier, const Input&> {
public:
	using InputListenerWrapper<ActionIdentifier, const Input&>::InputListenerWrapper;

	void onInput(const Input& input) override {
		callback(input);
	}
};

template<class ActionIdentifier, class DerivedInput>
class DerivedInputListenerWrapper: public InputListenerWrapper<ActionIdentifier, const DerivedInput&> {
public:
	using InputListenerWrapper<ActionIdentifier, const DerivedInput&>::InputListenerWrapper;

	void onInput(const Input & input) override {
		try {
			const DerivedInput& derivedInput = dynamic_cast<const DerivedInput&>(input);
			callback(derivedInput);
		} catch (std::bad_cast& bc) {}
	}
};


template<class ActionIdentifier>
class SimpleInputListenerWrapper: public InputListenerWrapper<ActionIdentifier> {
public:
	using InputListenerWrapper<ActionIdentifier>::InputListenerWrapper;
	
	void onInput(const ActionIdentifier& identifier, const Input& input) override {
		callback();
	}
};

#endif // ACTIONWRAPPER_HPP
