#include "WindowInput.hpp"



WindowInput::WindowInput(sf::Event::EventType eventType) : eventType(eventType) {}
sf::Event::EventType WindowInput::getEventType() const { return eventType; }
Input::Source WindowInput::getSource() const { return Source::OtherWindow; }
Input::Code WindowInput::getCode() const { return getEventType(); }


ResizeInput::ResizeInput(sf::Vector2i size) : WindowInput(sf::Event::Resized), size(size) {}
sf::Vector2i ResizeInput::getResize() const { return size; }
Input::Source ResizeInput::getSource() const { return Source::Resize; }
