#include "JoystickInput.hpp"

JoystickInput::JoystickInput(Joystick::ID joystickId) : joystickId(joystickId) {}
Joystick::ID JoystickInput::getJoystickId() const { return joystickId; }
Input::AdditionalCode JoystickInput::getAdditionalCode() const { return getJoystickId(); }



JoystickButtonInput::JoystickButtonInput(Joystick::ID joystickId, Joystick::Button button, float strength) : JoystickInput(joystickId), button(button), strength(strength) {}
Joystick::Button JoystickButtonInput::getButton() const { return button; }
Input::Source JoystickButtonInput::getSource() const { return Source::JoystickButton; }
float JoystickButtonInput::getStrength() const { return strength; }
Input::Code JoystickButtonInput::getCode() const { return getButton(); }



JoystickMoveInput::JoystickMoveInput(Joystick::ID joystickId, Joystick::Axis axis, float delta) : JoystickInput(joystickId), axis(axis), delta(delta) {}
float JoystickMoveInput::getDelta() const { return delta; }
Joystick::Axis JoystickMoveInput::getAxis() const { return axis; }
Input::Source JoystickMoveInput::getSource() const { return Source::JoystickMove; }
Input::Code JoystickMoveInput::getCode() const { return getAxis(); }



JoystickStatusInput::JoystickStatusInput(Joystick::ID joystickId, bool connection) : JoystickInput(joystickId), connection(connection) {}
bool JoystickStatusInput::isConnection() const { return connection; }
bool JoystickStatusInput::isDisconnection() const { return !isConnection(); }
Input::Source JoystickStatusInput::getSource() const { return Source::JoystickStatus; }
Input::Code JoystickStatusInput::getCode() const { return isConnection(); }



JoystickPositionInput::JoystickPositionInput(Joystick::ID joystickId, Joystick::Stick stick, sf::Vector2f position) : JoystickInput(joystickId), position(position), stick(stick), previousPosition(0.f, 0.f) {}
void JoystickPositionInput::setPosition(sf::Vector2f newPosition) {
	previousPosition = position;
	position = newPosition;
}
Joystick::Stick JoystickPositionInput::getStick() const { return stick; }
Joystick::Axis JoystickPositionInput::getHorizontalAxis() const { return stick == Joystick::LeftStick ? Joystick::LeftHorizontalAxis : Joystick::RightHorizontalAxis; }
Joystick::Axis JoystickPositionInput::getVerticalAxis() const   { return stick == Joystick::LeftStick ? Joystick::LeftVerticalAxis   : Joystick::RightVerticalAxis; }
sf::Vector2f JoystickPositionInput::getPosition() const { return position; }
sf::Vector2f JoystickPositionInput::getPreviousPosition() const { return previousPosition; }
sf::Vector2f JoystickPositionInput::getMove() const { return getPosition() - getPreviousPosition(); }
Input::Source JoystickPositionInput::getSource() const { return Source::JoystickPosition; }
Input::Code JoystickPositionInput::getCode() const { return getStick(); }
bool JoystickPositionInput::isRealtime() const { return true; }
