#ifndef INPUTMATCHERBUILDER_HPP
#define INPUTMATCHERBUILDER_HPP

#include "InputMatcher.hpp"
#include "JoystickInput.hpp"
#include "KeyboardInput.hpp"

#include <Utilities/Builder.hpp>

#include <SFML/Window/Event.hpp>

class InputMatcherBuilder: public Builder<InputMatcher*> {
public:
	void clean() override;
	InputMatcher* build() const override;
	
	InputMatcherBuilder& setInputID(Input::ID id);
	InputMatcherBuilder& setInputSource(Input::Source source);
	
	InputMatcherBuilder& setEventType(sf::Event::EventType eventType);
	InputMatcherBuilder& setUnicode(sf::Uint32 unicode);
	
	InputMatcherBuilder& setKeyboardKey(sf::Keyboard::Key key);
	InputMatcherBuilder& setKeyboardKey(sf::Keyboard::Key key, KeyboardInput::AdditionalKeyFlag additionalKey);
	
	InputMatcherBuilder& setMouseButton(sf::Mouse::Button button);
	InputMatcherBuilder& setMouseWheel(sf::Mouse::Wheel wheel);
	InputMatcherBuilder& setMouseWheel(sf::Mouse::Wheel wheel, bool scrollUp);
	InputMatcherBuilder& setMousePosition();

	InputMatcherBuilder& setJoystickButton(Joystick::Button button);
	InputMatcherBuilder& setJoystickButton(Joystick::Button button, Joystick::ID id);
	InputMatcherBuilder& setJoystickStick(Joystick::Stick stick);
	InputMatcherBuilder& setJoystickStick(Joystick::Stick stick, Joystick::ID id);
	InputMatcherBuilder& setJoystickPosition(Joystick::Stick stick);
	InputMatcherBuilder& setJoystickPosition(Joystick::Stick stick, Joystick::ID id);


	InputMatcherBuilder& setRequirePressed(bool requirePressed);

	InputMatcherBuilder& setStrength(Math::Range<float> strength);
	InputMatcherBuilder& setMinStrength(float strength);
	InputMatcherBuilder& setMaxStrength(float strength);

	InputMatcherBuilder& setDuration(Math::Range<sf::Time> duration);
	InputMatcherBuilder& setMinDuration(sf::Time duration);
	InputMatcherBuilder& setMaxDuration(sf::Time duration);

private:
	InputMatcherBuilder& self();


private:
	Input::ID id;
	Math::Range<float> strength;
	Math::Range<sf::Time> duration;
	bool requirePressed;

	//InputMatcher* createMousePosition();
	//InputMatcher* createJoystickPosition(Joystick::Stick stick);
};

#endif // INPUTMATCHERBUILDER_HPP