#ifndef MOUSEINPUT_HPP
#define MOUSEINPUT_HPP

#include "Input.hpp"


class MouseInput : virtual public Input {
protected:
	MouseInput(sf::Vector2i position);

public:
	sf::Vector2i getPosition() const;

protected:
	sf::Vector2i position;
};


class MouseButtonInput : public ButtonInput, public MouseInput {
public:
	MouseButtonInput(sf::Vector2i position, sf::Mouse::Button button);

	sf::Mouse::Button getButton() const;
	Input::Source getSource() const override;
	Input::Code getCode() const override;
private:
	sf::Mouse::Button button;
};

class MouseWheelInput : public MouseInput {
public:
	MouseWheelInput(sf::Vector2i position, sf::Mouse::Wheel wheelId, float delta);

	sf::Mouse::Wheel getWheelId() const;
	float getDelta() const;
	bool isScrollUp() const;

	float getStrength() const override;
	Input::Source getSource() const override;
	Input::Code getCode() const override;
	Input::AdditionalCode getAdditionalCode() const override;

private:
	sf::Mouse::Wheel wheelId;
	float delta;
};

class MouseMoveInput : public MouseInput {
public:
	MouseMoveInput(sf::Vector2i position);

	Input::Source getSource() const override;
};

class MousePositionInput : public MouseInput {
public:
	MousePositionInput(sf::Vector2i position);

	void setPosition(sf::Vector2i newPosition);

	sf::Vector2i getPreviousPosition() const;
	sf::Vector2i getMove() const;

	Input::Source getSource() const override;

	bool isRealtime() const override;

private:
	sf::Vector2i previousPosition;
};


#endif // MOUSEINPUT_HPP
