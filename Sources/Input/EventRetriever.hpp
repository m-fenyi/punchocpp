#ifndef EVENTRETRIEVER_HPP
#define EVENTRETRIEVER_HPP

#include <SFML/System/Vector2.hpp>
#include <list>
#include "JoystickInput.hpp"

namespace sf {
	class Event;
}

class EventRetriever {
public:
	virtual bool retrieveEvent(sf::Event& event) = 0;
	virtual bool getMousePosition(sf::Vector2i& mousePosition) = 0;
	virtual bool getStickPosition(Joystick::ID joystickId, Joystick::Stick stickId, sf::Vector2f & axis) = 0;
	virtual std::list<unsigned> getConnectedJoystick() = 0;

	virtual bool isPressed(sf::Keyboard::Key key) = 0;
	virtual bool isPressed(sf::Mouse::Button button) = 0;
	virtual bool isPressed(Joystick::ID joystickId, Joystick::Button button) = 0;
};


#endif // EVENTRETRIEVER_HPP