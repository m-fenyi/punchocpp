#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <Audio/AudioPlayer.hpp>
#include <Configuration/Configuration.hpp>
#include <DAL/TextureWrapper.hpp>
#include <Input/InputProducer.hpp>
#include <GUI/StateStack.hpp>
#include <Window/WindowWrapper.hpp>

#include <DAL/ConfigurationRepository.hpp>
#include <DAL/GameDataRepository.hpp>

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>

namespace Json {
	class Value;
};

class Application {
public:
	Application();
	void run();
		 
		 
private: 
	void processInput(sf::Time dt);
	void update(sf::Time dt);
	void render();
		 
	void updateStatistics(sf::Time dt);
	void registerStates();

	void initData();

	void exit();

private:
	static const sf::Time	TimePerFrame;

	WindowWrapper           windowWrapper;
	TextureWrapper			textures;
	FontHolder				fonts;
	AudioPlayer				audio;
	InputProducer           inputManager;
	ConfigurationRepository config;
	GameDataRepository      gameData;

	StateStack				stateStack;


#ifdef _DEBUG
	sf::Text				statisticsText;
	sf::Time				statisticsUpdateTime;
	std::size_t				statisticsNumFrames;
#endif // _DEBUG
};

#endif // APPLICATION_HPP