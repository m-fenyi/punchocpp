#include "Application.hpp"

#include <DAL/Filepath.hpp>
#include <DAL/DataTables.hpp>

#include <Nodes/SceneNode.hpp>
#include <Nodes/CharacterEntity.hpp>
#include <Nodes/Weapon.hpp>
#include <Nodes/PickupNode.hpp>
#include <Nodes/ParticleNode.hpp>
#include <Nodes/ProjectileNode.hpp>
#include <Nodes/EnvironmentNode.hpp>
#include <Nodes/WeaponNode.hpp>

#include <DAL/WorldRepository.hpp>

#include <Window/SFMLEventRetriever.hpp>

#include <GUI/State.hpp>
#include <GUI/StateIdentifiers.hpp>
#include <GUI/LoadingState.hpp>
#include <GUI/GameState.hpp>
#include <GUI/MenuState.hpp>
#include <GUI/PauseState.hpp>
#include <GUI/GameOverState.hpp>
#include <GUI/SettingsState.hpp>
#include <GUI/GameSettingsState.hpp>
#include <GUI/AudioSettingsState.hpp>
#include <GUI/GraphicsSettingsState.hpp>
#include <GUI/ControlsSettingsState.hpp>

#include <Utilities/Utilities.hpp>

LOGCAT("Application");
const sf::Time Application::TimePerFrame = sf::seconds(1.f / 60.f);

Application::Application():
	config(),
	gameData(),
	windowWrapper(),
	audio(),
	textures(),
	fonts(),
	inputManager(),
	stateStack(State::Context(windowWrapper, textures, fonts, audio, config, gameData))
#ifdef _DEBUG
	,
	statisticsText(),
	statisticsUpdateTime(),
	statisticsNumFrames(0) 
#endif // _DEBUG

{
	fonts.load(Fonts::Main, FilePath::Font::main);

#ifdef _DEBUG
	statisticsText.setFont(fonts.get(Fonts::Main));
	statisticsText.setPosition(5.f, 5.f);
	statisticsText.setCharacterSize(10u);
#endif // _DEBUG

#ifndef _DEBUG
	Log::setLoggingLevel(Log::Info);
#endif // _DEBUG

	initData();


	config.loadOrDefault();
	auto& gameConfig = config.getConfiguration();

	windowWrapper.setConfiguration(gameConfig.getGraphicsConfiguration());
	windowWrapper.createWindow();

	audio.setConfiguration(gameConfig.getAudioConfiguration());
	
	gameData.loadOrDefault();

	Context::set(State::Context(windowWrapper, textures, fonts, audio, config, gameData));

	registerStates();
	stateStack.pushState(States::Loading);

	SFMLEventRetriever* eventRetriever = new SFMLEventRetriever(windowWrapper.getWindow());
	inputManager.setEventRetriever(eventRetriever);
}

void Application::run() {
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	while (windowWrapper.getWindow().isOpen()) {
		sf::Time dt = clock.restart();
		timeSinceLastUpdate += dt;
		while (timeSinceLastUpdate > TimePerFrame) {
			timeSinceLastUpdate -= TimePerFrame;

			processInput(TimePerFrame);
			update(TimePerFrame);

			if (stateStack.isEmpty()) exit();
		}

		updateStatistics(dt);
		render();
	}
}

void Application::processInput(sf::Time dt) {
	for (auto input : inputManager.retrieveInputs(dt)) {
		stateStack.handleInput(*input);
	}
}

void Application::update(sf::Time dt) {
	stateStack.update(dt);
}

void Application::render() {
	windowWrapper.getWindow().clear();

	stateStack.draw(windowWrapper.getWindow());

	windowWrapper.getWindow().setView(windowWrapper.getWindow().getDefaultView());

#ifdef _DEBUG
	windowWrapper.getWindow().draw(statisticsText);
#endif // _DEBUG

	windowWrapper.getWindow().display();
}


void Application::registerStates() {
	stateStack.registerState<LoadingState>          (States::Loading);
	stateStack.registerState<MenuState>             (States::Menu);
	stateStack.registerState<GameState>             (States::Game);
	stateStack.registerState<PauseState>            (States::Pause);
	stateStack.registerState<GameOverState>         (States::GameOver);

	stateStack.registerState<SettingsState>         (States::Settings);
	stateStack.registerState<GameSettingsState>     (States::GameSettings);
	stateStack.registerState<AudioSettingsState>    (States::AudioSettings);
	stateStack.registerState<GraphicsSettingsState> (States::GraphicsSettings);
	stateStack.registerState<ControlsSettingsState> (States::ControlsSettings);
}

void Application::initData() {
	//TODO Use Repository
	//World::initTable();
	//CharacterEntity::initTable();
	//WeaponNode::initTable();
	//ParticleNode::initTable();
	//PickupNode::initTable();
	//ProjectileNode::initTable();
	//EnvironmentNode::initTable();

	GUI::setTranslationTable(initializeUITranslationData());
}

void Application::exit() {
	config.save();
	windowWrapper.getWindow().close();
}

#ifdef _DEBUG

void Application::updateStatistics(sf::Time dt) {
	statisticsUpdateTime += dt;
	statisticsNumFrames += 1;
	if (statisticsUpdateTime >= sf::seconds(1.0f)) {
		statisticsText.setString("FPS: " + toString(statisticsNumFrames));

		statisticsUpdateTime -= sf::seconds(1.0f);
		statisticsNumFrames = 0;
	}
}

#else
void Application::updateStatistics(sf::Time dt) {}
#endif // _DEBUG

