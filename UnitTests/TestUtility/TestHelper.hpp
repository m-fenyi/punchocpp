#ifndef TESTHELPER_HPP
#define TESTHELPER_HPP
#pragma once
#include "CppUnitTest.h"

#include <functional>

class TestHelper {
public: 
	template<class ObjectsClass, class Function>
	static void Apply(ObjectsClass& objects,
					  Function function) {

		for (auto& object: objects) {
			function(object[i]);
		}
	}

	template<class ObjectsClass, class Method, class... Args>
	static void ApplyM(
		ObjectsClass& objects,
		Method method,
		Args... args) {

		for (auto& object : objects) {
			(object.*method)(args...);
		}
	}


	template<class ExpectedClass, class TestClass, class Action>
	static void AreEqual(
		ExpectedClass expectedValue,
		std::vector<TestClass>& actualValues,
		Action testMethod) {

		for (size_t i = 0; i < actualValues.size(); ++i) {
			Assert::AreEqual(expectedValue, testMethod(actualValues[i]));
		}
	}

	template<class ExpectedClass, class TestClass, class Action>
	static void AreEqual(
		std::vector<ExpectedClass> expectedValues,
		std::vector<TestClass>& actualValues,
		Action testMethod) {

		for (size_t i = 0; i < actualValues.size(); ++i) {
			// Cast required for 'Assert::AreEqual' resolution
			Assert::AreEqual((ExpectedClass)expectedValues[i], testMethod(actualValues[i]));
		}
	}

	template<class ExpectedClass, class TestClass, class Action, class... Args>
	static void AreEqualM(
		ExpectedClass expectedValue,
		std::vector<TestClass>& actualValues,
		Action testMethod,
		Args... args) {

		for (size_t i = 0; i < actualValues.size(); ++i) {
			Assert::AreEqual(expectedValue, (actualValues[i].*testMethod)(args...));
		}
	}

	template<class ExpectedClass, class TestClass, class Action, class... Args>
	static void AreEqualM(
		std::vector<ExpectedClass> expectedValues,
		std::vector<TestClass>& actualValues,
		Action testMethod,
		Args... args) {

		for (size_t i = 0; i < actualValues.size(); ++i) {
			// Cast required for 'Assert::AreEqual' resolution
			Assert::AreEqual((ExpectedClass)expectedValues[i], (actualValues[i].*testMethod)(args...));
		}
	}


	template<class ExpectedClass, class TestClass, class TestAction, class PostAction>
	static void AreEqual(
		std::vector<ExpectedClass> expectedValues,
		TestClass& actualValue,
		TestAction testMethod,
		PostAction postTest) {

		for (ExpectedClass expectedValue: expectedValues) {
			Assert::AreEqual(expectedValue, testMethod(actualValue));
			postTest(actualValue);
		}
	}

	template<class ExpectedClass, class TestClass, class TestAction, class PostAction, class... Args>
	static void AreEqualM(
		std::vector<ExpectedClass> expectedValues,
		TestClass& actualValue,
		TestAction testMethod,
		PostAction postTest,
		Args... args) {

		for (ExpectedClass expectedValue : expectedValues) {
			Assert::AreEqual(expectedValue, (actualValue.*testMethod)());
			(actualValue.*postTest)(args...);
		}
	}

	template<class ExpectedClass, class TestClass, class TestAction, class PostAction, class... Args>
	static void AreEqualM(
		std::vector<ExpectedClass> expectedValues,
		TestClass& actualValue,
		ExpectedClass epsilon,
		TestAction testMethod,
		PostAction postTest,
		Args... args) {

		for (ExpectedClass expectedValue : expectedValues) {
			Assert::AreEqual(expectedValue, (actualValue.*testMethod)(), epsilon);
			(actualValue.*postTest)(args...);
		}
	}
};

#endif // TESTHELPER_HPP