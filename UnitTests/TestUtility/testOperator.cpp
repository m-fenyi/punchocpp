#include "testOperator.hpp"



bool operator==(const WorldData::EnemyData & left, const WorldData::EnemyData & right) {
	return left.type == right.type && left.position == right.position;
}

bool operator==(const AnimationData & left, const AnimationData & right) {
	return left.animation == right.animation && left.info.kind == right.info.kind && (left.info.kind == Type::SpriteKind ? left.info.id.sprite == right.info.id.sprite : left.info.id.anim == right.info.id.anim);
}
