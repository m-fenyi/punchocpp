#include "TestEventRetriever.hpp"

#include <boolinq/boolinq.h>

TestEventRetriever::TestEventRetriever() : events() {}

void TestEventRetriever::addEvent(sf::Event event) { events.emplace_back(event); updateButtonStatus(event); }

void TestEventRetriever::addEvents(std::vector<sf::Event>& events) { for (auto event : events) addEvent(event); }

bool TestEventRetriever::retrieveEvent(sf::Event& event) {
	if (events.empty()) return false;
	event = events.front();
	buttonStatus.remove_if([](sf::Event e) { return e.type == sf::Event::JoystickMoved; });
	events.pop_front();
	return true;
}

void TestEventRetriever::addMousePosition(sf::Vector2i position) { 
	mousePosition.emplace_back(position);
}

void TestEventRetriever::addMousePositions(std::vector<sf::Vector2i>& positions) {
	for (auto position : positions) 
		addMousePosition(position);
}

bool TestEventRetriever::getMousePosition(sf::Vector2i& position) {
	if (mousePosition.empty()) return false;
	position = mousePosition.front();
	mousePosition.pop_front();
	return true;
}

void TestEventRetriever::addStickPosition(unsigned joystickId, Joystick::Stick stickId, sf::Vector2f position) {
	stickPosition[std::make_pair(joystickId, stickId)].emplace_back(position);
	if (!linq::from(connectedJoystick).contains(joystickId)) {
		connectedJoystick.push_back(joystickId);
	}
}

void TestEventRetriever::addStickPositions(unsigned joystickId, Joystick::Stick stickId, std::vector<sf::Vector2f>& positions) { for (auto position : positions) addStickPosition(joystickId, stickId, position); }

bool TestEventRetriever::getStickPosition(Joystick::ID joystickId, Joystick::Stick stickId, sf::Vector2f & position) {
	auto id = std::make_pair(joystickId, stickId);
	if (stickPosition[id].empty()) return false;
	position = stickPosition[id].front();
	stickPosition[id].pop_front();
	return true;
}

std::list<unsigned> TestEventRetriever::getConnectedJoystick() {
	return connectedJoystick;
}

void TestEventRetriever::updateButtonStatus(sf::Event & event) {
	auto keyReleasedPredicate = [event](sf::Event e) { return e.type == sf::Event::KeyReleased && e.key.code == event.key.code; };
	auto keyPressedPredicate  = [event](sf::Event e) { return e.type == sf::Event::KeyPressed && e.key.code == event.key.code; };
	auto mouseReleasedPredicate = [event](sf::Event e) { return e.type == sf::Event::MouseButtonReleased && e.mouseButton.button == event.mouseButton.button; };
	auto mousePressedPredicate  = [event](sf::Event e) { return e.type == sf::Event::MouseButtonPressed && e.mouseButton.button == event.mouseButton.button; };
	auto joystickReleasedPredicate = [event](sf::Event e) { return e.type == sf::Event::JoystickButtonReleased && e.joystickButton.joystickId == event.joystickButton.joystickId && e.joystickButton.button == event.joystickButton.button; };
	auto joystickPressedPredicate  = [event](sf::Event e) { return e.type == sf::Event::JoystickButtonPressed && e.joystickButton.joystickId == event.joystickButton.joystickId && e.joystickButton.button == event.joystickButton.button; };

	switch (event.type) {
	case sf::Event::KeyPressed:
		buttonStatus.remove_if(keyReleasedPredicate);
		if (!linq::from(buttonStatus).any(keyPressedPredicate)) {
			buttonStatus.emplace_back(event);
		}
		break;
	case sf::Event::KeyReleased:
		buttonStatus.remove_if(keyPressedPredicate);
		if (!linq::from(buttonStatus).any(keyReleasedPredicate)) {
			buttonStatus.emplace_back(event);
		}
		break;

	case sf::Event::MouseButtonPressed:
		buttonStatus.remove_if(mouseReleasedPredicate);
		if (!linq::from(buttonStatus).any(mousePressedPredicate)) {
			buttonStatus.emplace_back(event);
		}
		break;
	case sf::Event::MouseButtonReleased:
		buttonStatus.remove_if(mousePressedPredicate);
		if (!linq::from(buttonStatus).any(mouseReleasedPredicate)) {
			buttonStatus.emplace_back(event);
		}
		break;

	case sf::Event::JoystickButtonPressed:
		buttonStatus.remove_if(joystickReleasedPredicate);
		if (!linq::from(buttonStatus).any(joystickPressedPredicate)) {
			buttonStatus.emplace_back(event);
		}
		break;
	case sf::Event::JoystickButtonReleased:
		buttonStatus.remove_if(joystickPressedPredicate);
		if (!linq::from(buttonStatus).any(joystickReleasedPredicate)) {
			buttonStatus.emplace_back(event);
		}
		break;
	case sf::Event::JoystickMoved:
		buttonStatus.emplace_back(event);
		break;
	}
}

bool TestEventRetriever::isPressed(sf::Keyboard::Key key) {
	for (auto status : buttonStatus) {
		if (status.type == sf::Event::KeyPressed && status.key.code == key) {
			return true;
		}
	}
	return false;
}

bool TestEventRetriever::isPressed(sf::Mouse::Button button) {
	for (auto status : buttonStatus) {
		if (status.type == sf::Event::MouseButtonPressed && status.mouseButton.button == button) {
			return true;
		}
	}
	return false;
}

bool TestEventRetriever::isPressed(Joystick::ID joystickId, Joystick::Button button) {
	for (auto status : buttonStatus) {
		if (status.type == sf::Event::JoystickButtonPressed 
			&& status.joystickButton.joystickId == joystickId
			&& status.joystickButton.button == button) {
			return true;
		}
		else if (status.type == sf::Event::JoystickMoved
				 && status.joystickMove.joystickId == joystickId) {
			switch (status.joystickMove.axis) {
			case sf::Joystick::PovX:
				if (status.joystickMove.position > 0.f && button == Joystick::DpadRight) return true;
				if (status.joystickMove.position < 0.f && button == Joystick::DpadLeft) return true;
				break;
			case sf::Joystick::PovY:
				if (status.joystickMove.position > 0.f && button == Joystick::DpadUp) return true;
				if (status.joystickMove.position < 0.f && button == Joystick::DpadDown) return true;
				break;
			case sf::Joystick::Z:
				if (status.joystickMove.position > 0.f && button == Joystick::RightTrigger) return true;
				if (status.joystickMove.position < 0.f && button == Joystick::LeftTrigger) return true;
				break;
			}
		}
	}
	return false;
}
