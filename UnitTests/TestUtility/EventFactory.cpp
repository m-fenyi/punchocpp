#include "EventFactory.hpp"


sf::Event EventFactory::windowEvent(sf::Event::EventType eventType, sf::Vector2i resize) {
	sf::Event windowEvent;
	windowEvent.type = eventType;
	if (eventType == sf::Event::Resized) {
		windowEvent.size.width = resize.x;
		windowEvent.size.height = resize.y;
	}

	return windowEvent;
}

sf::Event EventFactory::textEvent(sf::Uint32 unicode) {
	sf::Event textEvent;
	textEvent.type = sf::Event::TextEntered;
	textEvent.text.unicode = unicode;

	return textEvent;
}
sf::Event EventFactory::keyboardEvent(sf::Keyboard::Key code, bool pressed, bool alt, bool control, bool shift, bool system) {
	sf::Event keyEvent;
	keyEvent.type = pressed ? sf::Event::KeyPressed : sf::Event::KeyReleased;
	keyEvent.key.code = code;
	keyEvent.key.alt = alt;
	keyEvent.key.control = control;
	keyEvent.key.shift = shift;
	keyEvent.key.system = system;

	return keyEvent;
}

sf::Event EventFactory::mouseButtonEvent(sf::Mouse::Button button, bool pressed, sf::Vector2i position) {
	sf::Event mouseEvent;
	mouseEvent.type = pressed ? sf::Event::MouseButtonPressed : sf::Event::MouseButtonReleased;
	mouseEvent.mouseButton.button = button;
	mouseEvent.mouseButton.x = position.x;
	mouseEvent.mouseButton.y = position.y;

	return mouseEvent;
}
sf::Event EventFactory::mouseWheelEvent(sf::Mouse::Wheel wheel, float delta, sf::Vector2i position) {
	sf::Event mouseEvent;
	mouseEvent.type = sf::Event::MouseWheelScrolled;
	mouseEvent.mouseWheelScroll.wheel = wheel;
	mouseEvent.mouseWheelScroll.delta = delta;
	mouseEvent.mouseWheelScroll.x = position.x;
	mouseEvent.mouseWheelScroll.y = position.y;

	return mouseEvent;
}
sf::Event EventFactory::mouseMoveEvent(sf::Vector2i position) {
	sf::Event mouseEvent;
	mouseEvent.type = sf::Event::MouseMoved;
	mouseEvent.mouseMove.x = position.x;
	mouseEvent.mouseMove.y = position.y;

	return mouseEvent;
}

sf::Event EventFactory::joystickButtonEvent(Joystick::Button button, bool pressed, unsigned joystickId) {
	sf::Event joystickEvent;
	joystickEvent.type = pressed ? sf::Event::JoystickButtonPressed : sf::Event::JoystickButtonReleased;
	joystickEvent.joystickButton.button = button;
	joystickEvent.joystickButton.joystickId = joystickId;

	return joystickEvent;
}
sf::Event EventFactory::joystickMoveEvent(Joystick::Axis axis, float position, unsigned joystickId) {
	sf::Event joystickEvent;
	joystickEvent.type = sf::Event::JoystickMoved;
	joystickEvent.joystickMove.axis = (sf::Joystick::Axis)axis;
	joystickEvent.joystickMove.joystickId = joystickId;
	joystickEvent.joystickMove.position = position;

	return joystickEvent;
}
sf::Event EventFactory::joystickStatusEvent(bool connected, unsigned joystickId) {
	sf::Event joystickEvent;
	joystickEvent.type = connected ? sf::Event::JoystickConnected : sf::Event::JoystickDisconnected;
	joystickEvent.joystickConnect.joystickId = joystickId;

	return joystickEvent;
}