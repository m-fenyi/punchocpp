#ifndef TEST_EVENTFACTORY_HPP
#define TEST_EVENTFACTORY_HPP

#include <SFML/Window/Event.hpp>
#include <Input/JoystickInput.hpp>

class EventFactory{
public:
	static sf::Event windowEvent(sf::Event::EventType eventType, sf::Vector2i resize = sf::Vector2i(0, 0));

	static sf::Event textEvent(sf::Uint32 unicode);
	static sf::Event keyboardEvent(sf::Keyboard::Key code, bool pressed, bool alt = false, bool control = false, bool shift = false, bool system = false);

	static sf::Event mouseButtonEvent(sf::Mouse::Button button, bool pressed, sf::Vector2i position);
	static sf::Event mouseWheelEvent(sf::Mouse::Wheel wheel, float delta, sf::Vector2i position);
	static sf::Event mouseMoveEvent(sf::Vector2i position);

	static sf::Event joystickButtonEvent(Joystick::Button button, bool pressed, unsigned joystickId);
	static sf::Event joystickMoveEvent(Joystick::Axis axis, float position, unsigned joystickId);
	static sf::Event joystickStatusEvent(bool connected, unsigned joystickId);
};

#endif // TEST_EVENTFACTORY_HPP
