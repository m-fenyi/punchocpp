#ifndef TEST_TESTEVENTRETRIEVER_HPP
#define TEST_TESTEVENTRETRIEVER_HPP



#include <Input/EventRetriever.hpp>
#include <vector>
#include <map>


class TestEventRetriever : public EventRetriever {
public:
	TestEventRetriever();
	void addEvent(sf::Event event);
	void addEvents(std::vector<sf::Event>& events);
	bool retrieveEvent(sf::Event& event) override;

	void addMousePosition(sf::Vector2i position);
	void addMousePositions(std::vector<sf::Vector2i>& positions);
	bool getMousePosition(sf::Vector2i& position) override;

	void addStickPosition(unsigned joystickId, Joystick::Stick stickId, sf::Vector2f position);
	void addStickPositions(unsigned joystickId, Joystick::Stick stickId, std::vector<sf::Vector2f>& positions);
	bool getStickPosition(Joystick::ID joystickId, Joystick::Stick stickId, sf::Vector2f & position) override;

	bool isPressed(sf::Keyboard::Key key) override;
	bool isPressed(sf::Mouse::Button button) override;
	bool isPressed(Joystick::ID joystickId, Joystick::Button button) override;

	std::list<unsigned> getConnectedJoystick() override;

private:
	void updateButtonStatus(sf::Event& event);

	std::list<sf::Event> events;
	std::list<sf::Event> buttonStatus;
	std::list<sf::Vector2i> mousePosition;
	std::list<unsigned> connectedJoystick;
	std::map<std::pair<unsigned, Joystick::Stick>, std::list<sf::Vector2f>> stickPosition;

};

#endif // TEST_TESTEVENTRETRIEVER_HPP
