#ifndef CUSTOMTOSTRING_HPP
#define CUSTOMTOSTRING_HPP

#include "CppUnitTestAssert.h"

#include <DAL/DataStruct.hpp>

#include <Utilities/String.hpp>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Time.hpp>

#include <Input/Input.hpp>

namespace Microsoft {
	namespace VisualStudio {
		namespace CppUnitTestFramework {
			// ## SFML ##
			template<> inline std::wstring ToString<sf::Color>(const sf::Color& t) { RETURN_WIDE_STRING(t.r << ", " << t.g << ", " << t.b << ", " << t.a); }
			
			template<> inline std::wstring ToString<sf::IntRect>(const sf::IntRect& t) { RETURN_WIDE_STRING("(" << t.left << ", " << t.top << "), (" << t.width << ", " << t.height << ")"); }
			template<> inline std::wstring ToString<sf::FloatRect>(const sf::FloatRect& t) { RETURN_WIDE_STRING("(" << t.left << ", " << t.top << "), (" << t.width << ", " << t.height << ")"); }
			
			template<> inline std::wstring ToString<sf::Time>(const sf::Time& t) { RETURN_WIDE_STRING(t.asMilliseconds() << " milliseconds, " << t.asSeconds() << " seconds"); }
			
			template<> inline std::wstring ToString<sf::Vector2i>(const sf::Vector2i& t) { RETURN_WIDE_STRING(t.x << ", " << t.y); }
			template<> inline std::wstring ToString<sf::Vector2f>(const sf::Vector2f& t) { RETURN_WIDE_STRING(t.x << ", " << t.y); }

			// ## PROJECT CLASS ##
			template<> inline std::wstring ToString<AnimationData>(const AnimationData& t) { RETURN_WIDE_STRING("AnimationType: " << t.animation << ", GraphicKind: " << t.info.kind << ", GraphicID: " << (t.info.kind == Type::SpriteKind?t.info.id.sprite: t.info.id.anim)); }
			template<> inline std::wstring ToString<WorldData::EnemyData>(const WorldData::EnemyData& t) { RETURN_WIDE_STRING("Type: " << t.type << ", Position: " << ToString(t.position)); }

			// ## PUNCHO ENUM ##
			template<> inline std::wstring ToString<Input::Source>(const Input::Source& t) { RETURN_WIDE_STRING(t); }
		}
	}
}

#endif //CUSTOMTOSTRING_HPP

