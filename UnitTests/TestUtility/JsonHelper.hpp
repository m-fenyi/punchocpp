#ifndef UNITTESTS_JSONHELPER_HPP
#define UNITTESTS_JSONHELPER_HPP


#include <json/json.h>

namespace JsonHelper {
	Json::Value parseFromString(const std::string& json);
	std::string writeToString(const Json::Value& json);
}

#endif // UNITTESTS_JSONHELPER_HPP

