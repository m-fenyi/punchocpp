#ifndef TEST_TESTOPERATOR_HPP
#define TEST_TESTOPERATOR_HPP


#include <DAL/DataStruct.hpp>

bool operator==(const WorldData::EnemyData& left, const WorldData::EnemyData& right);
bool operator==(const AnimationData& left, const AnimationData& right);


#endif // !TEST_TESTOPERATOR_HPP
