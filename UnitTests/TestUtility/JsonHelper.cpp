#include "JsonHelper.hpp"



namespace JsonHelper {
	Json::Value parseFromString(const std::string & json) {
		Json::Value root;
		std::string error;
		Json::CharReaderBuilder rbuilder;
		bool ok = rbuilder.newCharReader()->parse(json.c_str(), json.c_str() + json.size(), &root, &error);
		return root;
	}

	std::string writeToString(const Json::Value& json) {
		Json::StreamWriterBuilder wbuilder;
		wbuilder["indentation"] = "";
		std::string document(Json::writeString(wbuilder, json));
		
		return document;
	}

}
