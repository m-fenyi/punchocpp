#include "CppUnitTest.h"
#include <TestUtility/CustomToString.hpp>
#include <TestUtility/testOperator.hpp>

#include <Input/InputMatcher.hpp>
#include <Input/KeyboardInput.hpp>
#include <Input/JoystickInput.hpp>

#include <Utilities/Math.hpp>

#include <boolinq/boolinq.h>

#include <memory>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace InputTests {

	TEST_CLASS(InputMatcherTests) {
	public:
		TEST_METHOD(SimpleInputMatcherTest) {
			Input& input = JoystickButtonInput(1, Joystick::A, 0.5f);
			input.update(sf::milliseconds(500));

			std::vector<SimpleInputMatcher> matchers = {
				SimpleInputMatcher(Input::ID(Input::All)),
				SimpleInputMatcher(Input::ID(Input::Joystick)),
				SimpleInputMatcher(Input::ID(Input::Button)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0), Range::above(0.4f)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0), Range::above(0.5f)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0), Range::above(sf::milliseconds(400))),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0), Range::above(sf::milliseconds(500))),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0), Range::above(0.4f), Range::above(sf::milliseconds(500))),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0, 1)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0, 1), Range::above(0.4f)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0, 1), Range::above(sf::milliseconds(500))),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0, 1), Range::above(0.4f), Range::above(sf::milliseconds(500)))
			};
			std::vector<SimpleInputMatcher> failers = {
				SimpleInputMatcher(Input::ID(Input::Keyboard)),
				SimpleInputMatcher(Input::ID(Input::Joystick      , 1)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 1)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0), Range::above(0.6f)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0), Range::above(sf::milliseconds(600))),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0), Range::above(0.6f), Range::above(sf::milliseconds(600))),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0, 0)),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 0, 0), Range::above(0.6f), Range::above(sf::milliseconds(600))),
				SimpleInputMatcher(Input::ID(Input::JoystickButton, 1, 0), Range::above(0.6f), Range::above(sf::milliseconds(600))),
				SimpleInputMatcher(Input::ID(Input::Keyboard      , 1, 0), Range::above(0.6f), Range::above(sf::milliseconds(600))),
			};
			int i = 0;
			for (auto& matcher : matchers) {
				Assert::IsTrue(matcher.matches(input), std::to_wstring(i).c_str());
				++i;
			}
			for (auto& failer : failers) {
				Assert::IsFalse(failer.matches(input));
			}
		}


		TEST_METHOD(TimedInputMatcherTest) {
			Input& input = JoystickButtonInput(1, Joystick::A, 0.5f);
			input.update(sf::milliseconds(500));
			SimpleInputMatcher smatcher(Input::ID(Input::All, 0));
			SimpleInputMatcher sfailer(Input::ID(Input::Keyboard , 0));
			std::vector<TimedInputMatcher> matchers = {
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
			};
			for (auto& matcher : matchers) { matcher.startTimed(); }
			matchers[1].startEndless();
			matchers.back().startEndless();

			std::vector<TimedInputMatcher> failers = {
				TimedInputMatcher(sfailer , sf::milliseconds(500)),
				TimedInputMatcher(sfailer , sf::milliseconds(500)),
				TimedInputMatcher(sfailer , sf::milliseconds(500)),
				TimedInputMatcher(sfailer , sf::milliseconds(500)),
				TimedInputMatcher(sfailer , sf::milliseconds(500)),
				TimedInputMatcher(sfailer , sf::milliseconds(500)),
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
				TimedInputMatcher(smatcher, sf::milliseconds(500)),
			};
			for (auto& failer : failers) { failer.startTimed(); }
			failers.front().startEndless();
			failers.back().stop();

			sf::Time update = sf::Time::Zero;
			for (auto& matcher : matchers) {
				matcher.update(update);
				Assert::IsTrue(matcher.matches(input));
				update += sf::milliseconds(99);
			}
			update = sf::Time::Zero;
			for (auto& failer : failers) {
				failer.update(update);
				Assert::IsFalse(failer.matches(input));
				update += sf::milliseconds(100);
			}
		}
	
	
		TEST_METHOD(SequenceInputMatcherTest) {
			Assert::IsTrue(true);

			/*
			Matcher:
				Complete sequence
				Start too late
				Wrong first input, correct sequence

			Failer:
				Empty sequence
				Fail Matcher
				Fail second timer
				Fail last timer
			*/

			Input& input = JoystickButtonInput(1, Joystick::A, 0.5f);
			input.update(sf::milliseconds(500));

			std::vector<SequenceInputMatcher> matchers = {
			};
			std::vector<SequenceInputMatcher> failers = {
				SequenceInputMatcher(),
			};
			for (auto& matcher : matchers) {
				Assert::IsTrue(matcher.matches(input));
			}
			for (auto& failer : failers) {
				Assert::IsFalse(failer.matches(input));
			}
		}
	};

}