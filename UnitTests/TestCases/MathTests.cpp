#include "CppUnitTest.h"
#include <TestUtility/CustomToString.hpp>
#include <TestUtility/testOperator.hpp>

#include <Utilities/Math.hpp>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UtilitiesTests {

	namespace MathTests {
		TEST_CLASS(RangeTests) {
		public:
			TEST_METHOD(RangeAccessTest) {
				enum RangeStatus {
					Contains = 1 << 0,
					AboveMin = 1 << 1,
					AboveMax = 1 << 2,
					BelowMin = 1 << 3,
					BelowMax = 1 << 4,
					HasMin   = 1 << 5,
					HasMax   = 1 << 6,
					None     = 0b1111111,
				};
				struct RangeTest {
					Math::Range<int> range;
					unsigned statusFlag;
					int distance;
				};

				std::vector<RangeTest> ranges = {
					{ Range::between(-5, 0) ,            AboveMin | AboveMax                       | HasMin | HasMax,  5 },
					{ Range::between( 0, 5) ,            AboveMin | AboveMax                       | HasMin | HasMax,  5 },
					{ Range::between( 0,10) , Contains | AboveMin                       | BelowMax | HasMin | HasMax, 10 },
					{ Range::between( 5,10) , Contains | AboveMin                       | BelowMax | HasMin | HasMax,  5 },
					{ Range::between(10,20) ,                                  BelowMin | BelowMax | HasMin | HasMax, 10 },

					{ Range::between( 0,-5) ,            AboveMin | AboveMax                       | HasMin | HasMax,  5 },
					{ Range::between( 5, 0) ,            AboveMin | AboveMax                       | HasMin | HasMax,  5 },
					{ Range::between(10, 0) , Contains | AboveMin                       | BelowMax | HasMin | HasMax, 10 },
					{ Range::between(10, 5) , Contains | AboveMin                       | BelowMax | HasMin | HasMax,  5 },
					{ Range::between(20,10) ,                                  BelowMin | BelowMax | HasMin | HasMax, 10 },

					{ Range::equal(0)       ,            AboveMin | AboveMax                       | HasMin | HasMax, 0 },
					{ Range::equal(5)       , Contains | AboveMin                       | BelowMax | HasMin | HasMax, 0 },
					{ Range::equal(10)      ,                                  BelowMin | BelowMax | HasMin | HasMax, 0 },

					{ Range::below(0)       ,            AboveMin | AboveMax                                | HasMax, 0 },
					{ Range::below(5)       ,            AboveMin | AboveMax                                | HasMax, 0 },
					{ Range::below(10)      , Contains | AboveMin                       | BelowMax          | HasMax, 0 },

					{ Range::above(0)       , Contains | AboveMin                       | BelowMax | HasMin         , 0 },
					{ Range::above(5)       , Contains | AboveMin                       | BelowMax | HasMin         , 0 },
					{ Range::above(10)      ,                                  BelowMin | BelowMax | HasMin         , 0 },
					
					{ Range::positive<int>(), Contains | AboveMin                       | BelowMax | HasMin         , 0 },
					
					{ Range::negative<int>(),            AboveMin | AboveMax                                | HasMax, 0 },

					{ Range::any<int>()     , Contains | AboveMin                       | BelowMax                  , 0 },
				};

				int value = 5;
				int i = 0;
				for (auto range : ranges) {
					std::wstring stringReason = L"Error for Range " + std::to_wstring(i);
					const wchar_t* reason = stringReason.c_str();
					Assert::IsTrue((range.range.contains(value)   ? Contains : None) & range.statusFlag, reason);
					Assert::IsTrue((range.range.isBelowMin(value) ? BelowMin : None) & range.statusFlag, reason);
					Assert::IsTrue((range.range.isAboveMin(value) ? AboveMin : None) & range.statusFlag, reason);
					Assert::IsTrue((range.range.isBelowMax(value) ? BelowMax : None) & range.statusFlag, reason);
					Assert::IsTrue((range.range.isAboveMax(value) ? AboveMax : None) & range.statusFlag, reason);
					Assert::IsTrue((range.range.hasMin()          ? HasMin   : None) & range.statusFlag, reason);
					Assert::IsTrue((range.range.hasMax()          ? HasMax   : None) & range.statusFlag, reason);
				
					if (!range.range.hasMin() && !range.range.hasMax()) {
						Assert::ExpectException<std::bad_optional_access>([&] { range.range.distance(); }, reason);
						Assert::ExpectException<std::bad_optional_access>([&] { range.range.getMin  (); }, reason);
						Assert::ExpectException<std::bad_optional_access>([&] { range.range.getMax  (); }, reason);
					} else if (!range.range.hasMin()) {
						Assert::ExpectException<std::bad_optional_access>([&] { range.range.distance(); }, reason);
						Assert::ExpectException<std::bad_optional_access>([&] { range.range.getMin  (); }, reason);
					} else if (!range.range.hasMax()) {
						Assert::ExpectException<std::bad_optional_access>([&] { range.range.distance(); }, reason);
						Assert::ExpectException<std::bad_optional_access>([&] { range.range.getMax  (); }, reason);
					} else {
						Assert::AreEqual(range.distance, range.range.distance(), reason);
					}
					i++;
				}
			}

			TEST_METHOD(RangeModifyTest) {
				Math::Range<int> range;

				Assert::ExpectException<std::bad_optional_access>([&] { range.getMin(); });
				Assert::ExpectException<std::bad_optional_access>([&] { range.getMax(); });
			
				range.setMin(0);

				Assert::AreEqual(0, range.getMin());
				Assert::ExpectException<std::bad_optional_access>([&] { range.getMax(); });

				range.setMax(10);

				Assert::AreEqual( 0, range.getMin());
				Assert::AreEqual(10, range.getMax());

				range.unboundMax();

				Assert::AreEqual(0, range.getMin());
				Assert::ExpectException<std::bad_optional_access>([&] { range.getMax(); });
			
				range.setMax(10);
				range.unboundMin();

				Assert::ExpectException<std::bad_optional_access>([&] { range.getMin(); });
				Assert::AreEqual(10, range.getMax());

				range.unboundMax();

				Assert::ExpectException<std::bad_optional_access>([&] { range.getMin(); });
				Assert::ExpectException<std::bad_optional_access>([&] { range.getMax(); });

				range.setMax(0);

				Assert::ExpectException<std::bad_optional_access>([&] { range.getMin(); });
				Assert::AreEqual(0, range.getMax());

				range.setMin(10);

				Assert::AreEqual( 0, range.getMin());
				Assert::AreEqual(10, range.getMax());
			}

			TEST_METHOD(RangeBoundTest) {
				Math::Range<int> range = Range::between(3,7);
				range.setStep(1);
				std::vector<std::pair<int, int>> values;
				// No Strict, Simple bound
				range.setMinStrict(false);
				range.setMaxStrict(false);
				values = {
					{5,   5},
					{3,   3},
					{7,   7},
					{3,   1},
					{7,   9},
					{3, -50},
					{7,  50}
				};
				for (auto& p : values) {
					Assert::AreEqual(p.first, range.bound(p.second));
				}

				// Min Strict, Simple bound
				range.setMinStrict(true);
				range.setMaxStrict(false);
				values = {
					{5,   5},
					{4,   3},
					{7,   7},
					{4,   1},
					{7,   9},
					{4, -50},
					{7,  50}
				};
				for (auto& p : values) {
					Assert::AreEqual(p.first, range.bound(p.second));
				}

				// Max Strict, Simple bound
				range.setMinStrict(false);
				range.setMaxStrict(true);
				values = {
					{5,   5},
					{3,   3},
					{6,   7},
					{3,   1},
					{6,   9},
					{3, -50},
					{6,  50}
				};
				for (auto& p : values) {
					Assert::AreEqual(p.first, range.bound(p.second));
				}

				// Min Max Strict, Simple bound
				range.setMinStrict(true);
				range.setMaxStrict(true);
				values = {
					{5,   5},
					{4,   3},
					{6,   7},
					{4,   1},
					{6,   9},
					{4, -50},
					{6,  50}
				};
				for (auto& p : values) {
					Assert::AreEqual(p.first, range.bound(p.second));
				}



				// No Strict, Loop bound
				range.setMinStrict(false);
				range.setMaxStrict(false);
				values = {
					{5,  5},
					{3,  3},
					{3,  7},
					{5,  1},
					{5,  9},
					{6, -20},
					{4,  20}
				};
				for (auto& p : values) {
					Assert::AreEqual(p.first, range.boundMod(p.second));
				}

				// Min Strict, Loop bound
				range.setMinStrict(true);
				range.setMaxStrict(false);
				for (auto& p : values) {
					Assert::AreEqual(p.first, range.boundMod(p.second));
				}

				// Max Strict, Loop bound
				range.setMinStrict(false);
				range.setMaxStrict(true);
				for (auto& p : values) {
					Assert::AreEqual(p.first, range.boundMod(p.second));
				}

				// Min Max Strict, Loop bound
				range.setMinStrict(true);
				range.setMaxStrict(true);
				for (auto& p : values) {
					Assert::AreEqual(p.first, range.boundMod(p.second));
				}
			}
		};
	}

}