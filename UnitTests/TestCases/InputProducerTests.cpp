#include "CppUnitTest.h"
#include <TestUtility/CustomToString.hpp>
#include <TestUtility/testOperator.hpp>

#include <TestUtility/TestEventRetriever.hpp>
#include <TestUtility/EventFactory.hpp>

#include <Input/EventRetriever.hpp>
#include <Input/InputProducer.hpp>
#include <Input/JoystickInput.hpp>
#include <Input/KeyboardInput.hpp>
#include <Input/MouseInput.hpp>
#include <Input/WindowInput.hpp>

#include <boolinq/boolinq.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace InputTests {

	TEST_CLASS(InputProducerTests) {
	private:
		std::function<unsigned(Input*)> distinct = [](Input* in) { return in->hashCode(); };


	public:
		TEST_METHOD(WindowInputTest) {
			std::vector<sf::Event> events = {
				EventFactory::windowEvent(sf::Event::Closed),
				EventFactory::windowEvent(sf::Event::LostFocus),
				EventFactory::windowEvent(sf::Event::GainedFocus),
				EventFactory::windowEvent(sf::Event::MouseEntered),
				EventFactory::windowEvent(sf::Event::MouseLeft),
				EventFactory::windowEvent(sf::Event::Resized, sf::Vector2i(100, 200)),
				EventFactory::windowEvent(sf::Event::Resized, sf::Vector2i(200, 100))
			};

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			eventRetriever->addEvents(events);
			InputProducer producer(eventRetriever);
			auto inputs = producer.retrieveInputs(sf::seconds(0));

			Assert::AreEqual(events.size(), inputs.size());

			Assert::AreEqual(6, linq::from(inputs).distinct(distinct).count());

			for (size_t i = 0; i < events.size(); ++i) {
				WindowInput* input = dynamic_cast<WindowInput*>(inputs[i]);
				Assert::IsTrue(Input::Window | input->getSource());
				if (events[i].type == sf::Event::Resized) {
					Assert::AreEqual(Input::Resize, input->getSource());
					Assert::AreEqual(events[i].size.width, (unsigned)((ResizeInput*)input)->getResize().x);
					Assert::AreEqual(events[i].size.height, (unsigned)((ResizeInput*)input)->getResize().y);
				} else {
					Assert::AreEqual(Input::OtherWindow, input->getSource());
				}
				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				Assert::AreEqual(0.0f, input->getStrength());
				Assert::IsFalse(input->isRealtime());

				Assert::AreEqual((unsigned)events[i].type, (unsigned)input->getEventType());
				Assert::AreEqual((unsigned)events[i].type, input->getCode());
			}
		}

		TEST_METHOD(ButtonInputTest) {
			std::vector<sf::Event> events = {
				EventFactory::joystickButtonEvent(Joystick::B, true, 0),
				EventFactory::joystickButtonEvent(Joystick::A, true, 1),
				EventFactory::keyboardEvent(sf::Keyboard::A, true),
				EventFactory::mouseButtonEvent(sf::Mouse::Left, true, sf::Vector2i(0,0))
			};
			std::vector<sf::Event> axisEvents = {
				EventFactory::joystickMoveEvent(Joystick::Axis::HorizontalButton, 0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::Axis::VerticalButton, -0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::Axis::Trigger, 0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::Axis::Trigger, -0.5f, 0)
			};

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			InputProducer producer(eventRetriever);

			eventRetriever->addEvents(events);
			eventRetriever->addEvents(axisEvents);
			auto inputs = producer.retrieveInputs(sf::seconds(0));
			Assert::AreEqual(events.size() + axisEvents.size(), inputs.size());
			for (auto input : inputs) {
				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				Assert::IsTrue(dynamic_cast<ButtonInput*>(input)->isPressed());
				Assert::IsFalse(dynamic_cast<ButtonInput*>(input)->isReleased());
			}

			eventRetriever->addEvents(axisEvents);
			inputs = producer.retrieveInputs(sf::milliseconds(1000));
			Assert::AreEqual(events.size() + axisEvents.size(), inputs.size());
			for (auto input : inputs) {
				Assert::AreEqual(sf::milliseconds(1000), input->getLifeTime());
				Assert::IsTrue(dynamic_cast<ButtonInput*>(input)->isPressed());
				Assert::IsFalse(dynamic_cast<ButtonInput*>(input)->isReleased());
			}

			std::vector<sf::Event> releaseEvents = {
				EventFactory::joystickButtonEvent(Joystick::B, false, 0),
				EventFactory::joystickButtonEvent(Joystick::A, false, 0),
				EventFactory::joystickButtonEvent(Joystick::A, true, 1),
				EventFactory::joystickMoveEvent(Joystick::Axis::VerticalButton, -0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::Axis::Trigger, 0.5f, 0),
				EventFactory::keyboardEvent(sf::Keyboard::A, false),
			};
			std::vector<bool> pressedInputs = {false, true, false, true, false, true, true, false};
			eventRetriever->addEvents(releaseEvents);
			inputs = producer.retrieveInputs(sf::milliseconds(1000));
			Assert::AreEqual((size_t)8, inputs.size());
			Assert::AreEqual(pressedInputs.size(), inputs.size());
			for (size_t i = 0; i < inputs.size(); ++i) {
				Input* input = inputs[i];
				if (input->getSource() == Input::JoystickButton && dynamic_cast<JoystickButtonInput*>(input)->getJoystickId() == 0 && dynamic_cast<JoystickButtonInput*>(input)->getButton() == Joystick::A) {
					Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				} else {
					Assert::AreEqual(sf::milliseconds(2000), input->getLifeTime());
				}

				Assert::AreEqual((bool)pressedInputs[i], dynamic_cast<ButtonInput*>(input)->isPressed());
				Assert::AreNotEqual((bool)pressedInputs[i], dynamic_cast<ButtonInput*>(input)->isReleased());
			}


			inputs = producer.retrieveInputs(sf::milliseconds(1000));
			pressedInputs = { true, true, false, false };
			Assert::AreEqual((size_t)4, inputs.size());
			for (size_t i = 0; i < inputs.size(); ++i) {
				Input* input = inputs[i];

				Assert::AreEqual(sf::milliseconds(3000), input->getLifeTime());
				
				Assert::AreEqual((bool)pressedInputs[i], dynamic_cast<ButtonInput*>(input)->isPressed());
				Assert::AreNotEqual((bool)pressedInputs[i], dynamic_cast<ButtonInput*>(input)->isReleased());
			}
		}

		TEST_METHOD(UpdateInputTest) {
			std::vector<sf::Event> events = {
				EventFactory::joystickButtonEvent(Joystick::B, true, 0),
				EventFactory::keyboardEvent(sf::Keyboard::A, true),
				EventFactory::textEvent(0x0061),
				EventFactory::mouseButtonEvent(sf::Mouse::Left, true, sf::Vector2i(0,0)),
				EventFactory::mouseMoveEvent(sf::Vector2i(0,0)),
				EventFactory::mouseWheelEvent(sf::Mouse::Wheel::HorizontalWheel, 0.5f, sf::Vector2i(0,0)),
				EventFactory::joystickStatusEvent(false, 0)
			};

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			InputProducer producer(eventRetriever);
			eventRetriever->addEvents(events);
			auto inputs = producer.retrieveInputs(sf::seconds(0));

			Assert::AreEqual(events.size(), inputs.size());

			inputs = producer.retrieveInputs(sf::milliseconds(1000));
			Assert::AreEqual((size_t)3, inputs.size());

			std::vector<sf::Event> releaseEvents = {
				EventFactory::joystickButtonEvent(Joystick::B, false, 0),
				EventFactory::keyboardEvent(sf::Keyboard::A, false),
			};
			eventRetriever->addEvents(releaseEvents);
			inputs = producer.retrieveInputs(sf::milliseconds(1000));
			Assert::AreEqual((size_t)3, inputs.size());

			inputs = producer.retrieveInputs(sf::milliseconds(1000));
			Assert::AreEqual((size_t)1, inputs.size());
		}

		// ### KEYBOARD ### //
		TEST_METHOD(TextInputTest) {
			std::vector<sf::Event> events = {
				EventFactory::textEvent(0x0041),
				EventFactory::textEvent(0x0042),
				EventFactory::textEvent(0x0043),
				EventFactory::textEvent(0x0061),
				EventFactory::textEvent(0x0062)
			};
			std::string chars = "ABCab";

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			eventRetriever->addEvents(events);
			InputProducer producer(eventRetriever);
			auto inputs = producer.retrieveInputs(sf::seconds(0));

			Assert::AreEqual(events.size(), inputs.size());
			Assert::AreEqual(1, linq::from(inputs).distinct(distinct).count());

			for (size_t i = 0; i < events.size(); ++i) {
				TextInput* input = dynamic_cast<TextInput*>(inputs[i]);
				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				Assert::AreEqual(Input::Text, input->getSource());
				Assert::AreEqual(0.0f, input->getStrength());
				Assert::AreEqual((unsigned)0, input->getCode());
				Assert::IsFalse(input->isRealtime());

				Assert::AreEqual(chars[i], input->getChar());
				Assert::AreEqual(events[i].text.unicode, input->getUnicode());
			}
		}
		TEST_METHOD(KeyboardInputTest) {
			std::vector<sf::Event> events = {
				EventFactory::keyboardEvent(sf::Keyboard::A, true, false, false, false, false),
				EventFactory::keyboardEvent(sf::Keyboard::B, true, false, false, false, true),
				EventFactory::keyboardEvent(sf::Keyboard::C, true, false, false, true , false),
				EventFactory::keyboardEvent(sf::Keyboard::D, true, false, false, true , true),
				EventFactory::keyboardEvent(sf::Keyboard::E, true, false, true , false, false),
				EventFactory::keyboardEvent(sf::Keyboard::F, true, false, true , false, true),
				EventFactory::keyboardEvent(sf::Keyboard::G, true, false, true , true , false),
				EventFactory::keyboardEvent(sf::Keyboard::H, true, false, true , true , true),
				EventFactory::keyboardEvent(sf::Keyboard::I, true, true , false, false, false),
				EventFactory::keyboardEvent(sf::Keyboard::J, true, true , false, false, true),
				EventFactory::keyboardEvent(sf::Keyboard::K, true, true , false, true , false),
				EventFactory::keyboardEvent(sf::Keyboard::L, true, true , false, true , true),
				EventFactory::keyboardEvent(sf::Keyboard::M, true, true , true , false, false),
				EventFactory::keyboardEvent(sf::Keyboard::N, true, true , true , false, true),
				EventFactory::keyboardEvent(sf::Keyboard::O, true, true , true , true , false),
				EventFactory::keyboardEvent(sf::Keyboard::P, true, true , true , true , true) };

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			eventRetriever->addEvents(events);

			InputProducer producer(eventRetriever);
			auto inputs = producer.retrieveInputs(sf::seconds(0));

			Assert::AreEqual(events.size(), inputs.size());
			Assert::AreEqual(16, linq::from(inputs).distinct(distinct).count());

			for (size_t i = 0; i < events.size(); ++i) {
				KeyboardInput* input = dynamic_cast<KeyboardInput*>(inputs[i]);
				Assert::AreEqual(Input::Keyboard, input->getSource());
				Assert::AreEqual(1.0f, input->getStrength());
				Assert::IsTrue(input->isRealtime());

				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				Assert::AreEqual((unsigned)events[i].key.code, (unsigned)input->getKey());

				Assert::IsTrue(input->isPressed());
				Assert::IsFalse(input->isReleased());

				Assert::AreEqual(events[i].key.alt, (bool)(input->getAdditionalKey() & KeyboardInput::Alt));
				Assert::AreEqual(events[i].key.control, (bool)(input->getAdditionalKey() & KeyboardInput::Control));
				Assert::AreEqual(events[i].key.shift, (bool)(input->getAdditionalKey() & KeyboardInput::Shift));
				Assert::AreEqual(events[i].key.system, (bool)(input->getAdditionalKey() & KeyboardInput::System));
			}
		}

		//### MOUSE ### //
		TEST_METHOD(MouseButtonInputTest) {
			std::vector<sf::Event> events = {
				EventFactory::mouseButtonEvent(sf::Mouse::Left  , true, sf::Vector2i(0,0)),
				EventFactory::mouseButtonEvent(sf::Mouse::Middle, true, sf::Vector2i(10,0)),
				EventFactory::mouseButtonEvent(sf::Mouse::Right , true, sf::Vector2i(0,10))
			};

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			eventRetriever->addEvents(events);
			InputProducer producer(eventRetriever);
			auto inputs = producer.retrieveInputs(sf::seconds(0));

			Assert::AreEqual(events.size(), inputs.size());
			Assert::AreEqual(3, linq::from(inputs).distinct(distinct).count());

			for (size_t i = 0; i < events.size(); ++i) {
				MouseButtonInput* input = dynamic_cast<MouseButtonInput*>(inputs[i]);
				Assert::AreEqual(Input::MouseButton, input->getSource());
				Assert::AreEqual(1.0f, input->getStrength());
				Assert::IsTrue(input->isRealtime());

				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				Assert::IsTrue(input->isPressed());
				Assert::IsFalse(input->isReleased());

				Assert::AreEqual((unsigned)events[i].mouseButton.button, (unsigned)input->getButton());
				Assert::AreEqual((unsigned)events[i].mouseButton.button, input->getCode());
				Assert::AreEqual(events[i].mouseButton.x, input->getPosition().x);
				Assert::AreEqual(events[i].mouseButton.y, input->getPosition().y);
			}
		}
		TEST_METHOD(MouseWheelInputTest) {
			std::vector<sf::Event> events = {
				EventFactory::mouseWheelEvent(sf::Mouse::HorizontalWheel, 0.5f, sf::Vector2i(0,0)),
				EventFactory::mouseWheelEvent(sf::Mouse::HorizontalWheel, 1.f , sf::Vector2i(0,10)),
				EventFactory::mouseWheelEvent(sf::Mouse::VerticalWheel  , 1.f , sf::Vector2i(10,10)),
				EventFactory::mouseWheelEvent(sf::Mouse::VerticalWheel  , -1.f, sf::Vector2i(10,0)),
			};

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			eventRetriever->addEvents(events);
			InputProducer producer(eventRetriever);
			auto inputs = producer.retrieveInputs(sf::seconds(0));

			Assert::AreEqual(events.size(), inputs.size());
			Assert::AreEqual(3, linq::from(inputs).distinct(distinct).count());

			for (size_t i = 0; i < events.size(); ++i) {
				MouseWheelInput* input = dynamic_cast<MouseWheelInput*>(inputs[i]);
				Assert::AreEqual(Input::MouseWheel, input->getSource());
				Assert::AreEqual(10.f / events[i].mouseWheelScroll.delta, input->getStrength());
				Assert::IsFalse(input->isRealtime());

				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());

				Assert::AreEqual(events[i].mouseWheelScroll.x, input->getPosition().x);
				Assert::AreEqual(events[i].mouseWheelScroll.y, input->getPosition().y);

				Assert::AreEqual(events[i].mouseWheelScroll.delta, input->getDelta());
				Assert::AreEqual((unsigned)events[i].mouseWheelScroll.wheel, (unsigned)input->getWheelId());
			}
		}
		TEST_METHOD(MouseMoveInputTest) {
			std::vector<sf::Event> events = {
				EventFactory::mouseMoveEvent(sf::Vector2i(0,0)),
				EventFactory::mouseMoveEvent(sf::Vector2i(10,0)),
				EventFactory::mouseMoveEvent(sf::Vector2i(0,10))
			};

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			eventRetriever->addEvents(events);
			InputProducer producer(eventRetriever);
			auto inputs = producer.retrieveInputs(sf::seconds(0));

			Assert::AreEqual(events.size(), inputs.size());
			Assert::AreEqual(1, linq::from(inputs).distinct(distinct).count());

			for (size_t i = 0; i < events.size(); ++i) {
				MouseMoveInput* input = dynamic_cast<MouseMoveInput*>(inputs[i]);
				Assert::AreEqual(Input::MouseMove, input->getSource());
				Assert::AreEqual(0.0f, input->getStrength());
				Assert::IsFalse(input->isRealtime());
				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				Assert::AreEqual((unsigned)0, input->getCode());

				Assert::AreEqual(events[i].mouseMove.x, input->getPosition().x);
				Assert::AreEqual(events[i].mouseMove.y, input->getPosition().y);
			}
		}
		TEST_METHOD(MousePositionInputTest) {
			std::vector<sf::Vector2i> positions = {
				sf::Vector2i(0, 0),
				sf::Vector2i(0,10),
				sf::Vector2i(10, 0),
				sf::Vector2i(0,20),
				sf::Vector2i(10, 0)
			};

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			eventRetriever->addMousePositions(positions);
			InputProducer producer(eventRetriever);

			sf::Vector2i previous, current;
			for (size_t i = 0; i < positions.size() - 1; ++i) {
				auto inputs = producer.retrieveInputs(sf::seconds(0));
				Assert::AreEqual((size_t)1, inputs.size());

				MousePositionInput* input = dynamic_cast<MousePositionInput*>(inputs[0]);
				Assert::AreEqual(Input::MousePosition, input->getSource());
				Assert::AreEqual(0.0f, input->getStrength());
				Assert::IsTrue(input->isRealtime());
				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				Assert::AreEqual((unsigned)0, input->getCode());

				current = positions[i + 1];
				previous = positions[i];
				Assert::AreEqual(current, input->getPosition());
				Assert::AreEqual(previous, input->getPreviousPosition());
				Assert::AreEqual(current - previous, input->getMove());

			}
		}

		// ### JOYSTICK ### //
		TEST_METHOD(JoystickButtonInputTest) {
			std::vector<sf::Event> buttonEvents = {
				EventFactory::joystickButtonEvent(Joystick::A, true, 0),
				EventFactory::joystickButtonEvent(Joystick::B, true, 1),
				EventFactory::joystickButtonEvent(Joystick::X, true, 2),
				EventFactory::joystickButtonEvent(Joystick::Y, true, 0),
			};
			std::vector<sf::Event> axisEvents = {
				EventFactory::joystickMoveEvent(Joystick::Axis::HorizontalButton, 0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::Axis::HorizontalButton, -0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::Axis::VerticalButton, 0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::Axis::VerticalButton, -0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::Axis::Trigger, 0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::Axis::Trigger, -0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::Axis::Trigger, -0.5f, 1),
			};
			std::vector<Joystick::Button> axisToButton = {
				Joystick::DpadRight,
				Joystick::DpadLeft,
				Joystick::DpadUp,
				Joystick::DpadDown,
				Joystick::RightTrigger,
				Joystick::LeftTrigger,
				Joystick::LeftTrigger,
			};


			TestEventRetriever* eventRetriever = new TestEventRetriever();
			eventRetriever->addEvents(buttonEvents);
			eventRetriever->addEvents(axisEvents);
			InputProducer producer(eventRetriever);
			auto inputs = producer.retrieveInputs(sf::seconds(0));

			Assert::AreEqual(buttonEvents.size() + axisEvents.size(), inputs.size());
			Assert::AreEqual(11, linq::from(inputs).distinct(distinct).count());

			for (size_t i = 0; i < inputs.size(); ++i) {
				JoystickButtonInput* input = dynamic_cast<JoystickButtonInput*>(inputs[i]);
				bool isAxisEvent = i >= buttonEvents.size();
				size_t index = !isAxisEvent ? i : i - buttonEvents.size();
				sf::Event event;
				if (!isAxisEvent) event = buttonEvents[i];
				else event = axisEvents[i - buttonEvents.size()];

				Assert::AreEqual(Input::JoystickButton, input->getSource());
				Assert::IsTrue(input->isRealtime());

				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				Assert::IsTrue(input->isPressed());
				Assert::IsFalse(input->isReleased());

				if (!isAxisEvent) {
					Assert::AreEqual(1.0f, input->getStrength());
					Assert::AreEqual(event.joystickButton.button, (unsigned)input->getButton());
					Assert::AreEqual(event.joystickButton.joystickId, input->getJoystickId());
				} else if (event.type == sf::Event::JoystickMoved) {
					Assert::AreEqual(std::abs(event.joystickMove.position), input->getStrength());
					Assert::AreEqual((unsigned)axisToButton[index], (unsigned)input->getButton());
					Assert::AreEqual(event.joystickMove.joystickId, input->getJoystickId());
				} else {
					Assert::IsTrue(false);
				}
			}
		}
		TEST_METHOD(JoystickStatusInputTest) {
			std::vector<sf::Event> events = {
				EventFactory::joystickStatusEvent(true, 0),
				EventFactory::joystickStatusEvent(false, 1),
				EventFactory::joystickStatusEvent(false, 0),
				EventFactory::joystickStatusEvent(true, 0),
			};

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			eventRetriever->addEvents(events);
			InputProducer producer(eventRetriever);
			auto inputs = producer.retrieveInputs(sf::seconds(0));

			Assert::AreEqual(events.size(), inputs.size());
			Assert::AreEqual(3, linq::from(inputs).distinct(distinct).count());

			for (size_t i = 0; i < events.size(); ++i) {
				JoystickStatusInput* input = dynamic_cast<JoystickStatusInput*>(inputs[i]);
				Assert::AreEqual(Input::JoystickStatus, input->getSource());
				Assert::AreEqual(0.0f, input->getStrength());
				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				Assert::IsFalse(input->isRealtime());

				Assert::AreEqual(events[i].type == sf::Event::JoystickConnected, input->isConnection());
				Assert::AreEqual(events[i].type == sf::Event::JoystickDisconnected, input->isDisconnection());
				Assert::AreEqual(events[i].joystickConnect.joystickId, input->getJoystickId());
			}
		}
		TEST_METHOD(JoystickMoveInputTest) {
			std::vector<sf::Event> events = {
				EventFactory::joystickMoveEvent(Joystick::LeftHorizontalAxis , 0.5f, 0),
				EventFactory::joystickMoveEvent(Joystick::RightHorizontalAxis, 0.5f, 1),
				EventFactory::joystickMoveEvent(Joystick::LeftVerticalAxis   , 0.5f, 2),
				EventFactory::joystickMoveEvent(Joystick::RightVerticalAxis  , 0.5f, 1)
			};

			TestEventRetriever* eventRetriever = new TestEventRetriever();
			eventRetriever->addEvents(events);
			InputProducer producer(eventRetriever);
			auto inputs = producer.retrieveInputs(sf::seconds(0));

			Assert::AreEqual(events.size(), inputs.size());
			Assert::AreEqual(4, linq::from(inputs).distinct(distinct).count());

			for (size_t i = 0; i < events.size(); ++i) {
				JoystickMoveInput* input = dynamic_cast<JoystickMoveInput*>(inputs[i]);
				Assert::AreEqual(Input::JoystickMove, input->getSource());
				Assert::AreEqual(0.0f, input->getStrength());
				Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
				Assert::IsFalse(input->isRealtime());

				Assert::AreEqual((unsigned)events[i].joystickMove.axis, (unsigned)input->getAxis());
				Assert::AreEqual(events[i].joystickMove.position, input->getDelta());
				Assert::AreEqual(events[i].joystickMove.joystickId, input->getJoystickId());
			}
		}
		TEST_METHOD(JoystickPositionInputTest) {
			using PositionsMap = std::map<std::pair<unsigned, Joystick::Stick>, std::vector<sf::Vector2f>>;
			PositionsMap positions = {
				{ { 0,Joystick::LeftStick  }, {sf::Vector2f(0,0),sf::Vector2f(10,0),sf::Vector2f(0,10),sf::Vector2f(20,0),sf::Vector2f(0,20)} },
				{ { 0,Joystick::RightStick }, {sf::Vector2f(1,0),sf::Vector2f(10,1),sf::Vector2f(1,10),sf::Vector2f(20,1),sf::Vector2f(1,20)} },
				{ { 1,Joystick::LeftStick  }, {sf::Vector2f(2,0),sf::Vector2f(10,2),sf::Vector2f(2,10),sf::Vector2f(20,2),sf::Vector2f(2,20)} },
				{ { 1,Joystick::RightStick }, {sf::Vector2f(3,0),sf::Vector2f(10,3),sf::Vector2f(3,10),sf::Vector2f(20,3),sf::Vector2f(3,20)} }
			};
			TestEventRetriever* eventRetriever = new TestEventRetriever();

			for (auto position : positions) {
				eventRetriever->addStickPositions(position.first.first, position.first.second, position.second);
			}

			InputProducer producer(eventRetriever);

			for (size_t i = 0; i < positions.begin()->second.size() - 1; ++i) {
				auto inputs = producer.retrieveInputs(sf::seconds(0));
				Assert::AreEqual(positions.size(), inputs.size());
				Assert::AreEqual(4, linq::from(inputs).distinct(distinct).count());

				int inputIndex = 0;
				for (auto it = positions.begin(); it != positions.end(); ++it) {
					JoystickPositionInput* input = dynamic_cast<JoystickPositionInput*>(inputs[inputIndex]);
					Assert::AreEqual(Input::JoystickPosition, input->getSource());
					Assert::AreEqual(0.0f, input->getStrength());
					Assert::AreEqual(sf::Time::Zero, input->getLifeTime());
					Assert::IsTrue(input->isRealtime());


					std::pair<unsigned, Joystick::Stick> key = it->first;
					Assert::AreEqual(key.first, input->getJoystickId());
					Assert::AreEqual((unsigned)key.second, (unsigned)input->getStick());

					sf::Vector2f position = it->second[i + 1];
					sf::Vector2f previousPosition = it->second[i];
					Assert::AreEqual(position, input->getPosition());
					Assert::AreEqual(previousPosition, input->getPreviousPosition());
					Assert::AreEqual(position - previousPosition, input->getMove());

					inputIndex++;
				}
			}
		}
	};

}