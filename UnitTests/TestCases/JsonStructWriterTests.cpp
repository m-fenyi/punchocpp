#include "CppUnitTest.h"
#include <TestUtility/CustomToString.hpp>
#include <TestUtility/testOperator.hpp>
#include <TestUtility/JsonHelper.hpp>
#include <Utilities/File.hpp>

#include <DAL/JsonStructWriter.hpp>

#include <json/json.h>

#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Json {

	TEST_CLASS(JsonStructWriterTests) {
		
	public:
		TEST_METHOD(SimpleStructWriteTest) {
			struct SubComplexStruct {
				int simpleData1;
				int simpleData2;
			};
			struct ComplexStruct {
				SubComplexStruct data;
				int simpleData;
			};
			struct TestStruct {
				bool             testBool;
				sf::Color        testColor;
				int              testInt;
				float            testFloat;
				sf::Time         testTime;
				sf::Vector2i     testIntPoint;
				sf::Vector2f     testFloatPoint;
				sf::IntRect      testIntRect;
				sf::FloatRect    testFloatRect;
				std::string      testString;
				Math::Range<int> testRange;

				ComplexStruct    testComplex;
				std::vector<int> testVector;

				int              testRequiredMatch;
				int              testRequiredNoMatch;
				int              testNoRequiredMatch;
				int              testNoRequiredNoMatch;
			};

			TestStruct data;
			ComplexStruct complexData;
			SubComplexStruct subComplexData;
			
			JsonFieldsInfo fields1 = {
				JsonFieldInfo("simple1", &subComplexData.simpleData1, true, new IntegerRule()),
				JsonFieldInfo("simple2", &subComplexData.simpleData2, true, new IntegerRule())
			};
			JsonStructInfo<SubComplexStruct> structInfo1(fields1, &subComplexData);
			JsonFieldsInfo fields2 = {
				JsonFieldInfo("subComplex", &complexData.data      , true, new ComplexRule<SubComplexStruct>(std::move(structInfo1))),
				JsonFieldInfo("simple"    , &complexData.simpleData, true, new IntegerRule())
			};
			JsonStructInfo<ComplexStruct> structInfo2(fields2, &complexData);
			JsonFieldsInfo fields = {
				JsonFieldInfo("bool"      , &data.testBool             , true , new BoolRule()),
				JsonFieldInfo("color"     , &data.testColor            , true , new ColorRule()),
				JsonFieldInfo("int"       , &data.testInt              , true , new IntegerRule()),
				JsonFieldInfo("float"     , &data.testFloat            , true , new FloatRule()),
				JsonFieldInfo("time"      , &data.testTime             , true , new TimeRule()),
				JsonFieldInfo("intPoint"  , &data.testIntPoint         , true , new IntPointRule()),
				JsonFieldInfo("floatPoint", &data.testFloatPoint       , true , new FloatPointRule()),
				JsonFieldInfo("intRect"   , &data.testIntRect          , true , new IntRectRule()),
				JsonFieldInfo("floatRect" , &data.testFloatRect        , true , new FloatRectRule()),
				JsonFieldInfo("string"    , &data.testString           , true , new StringRule()),
				JsonFieldInfo("range"     , &data.testRange            , true , new RangeRule<int>()),
				JsonFieldInfo("complex"   , &data.testComplex          , true , new ComplexRule<ComplexStruct>(structInfo2)),
				JsonFieldInfo("vector"    , &data.testVector           , true , new VectorRule<int>(new IntegerRule())),
				
				JsonFieldInfo("RM"        , &data.testRequiredMatch    , true , new IntegerRule()),
				JsonFieldInfo("RNM"       , &data.testRequiredNoMatch  , true , new IntegerRule()),
				JsonFieldInfo("NRM"       , &data.testNoRequiredMatch  , false, new IntegerRule()),
				JsonFieldInfo("NRNM"      , &data.testNoRequiredNoMatch, false, new IntegerRule())
			};
			JsonStructInfo<TestStruct> structInfo(fields, &data);
			
			TestStruct writeData{
				true,
				sf::Color(1,2,3,4),
				5,
				6.f,
				sf::milliseconds(555),
				sf::Vector2i(2,3),
				sf::Vector2f(4.5f,5.5f),
				sf::IntRect(6,7,8,9),
				sf::FloatRect(1.5f, 2.5f, 3.5f, 4.5f),
				"test",
				Range::above(5),
				ComplexStruct{SubComplexStruct{7,8},9},
				{1, 2, 4, 8, 16},
				5,
				15,
				5,
				15
			};
			Json::Value value = JsonStructWriter::structToJson(structInfo, writeData);

			std::string actual(JsonHelper::writeToString(value));
			std::string expected = "{\"NRM\":5,\"NRNM\":15,\"RM\":5,\"RNM\":15,\"bool\":true,\"color\":[1,2,3,4],\"complex\":{\"simple\":9,\"subComplex\":{\"simple1\":7,\"simple2\":8}},\"float\":6.0,\"floatPoint\":[4.5,5.5],\"floatRect\":[1.5,2.5,3.5,4.5],\"int\":5,\"intPoint\":[2,3],\"intRect\":[6,7,8,9],\"range\":{\"min\":5},\"string\":\"test\",\"time\":555,\"vector\":[1,2,4,8,16]}";
			
			Assert::AreEqual(expected, actual);
		}

	};

}