#include "CppUnitTest.h"
#include <TestUtility/CustomToString.hpp>
#include <TestUtility/testOperator.hpp>
#include <TestUtility/TestHelper.hpp>

#include <Utilities/Time.hpp>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UtilitiesTests::TimeTests {
	TEST_CLASS(DurationTests) {
	public:
		TEST_METHOD(CompletedTest) {
			std::vector<Time::Duration> testValues{
				Time::Duration(sf::seconds(-10)),
				Time::Duration(sf::seconds(0)),
				Time::Duration(sf::seconds(2)),
				Time::Duration(sf::seconds(5)),
				Time::Duration(sf::seconds(10)),
				Time::Duration()
			};

			std::vector<bool> completed{ true, true, false, false, false, false };
			TestHelper::AreEqualM(completed, testValues, &Time::Duration::isCompleted);

			TestHelper::ApplyM(testValues, &Time::Duration::update, sf::seconds(5));

			std::vector<bool> completed2{ true, true, true, true, false, false };
			TestHelper::AreEqualM(completed2, testValues, &Time::Duration::isCompleted);

			TestHelper::ApplyM(testValues, &Time::Duration::update, sf::seconds(5));

			std::vector<bool> completed3{ true, true, true, true, true, false };
			TestHelper::AreEqualM(completed3, testValues, &Time::Duration::isCompleted);

			TestHelper::ApplyM(testValues, &Time::Duration::setEndTime, sf::seconds(5));

			TestHelper::ApplyM(testValues, &Time::Duration::reset);

			TestHelper::AreEqualM(false, testValues, &Time::Duration::isCompleted);
		}

		TEST_METHOD(ProgressTest) {
			std::vector<Time::Duration> testValues{
				Time::Duration(sf::seconds(-10)),
				Time::Duration(sf::seconds(0)),
				Time::Duration(sf::seconds(2)),
				Time::Duration(sf::seconds(5)),
				Time::Duration(sf::seconds(10)),
				Time::Duration()
			};

			std::vector<float> progress{ 1.f, 1.f, 0.f, 0.f, 0.f, 0.f };
			TestHelper::AreEqualM(progress, testValues, &Time::Duration::getProgress);

			TestHelper::ApplyM(testValues, &Time::Duration::update, sf::seconds(5));

			std::vector<float> progress1{ 1.f, 1.f, 2.5f, 1.f, 0.5f, 0.f };
			TestHelper::AreEqualM(progress1, testValues, &Time::Duration::getProgress);

			TestHelper::ApplyM(testValues, &Time::Duration::update, sf::seconds(5));

			std::vector<float> progress2{ 1.f, 1.f, 5.f, 2.f, 1.f, 0.f };
			TestHelper::AreEqualM(progress2, testValues, &Time::Duration::getProgress);

			TestHelper::ApplyM(testValues, &Time::Duration::setEndTime, sf::seconds(5));

			TestHelper::AreEqualM(2.f, testValues, &Time::Duration::getProgress);

			TestHelper::ApplyM(testValues, &Time::Duration::reset);
			TestHelper::AreEqualM(0.f, testValues, &Time::Duration::getProgress);
		}

		TEST_METHOD(TimeTest) {
			std::vector<Time::Duration> testValues{
				Time::Duration(sf::seconds(-10)),
				Time::Duration(sf::seconds(0)),
				Time::Duration(sf::seconds(2)),
				Time::Duration(sf::seconds(5)),
				Time::Duration(sf::seconds(10)),
				Time::Duration()
			};

			std::vector<sf::Time> timeLeft0{ sf::Time::Zero, sf::Time::Zero, sf::seconds(2), sf::seconds(5), sf::seconds(10) , sf::Time::Zero };
			TestHelper::AreEqualM(timeLeft0, testValues, &Time::Duration::getTimeLeft);

			TestHelper::ApplyM(testValues, &Time::Duration::update, sf::seconds(5));

			std::vector<sf::Time> timeLeft1{ sf::Time::Zero, sf::Time::Zero, sf::Time::Zero, sf::Time::Zero, sf::seconds(5) , sf::Time::Zero };
			TestHelper::AreEqualM(timeLeft1, testValues, &Time::Duration::getTimeLeft);

			TestHelper::ApplyM(testValues, &Time::Duration::update, sf::seconds(5));

			TestHelper::AreEqualM(sf::Time::Zero, testValues, &Time::Duration::getTimeLeft);

			TestHelper::AreEqualM(sf::seconds(10), testValues, &Time::Duration::getCurrentTime);

			std::vector<sf::Time> endTime{ sf::seconds(-10), sf::seconds(0), sf::seconds(2), sf::seconds(5), sf::seconds(10) , sf::Time::Zero };
			TestHelper::AreEqualM(endTime, testValues, &Time::Duration::getEndTime);

			TestHelper::ApplyM(testValues, &Time::Duration::setEndTime, sf::seconds(5));

			TestHelper::AreEqualM(sf::seconds(5), testValues, &Time::Duration::getEndTime);

			TestHelper::ApplyM(testValues, &Time::Duration::reset);

			TestHelper::AreEqualM(sf::Time::Zero, testValues, &Time::Duration::getCurrentTime);
		}

		};

		TEST_CLASS(TransitionTests) {
	public:
		TEST_METHOD(LinearTransitionTest) {
			using TestTransition = Time::LinearTransition<float>;
			std::vector<TestTransition> testValues{
				TestTransition(-10.f,-5.f, sf::seconds(10)),
				TestTransition(-5.f,  0.f, sf::seconds(10)),
				TestTransition(-5.f,  5.f, sf::seconds(10)),
				TestTransition(0.f,  5.f, sf::seconds(10)),
				TestTransition(5.f, 10.f, sf::seconds(10)),
			};

			std::vector<float> value0{ -10.f, -5.f, -5.f, 0.f, 5.f };
			TestHelper::AreEqualM(value0, testValues, &TestTransition::getValue);

			TestHelper::ApplyM(testValues, &TestTransition::update, sf::milliseconds(2500));

			std::vector<float> value1{ -8.75f, -3.75f, -2.5f, 1.25f, 6.25f };
			TestHelper::AreEqualM(value1, testValues, &TestTransition::getValue);

			TestHelper::ApplyM(testValues, &TestTransition::update, sf::milliseconds(2500));

			std::vector<float> value2{ -7.5f, -2.5f, 0.f, 2.5f, 7.5f };
			TestHelper::AreEqualM(value2, testValues, &TestTransition::getValue);

			TestHelper::ApplyM(testValues, &TestTransition::update, sf::milliseconds(2500));

			std::vector<float> value3{ -6.25f, -1.25f, 2.5f, 3.75f, 8.75f };
			TestHelper::AreEqualM(value3, testValues, &TestTransition::getValue);
			TestHelper::AreEqualM(false , testValues, &TestTransition::hasEnded);

			TestHelper::ApplyM(testValues, &TestTransition::update, sf::milliseconds(2500));

			std::vector<float> value4{ -5.f, 0.f, 5.f, 5.f, 10.f };
			TestHelper::AreEqualM(value4, testValues, &TestTransition::getValue);
			TestHelper::AreEqualM(true  , testValues, &TestTransition::hasEnded);
		}

		TEST_METHOD(EndlessTransitionTest) {
			using TestTransition = Time::EndlessTransition<float>;
			std::vector<TestTransition> testValues{
				TestTransition(5.f),
				TestTransition(5.f, 0.f),
				TestTransition(5.f, 1.f),
				TestTransition(5.f, -1.f),
			};

			std::vector<float> value0{ 5.f, 5.f, 5.f, 5.f };
			TestHelper::AreEqualM(value0, testValues, &TestTransition::getValue);

			TestHelper::ApplyM(testValues, &TestTransition::update, sf::milliseconds(500));

			std::vector<float> value1{ 5.f, 5.f, 5.5f, 4.5f };
			TestHelper::AreEqualM(value1, testValues, &TestTransition::getValue);

			TestHelper::ApplyM(testValues, &TestTransition::update, sf::milliseconds(1500));

			std::vector<float> value2{ 5.f, 5.f, 7.f, 3.f };
			TestHelper::AreEqualM(value2, testValues, &TestTransition::getValue);
		}

		TEST_METHOD(TransitionSequenceTest) {
			using TestTransition = Time::TransitionSequence<float>;

			std::vector<Time::Transition<float>*> transitions = {
				new Time::LinearTransition<float>(0.f,12.f, sf::milliseconds(1600)),
				new Time::LinearTransition<float>(12.f,8.f, sf::milliseconds(500)),
				new Time::EndlessTransition<float>(10.f),
				new Time::LinearTransition<float>(10.f, 0.f, sf::seconds(2)),
			};

			TestTransition testValue(transitions);

			std::vector<float> expected0 = {
				0.f, 1.5f, 3.f, 4.5f, 6.f, 7.5f, 9.f, 10.5f, 12.f, 10.4f, 8.8f, 10.f, 10.f, 10.f, 10.f
			};
			TestHelper::AreEqualM(expected0, testValue, 0.01f, &TestTransition::getValue, &TestTransition::update, sf::milliseconds(200));
			testValue.end();

			Assert::AreEqual(false, testValue.hasEnded());
			std::vector<float> expected1 = {
				10.f, 8.75f, 7.5f, 6.25f, 5.f, 3.75f, 2.5f
			};
			TestHelper::AreEqualM(expected1, testValue, 0.01f, &TestTransition::getValue, &TestTransition::update, sf::milliseconds(250));
			Assert::AreEqual(false, testValue.hasEnded());
			Assert::AreEqual(1.25f, testValue.getValue());

			testValue.update(sf::milliseconds(250));
			
			Assert::AreEqual(0.f, testValue.getValue());
			Assert::AreEqual(true, testValue.hasEnded());

			testValue.reset();
			std::vector<float> expected2 = {
				0.f, 8.8f, 10.f, 10.f, 10.f, 10.f, 10.f, 10.f, 10.f, 10.f
			};
			TestHelper::AreEqualM(expected2, testValue, 0.01f, &TestTransition::getValue, &TestTransition::update, sf::seconds(2));
			Assert::AreEqual(10.f, testValue.getValue());

			testValue.reset();
			Assert::AreEqual(0.f, testValue.getValue());
			testValue.update(sf::milliseconds(800));
			Assert::AreEqual(6.f, testValue.getValue());
			testValue.end();
			std::vector<float> expected3 = {
				6.f, 5.4f, 4.8f, 4.2f, 3.6f, 3.f, 2.4f, 1.8f, 1.2f, .6f, 0.f
			};
			TestHelper::AreEqualM(expected3, testValue, 0.01f, &TestTransition::getValue, &TestTransition::update, sf::milliseconds(200));
		}
	};
}