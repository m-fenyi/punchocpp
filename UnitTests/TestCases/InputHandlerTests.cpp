#include "CppUnitTest.h"
#include <TestUtility/CustomToString.hpp>
#include <TestUtility/testOperator.hpp>

#include <Input/InputHandler.hpp>
#include <Input/WindowInput.hpp>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace InputTests {

	TEST_CLASS(InputHandlerTests) {
	public:
		
		TEST_METHOD(DescriptiveInputHandlerTest) {
			enum TestAction {
				Matching,
				Multi,
				Multi1,
				Multi2,
				Multi3,

				Failing,
				NoAction,
				NoMatcher
			};
			std::string test;
			InputHandler<TestAction> handler;

			auto multiFunc = [&] { test += "3"; };
			handler.setListener<void>(Matching, [&] { test += "1"; });
			handler.setListener<void>(Multi, [&] { test += "2"; });
			handler.setListener<void>(Multi1, multiFunc);
			handler.setListener<void>(Multi2, multiFunc);
			handler.setListener<void>(Multi3, multiFunc);

			handler.setListener<void>(Failing, [&] { test += "6"; });
			handler.setListener<void>(NoMatcher, [&] { test += "7"; });


			InputMatcherBuilder builder;
			auto multiMatch = builder.setEventType(sf::Event::GainedFocus).build();
			handler.addMatcher(builder.setEventType(sf::Event::Closed).build(), Matching);
			handler.addMatcher(multiMatch, Multi);
			handler.addMatcher(multiMatch, Multi);
			handler.addMatcher(multiMatch, Multi);
			handler.addMatcher(builder.setEventType(sf::Event::LostFocus).build(), Multi1);
			handler.addMatcher(builder.setEventType(sf::Event::MouseEntered).build(), Multi2);
			handler.addMatcher(builder.setEventType(sf::Event::MouseLeft).build(), Multi3);

			handler.addMatcher(builder.setKeyboardKey(sf::Keyboard::A).build(), Failing);
			handler.addMatcher(builder.setInputSource(Input::All).build(), NoAction);

			std::vector<WindowInput> inputs = {
				WindowInput(sf::Event::Closed),
				WindowInput(sf::Event::GainedFocus),
				WindowInput(sf::Event::LostFocus),
				WindowInput(sf::Event::MouseEntered),
				WindowInput(sf::Event::MouseLeft),
			};

			for (auto input : inputs) handler.handleInput(input);

			Assert::AreEqual(std::string("1222333"), test);
		}
	};

}