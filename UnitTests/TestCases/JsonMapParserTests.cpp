#include "CppUnitTest.h"
#include <TestUtility/CustomToString.hpp>
#include <TestUtility/testOperator.hpp>
#include <TestUtility/JsonHelper.hpp>
#include <Utilities/File.hpp>

#include <DAL/JsonMapParser.hpp>

#include <json/json.h>

#include <boolinq/boolinq.h>

#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Json {

	TEST_CLASS(JsonMapParserTests) {

	public:
		TEST_METHOD(ParseTest) {
			struct ExpectedPair {
				std::string key;
				std::string value;
			};
			std::string json = "{"
					"\"base\" : \"value\","
					"\"prefix1\" : {"
						"\"base1\" : \"value1/1\","
						"\"base2\" : \"value1/2\","
						"\"prefix1-1\" : {"
							"\"base1\" : \"value1-1/1\","
							"\"base2\" : \"value1-1/2\","
							"\"base3\" : 0"
						"},"
						"\"prefix1-2\" : {"
							"\"base1\" : \"value1-2/1\","
							"\"base2\" : \"value1-2/2\""
						"}"
					"},"
					"\"prefix2\" : {"
						"\"base1\" : \"value2/1\","
						"\"base2\" : \"value2/2\","
						"\"prefix2-1\" : {"
							"\"base1\" : \"value2-1/1\","
							"\"base2\" : \"value2-1/2\","
							"\"prefix2-1-1\" : {"
								"\"base1\" : \"value2-1-1/1\","
								"\"base2\" : \"value2-1-1/2\""
							"}"
						"}"
					"}"
				"}";
			Json::Value root = JsonHelper::parseFromString(json);

			std::vector<ExpectedPair> expected = {
				{ "base"                               , "value" },
				{ "prefix1.base1"                      , "value1/1" },
				{ "prefix1.base2"                      , "value1/2" },
				{ "prefix1.prefix1-1.base1"            , "value1-1/1" },
				{ "prefix1.prefix1-1.base2"            , "value1-1/2" },
				{ "prefix1.prefix1-2.base1"            , "value1-2/1" },
				{ "prefix1.prefix1-2.base2"            , "value1-2/2" },

				{ "prefix2.base1"                      , "value2/1" },
				{ "prefix2.base2"                      , "value2/2" },
				{ "prefix2.prefix2-1.base1"            , "value2-1/1" },
				{ "prefix2.prefix2-1.base2"            , "value2-1/2" },
				{ "prefix2.prefix2-1.prefix2-1-1.base1", "value2-1-1/1" },
				{ "prefix2.prefix2-1.prefix2-1-1.base2", "value2-1-1/2" }
			};

			auto map = JsonMapParser::parseJson(root);

			Assert::AreEqual((size_t)13, map.size());

			for (auto pair : expected) {
				Assert::IsTrue(linq::from(map).select([](auto p) { return p.first; }).contains(pair.key));
			}

			for (auto pair : expected) {
				Assert::AreEqual(pair.value, map[pair.key]);
			}
		}
	};

}