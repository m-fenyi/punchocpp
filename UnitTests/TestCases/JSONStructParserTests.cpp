#include "CppUnitTest.h"
#include <TestUtility/CustomToString.hpp>
#include <TestUtility/JsonHelper.hpp>
#include <TestUtility/testOperator.hpp>
#include <Utilities/File.hpp>

#include <DAL/JsonStructParser.hpp>

#include <json/json.h>

#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Json {


	TEST_CLASS(JsonStructParserTests) {
		template<class type>
		struct IncorrectValues {
			type incorrectArray;
			type incorrectBool;
			type incorrectNull;
			type incorrectNumeric;
			type incorrectString;
			type incorrectObject;
		};

		template<class type>
		struct RequiredValues {
			type requiredPresent;
			type requiredMissing;
			type notRequiredPresent;
			type notRequiredMissing;
		};

		template<class type>
		struct TestStruct : IncorrectValues<type>, RequiredValues<type> { };

	public:
		TEST_METHOD(BoolTests) { 
			struct BoolStruct : TestStruct<bool> {
				bool isTrue;
				bool isFalse;
				bool integerTrue;
				bool integerFalse;
			};
			std::string json = "{"
				"\"isTrue\"            : true,"
				"\"isFalse\"           : false,"
				"\"integerTrue\"       : 1,"
				"\"integerFalse\"      : 0,"
				"\"requiredPresent\"   : true,"
				"\"notRequiredPresent\": true,"

				"\"incorrectArray\"    : [ ],"
				"\"incorrectNull\"     : null,"
				"\"incorrectObject\"   : {},"
				"\"incorrectString\"   : \"\""
				"}";

			BoolStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("isTrue"            , &dataStruct.isTrue            , true , new BoolRule(false)),
				JsonFieldInfo("isFalse"           , &dataStruct.isFalse           , true , new BoolRule(false)),
				JsonFieldInfo("integerTrue"       , &dataStruct.integerTrue       , true , new BoolRule(false)),
				JsonFieldInfo("integerFalse"      , &dataStruct.integerFalse      , true , new BoolRule(false)),
				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new BoolRule(false)),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new BoolRule(false)),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new BoolRule(false)),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new BoolRule(false)),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new BoolRule(true)),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new BoolRule(true)),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new BoolRule(true)),
				JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new BoolRule(true)),
			};
			JsonStructInfo<BoolStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);
			
			Assert::IsTrue(dataStruct.isTrue);
			Assert::IsFalse(dataStruct.isFalse);

			Assert::IsTrue(dataStruct.integerTrue);
			Assert::IsFalse(dataStruct.integerFalse);

			Assert::IsTrue(dataStruct.requiredPresent);
			Assert::IsFalse(dataStruct.requiredMissing);

			Assert::IsTrue(dataStruct.notRequiredPresent);
			Assert::IsFalse(dataStruct.notRequiredMissing);

			Assert::IsTrue(dataStruct.incorrectArray);
			Assert::IsFalse(dataStruct.incorrectNull);
			Assert::IsTrue(dataStruct.incorrectObject);
			Assert::IsTrue(dataStruct.incorrectString);
		}

		TEST_METHOD(ColorTests) { 
			struct ColorStruct : TestStruct<sf::Color> {
				sf::Color rgb;
				sf::Color rgba;
			};
			std::string json = "{"
				"\"rgb\"               : [128,128,128],"
				"\"rgba\"              : [128,128,128,128],"
				"\"requiredPresent\"   : [128,128,128],"
				"\"notRequiredPresent\": [128,128,128],"
				"\"incorrectArray\"    : [],"
				"\"incorrectBool\"     : true,"
				"\"incorrectNull\"     : null,"
				"\"incorrectNumeric\"  : 0,"
				"\"incorrectObject\"   : {},"
				"\"incorrectString\"   : \"\""
				"}";

			sf::Color default = sf::Color::White;
			ColorStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("rgb"               , &dataStruct.rgb               , true , new ColorRule(default)),
				JsonFieldInfo("rgba"              , &dataStruct.rgba              , true , new ColorRule(default)),
				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new ColorRule(default)),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new ColorRule(default)),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new ColorRule(default)),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new ColorRule(default)),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new ColorRule(default)),
				JsonFieldInfo("incorrectBool"     , &dataStruct.incorrectBool     , true , new ColorRule(default)),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new ColorRule(default)),
				JsonFieldInfo("incorrectNumeric"  , &dataStruct.incorrectNumeric  , true , new ColorRule(default)),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new ColorRule(default)),
				JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new ColorRule(default)),
			};
			JsonStructInfo<ColorStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			Assert::AreEqual(sf::Color(128,128,128)    , dataStruct.rgb);
			Assert::AreEqual(sf::Color(128,128,128,128), dataStruct.rgba);
			Assert::AreEqual(sf::Color(128,128,128)    , dataStruct.requiredPresent);
			Assert::AreEqual(sf::Color()               , dataStruct.requiredMissing);
			Assert::AreEqual(sf::Color(128,128,128)    , dataStruct.notRequiredPresent);
			Assert::AreEqual(default                   , dataStruct.notRequiredMissing);

			Assert::AreEqual(default                   , dataStruct.incorrectArray);
			Assert::AreEqual(default                   , dataStruct.incorrectBool);
			Assert::AreEqual(sf::Color()               , dataStruct.incorrectNull);
			Assert::AreEqual(default                   , dataStruct.incorrectNumeric);
			Assert::AreEqual(default                   , dataStruct.incorrectObject);
			Assert::AreEqual(default                   , dataStruct.incorrectString);
		}
		
		TEST_METHOD(IntegerTests) {
			struct IntegerStruct : TestStruct<int> {
				int positive;
				int negative;
				int floating;
				int unsignedPositive;
				int unsignedNegative;
				int unsignedMissing;
				int belowMin;
				int equalMin;
				int aboveMinBelowMax;
				int equalMax;
				int aboveMax;
			};
			std::string json = "{"
				"\"positive\"          :  10,"
				"\"negative\"          : -10,"
				"\"unsignedPositive\"  :  10,"
				"\"unsignedNegative\"  : -10,"
				"\"floating\"          :  10.8,"
				"\"requiredPresent\"   : -10,"
				"\"notRequiredPresent\": -10,"
				"\"belowMin\"          : -100,"
				"\"equalMin\"          : -50,"
				"\"aboveMinBelowMax\"  :  5,"
				"\"equalMax\"          :  50,"
				"\"aboveMax\"          :  100,"

				"\"incorrectArray\"    :  [ ],"
				"\"incorrectBool\"     :  true,"
				"\"incorrectNull\"     :  null,"
				"\"incorrectObject\"   :  {},"
				"\"incorrectString\"   :  \"\""
				"}";

			int min(-50), max(50), default(7);
			IntegerStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("positive"          , &dataStruct.positive          , true , new IntegerRule(default)),
				JsonFieldInfo("negative"          , &dataStruct.negative          , true , new IntegerRule(default)),
				JsonFieldInfo("floating"          , &dataStruct.floating          , true , new IntegerRule(default)),
				JsonFieldInfo("unsignedPositive"  , &dataStruct.unsignedPositive  , true , new IntegerRule(false)),
				JsonFieldInfo("unsignedNegative"  , &dataStruct.unsignedNegative  , true , new IntegerRule(false)),
				JsonFieldInfo("unsignedMissing"   , &dataStruct.unsignedMissing   , true , new IntegerRule(false)),
				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new IntegerRule(default)),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new IntegerRule(default)),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new IntegerRule(default)),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new IntegerRule(default)),
				JsonFieldInfo("belowMin"          , &dataStruct.belowMin          , true , new IntegerRule(Range::between(min, max), default)),
				JsonFieldInfo("equalMin"          , &dataStruct.equalMin          , true , new IntegerRule(Range::between(min, max), default)),
				JsonFieldInfo("aboveMinBelowMax"  , &dataStruct.aboveMinBelowMax  , true , new IntegerRule(Range::between(min, max), default)),
				JsonFieldInfo("equalMax"          , &dataStruct.equalMax          , true , new IntegerRule(Range::between(min, max), default)),
				JsonFieldInfo("aboveMax"          , &dataStruct.aboveMax          , true , new IntegerRule(Range::between(min, max), default)),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new IntegerRule(default)),
				JsonFieldInfo("incorrectBool"     , &dataStruct.incorrectBool     , true , new IntegerRule(default)),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new IntegerRule(default)),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new IntegerRule(default)),
				JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new IntegerRule(default)),
			};
			JsonStructInfo<IntegerStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			Assert::AreEqual(10     , dataStruct.positive);
			Assert::AreEqual(-10    , dataStruct.negative);
			Assert::AreEqual(10     , dataStruct.floating);

			Assert::AreEqual(10     , dataStruct.unsignedPositive);
			Assert::AreEqual(0      , dataStruct.unsignedNegative);
			Assert::AreEqual(0      , dataStruct.unsignedMissing);
			
			Assert::AreEqual(-10    , dataStruct.requiredPresent);
			Assert::AreEqual(0      , dataStruct.requiredMissing);

			Assert::AreEqual(-10    , dataStruct.notRequiredPresent);
			Assert::AreEqual(default, dataStruct.notRequiredMissing);

			Assert::AreEqual(7      , dataStruct.belowMin);
			Assert::AreEqual(-50    , dataStruct.equalMin);
			Assert::AreEqual(5      , dataStruct.aboveMinBelowMax);
			Assert::AreEqual(default, dataStruct.equalMax);
			Assert::AreEqual(default, dataStruct.aboveMax);

			Assert::AreEqual(default, dataStruct.incorrectArray);
			Assert::AreEqual(default, dataStruct.incorrectBool);
			Assert::AreEqual(0      , dataStruct.incorrectNull);
			Assert::AreEqual(default, dataStruct.incorrectObject);
			Assert::AreEqual(default, dataStruct.incorrectString);

		}
		TEST_METHOD(FloatTests) {
			struct FloatStruct : TestStruct<float> {
				float positive;
				float negative;
				float integer;
				float unsignedPositive;
				float unsignedNegative;
				float unsignedMissing;
				float belowMin;
				float equalMin;
				float aboveMinBelowMax;
				float equalMax;
				float aboveMax;
			};
			std::string json = "{"
				"\"positive\"          :  10.5,"
				"\"negative\"          : -10.5,"
				"\"integer\"           :  10,"
				"\"unsignedPositive\"  :  10.5,"
				"\"unsignedNegative\"  : -10.5,"
				"\"requiredPresent\"   : -10.5,"
				"\"notRequiredPresent\": -10.5,"
				"\"belowMin\"          : -100.5,"
				"\"equalMin\"          : -50.5,"
				"\"aboveMinBelowMax\"  :  5.5,"
				"\"equalMax\"          :  50.5,"
				"\"aboveMax\"          :  100.5,"

				"\"incorrectArray\"    :  [ ],"
				"\"incorrectBool\"     :  true,"
				"\"incorrectNull\"     :  null,"
				"\"incorrectObject\"   :  {},"
				"\"incorrectString\"   :  \"\""
				"}";

			float min(-50.5f), max(50.5f), default(7.5f);
			FloatStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("positive"          , &dataStruct.positive          , true , new FloatRule(default)),
				JsonFieldInfo("negative"          , &dataStruct.negative          , true , new FloatRule(default)),
				JsonFieldInfo("integer"           , &dataStruct.integer           , true , new FloatRule(default)),
				JsonFieldInfo("unsignedPositive"  , &dataStruct.unsignedPositive  , true , new FloatRule(false)),
				JsonFieldInfo("unsignedNegative"  , &dataStruct.unsignedNegative  , true , new FloatRule(false)),
				JsonFieldInfo("unsignedMissing"   , &dataStruct.unsignedMissing   , true , new FloatRule(false)),
				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new FloatRule(default)),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new FloatRule(default)),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new FloatRule(default)),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new FloatRule(default)),
				JsonFieldInfo("belowMin"          , &dataStruct.belowMin          , true , new FloatRule(Range::between(min, max), default)),
				JsonFieldInfo("equalMin"          , &dataStruct.equalMin          , true , new FloatRule(Range::between(min, max), default)),
				JsonFieldInfo("aboveMinBelowMax"  , &dataStruct.aboveMinBelowMax  , true , new FloatRule(Range::between(min, max), default)),
				JsonFieldInfo("equalMax"          , &dataStruct.equalMax          , true , new FloatRule(Range::between(min, max), default)),
				JsonFieldInfo("aboveMax"          , &dataStruct.aboveMax          , true , new FloatRule(Range::between(min, max), default)),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new FloatRule(default)),
				JsonFieldInfo("incorrectBool"     , &dataStruct.incorrectBool     , true , new FloatRule(default)),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new FloatRule(default)),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new FloatRule(default)),
				JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new FloatRule(default)),
			};
			JsonStructInfo<FloatStruct> structInfo(fields, &dataStruct);
			
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			Assert::AreEqual(10.5f  , dataStruct.positive);
			Assert::AreEqual(-10.5f , dataStruct.negative);
			Assert::AreEqual(10.f   , dataStruct.integer);

			Assert::AreEqual(10.5f  , dataStruct.unsignedPositive);
			Assert::AreEqual(0.f    , dataStruct.unsignedNegative);
			Assert::AreEqual(0.f    , dataStruct.unsignedMissing);

			Assert::AreEqual(-10.5f , dataStruct.requiredPresent);
			Assert::AreEqual(0.f    , dataStruct.requiredMissing);

			Assert::AreEqual(-10.5f , dataStruct.notRequiredPresent);
			Assert::AreEqual(default, dataStruct.notRequiredMissing);

			Assert::AreEqual(default, dataStruct.belowMin);
			Assert::AreEqual(-50.5f , dataStruct.equalMin);
			Assert::AreEqual(5.5f   , dataStruct.aboveMinBelowMax);
			Assert::AreEqual(default, dataStruct.equalMax);
			Assert::AreEqual(default, dataStruct.aboveMax);

			Assert::AreEqual(default, dataStruct.incorrectArray);
			Assert::AreEqual(default, dataStruct.incorrectBool);
			Assert::AreEqual(0.f    , dataStruct.incorrectNull);
			Assert::AreEqual(default, dataStruct.incorrectObject);
			Assert::AreEqual(default, dataStruct.incorrectString);
		}

		TEST_METHOD(IntegerPointTests) {
			using Point = sf::Vector2i;
			struct IntegerPointStruct : TestStruct<Point> {
				Point XYpositive;
				Point XpositiveYnegative;
				Point XnegativeYpositive;
				Point XYnegative;

				Point XYfloating;

				Point unsignedXYpositive;
				Point unsignedXpositiveYnegative;
				Point unsignedXnegativeYpositive;
				Point unsignedXYnegative;

				Point XYbelowMin;
				Point XbelowMin;
				Point YbelowMin;
				
				Point XYaboveMinBelowMax;
				
				Point XYaboveMax;
				Point XaboveMax;
				Point YaboveMax;
				
				Point XbelowMinYaboveMax;
				Point XaboveMaxYbelowMin;

				Point XYequalMin;
				Point XequalMin;
				Point YequalMin;

				Point XequalMinYequalMax;
				Point XequalMaxYequalMin;

				Point XYequalMax;
				Point XequalMax;
				Point YequalMax;
			};
			std::string json = "{"
				"\"XYpositive\"                : [  10,   10],"
				"\"XpositiveYnegative\"        : [  10,  -10],"
				"\"XnegativeYpositive\"        : [ -10,   10],"
				"\"XYnegative\"                : [ -10,  -10],"

				"\"XYfloating\"                : [10.8, 10.8],"

				"\"unsignedXYpositive\"        : [  10,   10],"
				"\"unsignedXpositiveYnegative\": [  10,  -10],"
				"\"unsignedXnegativeYpositive\": [ -10,   10],"
				"\"unsignedXYnegative\"        : [ -10,  -10],"

				"\"requiredPresent\"           : [  10,   10],"
				"\"notRequiredPresent\"        : [  10,   10],"

				"\"XYbelowMin\"                : [-100, -100],"
				"\"XbelowMin\"                 : [-100,   10],"
				"\"YbelowMin\"                 : [  10, -100],"

				"\"XYaboveMinBelowMax\"        : [  10,   10],"

				"\"XYaboveMax\"                : [ 100,  100],"
				"\"XaboveMax\"                 : [ 100,   10],"
				"\"YaboveMax\"                 : [  10,  100],"

				"\"XbelowMinYaboveMax\"        : [-100,  100],"
				"\"XaboveMaxYbelowMin\"        : [ 100, -100],"

				"\"XYequalMin\"                : [ -50,  -50],"
				"\"XequalMin\"                 : [ -50,   10],"
				"\"YequalMin\"                 : [  10,  -50],"

				"\"XequalMinYequalMax\"        : [ -50,   50],"
				"\"XequalMaxYequalMin\"        : [  50,  -50],"

				"\"XYequalMax\"                : [  50,   50],"
				"\"XequalMax\"                 : [  50,   10],"
				"\"YequalMax\"                 : [  10,   50],"

				"\"incorrectArray\"             : [ ],"
				"\"incorrectBool\"              : true,"
				"\"incorrectNull\"              : null,"
				"\"incorrectNumeric\"           : 5,"
				"\"incorrectObject\"            : {},"
				"\"incorrectString\"            : \"\""
				"}";

			Point default(6, 7), min(-50, -50), max(50, 50);
			IntegerPointStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("XYpositive"                 , &dataStruct.XYpositive                , true , new IntPointRule(default)),
				JsonFieldInfo("XpositiveYnegative"         , &dataStruct.XpositiveYnegative        , true , new IntPointRule(default)),
				JsonFieldInfo("XnegativeYpositive"         , &dataStruct.XnegativeYpositive        , true , new IntPointRule(default)),
				JsonFieldInfo("XYnegative"                 , &dataStruct.XYnegative                , true , new IntPointRule(default)),

				JsonFieldInfo("XYfloating"                 , &dataStruct.XYfloating                , true , new IntPointRule(default)),

				JsonFieldInfo("unsignedXYpositive"         , &dataStruct.unsignedXYpositive        , true , new IntPointRule(false)),
				JsonFieldInfo("unsignedXpositiveYnegative" , &dataStruct.unsignedXpositiveYnegative, true , new IntPointRule(false)),
				JsonFieldInfo("unsignedXnegativeYpositive" , &dataStruct.unsignedXnegativeYpositive, true , new IntPointRule(false)),
				JsonFieldInfo("unsignedXYnegative"         , &dataStruct.unsignedXYnegative        , true , new IntPointRule(false)),

				JsonFieldInfo("requiredPresent"            , &dataStruct.requiredPresent           , true , new IntPointRule(default)),
				JsonFieldInfo("requiredMissing"            , &dataStruct.requiredMissing           , true , new IntPointRule(default)),
				JsonFieldInfo("notRequiredPresent"         , &dataStruct.notRequiredPresent        , false, new IntPointRule(default)),
				JsonFieldInfo("notRequiredMissing"         , &dataStruct.notRequiredMissing        , false, new IntPointRule(default)),

				JsonFieldInfo("XYbelowMin"                 , &dataStruct.XYbelowMin                , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XbelowMin"                  , &dataStruct.XbelowMin                 , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("YbelowMin"                  , &dataStruct.YbelowMin                 , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XYaboveMinBelowMax"         , &dataStruct.XYaboveMinBelowMax        , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XYaboveMax"                 , &dataStruct.XYaboveMax                , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XaboveMax"                  , &dataStruct.XaboveMax                 , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("YaboveMax"                  , &dataStruct.YaboveMax                 , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XbelowMinYaboveMax"         , &dataStruct.XbelowMinYaboveMax        , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XaboveMaxYbelowMin"         , &dataStruct.XaboveMaxYbelowMin        , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XYequalMin"                 , &dataStruct.XYequalMin                , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XequalMin"                  , &dataStruct.XequalMin                 , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("YequalMin"                  , &dataStruct.YequalMin                 , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XequalMinYequalMax"         , &dataStruct.XequalMinYequalMax        , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XequalMaxYequalMin"         , &dataStruct.XequalMaxYequalMin        , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XYequalMax"                 , &dataStruct.XYequalMax                , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XequalMax"                  , &dataStruct.XequalMax                 , true , new IntPointRule(Range::between(min, max), default)),
				JsonFieldInfo("YequalMax"                  , &dataStruct.YequalMax                 , true , new IntPointRule(Range::between(min, max), default)),

				JsonFieldInfo("incorrectArray"             , &dataStruct.incorrectArray            , true , new IntPointRule(default)),
				JsonFieldInfo("incorrectBool"              , &dataStruct.incorrectBool             , true , new IntPointRule(default)),
				JsonFieldInfo("incorrectNull"              , &dataStruct.incorrectNull             , true , new IntPointRule(default)),
				JsonFieldInfo("incorrectNumeric"           , &dataStruct.incorrectNumeric          , true , new IntPointRule(default)),
				JsonFieldInfo("incorrectObject"            , &dataStruct.incorrectObject           , true , new IntPointRule(default)),
				JsonFieldInfo("incorrectString"            , &dataStruct.incorrectString           , true , new IntPointRule(default)),
			};
			JsonStructInfo<IntegerPointStruct> structInfo(fields, &dataStruct);
			
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			Assert::AreEqual(Point(10, 10)  , dataStruct.XYpositive);
			Assert::AreEqual(Point(10, -10) , dataStruct.XpositiveYnegative);
			Assert::AreEqual(Point(-10, 10) , dataStruct.XnegativeYpositive);
			Assert::AreEqual(Point(-10, -10), dataStruct.XYnegative);

			Assert::AreEqual(Point(10, 10)  , dataStruct.XYfloating);

			Assert::AreEqual(Point(10, 10)  , dataStruct.unsignedXYpositive);
			Assert::AreEqual(Point(0, 0)    , dataStruct.unsignedXpositiveYnegative);
			Assert::AreEqual(Point(0, 0)    , dataStruct.unsignedXnegativeYpositive);
			Assert::AreEqual(Point(0, 0)    , dataStruct.unsignedXYnegative);

			Assert::AreEqual(Point(10, 10)  , dataStruct.requiredPresent);
			Assert::AreEqual(Point(0, 0)    , dataStruct.requiredMissing);
			Assert::AreEqual(Point(10, 10)  , dataStruct.notRequiredPresent);
			Assert::AreEqual(default        , dataStruct.notRequiredMissing);

			Assert::AreEqual(default        , dataStruct.XYbelowMin);
			Assert::AreEqual(default        , dataStruct.XbelowMin);
			Assert::AreEqual(default        , dataStruct.YbelowMin);

			Assert::AreEqual(Point(10, 10)  , dataStruct.XYaboveMinBelowMax);

			Assert::AreEqual(default        , dataStruct.XYaboveMax);
			Assert::AreEqual(default        , dataStruct.XaboveMax);
			Assert::AreEqual(default        , dataStruct.YaboveMax);

			Assert::AreEqual(default        , dataStruct.XbelowMinYaboveMax);
			Assert::AreEqual(default        , dataStruct.XaboveMaxYbelowMin);

			Assert::AreEqual(Point(-50, -50), dataStruct.XYequalMin);
			Assert::AreEqual(Point(-50, 10) , dataStruct.XequalMin);
			Assert::AreEqual(Point(10, -50) , dataStruct.YequalMin);

			Assert::AreEqual(default        , dataStruct.XequalMinYequalMax);
			Assert::AreEqual(default        , dataStruct.XequalMaxYequalMin);

			Assert::AreEqual(default        , dataStruct.XYequalMax);
			Assert::AreEqual(default        , dataStruct.XequalMax);
			Assert::AreEqual(default        , dataStruct.YequalMax);

			Assert::AreEqual(default        , dataStruct.incorrectArray);
			Assert::AreEqual(default        , dataStruct.incorrectBool);
			Assert::AreEqual(Point(0, 0)    , dataStruct.incorrectNull);
			Assert::AreEqual(default        , dataStruct.incorrectNumeric);
			Assert::AreEqual(default        , dataStruct.incorrectObject);
			Assert::AreEqual(default        , dataStruct.incorrectString);
		}
		TEST_METHOD(FloatPointTests) {
			using Point = sf::Vector2f;
			struct FloatPointStruct : TestStruct<Point> {
				Point XYpositive;
				Point XpositiveYnegative;
				Point XnegativeYpositive;
				Point XYnegative;

				Point XYinteger;

				Point unsignedXYpositive;
				Point unsignedXpositiveYnegative;
				Point unsignedXnegativeYpositive;
				Point unsignedXYnegative;

				Point XYbelowMin;
				Point XbelowMin;
				Point YbelowMin;

				Point XYaboveMinBelowMax;

				Point XYaboveMax;
				Point XaboveMax;
				Point YaboveMax;

				Point XbelowMinYaboveMax;
				Point XaboveMaxYbelowMin;

				Point XYequalMin;
				Point XequalMin;
				Point YequalMin;

				Point XequalMinYequalMax;
				Point XequalMaxYequalMin;

				Point XYequalMax;
				Point XequalMax;
				Point YequalMax;
			};
			std::string json = "{"
				"\"XYpositive\"                : [  10.5,   10.5],"
				"\"XpositiveYnegative\"        : [  10.5,  -10.5],"
				"\"XnegativeYpositive\"        : [ -10.5,   10.5],"
				"\"XYnegative\"                : [ -10.5,  -10.5],"

				"\"XYinteger\"                 : [  10  ,   10  ],"

				"\"unsignedXYpositive\"        : [  10.5,   10.5],"
				"\"unsignedXpositiveYnegative\": [  10.5,  -10.5],"
				"\"unsignedXnegativeYpositive\": [ -10.5,   10.5],"
				"\"unsignedXYnegative\"        : [ -10.5,  -10.5],"

				"\"requiredPresent\"           : [  10.5,   10.5],"
				"\"notRequiredPresent\"        : [  10.5,   10.5],"

				"\"XYbelowMin\"                : [-100.5, -100.5],"
				"\"XbelowMin\"                 : [-100.5,   10.5],"
				"\"YbelowMin\"                 : [  10.5, -100.5],"

				"\"XYaboveMinBelowMax\"        : [  10.5,   10.5],"

				"\"XYaboveMax\"                : [ 100.5,  100.5],"
				"\"XaboveMax\"                 : [ 100.5,   10.5],"
				"\"YaboveMax\"                 : [  10.5,  100.5],"

				"\"XbelowMinYaboveMax\"        : [-100.5,  100.5],"
				"\"XaboveMaxYbelowMin\"        : [ 100.5, -100.5],"

				"\"XYequalMin\"                : [ -50.5,  -50.5],"
				"\"XequalMin\"                 : [ -50.5,   10.5],"
				"\"YequalMin\"                 : [  10.5,  -50.5],"

				"\"XequalMinYequalMax\"        : [ -50.5,   50.5],"
				"\"XequalMaxYequalMin\"        : [  50.5,  -50.5],"

				"\"XYequalMax\"                : [  50.5,   50.5],"
				"\"XequalMax\"                 : [  50.5,   10.5],"
				"\"YequalMax\"                 : [  10.5,   50.5],"

				"\"incorrectArray\"             : [ ],"
				"\"incorrectBool\"              : true,"
				"\"incorrectNull\"              : null,"
				"\"incorrectNumeric\"           : 5,"
				"\"incorrectObject\"            : {},"
				"\"incorrectString\"            : \"\""
				"}";

			Point default(6.5f, 7.5f), min(-50.5f, -50.5f), max(50.5f, 50.5f);
			FloatPointStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("XYpositive"                 , &dataStruct.XYpositive                , true , new FloatPointRule(default)),
				JsonFieldInfo("XpositiveYnegative"         , &dataStruct.XpositiveYnegative        , true , new FloatPointRule(default)),
				JsonFieldInfo("XnegativeYpositive"         , &dataStruct.XnegativeYpositive        , true , new FloatPointRule(default)),
				JsonFieldInfo("XYnegative"                 , &dataStruct.XYnegative                , true , new FloatPointRule(default)),
				JsonFieldInfo("XYinteger"                  , &dataStruct.XYinteger                 , true , new FloatPointRule(default)),
				JsonFieldInfo("unsignedXYpositive"         , &dataStruct.unsignedXYpositive        , true , new FloatPointRule(false)),
				JsonFieldInfo("unsignedXpositiveYnegative" , &dataStruct.unsignedXpositiveYnegative, true , new FloatPointRule(false)),
				JsonFieldInfo("unsignedXnegativeYpositive" , &dataStruct.unsignedXnegativeYpositive, true , new FloatPointRule(false)),
				JsonFieldInfo("unsignedXYnegative"         , &dataStruct.unsignedXYnegative        , true , new FloatPointRule(false)),
				JsonFieldInfo("requiredPresent"            , &dataStruct.requiredPresent           , true , new FloatPointRule(default)),
				JsonFieldInfo("requiredMissing"            , &dataStruct.requiredMissing           , true , new FloatPointRule(default)),
				JsonFieldInfo("notRequiredPresent"         , &dataStruct.notRequiredPresent        , false, new FloatPointRule(default)),
				JsonFieldInfo("notRequiredMissing"         , &dataStruct.notRequiredMissing        , false, new FloatPointRule(default)),
				JsonFieldInfo("XYbelowMin"                 , &dataStruct.XYbelowMin                , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XbelowMin"                  , &dataStruct.XbelowMin                 , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("YbelowMin"                  , &dataStruct.YbelowMin                 , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XYaboveMinBelowMax"         , &dataStruct.XYaboveMinBelowMax        , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XYaboveMax"                 , &dataStruct.XYaboveMax                , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XaboveMax"                  , &dataStruct.XaboveMax                 , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("YaboveMax"                  , &dataStruct.YaboveMax                 , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XbelowMinYaboveMax"         , &dataStruct.XbelowMinYaboveMax        , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XaboveMaxYbelowMin"         , &dataStruct.XaboveMaxYbelowMin        , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XYequalMin"                 , &dataStruct.XYequalMin                , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XequalMin"                  , &dataStruct.XequalMin                 , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("YequalMin"                  , &dataStruct.YequalMin                 , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XequalMinYequalMax"         , &dataStruct.XequalMinYequalMax        , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XequalMaxYequalMin"         , &dataStruct.XequalMaxYequalMin        , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XYequalMax"                 , &dataStruct.XYequalMax                , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("XequalMax"                  , &dataStruct.XequalMax                 , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("YequalMax"                  , &dataStruct.YequalMax                 , true , new FloatPointRule(Range::between(min, max), default)),
				JsonFieldInfo("incorrectArray"             , &dataStruct.incorrectArray            , true , new FloatPointRule(default)),
				JsonFieldInfo("incorrectBool"              , &dataStruct.incorrectBool             , true , new FloatPointRule(default)),
				JsonFieldInfo("incorrectNull"              , &dataStruct.incorrectNull             , true , new FloatPointRule(default)),
				JsonFieldInfo("incorrectNumeric"           , &dataStruct.incorrectNumeric          , true , new FloatPointRule(default)),
				JsonFieldInfo("incorrectObject"            , &dataStruct.incorrectObject           , true , new FloatPointRule(default)),
				JsonFieldInfo("incorrectString"            , &dataStruct.incorrectString           , true , new FloatPointRule(default)),
			};
			JsonStructInfo<FloatPointStruct> structInfo(fields, &dataStruct);
			
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			Assert::AreEqual(Point( 10.5f,  10.5f), dataStruct.XYpositive);
			Assert::AreEqual(Point( 10.5f, -10.5f), dataStruct.XpositiveYnegative);
			Assert::AreEqual(Point(-10.5f,  10.5f), dataStruct.XnegativeYpositive);
			Assert::AreEqual(Point(-10.5f, -10.5f), dataStruct.XYnegative);

			Assert::AreEqual(Point(  10.f,   10.f), dataStruct.XYinteger);

			Assert::AreEqual(Point( 10.5f,  10.5f), dataStruct.unsignedXYpositive);
			Assert::AreEqual(Point(  0.f ,   0.f ), dataStruct.unsignedXpositiveYnegative);
			Assert::AreEqual(Point(  0.f ,   0.f ), dataStruct.unsignedXnegativeYpositive);
			Assert::AreEqual(Point(  0.f ,   0.f ), dataStruct.unsignedXYnegative);

			Assert::AreEqual(Point( 10.5f,  10.5f), dataStruct.requiredPresent);
			Assert::AreEqual(Point(  0.f ,   0.f ), dataStruct.requiredMissing);
			Assert::AreEqual(Point( 10.5f,  10.5f), dataStruct.notRequiredPresent);
			Assert::AreEqual(default              , dataStruct.notRequiredMissing);
										         
			Assert::AreEqual(default              , dataStruct.XYbelowMin);
			Assert::AreEqual(default              , dataStruct.XbelowMin);
			Assert::AreEqual(default              , dataStruct.YbelowMin);

			Assert::AreEqual(Point( 10.5f,  10.5f), dataStruct.XYaboveMinBelowMax);

			Assert::AreEqual(default              , dataStruct.XYaboveMax);
			Assert::AreEqual(default              , dataStruct.XaboveMax);
			Assert::AreEqual(default              , dataStruct.YaboveMax);
										      
			Assert::AreEqual(default              , dataStruct.XbelowMinYaboveMax);
			Assert::AreEqual(default              , dataStruct.XaboveMaxYbelowMin);

			Assert::AreEqual(Point(-50.5f, -50.5f), dataStruct.XYequalMin);
			Assert::AreEqual(Point(-50.5f,  10.5f), dataStruct.XequalMin);
			Assert::AreEqual(Point( 10.5f, -50.5f), dataStruct.YequalMin);

			Assert::AreEqual(default              , dataStruct.XequalMinYequalMax);
			Assert::AreEqual(default              , dataStruct.XequalMaxYequalMin);
											      
			Assert::AreEqual(default              , dataStruct.XYequalMax);
			Assert::AreEqual(default              , dataStruct.XequalMax);
			Assert::AreEqual(default              , dataStruct.YequalMax);

			Assert::AreEqual(default              , dataStruct.incorrectArray);
			Assert::AreEqual(default              , dataStruct.incorrectBool);
			Assert::AreEqual(Point(0.f, 0.f)      , dataStruct.incorrectNull);
			Assert::AreEqual(default              , dataStruct.incorrectNumeric);
			Assert::AreEqual(default              , dataStruct.incorrectObject);
			Assert::AreEqual(default              , dataStruct.incorrectString);
		}

		TEST_METHOD(IntegerRectTests) { 
			using Rect = sf::IntRect;
			struct IntegerRectStruct : TestStruct<Rect> {
				Rect LTWHpositive;
				Rect LTWpositiveHnegative;
				Rect LTHpositiveWnegative;
				Rect LTpositiveWHnegative;
				Rect LWHpositiveTnegative;
				Rect LWpositiveTHnegative;
				Rect LHpositiveTWnegative;
				Rect LpositiveTWHnegative;
				Rect TWHpositiveLnegative;
				Rect TWpositiveLHnegative;
				Rect THpositiveLWnegative;
				Rect TpositiveLWHnegative;
				Rect WHpositiveLTnegative;
				Rect WpositiveLTHnegative;
				Rect HpositiveLTWnegative;
				Rect LTWHnegative;

				Rect floating;

				Rect unsignedLTWHpositive;
				Rect unsignedLTWpositiveHnegative;
				Rect unsignedLTHpositiveWnegative;
				Rect unsignedLTpositiveWHnegative;
				Rect unsignedLWHpositiveTnegative;
				Rect unsignedLWpositiveTHnegative;
				Rect unsignedLHpositiveTWnegative;
				Rect unsignedLpositiveTWHnegative;
				Rect unsignedTWHpositiveLnegative;
				Rect unsignedTWpositiveLHnegative;
				Rect unsignedTHpositiveLWnegative;
				Rect unsignedTpositiveLWHnegative;
				Rect unsignedWHpositiveLTnegative;
				Rect unsignedWpositiveLTHnegative;
				Rect unsignedHpositiveLTWnegative;
				Rect unsignedLTWHnegative;

				Rect LTWHbelowMin;
				Rect LTWbelowMinHaboveMax;
				Rect LTHbelowMinWaboveMax;
				Rect LTbelowMinWHaboveMax;
				Rect LWHbelowMinTaboveMax;
				Rect LWbelowMinTHaboveMax;
				Rect LHbelowMinTWaboveMax;
				Rect LbelowMinTWHaboveMax;
				Rect TWHbelowMinLaboveMax;
				Rect TWbelowMinLHaboveMax;
				Rect THbelowMinLWaboveMax;
				Rect TbelowMinLWHaboveMax;
				Rect WHbelowMinLTaboveMax;
				Rect WbelowMinLTHaboveMax;
				Rect HbelowMinLTWaboveMax;
				Rect LTWHaboveMax;

				Rect LTWHequalMin;
				Rect LTWequalMin;
				Rect LTHequalMin;
				Rect LTequalMin;
				Rect LWHequalMin;
				Rect LWequalMin;
				Rect LHequalMin;
				Rect LequalMin;
				Rect TWHequalMin;
				Rect TWequalMin;
				Rect THequalMin;
				Rect TequalMin;
				Rect WHequalMin;
				Rect WequalMin;
				Rect HequalMin;

				Rect LTWHequalMax;
				Rect LTWequalMax;
				Rect LTHequalMax;
				Rect LTequalMax;
				Rect LWHequalMax;
				Rect LWequalMax;
				Rect LHequalMax;
				Rect LequalMax;
				Rect TWHequalMax;
				Rect TWequalMax;
				Rect THequalMax;
				Rect TequalMax;
				Rect WHequalMax;
				Rect WequalMax;
				Rect HequalMax;

				Rect LTWequalMinHequalMax;
				Rect LTHequalMinWequalMax;
				Rect LTequalMinWHequalMax;
				Rect LWHequalMinTequalMax;
				Rect LWequalMinTHequalMax;
				Rect LHequalMinTWequalMax;
				Rect LequalMinTWHequalMax;
				Rect TWHequalMinLequalMax;
				Rect TWequalMinLHequalMax;
				Rect THequalMinLWequalMax;
				Rect TequalMinLWHequalMax;
				Rect WHequalMinLTequalMax;
				Rect WequalMinLTHequalMax;
				Rect HequalMinLTWequalMax;
			};
			std::string json = "{"
				"\"LTWHpositive\"                : [  10,   10,   10,   10],"
				"\"LTWpositiveHnegative\"        : [  10,   10,   10,  -10],"
				"\"LTHpositiveWnegative\"        : [  10,   10,  -10,   10],"
				"\"LTpositiveWHnegative\"        : [  10,   10,  -10,  -10],"
				"\"LWHpositiveTnegative\"        : [  10,  -10,   10,   10],"
				"\"LWpositiveTHnegative\"        : [  10,  -10,   10,  -10],"
				"\"LHpositiveTWnegative\"        : [  10,  -10,  -10,   10],"
				"\"LpositiveTWHnegative\"        : [  10,  -10,  -10,  -10],"
				"\"TWHpositiveLnegative\"        : [ -10,   10,   10,   10],"
				"\"TWpositiveLHnegative\"        : [ -10,   10,   10,  -10],"
				"\"THpositiveLWnegative\"        : [ -10,   10,  -10,   10],"
				"\"TpositiveLWHnegative\"        : [ -10,   10,  -10,  -10],"
				"\"WHpositiveLTnegative\"        : [ -10,  -10,   10,   10],"
				"\"WpositiveLTHnegative\"        : [ -10,  -10,   10,  -10],"
				"\"HpositiveLTWnegative\"        : [ -10,  -10,  -10,   10],"
				"\"LTWHnegative\"                : [ -10,  -10,  -10,  -10],"
				
				"\"floating\"                    : [10.8, 10.8, 10.8, 10.8],"

				"\"unsignedLTWHpositive\"        : [  10,   10,   10,   10],"
				"\"unsignedLTWpositiveHnegative\": [  10,   10,   10,  -10],"
				"\"unsignedLTHpositiveWnegative\": [  10,   10,  -10,   10],"
				"\"unsignedLTpositiveWHnegative\": [  10,   10,  -10,  -10],"
				"\"unsignedLWHpositiveTnegative\": [  10,  -10,   10,   10],"
				"\"unsignedLWpositiveTHnegative\": [  10,  -10,   10,  -10],"
				"\"unsignedLHpositiveTWnegative\": [  10,  -10,  -10,   10],"
				"\"unsignedLpositiveTWHnegative\": [  10,  -10,  -10,  -10],"
				"\"unsignedTWHpositiveLnegative\": [ -10,   10,   10,   10],"
				"\"unsignedTWpositiveLHnegative\": [ -10,   10,   10,  -10],"
				"\"unsignedTHpositiveLWnegative\": [ -10,   10,  -10,   10],"
				"\"unsignedTpositiveLWHnegative\": [ -10,   10,  -10,  -10],"
				"\"unsignedWHpositiveLTnegative\": [ -10,  -10,   10,   10],"
				"\"unsignedWpositiveLTHnegative\": [ -10,  -10,   10,  -10],"
				"\"unsignedHpositiveLTWnegative\": [ -10,  -10,  -10,   10],"
				"\"unsignedLTWHnegative\"        : [ -10,  -10,  -10,  -10],"

				"\"LTWHbelowMin\"                : [-100, -100, -100, -100],"
				"\"LTWbelowMinHaboveMax\"        : [-100, -100, -100,  100],"
				"\"LTHbelowMinWaboveMax\"        : [-100, -100,  100, -100],"
				"\"LTbelowMinWHaboveMax\"        : [-100, -100,  100,  100],"
				"\"LWHbelowMinTaboveMax\"        : [-100,  100, -100, -100],"
				"\"LWbelowMinTHaboveMax\"        : [-100,  100, -100,  100],"
				"\"LHbelowMinTWaboveMax\"        : [-100,  100,  100, -100],"
				"\"LbelowMinTWHaboveMax\"        : [-100,  100,  100,  100],"
				"\"TWHbelowMinLaboveMax\"        : [ 100, -100, -100, -100],"
				"\"TWbelowMinLHaboveMax\"        : [ 100, -100, -100,  100],"
				"\"THbelowMinLWaboveMax\"        : [ 100, -100,  100, -100],"
				"\"TbelowMinLWHaboveMax\"        : [ 100, -100,  100,  100],"
				"\"WHbelowMinLTaboveMax\"        : [ 100,  100, -100, -100],"
				"\"WbelowMinLTHaboveMax\"        : [ 100,  100, -100,  100],"
				"\"HbelowMinLTWaboveMax\"        : [ 100,  100,  100, -100],"
				"\"LTWHaboveMax\"                : [ 100,  100,  100,  100],"

				"\"LTWHequalMin\"                : [ -50,  -50,  -50,  -50],"
				"\"LTWequalMin\"                 : [ -50,  -50,  -50,   10],"
				"\"LTHequalMin\"                 : [ -50,  -50,   10,  -50],"
				"\"LTequalMin\"                  : [ -50,  -50,   10,   10],"
				"\"LWHequalMin\"                 : [ -50,   10,  -50,  -50],"
				"\"LWequalMin\"                  : [ -50,   10,  -50,   10],"
				"\"LHequalMin\"                  : [ -50,   10,   10,  -50],"
				"\"LequalMin\"                   : [ -50,   10,   10,   10],"
				"\"TWHequalMin\"                 : [  10,  -50,  -50,  -50],"
				"\"TWequalMin\"                  : [  10,  -50,  -50,   10],"
				"\"THequalMin\"                  : [  10,  -50,   10,  -50],"
				"\"TequalMin\"                   : [  10,  -50,   10,   10],"
				"\"WHequalMin\"                  : [  10,   10,  -50,  -50],"
				"\"WequalMin\"                   : [  10,   10,  -50,   10],"
				"\"HequalMin\"                   : [  10,   10,   10,  -50],"

				"\"LTWHequalMax\"                : [  50,   50,   50,   50],"
				"\"LTWequalMax\"                 : [  50,   50,   50,   10],"
				"\"LTHequalMax\"                 : [  50,   50,   10,   50],"
				"\"LTequalMax\"                  : [  50,   50,   10,   10],"
				"\"LWHequalMax\"                 : [  50,   10,   50,   50],"
				"\"LWequalMax\"                  : [  50,   10,   50,   10],"
				"\"LHequalMax\"                  : [  50,   10,   10,   50],"
				"\"LequalMax\"                   : [  50,   10,   10,   10],"
				"\"TWHequalMax\"                 : [  10,   50,   50,   50],"
				"\"TWequalMax\"                  : [  10,   50,   50,   10],"
				"\"THequalMax\"                  : [  10,   50,   10,   50],"
				"\"TequalMax\"                   : [  10,   50,   10,   10],"
				"\"WHequalMax\"                  : [  10,   10,   50,   50],"
				"\"WequalMax\"                   : [  10,   10,   50,   10],"
				"\"HequalMax\"                   : [  10,   10,   10,   50],"

				"\"LTWequalMinHequalMax\"        : [ -50,  -50,  -50,   50],"
				"\"LTHequalMinWequalMax\"        : [ -50,  -50,   50,  -50],"
				"\"LTequalMinWHequalMax\"        : [ -50,  -50,   50,   50],"
				"\"LWHequalMinTequalMax\"        : [ -50,   50,  -50,  -50],"
				"\"LWequalMinTHequalMax\"        : [ -50,   50,  -50,   50],"
				"\"LHequalMinTWequalMax\"        : [ -50,   50,   50,  -50],"
				"\"LequalMinTWHequalMax\"        : [ -50,   50,   50,   50],"
				"\"TWHequalMinLequalMax\"        : [  50,  -50,  -50,  -50],"
				"\"TWequalMinLHequalMax\"        : [  50,  -50,  -50,   50],"
				"\"THequalMinLWequalMax\"        : [  50,  -50,   50,  -50],"
				"\"TequalMinLWHequalMax\"        : [  50,  -50,   50,   50],"
				"\"WHequalMinLTequalMax\"        : [  50,   50,  -50,  -50],"
				"\"WequalMinLTHequalMax\"        : [  50,   50,  -50,   50],"
				"\"HequalMinLTWequalMax\"        : [  50,   50,   50,  -50],"

				"\"requiredPresent\"             : [  10,   10,   10,   10],"
				"\"notRequiredPresent\"          : [  10,   10,   10,   10],"

				"\"incorrectArray\"             : [ ],"
				"\"incorrectBool\"              : true,"
				"\"incorrectNull\"              : null,"
				"\"incorrectNumeric\"           : 5,"
				"\"incorrectObject\"            : {},"
				"\"incorrectString\"            : \"\""
				"}";

			Rect default(6, 7, 8, 9), min(-50, -50, -50, -50), max(50, 50, 50, 50);
			IntegerRectStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("LTWHpositive"                , &dataStruct.LTWHpositive                 , true , new IntRectRule(default)),
				JsonFieldInfo("LTWpositiveHnegative"        , &dataStruct.LTWpositiveHnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("LTHpositiveWnegative"        , &dataStruct.LTHpositiveWnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("LTpositiveWHnegative"        , &dataStruct.LTpositiveWHnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("LWHpositiveTnegative"        , &dataStruct.LWHpositiveTnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("LWpositiveTHnegative"        , &dataStruct.LWpositiveTHnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("LHpositiveTWnegative"        , &dataStruct.LHpositiveTWnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("LpositiveTWHnegative"        , &dataStruct.LpositiveTWHnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("TWHpositiveLnegative"        , &dataStruct.TWHpositiveLnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("TWpositiveLHnegative"        , &dataStruct.TWpositiveLHnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("THpositiveLWnegative"        , &dataStruct.THpositiveLWnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("TpositiveLWHnegative"        , &dataStruct.TpositiveLWHnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("WHpositiveLTnegative"        , &dataStruct.WHpositiveLTnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("WpositiveLTHnegative"        , &dataStruct.WpositiveLTHnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("HpositiveLTWnegative"        , &dataStruct.HpositiveLTWnegative         , true , new IntRectRule(default)),
				JsonFieldInfo("LTWHnegative"                , &dataStruct.LTWHnegative                 , true , new IntRectRule(default)),

				JsonFieldInfo("floating"                    , &dataStruct.floating                     , true , new IntRectRule(default)),

				JsonFieldInfo("unsignedLTWHpositive"        , &dataStruct.unsignedLTWHpositive         , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedLTWpositiveHnegative", &dataStruct.unsignedLTWpositiveHnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedLTHpositiveWnegative", &dataStruct.unsignedLTHpositiveWnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedLTpositiveWHnegative", &dataStruct.unsignedLTpositiveWHnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedLWHpositiveTnegative", &dataStruct.unsignedLWHpositiveTnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedLWpositiveTHnegative", &dataStruct.unsignedLWpositiveTHnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedLHpositiveTWnegative", &dataStruct.unsignedLHpositiveTWnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedLpositiveTWHnegative", &dataStruct.unsignedLpositiveTWHnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedTWHpositiveLnegative", &dataStruct.unsignedTWHpositiveLnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedTWpositiveLHnegative", &dataStruct.unsignedTWpositiveLHnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedTHpositiveLWnegative", &dataStruct.unsignedTHpositiveLWnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedTpositiveLWHnegative", &dataStruct.unsignedTpositiveLWHnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedWHpositiveLTnegative", &dataStruct.unsignedWHpositiveLTnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedWpositiveLTHnegative", &dataStruct.unsignedWpositiveLTHnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedHpositiveLTWnegative", &dataStruct.unsignedHpositiveLTWnegative , true , new IntRectRule(false)),
				JsonFieldInfo("unsignedLTWHnegative        ", &dataStruct.unsignedLTWHnegative         , true , new IntRectRule(false)),

				JsonFieldInfo("LTWHbelowMin"                , &dataStruct.LTWHbelowMin                 , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTWbelowMinHaboveMax"        , &dataStruct.LTWbelowMinHaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTHbelowMinWaboveMax"        , &dataStruct.LTHbelowMinWaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTbelowMinWHaboveMax"        , &dataStruct.LTbelowMinWHaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWHbelowMinTaboveMax"        , &dataStruct.LWHbelowMinTaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWbelowMinTHaboveMax"        , &dataStruct.LWbelowMinTHaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LHbelowMinTWaboveMax"        , &dataStruct.LHbelowMinTWaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LbelowMinTWHaboveMax"        , &dataStruct.LbelowMinTWHaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWHbelowMinLaboveMax"        , &dataStruct.TWHbelowMinLaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWbelowMinLHaboveMax"        , &dataStruct.TWbelowMinLHaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("THbelowMinLWaboveMax"        , &dataStruct.THbelowMinLWaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TbelowMinLWHaboveMax"        , &dataStruct.TbelowMinLWHaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WHbelowMinLTaboveMax"        , &dataStruct.WHbelowMinLTaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WbelowMinLTHaboveMax"        , &dataStruct.WbelowMinLTHaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("HbelowMinLTWaboveMax"        , &dataStruct.HbelowMinLTWaboveMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTWHaboveMax"                , &dataStruct.LTWHaboveMax                 , true , new IntRectRule(Range::between(min, max), default)),

				JsonFieldInfo("LTWHequalMin"                , &dataStruct.LTWHequalMin                 , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTWequalMin"                 , &dataStruct.LTWequalMin                  , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTHequalMin"                 , &dataStruct.LTHequalMin                  , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTequalMin"                  , &dataStruct.LTequalMin                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWHequalMin"                 , &dataStruct.LWHequalMin                  , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWequalMin"                  , &dataStruct.LWequalMin                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LHequalMin"                  , &dataStruct.LHequalMin                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LequalMin"                   , &dataStruct.LequalMin                    , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWHequalMin"                 , &dataStruct.TWHequalMin                  , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWequalMin"                  , &dataStruct.TWequalMin                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("THequalMin"                  , &dataStruct.THequalMin                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TequalMin"                   , &dataStruct.TequalMin                    , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WHequalMin"                  , &dataStruct.WHequalMin                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WequalMin"                   , &dataStruct.WequalMin                    , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("HequalMin"                   , &dataStruct.HequalMin                    , true , new IntRectRule(Range::between(min, max), default)),

				JsonFieldInfo("LTWHequalMax"                , &dataStruct.LTWHequalMax                 , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTWequalMax"                 , &dataStruct.LTWequalMax                  , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTHequalMax"                 , &dataStruct.LTHequalMax                  , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTequalMax"                  , &dataStruct.LTequalMax                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWHequalMax"                 , &dataStruct.LWHequalMax                  , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWequalMax"                  , &dataStruct.LWequalMax                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LHequalMax"                  , &dataStruct.LHequalMax                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LequalMax"                   , &dataStruct.LequalMax                    , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWHequalMax"                 , &dataStruct.TWHequalMax                  , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWequalMax"                  , &dataStruct.TWequalMax                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("THequalMax"                  , &dataStruct.THequalMax                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TequalMax"                   , &dataStruct.TequalMax                    , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WHequalMax"                  , &dataStruct.WHequalMax                   , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WequalMax"                   , &dataStruct.WequalMax                    , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("HequalMax"                   , &dataStruct.HequalMax                    , true , new IntRectRule(Range::between(min, max), default)),

				JsonFieldInfo("LTWequalMinHequalMax"        , &dataStruct.LTWequalMinHequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTHequalMinWequalMax"        , &dataStruct.LTHequalMinWequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTequalMinWHequalMax"        , &dataStruct.LTequalMinWHequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWHequalMinTequalMax"        , &dataStruct.LWHequalMinTequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWequalMinTHequalMax"        , &dataStruct.LWequalMinTHequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LHequalMinTWequalMax"        , &dataStruct.LHequalMinTWequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LequalMinTWHequalMax"        , &dataStruct.LequalMinTWHequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWHequalMinLequalMax"        , &dataStruct.TWHequalMinLequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWequalMinLHequalMax"        , &dataStruct.TWequalMinLHequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("THequalMinLWequalMax"        , &dataStruct.THequalMinLWequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TequalMinLWHequalMax"        , &dataStruct.TequalMinLWHequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WHequalMinLTequalMax"        , &dataStruct.WHequalMinLTequalMax         , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo( "WequalMinLTHequalMax"        , &dataStruct.WequalMinLTHequalMax        , true , new IntRectRule(Range::between(min, max), default)),
				JsonFieldInfo("HequalMinLTWequalMax"        , &dataStruct.HequalMinLTWequalMax         , true , new IntRectRule(Range::between(min, max), default)),

				JsonFieldInfo("requiredPresent"             , &dataStruct.requiredPresent              , true , new IntRectRule(default)),
				JsonFieldInfo("requiredMissing"             , &dataStruct.requiredMissing              , true , new IntRectRule(default)),
				JsonFieldInfo("notRequiredPresent"          , &dataStruct.notRequiredPresent           , false, new IntRectRule(default)),
				JsonFieldInfo("notRequiredMissing"          , &dataStruct.notRequiredMissing           , false, new IntRectRule(default)),

				JsonFieldInfo("incorrectArray"              , &dataStruct.incorrectArray               , true , new IntRectRule(default)),
				JsonFieldInfo("incorrectBool"               , &dataStruct.incorrectBool                , true , new IntRectRule(default)),
				JsonFieldInfo("incorrectNull"               , &dataStruct.incorrectNull                , true , new IntRectRule(default)),
				JsonFieldInfo("incorrectNumeric"            , &dataStruct.incorrectNumeric             , true , new IntRectRule(default)),
				JsonFieldInfo("incorrectObject"             , &dataStruct.incorrectObject              , true , new IntRectRule(default)),
				JsonFieldInfo("incorrectString"             , &dataStruct.incorrectString              , true , new IntRectRule(default))
			};
			JsonStructInfo<IntegerRectStruct> structInfo(fields, &dataStruct);
			
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);
			
			Assert::AreEqual(Rect( 10,  10,  10,  10), dataStruct.LTWHpositive);
			Assert::AreEqual(Rect( 10,  10,  10, -10), dataStruct.LTWpositiveHnegative);
			Assert::AreEqual(Rect( 10,  10, -10,  10), dataStruct.LTHpositiveWnegative);
			Assert::AreEqual(Rect( 10,  10, -10, -10), dataStruct.LTpositiveWHnegative);
			Assert::AreEqual(Rect( 10, -10,  10,  10), dataStruct.LWHpositiveTnegative);
			Assert::AreEqual(Rect( 10, -10,  10, -10), dataStruct.LWpositiveTHnegative);
			Assert::AreEqual(Rect( 10, -10, -10,  10), dataStruct.LHpositiveTWnegative);
			Assert::AreEqual(Rect( 10, -10, -10, -10), dataStruct.LpositiveTWHnegative);
			Assert::AreEqual(Rect(-10,  10,  10,  10), dataStruct.TWHpositiveLnegative);
			Assert::AreEqual(Rect(-10,  10,  10, -10), dataStruct.TWpositiveLHnegative);
			Assert::AreEqual(Rect(-10,  10, -10,  10), dataStruct.THpositiveLWnegative);
			Assert::AreEqual(Rect(-10,  10, -10, -10), dataStruct.TpositiveLWHnegative);
			Assert::AreEqual(Rect(-10, -10,  10,  10), dataStruct.WHpositiveLTnegative);
			Assert::AreEqual(Rect(-10, -10,  10, -10), dataStruct.WpositiveLTHnegative);
			Assert::AreEqual(Rect(-10, -10, -10,  10), dataStruct.HpositiveLTWnegative);
			Assert::AreEqual(Rect(-10, -10, -10, -10), dataStruct.LTWHnegative);

			Assert::AreEqual(Rect(10, 10, 10, 10)    , dataStruct.floating);

			Assert::AreEqual(Rect(10, 10, 10, 10)    , dataStruct.unsignedLTWHpositive);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedLTWpositiveHnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedLTHpositiveWnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedLTpositiveWHnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedLWHpositiveTnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedLWpositiveTHnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedLHpositiveTWnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedLpositiveTWHnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedTWHpositiveLnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedTWpositiveLHnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedTHpositiveLWnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedTpositiveLWHnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedWHpositiveLTnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedWpositiveLTHnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedHpositiveLTWnegative);
			Assert::AreEqual(Rect()                  , dataStruct.unsignedLTWHnegative);

			Assert::AreEqual(default                 , dataStruct.LTWHbelowMin);
			Assert::AreEqual(default                 , dataStruct.LTWbelowMinHaboveMax);
			Assert::AreEqual(default                 , dataStruct.LTHbelowMinWaboveMax);
			Assert::AreEqual(default                 , dataStruct.LTbelowMinWHaboveMax);
			Assert::AreEqual(default                 , dataStruct.LWHbelowMinTaboveMax);
			Assert::AreEqual(default                 , dataStruct.LWbelowMinTHaboveMax);
			Assert::AreEqual(default                 , dataStruct.LHbelowMinTWaboveMax);
			Assert::AreEqual(default                 , dataStruct.LbelowMinTWHaboveMax);
			Assert::AreEqual(default                 , dataStruct.TWHbelowMinLaboveMax);
			Assert::AreEqual(default                 , dataStruct.TWbelowMinLHaboveMax);
			Assert::AreEqual(default                 , dataStruct.THbelowMinLWaboveMax);
			Assert::AreEqual(default                 , dataStruct.TbelowMinLWHaboveMax);
			Assert::AreEqual(default                 , dataStruct.WHbelowMinLTaboveMax);
			Assert::AreEqual(default                 , dataStruct.WbelowMinLTHaboveMax);
			Assert::AreEqual(default                 , dataStruct.HbelowMinLTWaboveMax);
			Assert::AreEqual(default                 , dataStruct.LTWHaboveMax);

			Assert::AreEqual(Rect(-50, -50, -50, -50), dataStruct.LTWHequalMin);
			Assert::AreEqual(Rect(-50, -50, -50, 10) , dataStruct.LTWequalMin);
			Assert::AreEqual(Rect(-50, -50, 10, -50) , dataStruct.LTHequalMin);
			Assert::AreEqual(Rect(-50, -50, 10, 10)  , dataStruct.LTequalMin);
			Assert::AreEqual(Rect(-50, 10, -50, -50) , dataStruct.LWHequalMin);
			Assert::AreEqual(Rect(-50, 10, -50, 10)  , dataStruct.LWequalMin);
			Assert::AreEqual(Rect(-50, 10, 10, -50)  , dataStruct.LHequalMin);
			Assert::AreEqual(Rect(-50, 10, 10, 10)   , dataStruct.LequalMin);
			Assert::AreEqual(Rect(10, -50, -50, -50) , dataStruct.TWHequalMin);
			Assert::AreEqual(Rect(10, -50, -50, 10)  , dataStruct.TWequalMin);
			Assert::AreEqual(Rect(10, -50, 10, -50)  , dataStruct.THequalMin);
			Assert::AreEqual(Rect(10, -50, 10, 10)   , dataStruct.TequalMin);
			Assert::AreEqual(Rect(10, 10, -50, -50)  , dataStruct.WHequalMin);
			Assert::AreEqual(Rect(10, 10, -50, 10)   , dataStruct.WequalMin);
			Assert::AreEqual(Rect(10, 10, 10, -50)   , dataStruct.HequalMin);

			Assert::AreEqual(default                 , dataStruct.LTWHequalMax);
			Assert::AreEqual(default                 , dataStruct.LTWequalMax);
			Assert::AreEqual(default                 , dataStruct.LTHequalMax);
			Assert::AreEqual(default                 , dataStruct.LTequalMax);
			Assert::AreEqual(default                 , dataStruct.LWHequalMax);
			Assert::AreEqual(default                 , dataStruct.LWequalMax);
			Assert::AreEqual(default                 , dataStruct.LHequalMax);
			Assert::AreEqual(default                 , dataStruct.LequalMax);
			Assert::AreEqual(default                 , dataStruct.TWHequalMax);
			Assert::AreEqual(default                 , dataStruct.TWequalMax);
			Assert::AreEqual(default                 , dataStruct.THequalMax);
			Assert::AreEqual(default                 , dataStruct.TequalMax);
			Assert::AreEqual(default                 , dataStruct.WHequalMax);
			Assert::AreEqual(default                 , dataStruct.WequalMax);
			Assert::AreEqual(default                 , dataStruct.HequalMax);

			Assert::AreEqual(default                 , dataStruct.LTWequalMinHequalMax);
			Assert::AreEqual(default                 , dataStruct.LTHequalMinWequalMax);
			Assert::AreEqual(default                 , dataStruct.LTequalMinWHequalMax);
			Assert::AreEqual(default                 , dataStruct.LWHequalMinTequalMax);
			Assert::AreEqual(default                 , dataStruct.LWequalMinTHequalMax);
			Assert::AreEqual(default                 , dataStruct.LHequalMinTWequalMax);
			Assert::AreEqual(default                 , dataStruct.LequalMinTWHequalMax);
			Assert::AreEqual(default                 , dataStruct.TWHequalMinLequalMax);
			Assert::AreEqual(default                 , dataStruct.TWequalMinLHequalMax);
			Assert::AreEqual(default                 , dataStruct.THequalMinLWequalMax);
			Assert::AreEqual(default                 , dataStruct.TequalMinLWHequalMax);
			Assert::AreEqual(default                 , dataStruct.WHequalMinLTequalMax);
			Assert::AreEqual(default                 , dataStruct.WequalMinLTHequalMax);
			Assert::AreEqual(default                 , dataStruct.HequalMinLTWequalMax);

			Assert::AreEqual(Rect(10, 10, 10, 10)    , dataStruct.requiredPresent);
			Assert::AreEqual(Rect()                  , dataStruct.requiredMissing);
			Assert::AreEqual(Rect(10, 10, 10, 10)    , dataStruct.notRequiredPresent);
			Assert::AreEqual(default                 , dataStruct.notRequiredMissing);

			Assert::AreEqual(default                 , dataStruct.incorrectArray);
			Assert::AreEqual(default                 , dataStruct.incorrectBool);
			Assert::AreEqual(Rect()                  , dataStruct.incorrectNull);
			Assert::AreEqual(default                 , dataStruct.incorrectNumeric);
			Assert::AreEqual(default                 , dataStruct.incorrectObject);
			Assert::AreEqual(default                 , dataStruct.incorrectString);
		}
		TEST_METHOD(FloatRectTests) { 
			using Rect = sf::FloatRect;
			struct FloatRectStruct : TestStruct<Rect> {
				Rect LTWHpositive;
				Rect LTWpositiveHnegative;
				Rect LTHpositiveWnegative;
				Rect LTpositiveWHnegative;
				Rect LWHpositiveTnegative;
				Rect LWpositiveTHnegative;
				Rect LHpositiveTWnegative;
				Rect LpositiveTWHnegative;
				Rect TWHpositiveLnegative;
				Rect TWpositiveLHnegative;
				Rect THpositiveLWnegative;
				Rect TpositiveLWHnegative;
				Rect WHpositiveLTnegative;
				Rect WpositiveLTHnegative;
				Rect HpositiveLTWnegative;
				Rect LTWHnegative;

				Rect integer;

				Rect unsignedLTWHpositive;
				Rect unsignedLTWpositiveHnegative;
				Rect unsignedLTHpositiveWnegative;
				Rect unsignedLTpositiveWHnegative;
				Rect unsignedLWHpositiveTnegative;
				Rect unsignedLWpositiveTHnegative;
				Rect unsignedLHpositiveTWnegative;
				Rect unsignedLpositiveTWHnegative;
				Rect unsignedTWHpositiveLnegative;
				Rect unsignedTWpositiveLHnegative;
				Rect unsignedTHpositiveLWnegative;
				Rect unsignedTpositiveLWHnegative;
				Rect unsignedWHpositiveLTnegative;
				Rect unsignedWpositiveLTHnegative;
				Rect unsignedHpositiveLTWnegative;
				Rect unsignedLTWHnegative;

				Rect LTWHbelowMin;
				Rect LTWbelowMinHaboveMax;
				Rect LTHbelowMinWaboveMax;
				Rect LTbelowMinWHaboveMax;
				Rect LWHbelowMinTaboveMax;
				Rect LWbelowMinTHaboveMax;
				Rect LHbelowMinTWaboveMax;
				Rect LbelowMinTWHaboveMax;
				Rect TWHbelowMinLaboveMax;
				Rect TWbelowMinLHaboveMax;
				Rect THbelowMinLWaboveMax;
				Rect TbelowMinLWHaboveMax;
				Rect WHbelowMinLTaboveMax;
				Rect WbelowMinLTHaboveMax;
				Rect HbelowMinLTWaboveMax;
				Rect LTWHaboveMax;

				Rect LTWHequalMin;
				Rect LTWequalMin;
				Rect LTHequalMin;
				Rect LTequalMin;
				Rect LWHequalMin;
				Rect LWequalMin;
				Rect LHequalMin;
				Rect LequalMin;
				Rect TWHequalMin;
				Rect TWequalMin;
				Rect THequalMin;
				Rect TequalMin;
				Rect WHequalMin;
				Rect WequalMin;
				Rect HequalMin;

				Rect LTWHequalMax;
				Rect LTWequalMax;
				Rect LTHequalMax;
				Rect LTequalMax;
				Rect LWHequalMax;
				Rect LWequalMax;
				Rect LHequalMax;
				Rect LequalMax;
				Rect TWHequalMax;
				Rect TWequalMax;
				Rect THequalMax;
				Rect TequalMax;
				Rect WHequalMax;
				Rect WequalMax;
				Rect HequalMax;

				Rect LTWequalMinHequalMax;
				Rect LTHequalMinWequalMax;
				Rect LTequalMinWHequalMax;
				Rect LWHequalMinTequalMax;
				Rect LWequalMinTHequalMax;
				Rect LHequalMinTWequalMax;
				Rect LequalMinTWHequalMax;
				Rect TWHequalMinLequalMax;
				Rect TWequalMinLHequalMax;
				Rect THequalMinLWequalMax;
				Rect TequalMinLWHequalMax;
				Rect WHequalMinLTequalMax;
				Rect WequalMinLTHequalMax;
				Rect HequalMinLTWequalMax;
			};
			std::string json = "{"
				"\"LTWHpositive\"                : [  10.5,   10.5,   10.5,   10.5],"
				"\"LTWpositiveHnegative\"        : [  10.5,   10.5,   10.5,  -10.5],"
				"\"LTHpositiveWnegative\"        : [  10.5,   10.5,  -10.5,   10.5],"
				"\"LTpositiveWHnegative\"        : [  10.5,   10.5,  -10.5,  -10.5],"
				"\"LWHpositiveTnegative\"        : [  10.5,  -10.5,   10.5,   10.5],"
				"\"LWpositiveTHnegative\"        : [  10.5,  -10.5,   10.5,  -10.5],"
				"\"LHpositiveTWnegative\"        : [  10.5,  -10.5,  -10.5,   10.5],"
				"\"LpositiveTWHnegative\"        : [  10.5,  -10.5,  -10.5,  -10.5],"
				"\"TWHpositiveLnegative\"        : [ -10.5,   10.5,   10.5,   10.5],"
				"\"TWpositiveLHnegative\"        : [ -10.5,   10.5,   10.5,  -10.5],"
				"\"THpositiveLWnegative\"        : [ -10.5,   10.5,  -10.5,   10.5],"
				"\"TpositiveLWHnegative\"        : [ -10.5,   10.5,  -10.5,  -10.5],"
				"\"WHpositiveLTnegative\"        : [ -10.5,  -10.5,   10.5,   10.5],"
				"\"WpositiveLTHnegative\"        : [ -10.5,  -10.5,   10.5,  -10.5],"
				"\"HpositiveLTWnegative\"        : [ -10.5,  -10.5,  -10.5,   10.5],"
				"\"LTWHnegative\"                : [ -10.5,  -10.5,  -10.5,  -10.5],"

				"\"integer\"                     : [    10,     10,     10,     10],"

				"\"unsignedLTWHpositive\"        : [  10.5,   10.5,   10.5,   10.5],"
				"\"unsignedLTWpositiveHnegative\": [  10.5,   10.5,   10.5,  -10.5],"
				"\"unsignedLTHpositiveWnegative\": [  10.5,   10.5,  -10.5,   10.5],"
				"\"unsignedLTpositiveWHnegative\": [  10.5,   10.5,  -10.5,  -10.5],"
				"\"unsignedLWHpositiveTnegative\": [  10.5,  -10.5,   10.5,   10.5],"
				"\"unsignedLWpositiveTHnegative\": [  10.5,  -10.5,   10.5,  -10.5],"
				"\"unsignedLHpositiveTWnegative\": [  10.5,  -10.5,  -10.5,   10.5],"
				"\"unsignedLpositiveTWHnegative\": [  10.5,  -10.5,  -10.5,  -10.5],"
				"\"unsignedTWHpositiveLnegative\": [ -10.5,   10.5,   10.5,   10.5],"
				"\"unsignedTWpositiveLHnegative\": [ -10.5,   10.5,   10.5,  -10.5],"
				"\"unsignedTHpositiveLWnegative\": [ -10.5,   10.5,  -10.5,   10.5],"
				"\"unsignedTpositiveLWHnegative\": [ -10.5,   10.5,  -10.5,  -10.5],"
				"\"unsignedWHpositiveLTnegative\": [ -10.5,  -10.5,   10.5,   10.5],"
				"\"unsignedWpositiveLTHnegative\": [ -10.5,  -10.5,   10.5,  -10.5],"
				"\"unsignedHpositiveLTWnegative\": [ -10.5,  -10.5,  -10.5,   10.5],"
				"\"unsignedLTWHnegative\"        : [ -10.5,  -10.5,  -10.5,  -10.5],"

				"\"LTWHbelowMin\"                : [-100.5, -100.5, -100.5, -100.5],"
				"\"LTWbelowMinHaboveMax\"        : [-100.5, -100.5, -100.5,  100.5],"
				"\"LTHbelowMinWaboveMax\"        : [-100.5, -100.5,  100.5, -100.5],"
				"\"LTbelowMinWHaboveMax\"        : [-100.5, -100.5,  100.5,  100.5],"
				"\"LWHbelowMinTaboveMax\"        : [-100.5,  100.5, -100.5, -100.5],"
				"\"LWbelowMinTHaboveMax\"        : [-100.5,  100.5, -100.5,  100.5],"
				"\"LHbelowMinTWaboveMax\"        : [-100.5,  100.5,  100.5, -100.5],"
				"\"LbelowMinTWHaboveMax\"        : [-100.5,  100.5,  100.5,  100.5],"
				"\"TWHbelowMinLaboveMax\"        : [ 100.5, -100.5, -100.5, -100.5],"
				"\"TWbelowMinLHaboveMax\"        : [ 100.5, -100.5, -100.5,  100.5],"
				"\"THbelowMinLWaboveMax\"        : [ 100.5, -100.5,  100.5, -100.5],"
				"\"TbelowMinLWHaboveMax\"        : [ 100.5, -100.5,  100.5,  100.5],"
				"\"WHbelowMinLTaboveMax\"        : [ 100.5,  100.5, -100.5, -100.5],"
				"\"WbelowMinLTHaboveMax\"        : [ 100.5,  100.5, -100.5,  100.5],"
				"\"HbelowMinLTWaboveMax\"        : [ 100.5,  100.5,  100.5, -100.5],"
				"\"LTWHaboveMax\"                : [ 100.5,  100.5,  100.5,  100.5],"

				"\"LTWHequalMin\"                : [ -50.5,  -50.5,  -50.5,  -50.5],"
				"\"LTWequalMin\"                 : [ -50.5,  -50.5,  -50.5,   10.5],"
				"\"LTHequalMin\"                 : [ -50.5,  -50.5,   10.5,  -50.5],"
				"\"LTequalMin\"                  : [ -50.5,  -50.5,   10.5,   10.5],"
				"\"LWHequalMin\"                 : [ -50.5,   10.5,  -50.5,  -50.5],"
				"\"LWequalMin\"                  : [ -50.5,   10.5,  -50.5,   10.5],"
				"\"LHequalMin\"                  : [ -50.5,   10.5,   10.5,  -50.5],"
				"\"LequalMin\"                   : [ -50.5,   10.5,   10.5,   10.5],"
				"\"TWHequalMin\"                 : [  10.5,  -50.5,  -50.5,  -50.5],"
				"\"TWequalMin\"                  : [  10.5,  -50.5,  -50.5,   10.5],"
				"\"THequalMin\"                  : [  10.5,  -50.5,   10.5,  -50.5],"
				"\"TequalMin\"                   : [  10.5,  -50.5,   10.5,   10.5],"
				"\"WHequalMin\"                  : [  10.5,   10.5,  -50.5,  -50.5],"
				"\"WequalMin\"                   : [  10.5,   10.5,  -50.5,   10.5],"
				"\"HequalMin\"                   : [  10.5,   10.5,   10.5,  -50.5],"

				"\"LTWHequalMax\"                : [  50.5,   50.5,   50.5,   50.5],"
				"\"LTWequalMax\"                 : [  50.5,   50.5,   50.5,   10.5],"
				"\"LTHequalMax\"                 : [  50.5,   50.5,   10.5,   50.5],"
				"\"LTequalMax\"                  : [  50.5,   50.5,   10.5,   10.5],"
				"\"LWHequalMax\"                 : [  50.5,   10.5,   50.5,   50.5],"
				"\"LWequalMax\"                  : [  50.5,   10.5,   50.5,   10.5],"
				"\"LHequalMax\"                  : [  50.5,   10.5,   10.5,   50.5],"
				"\"LequalMax\"                   : [  50.5,   10.5,   10.5,   10.5],"
				"\"TWHequalMax\"                 : [  10.5,   50.5,   50.5,   50.5],"
				"\"TWequalMax\"                  : [  10.5,   50.5,   50.5,   10.5],"
				"\"THequalMax\"                  : [  10.5,   50.5,   10.5,   50.5],"
				"\"TequalMax\"                   : [  10.5,   50.5,   10.5,   10.5],"
				"\"WHequalMax\"                  : [  10.5,   10.5,   50.5,   50.5],"
				"\"WequalMax\"                   : [  10.5,   10.5,   50.5,   10.5],"
				"\"HequalMax\"                   : [  10.5,   10.5,   10.5,   50.5],"

				"\"LTWequalMinHequalMax\"        : [ -50.5,  -50.5,  -50.5,   50.5],"
				"\"LTHequalMinWequalMax\"        : [ -50.5,  -50.5,   50.5,  -50.5],"
				"\"LTequalMinWHequalMax\"        : [ -50.5,  -50.5,   50.5,   50.5],"
				"\"LWHequalMinTequalMax\"        : [ -50.5,   50.5,  -50.5,  -50.5],"
				"\"LWequalMinTHequalMax\"        : [ -50.5,   50.5,  -50.5,   50.5],"
				"\"LHequalMinTWequalMax\"        : [ -50.5,   50.5,   50.5,  -50.5],"
				"\"LequalMinTWHequalMax\"        : [ -50.5,   50.5,   50.5,   50.5],"
				"\"TWHequalMinLequalMax\"        : [  50.5,  -50.5,  -50.5,  -50.5],"
				"\"TWequalMinLHequalMax\"        : [  50.5,  -50.5,  -50.5,   50.5],"
				"\"THequalMinLWequalMax\"        : [  50.5,  -50.5,   50.5,  -50.5],"
				"\"TequalMinLWHequalMax\"        : [  50.5,  -50.5,   50.5,   50.5],"
				"\"WHequalMinLTequalMax\"        : [  50.5,   50.5,  -50.5,  -50.5],"
				"\"WequalMinLTHequalMax\"        : [  50.5,   50.5,  -50.5,   50.5],"
				"\"HequalMinLTWequalMax\"        : [  50.5,   50.5,   50.5,  -50.5],"

				"\"requiredPresent\"             : [  10.5,   10.5,   10.5,   10.5],"
				"\"notRequiredPresent\"          : [  10.5,   10.5,   10.5,   10.5],"

				"\"incorrectArray\"             : [ ],"
				"\"incorrectBool\"              : true,"
				"\"incorrectNull\"              : null,"
				"\"incorrectNumeric\"           : 5,"
				"\"incorrectObject\"            : {},"
				"\"incorrectString\"            : \"\""
				"}";

			Rect default(6.5f, 7.5f, 8.5f, 9.5f), min(-50.5f, -50.5f, -50.5f, -50.5f), max(50.5f, 50.5f, 50.5f, 50.5f);
			FloatRectStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("LTWHpositive"                , &dataStruct.LTWHpositive                 , true , new FloatRectRule(default)),
				JsonFieldInfo("LTWpositiveHnegative"        , &dataStruct.LTWpositiveHnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("LTHpositiveWnegative"        , &dataStruct.LTHpositiveWnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("LTpositiveWHnegative"        , &dataStruct.LTpositiveWHnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("LWHpositiveTnegative"        , &dataStruct.LWHpositiveTnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("LWpositiveTHnegative"        , &dataStruct.LWpositiveTHnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("LHpositiveTWnegative"        , &dataStruct.LHpositiveTWnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("LpositiveTWHnegative"        , &dataStruct.LpositiveTWHnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("TWHpositiveLnegative"        , &dataStruct.TWHpositiveLnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("TWpositiveLHnegative"        , &dataStruct.TWpositiveLHnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("THpositiveLWnegative"        , &dataStruct.THpositiveLWnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("TpositiveLWHnegative"        , &dataStruct.TpositiveLWHnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("WHpositiveLTnegative"        , &dataStruct.WHpositiveLTnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("WpositiveLTHnegative"        , &dataStruct.WpositiveLTHnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("HpositiveLTWnegative"        , &dataStruct.HpositiveLTWnegative         , true , new FloatRectRule(default)),
				JsonFieldInfo("LTWHnegative"                , &dataStruct.LTWHnegative                 , true , new FloatRectRule(default)),

				JsonFieldInfo("integer"                     , &dataStruct.integer                      , true , new FloatRectRule(default)),

				JsonFieldInfo("unsignedLTWHpositive"        , &dataStruct.unsignedLTWHpositive         , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedLTWpositiveHnegative", &dataStruct.unsignedLTWpositiveHnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedLTHpositiveWnegative", &dataStruct.unsignedLTHpositiveWnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedLTpositiveWHnegative", &dataStruct.unsignedLTpositiveWHnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedLWHpositiveTnegative", &dataStruct.unsignedLWHpositiveTnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedLWpositiveTHnegative", &dataStruct.unsignedLWpositiveTHnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedLHpositiveTWnegative", &dataStruct.unsignedLHpositiveTWnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedLpositiveTWHnegative", &dataStruct.unsignedLpositiveTWHnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedTWHpositiveLnegative", &dataStruct.unsignedTWHpositiveLnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedTWpositiveLHnegative", &dataStruct.unsignedTWpositiveLHnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedTHpositiveLWnegative", &dataStruct.unsignedTHpositiveLWnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedTpositiveLWHnegative", &dataStruct.unsignedTpositiveLWHnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedWHpositiveLTnegative", &dataStruct.unsignedWHpositiveLTnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedWpositiveLTHnegative", &dataStruct.unsignedWpositiveLTHnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedHpositiveLTWnegative", &dataStruct.unsignedHpositiveLTWnegative , true , new FloatRectRule(false)),
				JsonFieldInfo("unsignedLTWHnegative        ", &dataStruct.unsignedLTWHnegative         , true , new FloatRectRule(false)),

				JsonFieldInfo("LTWHbelowMin"                , &dataStruct.LTWHbelowMin                 , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTWbelowMinHaboveMax"        , &dataStruct.LTWbelowMinHaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTHbelowMinWaboveMax"        , &dataStruct.LTHbelowMinWaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTbelowMinWHaboveMax"        , &dataStruct.LTbelowMinWHaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWHbelowMinTaboveMax"        , &dataStruct.LWHbelowMinTaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWbelowMinTHaboveMax"        , &dataStruct.LWbelowMinTHaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LHbelowMinTWaboveMax"        , &dataStruct.LHbelowMinTWaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LbelowMinTWHaboveMax"        , &dataStruct.LbelowMinTWHaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWHbelowMinLaboveMax"        , &dataStruct.TWHbelowMinLaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWbelowMinLHaboveMax"        , &dataStruct.TWbelowMinLHaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("THbelowMinLWaboveMax"        , &dataStruct.THbelowMinLWaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TbelowMinLWHaboveMax"        , &dataStruct.TbelowMinLWHaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WHbelowMinLTaboveMax"        , &dataStruct.WHbelowMinLTaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WbelowMinLTHaboveMax"        , &dataStruct.WbelowMinLTHaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("HbelowMinLTWaboveMax"        , &dataStruct.HbelowMinLTWaboveMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTWHaboveMax"                , &dataStruct.LTWHaboveMax                 , true , new FloatRectRule(Range::between(min, max), default)),

				JsonFieldInfo("LTWHequalMin"                , &dataStruct.LTWHequalMin                 , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTWequalMin"                 , &dataStruct.LTWequalMin                  , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTHequalMin"                 , &dataStruct.LTHequalMin                  , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTequalMin"                  , &dataStruct.LTequalMin                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWHequalMin"                 , &dataStruct.LWHequalMin                  , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWequalMin"                  , &dataStruct.LWequalMin                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LHequalMin"                  , &dataStruct.LHequalMin                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LequalMin"                   , &dataStruct.LequalMin                    , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWHequalMin"                 , &dataStruct.TWHequalMin                  , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWequalMin"                  , &dataStruct.TWequalMin                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("THequalMin"                  , &dataStruct.THequalMin                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TequalMin"                   , &dataStruct.TequalMin                    , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WHequalMin"                  , &dataStruct.WHequalMin                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WequalMin"                   , &dataStruct.WequalMin                    , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("HequalMin"                   , &dataStruct.HequalMin                    , true , new FloatRectRule(Range::between(min, max), default)),

				JsonFieldInfo("LTWHequalMax"                , &dataStruct.LTWHequalMax                 , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTWequalMax"                 , &dataStruct.LTWequalMax                  , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTHequalMax"                 , &dataStruct.LTHequalMax                  , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTequalMax"                  , &dataStruct.LTequalMax                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWHequalMax"                 , &dataStruct.LWHequalMax                  , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWequalMax"                  , &dataStruct.LWequalMax                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LHequalMax"                  , &dataStruct.LHequalMax                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LequalMax"                   , &dataStruct.LequalMax                    , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWHequalMax"                 , &dataStruct.TWHequalMax                  , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWequalMax"                  , &dataStruct.TWequalMax                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("THequalMax"                  , &dataStruct.THequalMax                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TequalMax"                   , &dataStruct.TequalMax                    , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WHequalMax"                  , &dataStruct.WHequalMax                   , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WequalMax"                   , &dataStruct.WequalMax                    , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("HequalMax"                   , &dataStruct.HequalMax                    , true , new FloatRectRule(Range::between(min, max), default)),

				JsonFieldInfo("LTWequalMinHequalMax"        , &dataStruct.LTWequalMinHequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTHequalMinWequalMax"        , &dataStruct.LTHequalMinWequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LTequalMinWHequalMax"        , &dataStruct.LTequalMinWHequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWHequalMinTequalMax"        , &dataStruct.LWHequalMinTequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LWequalMinTHequalMax"        , &dataStruct.LWequalMinTHequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LHequalMinTWequalMax"        , &dataStruct.LHequalMinTWequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("LequalMinTWHequalMax"        , &dataStruct.LequalMinTWHequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWHequalMinLequalMax"        , &dataStruct.TWHequalMinLequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TWequalMinLHequalMax"        , &dataStruct.TWequalMinLHequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("THequalMinLWequalMax"        , &dataStruct.THequalMinLWequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("TequalMinLWHequalMax"        , &dataStruct.TequalMinLWHequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("WHequalMinLTequalMax"        , &dataStruct.WHequalMinLTequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo( "WequalMinLTHequalMax"        , &dataStruct.WequalMinLTHequalMax         , true , new FloatRectRule(Range::between(min, max), default)),
				JsonFieldInfo("HequalMinLTWequalMax"        , &dataStruct.HequalMinLTWequalMax         , true , new FloatRectRule(Range::between(min, max), default)),

				JsonFieldInfo("requiredPresent"             , &dataStruct.requiredPresent              , true , new FloatRectRule(default)),
				JsonFieldInfo("requiredMissing"             , &dataStruct.requiredMissing              , true , new FloatRectRule(default)),
				JsonFieldInfo("notRequiredPresent"          , &dataStruct.notRequiredPresent           , false, new FloatRectRule(default)),
				JsonFieldInfo("notRequiredMissing"          , &dataStruct.notRequiredMissing           , false, new FloatRectRule(default)),

				JsonFieldInfo("incorrectArray"              , &dataStruct.incorrectArray               , true , new FloatRectRule(default)),
				JsonFieldInfo("incorrectBool"               , &dataStruct.incorrectBool                , true , new FloatRectRule(default)),
				JsonFieldInfo("incorrectNull"               , &dataStruct.incorrectNull                , true , new FloatRectRule(default)),
				JsonFieldInfo("incorrectNumeric"            , &dataStruct.incorrectNumeric             , true , new FloatRectRule(default)),
				JsonFieldInfo("incorrectObject"             , &dataStruct.incorrectObject              , true , new FloatRectRule(default)),
				JsonFieldInfo("incorrectString"             , &dataStruct.incorrectString              , true , new FloatRectRule(default)),
			};
			JsonStructInfo<FloatRectStruct> structInfo(fields, &dataStruct);
			
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			Assert::AreEqual(Rect( 10.5f,  10.5f,  10.5f,  10.5f), dataStruct.LTWHpositive);
			Assert::AreEqual(Rect( 10.5f,  10.5f,  10.5f, -10.5f), dataStruct.LTWpositiveHnegative);
			Assert::AreEqual(Rect( 10.5f,  10.5f, -10.5f,  10.5f), dataStruct.LTHpositiveWnegative);
			Assert::AreEqual(Rect( 10.5f,  10.5f, -10.5f, -10.5f), dataStruct.LTpositiveWHnegative);
			Assert::AreEqual(Rect( 10.5f, -10.5f,  10.5f,  10.5f), dataStruct.LWHpositiveTnegative);
			Assert::AreEqual(Rect( 10.5f, -10.5f,  10.5f, -10.5f), dataStruct.LWpositiveTHnegative);
			Assert::AreEqual(Rect( 10.5f, -10.5f, -10.5f,  10.5f), dataStruct.LHpositiveTWnegative);
			Assert::AreEqual(Rect( 10.5f, -10.5f, -10.5f, -10.5f), dataStruct.LpositiveTWHnegative);
			Assert::AreEqual(Rect(-10.5f,  10.5f,  10.5f,  10.5f), dataStruct.TWHpositiveLnegative);
			Assert::AreEqual(Rect(-10.5f,  10.5f,  10.5f, -10.5f), dataStruct.TWpositiveLHnegative);
			Assert::AreEqual(Rect(-10.5f,  10.5f, -10.5f,  10.5f), dataStruct.THpositiveLWnegative);
			Assert::AreEqual(Rect(-10.5f,  10.5f, -10.5f, -10.5f), dataStruct.TpositiveLWHnegative);
			Assert::AreEqual(Rect(-10.5f, -10.5f,  10.5f,  10.5f), dataStruct.WHpositiveLTnegative);
			Assert::AreEqual(Rect(-10.5f, -10.5f,  10.5f, -10.5f), dataStruct.WpositiveLTHnegative);
			Assert::AreEqual(Rect(-10.5f, -10.5f, -10.5f,  10.5f), dataStruct.HpositiveLTWnegative);
			Assert::AreEqual(Rect(-10.5f, -10.5f, -10.5f, -10.5f), dataStruct.LTWHnegative);

			Assert::AreEqual(Rect(10.f, 10.f, 10.f, 10.f)        , dataStruct.integer);

			Assert::AreEqual(Rect(10.5f, 10.5f, 10.5f, 10.5f)    , dataStruct.unsignedLTWHpositive);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedLTWpositiveHnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedLTHpositiveWnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedLTpositiveWHnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedLWHpositiveTnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedLWpositiveTHnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedLHpositiveTWnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedLpositiveTWHnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedTWHpositiveLnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedTWpositiveLHnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedTHpositiveLWnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedTpositiveLWHnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedWHpositiveLTnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedWpositiveLTHnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedHpositiveLTWnegative);
			Assert::AreEqual(Rect()                              , dataStruct.unsignedLTWHnegative);

			Assert::AreEqual(default                             , dataStruct.LTWHbelowMin);
			Assert::AreEqual(default                             , dataStruct.LTWbelowMinHaboveMax);
			Assert::AreEqual(default                             , dataStruct.LTHbelowMinWaboveMax);
			Assert::AreEqual(default                             , dataStruct.LTbelowMinWHaboveMax);
			Assert::AreEqual(default                             , dataStruct.LWHbelowMinTaboveMax);
			Assert::AreEqual(default                             , dataStruct.LWbelowMinTHaboveMax);
			Assert::AreEqual(default                             , dataStruct.LHbelowMinTWaboveMax);
			Assert::AreEqual(default                             , dataStruct.LbelowMinTWHaboveMax);
			Assert::AreEqual(default                             , dataStruct.TWHbelowMinLaboveMax);
			Assert::AreEqual(default                             , dataStruct.TWbelowMinLHaboveMax);
			Assert::AreEqual(default                             , dataStruct.THbelowMinLWaboveMax);
			Assert::AreEqual(default                             , dataStruct.TbelowMinLWHaboveMax);
			Assert::AreEqual(default                             , dataStruct.WHbelowMinLTaboveMax);
			Assert::AreEqual(default                             , dataStruct.WbelowMinLTHaboveMax);
			Assert::AreEqual(default                             , dataStruct.HbelowMinLTWaboveMax);
			Assert::AreEqual(default                             , dataStruct.LTWHaboveMax);

			Assert::AreEqual(Rect(-50.5f, -50.5f, -50.5f, -50.5f), dataStruct.LTWHequalMin);
			Assert::AreEqual(Rect(-50.5f, -50.5f, -50.5f,  10.5f), dataStruct.LTWequalMin);
			Assert::AreEqual(Rect(-50.5f, -50.5f,  10.5f, -50.5f), dataStruct.LTHequalMin);
			Assert::AreEqual(Rect(-50.5f, -50.5f,  10.5f,  10.5f), dataStruct.LTequalMin);
			Assert::AreEqual(Rect(-50.5f,  10.5f, -50.5f, -50.5f), dataStruct.LWHequalMin);
			Assert::AreEqual(Rect(-50.5f,  10.5f, -50.5f,  10.5f), dataStruct.LWequalMin);
			Assert::AreEqual(Rect(-50.5f,  10.5f,  10.5f, -50.5f), dataStruct.LHequalMin);
			Assert::AreEqual(Rect(-50.5f,  10.5f,  10.5f,  10.5f), dataStruct.LequalMin);
			Assert::AreEqual(Rect( 10.5f, -50.5f, -50.5f, -50.5f), dataStruct.TWHequalMin);
			Assert::AreEqual(Rect( 10.5f, -50.5f, -50.5f,  10.5f), dataStruct.TWequalMin);
			Assert::AreEqual(Rect( 10.5f, -50.5f,  10.5f, -50.5f), dataStruct.THequalMin);
			Assert::AreEqual(Rect( 10.5f, -50.5f,  10.5f,  10.5f), dataStruct.TequalMin);
			Assert::AreEqual(Rect( 10.5f,  10.5f, -50.5f, -50.5f), dataStruct.WHequalMin);
			Assert::AreEqual(Rect( 10.5f,  10.5f, -50.5f,  10.5f), dataStruct.WequalMin);
			Assert::AreEqual(Rect( 10.5f,  10.5f,  10.5f, -50.5f), dataStruct.HequalMin);

			Assert::AreEqual(default                             , dataStruct.LTWHequalMax);
			Assert::AreEqual(default                             , dataStruct.LTWequalMax);
			Assert::AreEqual(default                             , dataStruct.LTHequalMax);
			Assert::AreEqual(default                             , dataStruct.LTequalMax);
			Assert::AreEqual(default                             , dataStruct.LWHequalMax);
			Assert::AreEqual(default                             , dataStruct.LWequalMax);
			Assert::AreEqual(default                             , dataStruct.LHequalMax);
			Assert::AreEqual(default                             , dataStruct.LequalMax);
			Assert::AreEqual(default                             , dataStruct.TWHequalMax);
			Assert::AreEqual(default                             , dataStruct.TWequalMax);
			Assert::AreEqual(default                             , dataStruct.THequalMax);
			Assert::AreEqual(default                             , dataStruct.TequalMax);
			Assert::AreEqual(default                             , dataStruct.WHequalMax);
			Assert::AreEqual(default                             , dataStruct.WequalMax);
			Assert::AreEqual(default                             , dataStruct.HequalMax);

			Assert::AreEqual(default                             , dataStruct.LTWequalMinHequalMax);
			Assert::AreEqual(default                             , dataStruct.LTHequalMinWequalMax);
			Assert::AreEqual(default                             , dataStruct.LTequalMinWHequalMax);
			Assert::AreEqual(default                             , dataStruct.LWHequalMinTequalMax);
			Assert::AreEqual(default                             , dataStruct.LWequalMinTHequalMax);
			Assert::AreEqual(default                             , dataStruct.LHequalMinTWequalMax);
			Assert::AreEqual(default                             , dataStruct.LequalMinTWHequalMax);
			Assert::AreEqual(default                             , dataStruct.TWHequalMinLequalMax);
			Assert::AreEqual(default                             , dataStruct.TWequalMinLHequalMax);
			Assert::AreEqual(default                             , dataStruct.THequalMinLWequalMax);
			Assert::AreEqual(default                             , dataStruct.TequalMinLWHequalMax);
			Assert::AreEqual(default                             , dataStruct.WHequalMinLTequalMax);
			Assert::AreEqual(default                             , dataStruct.WequalMinLTHequalMax);
			Assert::AreEqual(default                             , dataStruct.HequalMinLTWequalMax);

			Assert::AreEqual(Rect(10.5f, 10.5f, 10.5f, 10.5f)    , dataStruct.requiredPresent);
			Assert::AreEqual(Rect()                              , dataStruct.requiredMissing);
			Assert::AreEqual(Rect(10.5f, 10.5f, 10.5f, 10.5f)    , dataStruct.notRequiredPresent);
			Assert::AreEqual(default                             , dataStruct.notRequiredMissing);

			Assert::AreEqual(default                             , dataStruct.incorrectArray);
			Assert::AreEqual(default                             , dataStruct.incorrectBool);
			Assert::AreEqual(Rect()                              , dataStruct.incorrectNull);
			Assert::AreEqual(default                             , dataStruct.incorrectNumeric);
			Assert::AreEqual(default                             , dataStruct.incorrectObject);
			Assert::AreEqual(default                             , dataStruct.incorrectString);
		}

		TEST_METHOD(StringTests) { 
			struct StringStruct : TestStruct<std::string> {
				std::string emptyAllowEmpty;
				std::string emptyDenyEmpty;
				std::string filledAllowEmpty;
				std::string filledDenyEmpty;
			};
			std::string json = "{"
				"\"emptyAllowEmpty\"   : \"\","
				"\"filledAllowEmpty\"  : \"test\","
				"\"emptyDenyEmpty\"    : \"\","
				"\"filledDenyEmpty\"   : \"test\","

				"\"requiredPresent\"   : \"test\","
				"\"notRequiredPresent\": \"test\","

				"\"incorrectArray\"    : [ ],"
				"\"incorrectBool\"     : false,"
				"\"incorrectNull\"     : null,"
				"\"incorrectNumeric\"  : 0,"
				"\"incorrectObject\"   : { }"
				"}";

			std::string default = "default";
			StringStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("emptyAllowEmpty"   , &dataStruct.emptyAllowEmpty   , true , new StringRule(default, true)),
				JsonFieldInfo("filledAllowEmpty"  , &dataStruct.filledAllowEmpty  , true , new StringRule(default, true)),
				JsonFieldInfo("emptyDenyEmpty"    , &dataStruct.emptyDenyEmpty    , true , new StringRule(default, false)),
				JsonFieldInfo("filledDenyEmpty"   , &dataStruct.filledDenyEmpty   , true , new StringRule(default, false)),

				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new StringRule(default)),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new StringRule(default)),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new StringRule(default)),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new StringRule(default)),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new StringRule(default)),
				JsonFieldInfo("incorrectBool"     , &dataStruct.incorrectBool     , true , new StringRule(default)),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new StringRule(default)),
				JsonFieldInfo("incorrectNumeric"  , &dataStruct.incorrectNumeric  , true , new StringRule(default)),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new StringRule(default))
			};
			JsonStructInfo<StringStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			Assert::AreEqual(std::string("")    , dataStruct.emptyAllowEmpty);
			Assert::AreEqual(std::string("test"), dataStruct.filledAllowEmpty);
			Assert::AreEqual(default            , dataStruct.emptyDenyEmpty);
			Assert::AreEqual(std::string("test"), dataStruct.filledDenyEmpty);

			Assert::AreEqual(std::string("test"), dataStruct.requiredPresent);
			Assert::AreEqual(std::string()      , dataStruct.requiredMissing);
			Assert::AreEqual(std::string("test"), dataStruct.notRequiredPresent);
			Assert::AreEqual(default            , dataStruct.notRequiredMissing);

			Assert::AreEqual(default            , dataStruct.incorrectArray);
			Assert::AreEqual(default            , dataStruct.incorrectBool);
			Assert::AreEqual(std::string()      , dataStruct.incorrectNull);
			Assert::AreEqual(default            , dataStruct.incorrectNumeric);
			Assert::AreEqual(default            , dataStruct.incorrectObject);
		}

		TEST_METHOD(TimeTests) { 
			struct TimeStruct :TestStruct<sf::Time> {
				sf::Time positive;
				sf::Time negative;

				sf::Time unsignedPositive;
				sf::Time unsignedNegative;
				
				sf::Time seconds;
				sf::Time floating;

				sf::Time belowMin;
				sf::Time equalMin;
				sf::Time aboveMinBelowMax;
				sf::Time equalMax;
				sf::Time aboveMax;

				sf::Time requiredPresent;
				sf::Time requiredMissing;
				sf::Time notRequiredPresent;
				sf::Time notRequiredMissing;

				sf::Time incorrectArray;
				sf::Time incorrectBool;
				sf::Time incorrectNull;
				sf::Time incorrectObject;
				sf::Time incorrectString;
			};

			std::string json = "{"
				"\"positive\"          :  100,"
				"\"negative\"          : -100,"

				"\"seconds\"           :  10000,"

				"\"unsignedPositive\"  :  100,"
				"\"unsignedNegative\"  : -100,"

				"\"floating\"          :  100.8,"

				"\"belowMin\"          : -1000,"
				"\"equalMin\"          : -500,"
				"\"aboveMinBelowMax\"  :  100,"
				"\"equalMax\"          :  500,"
				"\"aboveMax\"          :  1000,"

				"\"requiredPresent\"   :  100,"
				"\"notRequiredPresent\":  100,"

				"\"incorrectArray\"    : [ ],"
				"\"incorrectBool\"     : false,"
				"\"incorrectNull\"     : null,"
				"\"incorrectObject\"   : {},"
				"\"incorrectString\"   : \"\""
				"}";


			sf::Time min(sf::milliseconds(-500)), max(sf::milliseconds(500)), default(sf::milliseconds(100));
			TimeStruct dataStruct;
			JsonFieldsInfo fields = {
					JsonFieldInfo("positive"          , &dataStruct.positive          , true , new TimeRule(default)),
					JsonFieldInfo("negative"          , &dataStruct.negative          , true , new TimeRule(default)),

					JsonFieldInfo("unsignedPositive"  , &dataStruct.unsignedPositive  , true , new TimeRule(false)),
					JsonFieldInfo("unsignedNegative"  , &dataStruct.unsignedNegative  , true , new TimeRule(false)),

					JsonFieldInfo("seconds"           , &dataStruct.seconds           , true , new TimeRule(default)),
					JsonFieldInfo("floating"          , &dataStruct.floating          , true , new TimeRule(default)),

					JsonFieldInfo("belowMin"          , &dataStruct.belowMin          , true , new TimeRule(Range::between(min, max), default)),
					JsonFieldInfo("equalMin"          , &dataStruct.equalMin          , true , new TimeRule(Range::between(min, max), default)),
					JsonFieldInfo("aboveMinBelowMax"  , &dataStruct.aboveMinBelowMax  , true , new TimeRule(Range::between(min, max), default)),
					JsonFieldInfo("equalMax"          , &dataStruct.equalMax          , true , new TimeRule(Range::between(min, max), default)),
					JsonFieldInfo("aboveMax"          , &dataStruct.aboveMax          , true , new TimeRule(Range::between(min, max), default)),

					JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new TimeRule(default)),
					JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new TimeRule(default)),
					JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new TimeRule(default)),
					JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new TimeRule(default)),

					JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new TimeRule(default)),
					JsonFieldInfo("incorrectBool"     , &dataStruct.incorrectBool     , true , new TimeRule(default)),
					JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new TimeRule(default)),
					JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new TimeRule(default)),
					JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new TimeRule(default)),
			};
			JsonStructInfo<TimeStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			Assert::AreEqual(sf::milliseconds(100) , dataStruct.positive);
			Assert::AreEqual(sf::milliseconds(-100), dataStruct.negative);

			Assert::AreEqual(sf::milliseconds(100) , dataStruct.unsignedPositive);
			Assert::AreEqual(sf::milliseconds(0)   , dataStruct.unsignedNegative);

			Assert::AreEqual(sf::seconds(10)       , dataStruct.seconds);
			Assert::AreEqual(sf::milliseconds(100) , dataStruct.floating);

			Assert::AreEqual(default               , dataStruct.belowMin);
			Assert::AreEqual(sf::milliseconds(-500), dataStruct.equalMin);
			Assert::AreEqual(sf::milliseconds(100) , dataStruct.aboveMinBelowMax);
			Assert::AreEqual(default               , dataStruct.equalMax);
			Assert::AreEqual(default               , dataStruct.aboveMax);

			Assert::AreEqual(sf::milliseconds(100) , dataStruct.requiredPresent);
			Assert::AreEqual(sf::milliseconds(0)   , dataStruct.requiredMissing);
			Assert::AreEqual(sf::milliseconds(100) , dataStruct.notRequiredPresent);
			Assert::AreEqual(default               , dataStruct.notRequiredMissing);

			Assert::AreEqual(default               , dataStruct.incorrectArray);
			Assert::AreEqual(default               , dataStruct.incorrectBool);
			Assert::AreEqual(sf::milliseconds(0)   , dataStruct.incorrectNull);
			Assert::AreEqual(default               , dataStruct.incorrectObject);
			Assert::AreEqual(default               , dataStruct.incorrectString);
		}



		//{"ID": 1, "efficiency": 10}
		TEST_METHOD(ActionTests) { 
			using Action = PickupData::Action;
			struct ActionStruct : TestStruct<Action> {
				
			};
			std::string json = "{"
				
				"\"requiredPresent\"   : true,"
				"\"notRequiredPresent\": true,"

				"\"incorrectArray\"    : [ ],"
				"\"incorrectNull\"     : null,"
				"\"incorrectObject\"   : {},"
				"\"incorrectString\"   : \"\""
				"}";

			ActionStruct dataStruct;
			JsonFieldsInfo fields = {
				//JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new ActionRule(0, 5)),

				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new ActionRule()),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new ActionRule()),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new ActionRule()),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new ActionRule()),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new ActionRule()),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new ActionRule()),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new ActionRule()),
				JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new ActionRule()),
			};
			JsonStructInfo<ActionStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);
			Assert::IsTrue(true);
		}

		TEST_METHOD(AnimationPackTests) { 
			using Data = AnimationPackData;
			struct AnimationPackStruct : TestStruct<Data> {
				Data empty;
				Data filled;

				Data size4;
				Data size2;
			};
			std::string json = "{"
				"\"empty\"             : [],"
				"\"filled\"            : [ [0, 1, 11], [1, 1, 2] ],"

				"\"size4\"             : [ [0, 1, 11, 10] ],"
				"\"size2\"             : [ [0, 11] ],"

				"\"requiredPresent\"   : [ [0, 1, 11] ],"
				"\"notRequiredPresent\": [ [0, 1, 11] ],"

				"\"incorrectBool\"     : true,"
				"\"incorrectNull\"     : null,"
				"\"incorrectNumeric\"  : 0,"
				"\"incorrectObject\"   : {},"
				"\"incorrectString\"   : \"\""
				"}";

			AnimationPackStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("empty"             , &dataStruct.empty             , true , new VectorRule(new AnimationDataRule())),
				JsonFieldInfo("filled"            , &dataStruct.filled            , true , new VectorRule(new AnimationDataRule())),

				JsonFieldInfo("size4"             , &dataStruct.size4             , true , new VectorRule(new AnimationDataRule())),
				JsonFieldInfo("size2"             , &dataStruct.size2             , true , new VectorRule(new AnimationDataRule())),

				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new VectorRule(new AnimationDataRule())),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new VectorRule(new AnimationDataRule())),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new VectorRule(new AnimationDataRule())),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new VectorRule(new AnimationDataRule())),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new VectorRule(new AnimationDataRule())),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new VectorRule(new AnimationDataRule())),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new VectorRule(new AnimationDataRule())),
				JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new VectorRule(new AnimationDataRule())),
			};
			JsonStructInfo<AnimationPackStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);
			AnimationData type1{(Type::Animation) 0,(Type::GraphicKind) 1,(Textures::Sprites::ID) 11};
			AnimationData type2{(Type::Animation) 1,(Type::GraphicKind) 1,(Textures::Sprites::ID) 2};

			Assert::IsTrue(dataStruct.empty.empty());

			Assert::AreEqual((size_t)2, dataStruct.filled.size());
			Assert::AreEqual(type1, dataStruct.filled[0]);
			Assert::AreEqual(type2, dataStruct.filled[1]);

			Assert::IsTrue(dataStruct.size4.empty());
			Assert::IsTrue(dataStruct.size2.empty());

			Assert::AreEqual((size_t)1, dataStruct.requiredPresent.size());
			Assert::AreEqual(type1, dataStruct.requiredPresent[0]);

			Assert::AreEqual((size_t)1, dataStruct.notRequiredPresent.size());
			Assert::AreEqual(type1, dataStruct.notRequiredPresent[0]);

			Assert::IsTrue(dataStruct.requiredMissing.empty());
			Assert::IsTrue(dataStruct.notRequiredMissing.empty());

			Assert::IsTrue(dataStruct.incorrectBool.empty());
			Assert::IsTrue(dataStruct.incorrectNull.empty());
			Assert::IsTrue(dataStruct.incorrectNumeric.empty());
			Assert::IsTrue(dataStruct.incorrectObject.empty());
			Assert::IsTrue(dataStruct.incorrectString.empty());
		}

		TEST_METHOD(ComplexTests) {
			struct ComplexStruct1 {
				int simpleData1;
				int simpleData2;
			};
			struct ComplexStruct2 {
				ComplexStruct1 data;
				int simpleData;
			};
			struct ComplexStruct {
				ComplexStruct2 data2_1;
				ComplexStruct2 data2_2;
				ComplexStruct1 data1_1;
				ComplexStruct1 data1_2;
				int simpleData;
			};

			std::string json = "{"
				"\"complex1\" : {"
				"	\"subComplex\": {"
				"		\"simple1\":11,"
				"		\"simple2\":12"
				"	},"
				"	\"simple\": 1"
				"},"
				"\"complex3\" : {"
				"	\"simple1\":31,"
				"	\"simple2\":32"
				"},"
				"\"complex4\" : {"
				"	\"simple1\":41,"
				"	\"simple2\":42"
				"},"
				"\"simple\": 5"
				"}";

			ComplexStruct dataStruct;
			ComplexStruct1 dataStruct1;
			ComplexStruct2 dataStruct2;
			JsonFieldsInfo fields1 = {
				JsonFieldInfo("simple1", &dataStruct1.simpleData1, true, new IntegerRule(6)),
				JsonFieldInfo("simple2", &dataStruct1.simpleData2, true, new IntegerRule(7))
			};
			JsonStructInfo<ComplexStruct1> structInfo1(fields1, &dataStruct1);
			JsonFieldsInfo fields2 = {
				JsonFieldInfo("subComplex", &dataStruct2.data      , true, new ComplexRule<ComplexStruct1>(structInfo1)),
				JsonFieldInfo("simple"    , &dataStruct2.simpleData, true, new IntegerRule(5))
			};
			JsonStructInfo<ComplexStruct2> structInfo2(fields2, &dataStruct2);
			JsonFieldsInfo fields = {
				JsonFieldInfo("complex1", &dataStruct.data2_1    , true , new ComplexRule<ComplexStruct2>(structInfo2)),
				JsonFieldInfo("complex2", &dataStruct.data2_2    , false, new ComplexRule<ComplexStruct2>(structInfo2)),
				JsonFieldInfo("complex3", &dataStruct.data1_1    , true , new ComplexRule<ComplexStruct1>(structInfo1)),
				JsonFieldInfo("complex4", &dataStruct.data1_2    , true , new ComplexRule<ComplexStruct1>(structInfo1)),
				JsonFieldInfo("simple"  , &dataStruct.simpleData , true , new IntegerRule())
			};
			JsonStructInfo<ComplexStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			Assert::AreEqual(5 , dataStruct.simpleData);

			Assert::AreEqual(1 , dataStruct.data2_1.simpleData);
			Assert::AreEqual(11, dataStruct.data2_1.data.simpleData1);
			Assert::AreEqual(12, dataStruct.data2_1.data.simpleData2);
			
			Assert::AreEqual(5 , dataStruct.data2_2.simpleData);
			Assert::AreEqual(6 , dataStruct.data2_2.data.simpleData1);
			Assert::AreEqual(7 , dataStruct.data2_2.data.simpleData2);
			
			Assert::AreEqual(31, dataStruct.data1_1.simpleData1);
			Assert::AreEqual(32, dataStruct.data1_1.simpleData2);

			Assert::AreEqual(41, dataStruct.data1_2.simpleData1);
			Assert::AreEqual(42, dataStruct.data1_2.simpleData2);
		}

		TEST_METHOD(VectorTests) {
			struct Complex {
				int left;
				int right;
			};
			struct VectorStruct {
				std::vector<int> empty;
				std::vector<int> vector;
				std::vector<int> positive;
				std::vector<int> default;
				std::vector<Complex> complex;
			};
			std::string json = "{"
				"\"empty\":[],"
				"\"vector\":[0,1,2,4,8,16,32,64],"
				"\"positive\":[-8, -4, -2, -1, 0, 1, 2, 4, 8],"
				"\"default\": 3,"
				"\"complex\":[ {\"l\":0, \"r\":1}, {\"l\":2, \"r\":3}, {\"l\":4, \"r\":5} ]"
				"}";

			Complex complexStruct;
			JsonFieldsInfo complexFields = {
				JsonFieldInfo("l", &complexStruct.left , true, new IntegerRule()),
				JsonFieldInfo("r", &complexStruct.right, true, new IntegerRule())
			};
			JsonStructInfo<Complex> complexInfo(complexFields, &complexStruct);
			VectorStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("empty"   , &dataStruct.empty   , true , new VectorRule(new IntegerRule())),
				JsonFieldInfo("vector"  , &dataStruct.vector  , true , new VectorRule(new IntegerRule())),
				JsonFieldInfo("positive", &dataStruct.positive, true , new VectorRule(new IntegerRule(false))),
				JsonFieldInfo("default" , &dataStruct.default , true , new VectorRule(new IntegerRule(3), 5)),
				JsonFieldInfo("complex" , &dataStruct.complex , true , new VectorRule(new ComplexRule(complexInfo))),
			};
			JsonStructInfo<VectorStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			Assert::IsTrue(dataStruct.empty.empty());

			std::vector<int> expectedVector;
			expectedVector = { 0,1,2,4,8,16,32,64 };
			Assert::AreEqual(expectedVector.size(), dataStruct.vector.size());
			for (size_t i = 0; i < expectedVector.size(); ++i) Assert::AreEqual(expectedVector[i], dataStruct.vector[i]);
			
			
			expectedVector = { 0,1,2,4,8 };
			Assert::AreEqual(expectedVector.size(), dataStruct.positive.size());
			for (size_t i = 0; i < expectedVector.size(); ++i) Assert::AreEqual(expectedVector[i], dataStruct.positive[i]);
			
			
			expectedVector = { 3, 3, 3, 3, 3, };
			Assert::AreEqual(expectedVector.size(), dataStruct.default.size());
			for (size_t i = 0; i < expectedVector.size(); ++i) Assert::AreEqual(expectedVector[i], dataStruct.default[i]);

			std::vector<Complex> expectedComplex = { {0, 1}, {2, 3}, {4, 5} };
			Assert::AreEqual(expectedComplex.size(), dataStruct.complex.size());
			for (size_t i = 0; i < expectedComplex.size(); ++i) {
				Assert::AreEqual(expectedComplex[i].left , dataStruct.complex[i].left);
				Assert::AreEqual(expectedComplex[i].right, dataStruct.complex[i].right);
			}
		}

		TEST_METHOD(EnemyListTests) { 
			using Data = WorldData::EnemyPackData;
			struct EnemyListStruct : TestStruct<Data> {
				Data empty;
				Data filled;

				Data noType;
				Data noX;
				Data noY;
			};
			std::string json = "{"
				"\"empty\"             : [],"
				"\"filled\"            : ["
					"{\"type\": 1, \"x\":   20, \"y\":  150},"
					"{\"type\": 2, \"x\":   10, \"y\":  250}"
				"],"

				"\"noType\"            : [{\"x\":   10, \"y\":  250}],"
				"\"noX\"               : [{\"type\": 2, \"y\":  250}],"
				"\"noY\"               : [{\"type\": 2, \"x\":   10}],"

				"\"requiredPresent\"   : [ {\"type\": 1, \"x\":   20, \"y\":  150} ],"
				"\"notRequiredPresent\": [ {\"type\": 1, \"x\":   20, \"y\":  150} ],"

				"\"incorrectBool\"    : true,"
				"\"incorrectNull\"     : null,"
				"\"incorrectNumeric\"  : 0,"
				"\"incorrectObject\"   : {},"
				"\"incorrectString\"   : \"\""
				"}";

			EnemyListStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("empty"             , &dataStruct.empty             , true , new VectorRule(new EnemyDataRule())),
				JsonFieldInfo("filled"            , &dataStruct.filled            , true , new VectorRule(new EnemyDataRule())),

				JsonFieldInfo("noType"            , &dataStruct.noType            , true , new VectorRule(new EnemyDataRule())),
				JsonFieldInfo("noX"               , &dataStruct.noX               , true , new VectorRule(new EnemyDataRule())),
				JsonFieldInfo("noY"               , &dataStruct.noY               , true , new VectorRule(new EnemyDataRule())),

				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new VectorRule(new EnemyDataRule())),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new VectorRule(new EnemyDataRule())),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new VectorRule(new EnemyDataRule())),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new VectorRule(new EnemyDataRule())),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new VectorRule(new EnemyDataRule())),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new VectorRule(new EnemyDataRule())),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new VectorRule(new EnemyDataRule())),
				JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new VectorRule(new EnemyDataRule())),
			};
			JsonStructInfo<EnemyListStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);

			WorldData::EnemyData type1{ (Type::Character)1, sf::Vector2f(20, 150) };
			WorldData::EnemyData type2{ (Type::Character)2, sf::Vector2f(10, 250) };


			Assert::IsTrue(dataStruct.empty.empty());

			Assert::AreEqual((size_t)2, dataStruct.filled.size());
			Assert::AreEqual(type1, dataStruct.filled[0]);
			Assert::AreEqual(type2, dataStruct.filled[1]);
			
			Assert::IsTrue(dataStruct.noType.empty());
			Assert::IsTrue(dataStruct.noX.empty());
			Assert::IsTrue(dataStruct.noY.empty());

			Assert::AreEqual((size_t)1, dataStruct.requiredPresent.size());
			Assert::AreEqual(type1, dataStruct.requiredPresent[0]);

			Assert::AreEqual((size_t)1, dataStruct.notRequiredPresent.size());
			Assert::AreEqual(type1, dataStruct.notRequiredPresent[0]);
			
			Assert::IsTrue(dataStruct.requiredMissing.empty());
			Assert::IsTrue(dataStruct.notRequiredMissing.empty());

			Assert::IsTrue(dataStruct.incorrectBool.empty());
			Assert::IsTrue(dataStruct.incorrectNull.empty());
			Assert::IsTrue(dataStruct.incorrectNumeric.empty());
			Assert::IsTrue(dataStruct.incorrectObject.empty());
			Assert::IsTrue(dataStruct.incorrectString.empty());


		}



		/*
		[
			{"ID": 1, "data" : {"damage": 1, "destroyOnDamage": true} }
			{"ID": 8}
			{"ID": 16}
		]
		*/
		TEST_METHOD(AbilityTests) { 
			using Data = AbilityEntityData;
			struct AbilityStruct : TestStruct<Data> {
				
			};
			std::string json = "{"
				
				"\"requiredPresent\"   : true,"
				"\"notRequiredPresent\": true,"

				"\"incorrectArray\"    : [ ],"
				"\"incorrectNull\"     : null,"
				"\"incorrectObject\"   : {},"
				"\"incorrectString\"   : \"\""
				"}";

			AbilityStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new AbilityRule()),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new AbilityRule()),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new AbilityRule()),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new AbilityRule()),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new AbilityRule()),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new AbilityRule()),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new AbilityRule()),
				JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new AbilityRule()),
			};
			JsonStructInfo<AbilityStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);
			Assert::IsTrue(true);
		}
		/*
		[
			{"ID": 1 , "data" : {"hitpoints" : 100}},
			{"ID": 4},
			{"ID": 16}
			{"ID": 8, "data" : {"action": {"ID": 1, "efficiency": 10}}},
			{"ID":16, "data" : {"destroyOnImpact": true}}
		]
		*/
		TEST_METHOD(PropertyTests) {
			using Data = PropertyEntityData;
			struct PropertyStruct : TestStruct<Data> {

			};
			std::string json = "{"

				"\"requiredPresent\"   : true,"
				"\"notRequiredPresent\": true,"

				"\"incorrectArray\"    : [ ],"
				"\"incorrectNull\"     : null,"
				"\"incorrectObject\"   : {},"
				"\"incorrectString\"   : \"\""
				"}";

			PropertyStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new PropertyRule()),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new PropertyRule()),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new PropertyRule()),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new PropertyRule()),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new PropertyRule()),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new PropertyRule()),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new PropertyRule()),
				JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new PropertyRule()),
			};
			JsonStructInfo<PropertyStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);
			Assert::IsTrue(true);
		}
		TEST_METHOD(EffectTests) {
			using Data = EffectEntityData;
			struct EffectStruct : TestStruct<Data> {

			};
			std::string json = "{"

				"\"requiredPresent\"   : true,"
				"\"notRequiredPresent\": true,"

				"\"incorrectArray\"    : [ ],"
				"\"incorrectNull\"     : null,"
				"\"incorrectObject\"   : {},"
				"\"incorrectString\"   : \"\""
				"}";

			EffectStruct dataStruct;
			JsonFieldsInfo fields = {
				JsonFieldInfo("requiredPresent"   , &dataStruct.requiredPresent   , true , new EffectRule()),
				JsonFieldInfo("requiredMissing"   , &dataStruct.requiredMissing   , true , new EffectRule()),
				JsonFieldInfo("notRequiredPresent", &dataStruct.notRequiredPresent, false, new EffectRule()),
				JsonFieldInfo("notRequiredMissing", &dataStruct.notRequiredMissing, false, new EffectRule()),

				JsonFieldInfo("incorrectArray"    , &dataStruct.incorrectArray    , true , new EffectRule()),
				JsonFieldInfo("incorrectNull"     , &dataStruct.incorrectNull     , true , new EffectRule()),
				JsonFieldInfo("incorrectObject"   , &dataStruct.incorrectObject   , true , new EffectRule()),
				JsonFieldInfo("incorrectString"   , &dataStruct.incorrectString   , true , new EffectRule()),
			};
			JsonStructInfo<EffectStruct> structInfo(fields, &dataStruct);
			Json::Value root = JsonHelper::parseFromString(json);

			JsonStructParser::jsonToStruct(structInfo, root);
			Assert::IsTrue(true);
		}

	};

}