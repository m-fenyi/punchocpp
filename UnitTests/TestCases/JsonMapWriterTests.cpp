#include "CppUnitTest.h"
#include <TestUtility/CustomToString.hpp>
#include <TestUtility/JsonHelper.hpp>
#include <TestUtility/testOperator.hpp>
#include <Utilities/File.hpp>

#include <DAL/JsonMapWriter.hpp>

#include <json/json.h>

#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Json {

	TEST_CLASS(JsonMapWriterTests) {

	public:
		TEST_METHOD(JsonMapWriteTest) {
			std::string json = 
				"{\"base\":\"value\",\"prefix1\":{\"base1\":\"value1/1\",\"base2\":\"value1/2\",\"prefix1-1\":{\"base1\":\"value1-1/1\",\"base2\":\"value1-1/2\"},\"prefix1-2\":{\"base1\":\"value1-2/1\",\"base2\":\"value1-2/2\"}},\"prefix2\":{\"base1\":\"value2/1\",\"base2\":\"value2/2\",\"prefix2-1\":{\"base1\":\"value2-1/1\",\"base2\":\"value2-1/2\",\"prefix2-1-1\":{\"base1\":\"value2-1-1/1\",\"base2\":\"value2-1-1/2\"}}}}";
			
			std::map<std::string, std::string> map = {
				{ "base"                               , "value" },
				{ "prefix1.base1"                      , "value1/1" },
				{ "prefix1.base2"                      , "value1/2" },
				{ "prefix1.prefix1-1.base1"            , "value1-1/1" },
				{ "prefix1.prefix1-1.base2"            , "value1-1/2" },
				{ "prefix1.prefix1-2.base1"            , "value1-2/1" },
				{ "prefix1.prefix1-2.base2"            , "value1-2/2" },
				{ "prefix2.base1"                      , "value2/1" },
				{ "prefix2.base2"                      , "value2/2" },
				{ "prefix2.prefix2-1.base1"            , "value2-1/1" },
				{ "prefix2.prefix2-1.base2"            , "value2-1/2" },
				{ "prefix2.prefix2-1.prefix2-1-1.base1", "value2-1-1/1" },
				{ "prefix2.prefix2-1.prefix2-1-1.base2", "value2-1-1/2" }
			};

			Json::Value value = JsonMapWriter::writeJson(map);

			std::string document = JsonHelper::writeToString(value);

			Assert::AreEqual(json, document);
		}

	};

}